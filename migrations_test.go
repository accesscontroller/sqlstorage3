package sqlstorage3

import (
	"context"
	"testing"

	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
	"github.com/stretchr/testify/suite"
)

type TestCreateDatabaseSuite struct {
	db *pg.DB

	suite.Suite
}

func (ts *TestCreateDatabaseSuite) SetupTest() {
	db, err := connectDB()
	if err != nil {
		ts.FailNow("Can not connect DB", err)
	}

	ts.db = db

	// Register Many-to-Many Tables.
	orm.RegisterTable(&WebResourceCategoryToAccessGroup{})
	orm.RegisterTable(&ExternalUserToExternalGroup{})

	// Delete Tables.
	for i := range dbTables {
		if err := db.Model(dbTables[i]).DropTable(&orm.DropTableOptions{
			Cascade:  true,
			IfExists: true,
		}); err != nil {
			ts.FailNow("Can not delete existing Tables", err)
		}
	}
}

func (ts *TestCreateDatabaseSuite) TearDownTest() {
	for _, t := range dbTables {
		if err := ts.db.Model(t).DropTable(&orm.DropTableOptions{
			Cascade:  true,
			IfExists: true,
		}); err != nil {
			ts.FailNow("Can not delete existing Tables", err)
		}
	}
}

func (ts *TestCreateDatabaseSuite) TestCreate() {
	if err := createModelsContext(context.TODO(), ts.db); err != nil {
		ts.FailNow("Can not create Tables", err)
	}

	// Check tables existence.
	for _, t := range dbTables {
		_, err := ts.db.Model(t).Exec("SELECT 1 FROM ?TableName")

		ts.Assert().NoError(err, "Unexpected SELECT FROM error")
	}
}

func TestCreateDatabase(t *testing.T) {
	suite.Run(t, &TestCreateDatabaseSuite{})
}

package sqlstorage3

import (
	"context"
	"errors"
	"time"

	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func (s *SQLStorage) GetExternalUserV1Context(ctx context.Context, name,
	source storage.ResourceNameT) (*storage.ExternalUserV1, error) {
	var model = &ExternalUser{}

	if err := s.db.ModelContext(ctx, model).Relation("ExternalUsersGroupsSource").
		Where("external_users_groups_source.name = ? AND external_user.name = ?",
			source, name).Select(); err != nil {
		if errors.Is(err, pg.ErrNoRows) {
			return nil, storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: source,
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: name,
				},
			)
		}

		return nil, err
	}

	return model.toStorageExternalUser(), nil
}

func createExternalUserV1Tx(tx *pg.Tx, user *storage.ExternalUserV1) (*ExternalUser, error) {
	// Lookup Source.
	var userModel ExternalUser

	if err := tx.Model(&userModel.ExternalUsersGroupsSource).
		Where("external_users_groups_source.name = ?",
			user.Metadata.ExternalSource.SourceName).Select(); err != nil {
		if errors.Is(err, pg.ErrNoRows) {
			return nil, storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       user.Metadata.ExternalSource.SourceName,
				},
			)
		}

		return nil, err
	}

	userModel.CreatedAt = time.Now()
	userModel.ETag = user.Metadata.ETag
	userModel.Name = user.Metadata.Name
	userModel.ExternalUsersGroupsSourceID = userModel.ExternalUsersGroupsSource.ID

	// Create.
	if _, err := tx.Model(&userModel).Insert(); err != nil {
		if typedErr, ok := err.(pg.Error); ok {
			if typedErr.IntegrityViolation() {
				return nil, storage.NewErrResourceAlreadyExists([]interface{}{user.Metadata}, typedErr)
			}
		}

		return nil, err
	}

	return &userModel, nil
}

func (s *SQLStorage) CreateExternalUserV1Context(ctx context.Context, //nolint:dupl
	user *storage.ExternalUserV1, getCreated bool) (*storage.ExternalUserV1, error) {
	var model *ExternalUser

	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		m, err := createExternalUserV1Tx(tx, user)
		if err != nil {
			return err
		}

		model = m

		return nil
	}); err != nil {
		return nil, err
	}

	if getCreated {
		return model.toStorageExternalUser(), nil
	}

	return nil, nil
}

func (s *SQLStorage) UpdateExternalUserV1Context(ctx context.Context,
	oldName storage.ResourceNameT, user *storage.ExternalUserV1,
	getUpdated bool) (*storage.ExternalUserV1, error) {
	var dbUser ExternalUser

	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		// Lookup.
		if err := tx.Model(&dbUser).Relation("ExternalUsersGroupsSource").
			Where("external_user.name = ? AND external_users_groups_source.name = ?",
				oldName, user.Metadata.ExternalSource.SourceName).Select(); err != nil {
			if errors.Is(err, pg.ErrNoRows) {
				return storage.NewErrResourceNotFound(
					&storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             0,
						ExternalResource: true,
						ExternalSource:   user.Metadata.ExternalSource,
						Kind:             storage.ExternalUserKind,
						Name:             oldName,
					},
				)
			}

			return err
		}

		// Check ETag.
		if dbUser.ETag != user.Metadata.ETag {
			return storage.NewErrETagDoesNotMatch(dbUser.ETag, user.Metadata.ETag)
		}

		// Update.
		dbUser.UpdatedAt = time.Now()
		dbUser.ETag++
		dbUser.Name = user.Metadata.Name

		if _, err := tx.Model(&dbUser).WherePK().Update(); err != nil {
			if typedErr, ok := err.(pg.Error); ok {
				if typedErr.IntegrityViolation() {
					return storage.NewErrResourceAlreadyExists([]interface{}{user.Metadata}, typedErr)
				}
			}

			return err
		}

		return nil
	}); err != nil {
		return nil, err
	}

	if getUpdated {
		return dbUser.toStorageExternalUser(), nil
	}

	return nil, nil
}

func deleteExternalUserV1Tx(tx *pg.Tx, user *storage.ExternalUserV1) error {
	var dbModel ExternalUser

	// Lookup.
	if err := tx.Model(&dbModel).Relation("ExternalUsersGroupsSource").
		Where("external_users_groups_source.name = ? AND external_user.name = ?",
			user.Metadata.ExternalSource.SourceName, user.Metadata.Name).
		Select(); err != nil {
		if errors.Is(err, pg.ErrNoRows) {
			return storage.NewErrResourceNotFound(&user.Metadata)
		}

		return err
	}

	// Check ETag.
	if dbModel.ETag != user.Metadata.ETag {
		return storage.NewErrETagDoesNotMatch(
			dbModel.ETag, user.Metadata.ETag)
	}

	// Update ETags and UpdatedAt for all Groups to which the User belonged.
	// Load relations.
	var relations []*ExternalUserToExternalGroup

	if err := tx.Model(&relations).Relation("ExternalUser").
		Apply(func(q *orm.Query) (*orm.Query, error) {
			return q.Column("external_group_id"), nil
		}).Where("external_user.id = ?", dbModel.ID).Select(); err != nil {
		return err
	}

	if len(relations) > 0 {
		var groupIDs = make([]int64, len(relations))

		for i, r := range relations {
			groupIDs[i] = r.ExternalGroupID
		}

		var groups ExternalGroupSlice

		if err := tx.Model(&groups).Apply(func(q *orm.Query) (*orm.Query, error) {
			return q.Column("e_tag", "id"), nil
		}).WhereIn("external_group.id IN (?)", groupIDs).Select(); err != nil {
			return err
		}

		var curTime = time.Now()

		for _, g := range groups {
			g.ETag++
			g.UpdatedAt = curTime
		}

		if _, err := tx.Model(&groups).Column("e_tag", "updated_at").Update(); err != nil {
			return err
		}
	}

	// Delete.
	if _, err := tx.Model(&dbModel).WherePK().Delete(); err != nil {
		return err
	}

	return nil
}

func (s *SQLStorage) DeleteExternalUserV1Context(
	ctx context.Context, user *storage.ExternalUserV1) error {
	return s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		return deleteExternalUserV1Tx(tx, user)
	})
}

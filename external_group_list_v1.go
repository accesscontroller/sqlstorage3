package sqlstorage3

import (
	"context"

	"github.com/go-pg/pg/v10"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

// GetExternalGroupV1sContext returns filtered storage.ExternalGroupV1s.
func (s *SQLStorage) GetExternalGroupV1sContext(ctx context.Context,
	filter []storage.ExternalGroupFilterV1,
	externalUsersGroupsSourceName storage.ResourceNameT) ([]storage.ExternalGroupV1, error) {
	var (
		models ExternalGroupSlice
	)

	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		var q = tx.Model(&models)

		// Join.
		q = q.Relation("AccessGroup").Relation("ExternalUsersGroupsSource")

		// Filter.
		for _, f := range filter {
			if !isZero(f.Name) {
				q = q.WhereOr("external_group.name = ?", f.Name)
			}
		}

		q = q.Where("external_users_groups_source.name = ?", externalUsersGroupsSourceName)

		if err := q.Select(); err != nil {
			return err
		}

		return nil
	}); err != nil {
		return nil, err
	}

	return models.toStorageExternalGroups(), nil
}

// CreateExternalGroupV1sContext creates ExternalGroupV1s from
// given List. All the groups must be from the same ExternalUsersGroupsSourceV1.
// ToDo: optimize using maps caching for AccessGroup and ExternalUsersGroupsSource.
func (s *SQLStorage) CreateExternalGroupV1sContext(ctx context.Context,
	groups []storage.ExternalGroupV1,
	getCreated bool) ([]storage.ExternalGroupV1, error) {
	// Empty list - omit.
	if len(groups) == 0 {
		return []storage.ExternalGroupV1{}, nil
	}

	var (
		result []storage.ExternalGroupV1
	)

	if getCreated {
		result = make([]storage.ExternalGroupV1, 0, len(groups))
	}

	// Run in one Transaction.
	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		for i := range groups {
			tRes, err := createExternalGroupV1Tx(tx, &groups[i])
			if err != nil {
				return err
			}

			if getCreated {
				result = append(result, *tRes.toStorageExternalGroup())
			}
		}

		return nil
	}); err != nil {
		return nil, err
	}

	if getCreated {
		return result, nil
	}

	return nil, nil
}

// UpdateExternalGroupV1sContext updates all groups from given ExternalGroupV1s.
func (s *SQLStorage) UpdateExternalGroupV1sContext(ctx context.Context,
	list []storage.ExternalGroupV1, getUpdated bool) ([]storage.ExternalGroupV1, error) {
	var models ExternalGroupSlice

	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		// Allocate.
		models = make(ExternalGroupSlice, 0, len(list))

		// Update one-by-one.
		for i := range list {
			p := &list[i]

			updMod, err := updateExternalGroupV1Tx(tx, p.Metadata.Name, p)
			if err != nil {
				return err
			}

			if getUpdated {
				models = append(models, updMod)
			}
		}

		return nil
	}); err != nil {
		return nil, err
	}

	if getUpdated {
		return models.toStorageExternalGroups(), nil
	}

	return nil, nil
}

// DeleteExternalGroupV1sContext deletes all given ExternalGroupV1s.
func (s *SQLStorage) DeleteExternalGroupV1sContext(ctx context.Context,
	groups []storage.ExternalGroupV1) error {
	return s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		// Delete one by one.
		for i := range groups {
			if err := deleteExternalGroupV1Tx(tx, &groups[i]); err != nil {
				return err
			}
		}

		return nil
	})
}

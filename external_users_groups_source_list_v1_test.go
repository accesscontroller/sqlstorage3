package sqlstorage3

import (
	"context"
	"encoding/json"
	"testing"

	"github.com/go-pg/pg/v10"
	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

type TestExternalUsersGroupsSourceV1sSuite struct {
	db *pg.DB

	sqlStorage *SQLStorage

	suite.Suite
}

func (ts *TestExternalUsersGroupsSourceV1sSuite) SetupTest() {
	db, err := connectDB()
	if err != nil {
		ts.FailNow("Can not connect to test DB", err)
	}

	ts.db = db
	ts.sqlStorage = &SQLStorage{db: db}

	// Create DB Tables.
	if err := recreateTables(ts.db); err != nil {
		ts.FailNow("Can not initialize DB", err)
	}

	// Create some init data.

	// ExternalUsersGroupsSource.
	var externalUsersGroupsSources = []ExternalUsersGroupsSource{
		{
			ETag:              1,
			GetMode:           storage.ExternalUsersGroupsGetModePassive,
			GroupsGetSettings: `{ "source 1": "groups" }`,
			GroupsToLoad:      []string{"group1", "group2"},
			UsersGetSettings:  `{ "source 1": "users" }`,
			Type:              storage.ExternalUsersGroupsSourceTypeLDAP,
			Name:              "source 1",
			PollInterval:      121,
		},
		{
			ETag:              2,
			GetMode:           storage.ExternalUsersGroupsGetModePoll,
			GroupsGetSettings: `{ "source 2": "groups" }`,
			GroupsToLoad:      []string{"group1", "group2"},
			UsersGetSettings:  `{ "source 2": "users" }`,
			Type:              storage.ExternalUsersGroupsSourceTypeLDAP,
			Name:              "source 2",
			PollInterval:      122,
		},
		{
			ETag:              3,
			GetMode:           storage.ExternalUsersGroupsGetModePassive,
			GroupsGetSettings: `{ "source 3": "groups" }`,
			GroupsToLoad:      []string{"group4", "group41"},
			UsersGetSettings:  `{ "source 3": "users" }`,
			Type:              storage.ExternalUsersGroupsSourceTypeLDAP,
			Name:              "source 3",
			PollInterval:      123,
		},
		{
			ETag:              4,
			GetMode:           storage.ExternalUsersGroupsGetModePoll,
			GroupsGetSettings: `{ "source 4": "groups" }`,
			GroupsToLoad:      []string{"group4", "group5"},
			UsersGetSettings:  `{ "source 4": "users" }`,
			Type:              storage.ExternalUsersGroupsSourceTypeLDAP,
			Name:              "source 4",
			PollInterval:      124,
		},
	}

	for i := range externalUsersGroupsSources {
		if _, err := db.Model(&externalUsersGroupsSources[i]).Insert(); err != nil {
			ts.FailNow("Can not create init data", err.Error())
		}
	}
}

func (ts *TestExternalUsersGroupsSourceV1sSuite) TearDownTest() {
	if ts.db != nil {
		if err := ts.db.Close(); err != nil {
			ts.FailNow("Can not Close DB", err)
		}
	}
}

func (ts *TestExternalUsersGroupsSourceV1sSuite) TestGet() {
	var tests = []struct {
		filters []storage.ExternalUsersGroupsSourceFilterV1

		expectedResult []storage.ExternalUsersGroupsSourceV1
	}{
		{
			filters: []storage.ExternalUsersGroupsSourceFilterV1{
				{
					Name: "source 4",
				},
				{
					NameSubstringMatch: " 3",
				},
			},
			expectedResult: []storage.ExternalUsersGroupsSourceV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       3,
						Kind:       storage.ExternalUsersGroupsSourceKind,
						Name:       "source 3",
					},
					Data: storage.ExternalUsersGroupsSourceDataV1{
						PollIntervalSec: 123,
						GetMode:         storage.ExternalUsersGroupsGetModePassive,
						GroupsSourceSettings: map[string]string{
							"source 3": "groups",
						},
						GroupsToMonitor: []string{
							"group4", "group41",
						},
						Type: storage.ExternalUsersGroupsSourceTypeLDAP,
						UsersSourceSettings: map[string]string{
							"source 3": "users",
						},
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       4,
						Kind:       storage.ExternalUsersGroupsSourceKind,
						Name:       "source 4",
					},
					Data: storage.ExternalUsersGroupsSourceDataV1{
						PollIntervalSec: 124,
						GetMode:         storage.ExternalUsersGroupsGetModePoll,
						GroupsSourceSettings: map[string]string{
							"source 4": "groups",
						},
						GroupsToMonitor: []string{
							"group4", "group5",
						},
						Type: storage.ExternalUsersGroupsSourceTypeLDAP,
						UsersSourceSettings: map[string]string{
							"source 4": "users",
						},
					},
				},
			},
		},
		{
			filters: []storage.ExternalUsersGroupsSourceFilterV1{
				{
					Type: storage.ExternalUsersGroupsSourceTypeLDAP,
					Name: "source 3",
				},
				{
					NameSubstringMatch: " 3",
				},
			},
			expectedResult: []storage.ExternalUsersGroupsSourceV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       3,
						Kind:       storage.ExternalUsersGroupsSourceKind,
						Name:       "source 3",
					},
					Data: storage.ExternalUsersGroupsSourceDataV1{
						PollIntervalSec: 123,
						GetMode:         storage.ExternalUsersGroupsGetModePassive,
						GroupsSourceSettings: map[string]string{
							"source 3": "groups",
						},
						GroupsToMonitor: []string{
							"group4", "group41",
						},
						Type: storage.ExternalUsersGroupsSourceTypeLDAP,
						UsersSourceSettings: map[string]string{
							"source 3": "users",
						},
					},
				},
			},
		},
		{
			filters: []storage.ExternalUsersGroupsSourceFilterV1{
				{
					Type:    storage.ExternalUsersGroupsSourceTypeLDAP,
					Name:    "source 3",
					GetMode: storage.ExternalUsersGroupsGetModePoll,
				},
			},
			expectedResult: []storage.ExternalUsersGroupsSourceV1{},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.GetExternalUsersGroupsSourceV1sContext(context.TODO(),
			test.filters)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("error - can not continue")
		}

		if !ts.NotNil(got, "Must not return nil as result") {
			ts.FailNow("nil result - can not continue")
		}

		ts.Equal(test.expectedResult, got, "Unexpected result")
	}
}

func (ts *TestExternalUsersGroupsSourceV1sSuite) TestCreateSuccess() {
	var tests = []struct {
		getCreated bool

		sources []storage.ExternalUsersGroupsSourceV1
	}{
		{
			sources: []storage.ExternalUsersGroupsSourceV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       112,
						Kind:       storage.ExternalUsersGroupsSourceKind,
						Name:       "new source 1",
					},
					Data: storage.ExternalUsersGroupsSourceDataV1{
						PollIntervalSec: 11022,
						GetMode:         storage.ExternalUsersGroupsGetModePassive,
						GroupsSourceSettings: map[string]string{
							"key 1": "value 1",
						},
						GroupsToMonitor: []string{"group 1", "group 2"},
						UsersSourceSettings: map[string]string{
							"key 2": "value 2",
						},
						Type: storage.ExternalUsersGroupsSourceTypeLDAP,
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       113,
						Kind:       storage.ExternalUsersGroupsSourceKind,
						Name:       "new source 2",
					},
					Data: storage.ExternalUsersGroupsSourceDataV1{
						PollIntervalSec: 1162,
						GetMode:         storage.ExternalUsersGroupsGetModePoll,
						GroupsSourceSettings: map[string]string{
							"key 12": "value 1",
						},
						GroupsToMonitor: []string{"group 1", "group 2"},
						UsersSourceSettings: map[string]string{
							"key 22": "value 2",
						},
						Type: storage.ExternalUsersGroupsSourceTypeLDAP,
					},
				},
			},
		},
		{
			getCreated: true,
			sources: []storage.ExternalUsersGroupsSourceV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       112,
						Kind:       storage.ExternalUsersGroupsSourceKind,
						Name:       "new source 3",
					},
					Data: storage.ExternalUsersGroupsSourceDataV1{
						PollIntervalSec: 11022,
						GetMode:         storage.ExternalUsersGroupsGetModePassive,
						GroupsSourceSettings: map[string]string{
							"key 1": "value 1",
						},
						GroupsToMonitor: []string{"group 1", "group 2"},
						UsersSourceSettings: map[string]string{
							"key 2": "value 2",
						},
						Type: storage.ExternalUsersGroupsSourceTypeLDAP,
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       113,
						Kind:       storage.ExternalUsersGroupsSourceKind,
						Name:       "new source 4",
					},
					Data: storage.ExternalUsersGroupsSourceDataV1{
						PollIntervalSec: 1162,
						GetMode:         storage.ExternalUsersGroupsGetModePoll,
						GroupsSourceSettings: map[string]string{
							"key 12": "value 1",
						},
						GroupsToMonitor: []string{"group 1", "group 2"},
						UsersSourceSettings: map[string]string{
							"key 22": "value 2",
						},
						Type: storage.ExternalUsersGroupsSourceTypeLDAP,
					},
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.CreateExternalUsersGroupsSourceV1sContext(context.TODO(),
			test.sources, test.getCreated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("got error - can not continue")
		}

		for srcInd := range test.sources {
			srcP := &test.sources[srcInd]

			// Get from DB.
			var dbModel = &ExternalUsersGroupsSource{}

			if err := ts.db.Model(dbModel).
				Where("name = ?", srcP.Metadata.Name).Select(); err != nil {
				ts.FailNow("Can not load a Source from DB: ", err)
			}

			// Compare.
			ts.Equal(srcP.Metadata.Name, dbModel.Name)
			ts.Equal(srcP.Metadata.ETag, dbModel.ETag, "ETag must be 1")

			var expGGS, err = json.Marshal(srcP.Data.GroupsSourceSettings)
			if err != nil {
				ts.FailNow("Got json.Marshal error, can not continue: ", err)
			}

			ts.JSONEq(string(expGGS), dbModel.GroupsGetSettings)

			expUGS, err := json.Marshal(srcP.Data.UsersSourceSettings)
			if err != nil {
				ts.FailNow("Got json.Marshal error, can not continue: ", err)
			}

			ts.JSONEq(string(expUGS), dbModel.UsersGetSettings)

			ts.Equal(srcP.Data.GetMode, dbModel.GetMode)
			ts.Equal(srcP.Data.GroupsToMonitor, dbModel.GroupsToLoad)
			ts.Equal(srcP.Data.PollIntervalSec, dbModel.PollInterval)
			ts.Equal(srcP.Data.Type, dbModel.Type)

			if test.getCreated {
				if !ts.NotNil(got, "Must not return nil result") {
					ts.FailNow("nil result - can not continue")
				}

				ts.Equal(*srcP, got[srcInd], "Unexpected created value")
			}
		}
	}
}

func (ts *TestExternalUsersGroupsSourceV1sSuite) TestCreateConflict() {
	var tests = []struct {
		expectedError error

		sources []storage.ExternalUsersGroupsSourceV1
	}{
		{
			expectedError: storage.NewErrResourceAlreadyExists([]interface{}{
				storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       "source 1",
				},
			}),
			sources: []storage.ExternalUsersGroupsSourceV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						Kind:       storage.ExternalUsersGroupsSourceKind,
						Name:       "source 1",
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						Kind:       storage.ExternalUsersGroupsSourceKind,
						Name:       "new source 100",
					},
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.CreateExternalUsersGroupsSourceV1sContext(context.TODO(),
			test.sources, true)

		ts.Nil(got, "Must return nil result")

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("nil error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func TestExternalUsersGroupsSourceV1s(t *testing.T) {
	suite.Run(t, &TestExternalUsersGroupsSourceV1sSuite{})
}

package sqlstorage3

import (
	"context"
	"crypto/rand"
	"sort"
	"testing"
	"time"

	"github.com/go-pg/pg/v10"
	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

type TestExternalGroupToExternalUserV1sRelationsSuite struct {
	db *pg.DB

	sqlStorage *SQLStorage

	suite.Suite
}

func (ts *TestExternalGroupToExternalUserV1sRelationsSuite) SetupTest() {
	db, err := connectDB()
	if err != nil {
		ts.FailNow("Can not connect to test DB", err)
	}

	ts.db = db
	ts.sqlStorage = &SQLStorage{db: db}

	// Create DB Tables.
	if err := recreateTables(ts.db); err != nil {
		ts.FailNow("Can not initialize DB", err)
	}

	// Create some init data.
	var sources = ExternalUsersGroupsSourceSlice{
		{
			CreatedAt:         time.Now(),
			ETag:              1,
			GetMode:           storage.ExternalUsersGroupsGetModePassive,
			Name:              "source 1",
			GroupsGetSettings: `{}`,
			UsersGetSettings:  `{}`,
			GroupsToLoad:      []string{"group1", "group2"},
			PollInterval:      1800,
			Type:              storage.ExternalUsersGroupsSourceTypeLDAP,
		},
		{
			CreatedAt:         time.Now(),
			ETag:              2,
			GetMode:           storage.ExternalUsersGroupsGetModePassive,
			Name:              "source 2",
			GroupsGetSettings: `{}`,
			UsersGetSettings:  `{}`,
			GroupsToLoad:      []string{"group1", "group2"},
			PollInterval:      1800,
			Type:              storage.ExternalUsersGroupsSourceTypeLDAP,
		},
	}

	for _, gs := range sources {
		if _, err := db.Model(gs).Insert(); err != nil {
			ts.FailNow("Can not insert Test Data ExternalUsersGroupsSource", err)
		}
	}

	// Groups.
	var groups = ExternalGroupSlice{
		{
			ETag:                        1,
			CreatedAt:                   time.Now(),
			Name:                        "group 1 at source 1",
			ExternalUsersGroupsSourceID: sources[0].ID,
		},
		{
			ETag:                        2,
			CreatedAt:                   time.Now(),
			Name:                        "group 2 at source 1",
			ExternalUsersGroupsSourceID: sources[0].ID,
		},
		{
			ETag:                        3,
			CreatedAt:                   time.Now(),
			Name:                        "group 3 at source 1",
			ExternalUsersGroupsSourceID: sources[0].ID,
		},
		{
			ETag:                        1,
			CreatedAt:                   time.Now(),
			Name:                        "group 1 at source 2",
			ExternalUsersGroupsSourceID: sources[1].ID,
		},
		{
			ETag:                        2,
			CreatedAt:                   time.Now(),
			Name:                        "group 2 at source 2",
			ExternalUsersGroupsSourceID: sources[1].ID,
		},
	}

	for _, g := range groups {
		if _, err := db.Model(g).Insert(); err != nil {
			ts.FailNow("Can not insert Test Data ExternalGroup", err)
		}
	}

	// Users.
	var users = ExternalUserSlice{
		&ExternalUser{
			ExternalUsersGroupsSourceID: sources[0].ID,
			CreatedAt:                   time.Now(),
			ETag:                        1,
			Name:                        "user 1 at source 1",
		},
		&ExternalUser{
			ExternalUsersGroupsSourceID: sources[0].ID,
			CreatedAt:                   time.Now(),
			ETag:                        2,
			Name:                        "user 2 at source 1",
		},
		&ExternalUser{
			ExternalUsersGroupsSourceID: sources[0].ID,
			CreatedAt:                   time.Now(),
			ETag:                        3,
			Name:                        "user 3 at source 1",
		},
		&ExternalUser{
			ExternalUsersGroupsSourceID: sources[1].ID,
			CreatedAt:                   time.Now(),
			ETag:                        1,
			Name:                        "user 1 at source 2",
		},
		&ExternalUser{
			ExternalUsersGroupsSourceID: sources[1].ID,
			CreatedAt:                   time.Now(),
			ETag:                        2,
			Name:                        "user 2 at source 2",
		},
	}

	for _, u := range users {
		if _, err := db.Model(u).Insert(); err != nil {
			ts.FailNow("Can not inser Test ExternalUser", err)
		}
	}

	// Bind some users and groups.
	var bindings = []*ExternalUserToExternalGroup{
		{
			ExternalGroupID: groups[0].ID,
			CreatedAt:       time.Now(),
			ExternalUserID:  users[0].ID,
		},
		{
			ExternalGroupID: groups[0].ID,
			CreatedAt:       time.Now(),
			ExternalUserID:  users[1].ID,
		},
		{
			ExternalGroupID: groups[3].ID,
			CreatedAt:       time.Now(),
			ExternalUserID:  users[3].ID,
		},
	}

	for _, b := range bindings {
		if _, err := db.Model(b).Insert(); err != nil {
			ts.FailNow("Can not create Test ExternalUser to ExternalGroup binding", err)
		}
	}
}

func (ts *TestExternalGroupToExternalUserV1sRelationsSuite) TearDownTest() {
	if ts.db != nil {
		if err := ts.db.Close(); err != nil {
			ts.FailNow("Can not Close DB", err)
		}
	}
}

func (ts *TestExternalGroupToExternalUserV1sRelationsSuite) TestGet() {
	var tests = []struct {
		group  storage.ResourceNameT
		source storage.ResourceNameT

		expectedRelations storage.ExternalGroup2ExternalUserListRelationsV1
	}{
		{
			group:  "group 1 at source 1",
			source: "source 1",
			expectedRelations: storage.ExternalGroup2ExternalUserListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalGroup2ExternalUserListV1RelationsKind,
					Name:       "group 1 at source 1",
				},
				Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
					ExternalGroup:             "group 1 at source 1",
					ExternalUsersGroupsSource: "source 1",
					ExternalUsers: []storage.ResourceNameT{
						"user 1 at source 1",
						"user 2 at source 1",
					},
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.GetExternalGroupExternalUserV1sContext(context.TODO(),
			test.source, test.group)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		ts.Equal(test.expectedRelations, *got, "Unexpected result relations")
	}
}

func (ts *TestExternalGroupToExternalUserV1sRelationsSuite) TestGetNotFound() {
	var tests = []struct {
		group  storage.ResourceNameT
		source storage.ResourceNameT

		expectedError error
	}{
		{
			group:  "not found group 1 at source 1",
			source: "source 1",
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: "not found group 1 at source 1",
				},
			),
		},
		{
			group:  "group 1 at source 1",
			source: "not found source 1",
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "not found source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: "group 1 at source 1",
				},
			),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.GetExternalGroupExternalUserV1sContext(context.TODO(),
			test.source, test.group)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalGroupToExternalUserV1sRelationsSuite) TestBindSuccessCheckUsersMembership() {
	var tests = []struct {
		relations storage.ExternalGroup2ExternalUserListRelationsV1

		expectedUsers []storage.ResourceNameT
	}{
		{
			relations: storage.ExternalGroup2ExternalUserListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalGroup2ExternalUserListV1RelationsKind,
					Name:       "relations 1",
				},
				Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
					ExternalGroup:             "group 1 at source 1",
					ExternalUsersGroupsSource: "source 1",
					ExternalUsers: []storage.ResourceNameT{
						"user 1 at source 1",
						"user 3 at source 1",
					},
				},
			},
			expectedUsers: []storage.ResourceNameT{
				"user 1 at source 1",
				"user 2 at source 1",
				"user 3 at source 1",
			},
		},
		{ // The same request as the previous except that the request contains empty new Users slice.
			relations: storage.ExternalGroup2ExternalUserListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalGroup2ExternalUserListV1RelationsKind,
					Name:       "relations 1",
				},
				Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
					ExternalGroup:             "group 1 at source 1",
					ExternalUsersGroupsSource: "source 1",
					ExternalUsers:             []storage.ResourceNameT{}, // Empty Users slice case.
				},
			},
			expectedUsers: []storage.ResourceNameT{
				"user 1 at source 1",
				"user 2 at source 1",
				"user 3 at source 1",
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		// Update.
		gotErr := ts.sqlStorage.BindExternalGroup2ExternalUserV1sContext(context.TODO(), &test.relations)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Load users from DB.
		var dbGroupAfterUpdate ExternalGroup

		if err := ts.db.Model(&dbGroupAfterUpdate).Relation("ExternalUsers").
			Relation("ExternalUsersGroupsSource").
			Where("external_users_groups_source.name = ? AND external_group.name = ?",
				test.relations.Data.ExternalUsersGroupsSource,
				test.relations.Data.ExternalGroup).Select(); err != nil {
			ts.FailNow("Can not load External Users", err)
		}

		// Check length.
		if len(dbGroupAfterUpdate.ExternalUsers) != len(test.expectedUsers) {
			ts.FailNow("Expected users slice length is not equal to got users slice length")
		}

		// Make got User Names.
		var gotUsers = make([]storage.ResourceNameT, len(dbGroupAfterUpdate.ExternalUsers))

		for i, u := range dbGroupAfterUpdate.ExternalUsers {
			gotUsers[i] = u.Name
		}

		sort.Slice(gotUsers, func(i, j int) bool { return gotUsers[i] < gotUsers[j] })

		sort.Slice(test.expectedUsers, func(i, j int) bool { return test.expectedUsers[i] < test.expectedUsers[j] })

		ts.Equal(test.expectedUsers, gotUsers, "Unexpected Users result")
	}
}

func (ts *TestExternalGroupToExternalUserV1sRelationsSuite) TestBindSuccessCheckETagsUpdatedAt() {
	var tests = []struct {
		relations storage.ExternalGroup2ExternalUserListRelationsV1

		expectedUserETags map[storage.ResourceNameT]storage.ETagT
	}{
		{
			relations: storage.ExternalGroup2ExternalUserListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalGroup2ExternalUserListV1RelationsKind,
					Name:       "relations 1",
				},
				Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
					ExternalGroup:             "group 1 at source 1",
					ExternalUsersGroupsSource: "source 1",
					ExternalUsers: []storage.ResourceNameT{
						"user 1 at source 1",
						"user 3 at source 1",
					},
				},
			},
			expectedUserETags: map[storage.ResourceNameT]storage.ETagT{
				"user 1 at source 1": 2, // Was in Relations Update.
				"user 2 at source 1": 2,
				"user 3 at source 1": 4, // Was in Relations Update.
			},
		},
		{ // The same request as the previous except that the request contains empty new Users slice.
			relations: storage.ExternalGroup2ExternalUserListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalGroup2ExternalUserListV1RelationsKind,
					Name:       "relations 1",
				},
				Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
					ExternalGroup:             "group 1 at source 1",
					ExternalUsersGroupsSource: "source 1",
					ExternalUsers:             []storage.ResourceNameT{}, // Empty Users slice case.
				},
			},
			expectedUserETags: map[storage.ResourceNameT]storage.ETagT{ // No Users Update so no ETag change.
				"user 1 at source 1": 2, // Was in Relations Update in previous step.
				"user 2 at source 1": 2,
				"user 3 at source 1": 4, // Was in Relations Update in previous step.
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		// Load DB Group.
		var dbGroupBeforeUpdate ExternalGroup

		if err := ts.db.Model(&dbGroupBeforeUpdate).
			Relation("ExternalUsersGroupsSource").
			Where("external_users_groups_source.name = ? AND external_group.name = ?",
				test.relations.Data.ExternalUsersGroupsSource,
				test.relations.Data.ExternalGroup).Select(); err != nil {
			ts.FailNow("Can not load External Group before update", err)
		}

		// Update.
		gotErr := ts.sqlStorage.BindExternalGroup2ExternalUserV1sContext(context.TODO(), &test.relations)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Load users from DB.
		var dbGroupAfterUpdate ExternalGroup

		if err := ts.db.Model(&dbGroupAfterUpdate).
			Relation("ExternalUsersGroupsSource").
			Where("external_users_groups_source.name = ? AND external_group.name = ?",
				test.relations.Data.ExternalUsersGroupsSource,
				test.relations.Data.ExternalGroup).Select(); err != nil {
			ts.FailNow("Can not load External Users", err)
		}

		// Check that ETag and UpdatedAt changed for a Group.

		// Correct and check ETag.
		dbGroupBeforeUpdate.ETag++

		ts.Equalf(dbGroupBeforeUpdate.ETag, dbGroupAfterUpdate.ETag,
			"Unexpected ExternalGroup ETag increment, Group %s", dbGroupBeforeUpdate.Name)

		// Check UpdatedAt.
		var currentTime = time.Now()

		ts.WithinDurationf(currentTime, dbGroupAfterUpdate.UpdatedAt, time.Minute,
			"ExternalGroup UpdatedAt time is not within expected time range, Group %s",
			dbGroupAfterUpdate.Name)

		// Check every User for ETag and UpdatedAt.
		for _, dbUser := range dbGroupAfterUpdate.ExternalUsers {
			ts.Equalf(test.expectedUserETags[dbUser.Name], dbUser.ETag,
				"Unexpected ExternalUser ETag increment, User %s", dbUser.Name)

			// Bug: by some strange reason go-pg does not loads UpdatedAt value from DB.
			// ts.WithinDurationf(currentTime, dbUser.UpdatedAt, time.Minute,
			// 	"ExternalUser UpdatedAt time is not within expected time range, User: %s",
			// 	dbUser.Name)
		}
	}
}

func (ts *TestExternalGroupToExternalUserV1sRelationsSuite) TestBindGroupNotFound() {
	var tests = []struct {
		relations storage.ExternalGroup2ExternalUserListRelationsV1

		expectedError error
	}{
		{
			relations: storage.ExternalGroup2ExternalUserListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalGroup2ExternalUserListV1RelationsKind,
					Name:       "relations 1",
				},
				Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
					ExternalGroup:             "not found group 1 at source 1", // No group.
					ExternalUsersGroupsSource: "source 1",
					ExternalUsers: []storage.ResourceNameT{
						"user 1 at source 1",
						"user 3 at source 1",
					},
				},
			},
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: "not found group 1 at source 1",
				},
			),
		},
		{
			relations: storage.ExternalGroup2ExternalUserListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalGroup2ExternalUserListV1RelationsKind,
					Name:       "relations 1",
				},
				Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
					ExternalGroup:             "group 1 at source 1",
					ExternalUsersGroupsSource: "not found source 1", // No source.
					ExternalUsers: []storage.ResourceNameT{
						"user 1 at source 1",
						"user 3 at source 1",
					},
				},
			},
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "not found source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: "group 1 at source 1",
				},
			),
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.sqlStorage.BindExternalGroup2ExternalUserV1sContext(context.TODO(), &test.relations)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalGroupToExternalUserV1sRelationsSuite) TestBindUserNotFound() {
	var tests = []struct {
		relations storage.ExternalGroup2ExternalUserListRelationsV1

		expectedError error
	}{
		{
			relations: storage.ExternalGroup2ExternalUserListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalGroup2ExternalUserListV1RelationsKind,
					Name:       "relations 1",
				},
				Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
					ExternalGroup:             "group 1 at source 1",
					ExternalUsersGroupsSource: "source 1",
					ExternalUsers: []storage.ResourceNameT{
						"user 1 at source 1",
						"user 3 at source 1",
						"user 42 at source 1", // No a User with the name.
					},
				},
			},
			expectedError: storage.NewErrRelatedResourcesNotFound(storage.APIVersionV1Value,
				storage.ExternalUserKind, []storage.ResourceNameT{"user 42 at source 1"}, nil),
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.sqlStorage.BindExternalGroup2ExternalUserV1sContext(context.TODO(), &test.relations)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalGroupToExternalUserV1sRelationsSuite) TestUnbindSuccessCheckUsersMembership() {
	var tests = []struct {
		relations storage.ExternalGroup2ExternalUserListRelationsV1

		expectedUsers []storage.ResourceNameT
	}{
		{
			relations: storage.ExternalGroup2ExternalUserListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalGroup2ExternalUserListV1RelationsKind,
					Name:       "group 1 at source 1",
				},
				Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
					ExternalGroup:             "group 1 at source 1",
					ExternalUsersGroupsSource: "source 1",
					ExternalUsers: []storage.ResourceNameT{
						"user 1 at source 1",
						"user 3 at source 1",
					},
				},
			},
			expectedUsers: []storage.ResourceNameT{
				"user 2 at source 1",
			},
		},
		{ // The same as above but with empty Users slice to check the "empty slice" case.
			relations: storage.ExternalGroup2ExternalUserListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalGroup2ExternalUserListV1RelationsKind,
					Name:       "group 1 at source 1",
				},
				Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
					ExternalGroup:             "group 1 at source 1",
					ExternalUsersGroupsSource: "source 1",
					ExternalUsers:             []storage.ResourceNameT{},
				},
			},
			expectedUsers: []storage.ResourceNameT{
				"user 2 at source 1",
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.sqlStorage.UnBindExternalGroup2ExternalUserV1sContext(context.TODO(), &test.relations)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Load a Group from DB to check Users.
		var dbGroupAfterUpdate ExternalGroup

		if err := ts.db.Model(&dbGroupAfterUpdate).
			Relation("ExternalUsersGroupsSource").
			Relation("ExternalUsers").
			Where("external_group.name = ? AND external_users_groups_source.name = ?",
				test.relations.Data.ExternalGroup, test.relations.Data.ExternalUsersGroupsSource).
			Select(); err != nil {
			ts.FailNow("Can not load a Group from DB", err)
		}

		if len(test.expectedUsers) != len(dbGroupAfterUpdate.ExternalUsers) {
			ts.FailNow("expectedUsers length is not equal to loaded ExternalUsers length")
		}

		// Make Users list.
		var dbUsers = make([]storage.ResourceNameT, len(dbGroupAfterUpdate.ExternalUsers))

		for i, u := range dbGroupAfterUpdate.ExternalUsers {
			dbUsers[i] = u.Name
		}

		sort.Slice(dbUsers, func(i, j int) bool { return dbUsers[i] < dbUsers[j] })
		sort.Slice(test.expectedUsers, func(i, j int) bool { return test.expectedUsers[i] < test.expectedUsers[j] })

		ts.Equal(test.expectedUsers, dbUsers, "Unexpected loaded users value")
	}
}

func (ts *TestExternalGroupToExternalUserV1sRelationsSuite) TestUnbindSuccessCheckETagUpdatedAt() {
	var tests = []struct {
		relations storage.ExternalGroup2ExternalUserListRelationsV1

		expectedUserETags map[storage.ResourceNameT]storage.ETagT
	}{
		{
			relations: storage.ExternalGroup2ExternalUserListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalGroup2ExternalUserListV1RelationsKind,
					Name:       "group 1 at source 1",
				},
				Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
					ExternalGroup:             "group 1 at source 1",
					ExternalUsersGroupsSource: "source 1",
					ExternalUsers: []storage.ResourceNameT{
						"user 1 at source 1",
						"user 3 at source 1",
					},
				},
			},
			expectedUserETags: map[storage.ResourceNameT]storage.ETagT{
				"user 1 at source 1": 2, // Was in Relations Update.
				"user 2 at source 1": 2,
				"user 3 at source 1": 4, // Was in Relations Update.
			},
		},
		{ // The same as above but with empty Users slice to check the "empty slice" case.
			relations: storage.ExternalGroup2ExternalUserListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalGroup2ExternalUserListV1RelationsKind,
					Name:       "group 1 at source 1",
				},
				Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
					ExternalGroup:             "group 1 at source 1",
					ExternalUsersGroupsSource: "source 1",
					ExternalUsers:             []storage.ResourceNameT{},
				},
			},
			expectedUserETags: map[storage.ResourceNameT]storage.ETagT{
				"user 1 at source 1": 2, // Was in Relations Update in previous step.
				"user 2 at source 1": 2,
				"user 3 at source 1": 4, // Was in Relations Update in previous step.
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		// Load DB ExternalGroup.
		var dbGroupBeforeUpdate ExternalGroup

		if err := ts.db.Model(&dbGroupBeforeUpdate).Relation("ExternalUsersGroupsSource").
			Where("external_group.name = ? AND external_users_groups_source.name = ?",
				test.relations.Data.ExternalGroup, test.relations.Data.ExternalUsersGroupsSource).Select(); err != nil {
			ts.FailNowf("Can not load an ExternalGroup before update",
				"Name: %s, Source: %s, Error: %s")
		}

		gotErr := ts.sqlStorage.UnBindExternalGroup2ExternalUserV1sContext(context.TODO(), &test.relations)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Load a Group from DB to check Users.
		var dbGroupAfterUpdate ExternalGroup

		if err := ts.db.Model(&dbGroupAfterUpdate).
			Relation("ExternalUsersGroupsSource").
			Relation("ExternalUsers").
			Where("external_group.name = ? AND external_users_groups_source.name = ?",
				test.relations.Data.ExternalGroup, test.relations.Data.ExternalUsersGroupsSource).
			Select(); err != nil {
			ts.FailNow("Can not load a Group from DB", err)
		}

		// Check ETag increment and UpdatedAt for a Group.
		var currentTime = time.Now()

		dbGroupBeforeUpdate.ETag++

		ts.Equalf(dbGroupBeforeUpdate.ETag, dbGroupAfterUpdate.ETag,
			"Unexpected ExternalGroup ETag increment", "Group: %s, Source: %s",
			test.relations.Data.ExternalGroup, test.relations.Data.ExternalUsersGroupsSource)
		ts.WithinDurationf(currentTime, dbGroupAfterUpdate.UpdatedAt, time.Minute,
			"ExternalGroup UpdatedAt time is not within expected time range, Group %s",
			dbGroupAfterUpdate.Name)

		// Check ExternalUser ETags.
		// Load Users from DB.
		var userNames = make([]storage.ResourceNameT, 0, len(test.expectedUserETags))
		for name := range test.expectedUserETags {
			userNames = append(userNames, name)
		}

		var dbUsersAfterUpdate ExternalUserSlice

		if err := ts.db.Model(&dbUsersAfterUpdate).Relation("ExternalUsersGroupsSource").
			Where("external_users_groups_source.name = ?", test.relations.Data.ExternalUsersGroupsSource).
			WhereIn("external_user.name IN (?)", userNames).Select(); err != nil {
			ts.FailNowf("Can not load ExternalUsers from DB",
				"Names: %+v, Error: %s", userNames, err)
		}

		for _, dbUser := range dbUsersAfterUpdate {
			ts.Equalf(test.expectedUserETags[dbUser.Name], dbUser.ETag,
				"Unexpected ExternalUser ETag increment. User: %s, Source: %s",
				dbUser.Name, test.relations.Data.ExternalUsersGroupsSource)

			// Bug: by some strange reason go-pg does not load from DB ExternalUser.UpdatedAt.
			// ts.WithinDurationf(currentTime, dbUser.UpdatedAt, time.Minute,
			// "ExternalUser UpdatedAt time is not within expected time range, User: %s",
			// dbUser.Name)
		}
	}
}

func (ts *TestExternalGroupToExternalUserV1sRelationsSuite) TestUnbindGroupNotFound() {
	var tests = []struct {
		relations storage.ExternalGroup2ExternalUserListRelationsV1

		expectedError error
	}{
		{
			relations: storage.ExternalGroup2ExternalUserListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalGroup2ExternalUserListV1RelationsKind,
					Name:       "relations 1",
				},
				Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
					ExternalGroup:             "not found group 1 at source 1", // No group.
					ExternalUsersGroupsSource: "source 1",
					ExternalUsers: []storage.ResourceNameT{
						"user 1 at source 1",
						"user 3 at source 1",
					},
				},
			},
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: "not found group 1 at source 1",
				},
			),
		},
		{
			relations: storage.ExternalGroup2ExternalUserListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalGroup2ExternalUserListV1RelationsKind,
					Name:       "relations 1",
				},
				Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
					ExternalGroup:             "group 1 at source 1",
					ExternalUsersGroupsSource: "not found source 1", // No source.
					ExternalUsers: []storage.ResourceNameT{
						"user 1 at source 1",
						"user 3 at source 1",
					},
				},
			},
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "not found source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: "group 1 at source 1",
				},
			),
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.sqlStorage.UnBindExternalGroup2ExternalUserV1sContext(context.TODO(), &test.relations)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalGroupToExternalUserV1sRelationsSuite) TestUnbindUserNotFound() {
	var tests = []struct {
		relations storage.ExternalGroup2ExternalUserListRelationsV1

		expectedError error
	}{
		{
			relations: storage.ExternalGroup2ExternalUserListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalGroup2ExternalUserListV1RelationsKind,
					Name:       "relations 1",
				},
				Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
					ExternalGroup:             "group 1 at source 1",
					ExternalUsersGroupsSource: "source 1",
					ExternalUsers: []storage.ResourceNameT{
						"user 1 at source 1",
						"user 3 at source 1",
						"user 42 at source 1", // No a User with the name.
					},
				},
			},
			expectedError: storage.NewErrRelatedResourcesNotFound(storage.APIVersionV1Value,
				storage.ExternalUserKind, []storage.ResourceNameT{"user 42 at source 1"}, nil),
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.sqlStorage.UnBindExternalGroup2ExternalUserV1sContext(context.TODO(), &test.relations)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func TestExternalGroupToExternalUserV1sRelations(t *testing.T) {
	suite.Run(t, &TestExternalGroupToExternalUserV1sRelationsSuite{})
}

func testCheckNotAllUsersLoadedPrepareDifferentData(count int) ([]storage.ResourceNameT, ExternalUserSlice, error) {
	// Worst case.
	const nameLen = 64 // Just a stupid estimate.

	var (
		required = make([]storage.ResourceNameT, count)
		loaded   = make(ExternalUserSlice, count)
	)

	for i := 0; i < count; i++ {
		var data = make([]byte, nameLen)

		if _, err := rand.Read(data); err != nil {
			return nil, nil, err
		}

		required[i] = storage.ResourceNameT(data)

		if _, err := rand.Read(data); err != nil {
			return nil, nil, err
		}

		loaded[i] = &ExternalUser{
			Name: storage.ResourceNameT(data),
		}
	}

	return required, loaded, nil
}

func testCheckNotAllUsersLoadedPrepareEqualData(count int) ([]storage.ResourceNameT, ExternalUserSlice, error) {
	// Worst case.
	const nameLen = 64 // Just a stupid estimate.

	var (
		required = make([]storage.ResourceNameT, count)
		loaded   = make(ExternalUserSlice, count)
	)

	for i := 0; i < count; i++ {
		var data = make([]byte, nameLen)

		if _, err := rand.Read(data); err != nil {
			return nil, nil, err
		}

		required[i] = storage.ResourceNameT(data)

		loaded[i] = &ExternalUser{
			Name: storage.ResourceNameT(data),
		}
	}

	return required, loaded, nil
}

type TestCheckNotAllUsersLoadedErrorSuite struct {
	suite.Suite
}

func (ts *TestCheckNotAllUsersLoadedErrorSuite) TestSliceEqual() {
	var tests = []struct {
		required []storage.ResourceNameT

		loaded ExternalUserSlice
	}{
		{
			required: []storage.ResourceNameT{
				"user1", "user2",
			},
			loaded: ExternalUserSlice{
				{
					Name: "user2",
				},
				{
					Name: "user1",
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := checkNotAllUsersLoadedSliceError(test.required, test.loaded)

		ts.NoError(gotErr, "Must not return error")
	}
}

func (ts *TestCheckNotAllUsersLoadedErrorSuite) TestSliceNotEqual() {
	var tests = []struct {
		required []storage.ResourceNameT

		loaded ExternalUserSlice

		expectedError error
	}{
		{
			required: []storage.ResourceNameT{
				"user1", "user2",
			},
			loaded: ExternalUserSlice{
				{
					Name: "user2",
				},
			},
			expectedError: storage.NewErrRelatedResourcesNotFound(storage.APIVersionV1Value,
				storage.ExternalUserKind, []storage.ResourceNameT{"user1"}, nil),
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := checkNotAllUsersLoadedSliceError(test.required, test.loaded)

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestCheckNotAllUsersLoadedErrorSuite) TestMapEqual() {
	var tests = []struct {
		required []storage.ResourceNameT

		loaded ExternalUserSlice
	}{
		{
			required: []storage.ResourceNameT{
				"user1", "user2",
			},
			loaded: ExternalUserSlice{
				{
					Name: "user2",
				},
				{
					Name: "user1",
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := checkNotAllUsersLoadedMapError(test.required, test.loaded)

		ts.NoError(gotErr, "Must not return error")
	}
}

func (ts *TestCheckNotAllUsersLoadedErrorSuite) TestMapNotEqual() {
	var tests = []struct {
		required []storage.ResourceNameT

		loaded ExternalUserSlice

		expectedError error
	}{
		{
			required: []storage.ResourceNameT{
				"user1", "user2",
			},
			loaded: ExternalUserSlice{
				{
					Name: "user2",
				},
			},
			expectedError: storage.NewErrRelatedResourcesNotFound(storage.APIVersionV1Value,
				storage.ExternalUserKind, []storage.ResourceNameT{"user1"}, nil),
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := checkNotAllUsersLoadedMapError(test.required, test.loaded)

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func TestCheckNotAllUsersLoadedError(t *testing.T) {
	suite.Run(t, &TestCheckNotAllUsersLoadedErrorSuite{})
}

func BenchmarkCheckNotAllUsersLoadedSliceError10(b *testing.B) {
	required, loaded, err := testCheckNotAllUsersLoadedPrepareEqualData(10)
	if err != nil {
		b.Fatal(err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		if err := checkNotAllUsersLoadedSliceError(required, loaded); err != nil {
			b.Fatal("Unexpected error value", err)
		}
	}
}

func BenchmarkCheckNotAllUsersLoadedSliceError20(b *testing.B) {
	required, loaded, err := testCheckNotAllUsersLoadedPrepareEqualData(20)
	if err != nil {
		b.Fatal(err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		if err := checkNotAllUsersLoadedSliceError(required, loaded); err != nil {
			b.Fatal("Unexpected error value", err)
		}
	}
}

func BenchmarkCheckNotAllUsersLoadedSliceError30(b *testing.B) {
	required, loaded, err := testCheckNotAllUsersLoadedPrepareEqualData(30)
	if err != nil {
		b.Fatal(err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		if err := checkNotAllUsersLoadedSliceError(required, loaded); err != nil {
			b.Fatal("Unexpected error value", err)
		}
	}
}

func BenchmarkCheckNotAllUsersLoadedSliceError40(b *testing.B) {
	required, loaded, err := testCheckNotAllUsersLoadedPrepareEqualData(40)
	if err != nil {
		b.Fatal(err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		if err := checkNotAllUsersLoadedSliceError(required, loaded); err != nil {
			b.Fatal("Unexpected error value", err)
		}
	}
}

func BenchmarkCheckNotAllUsersLoadedSliceError50(b *testing.B) {
	required, loaded, err := testCheckNotAllUsersLoadedPrepareEqualData(50)
	if err != nil {
		b.Fatal(err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		if err := checkNotAllUsersLoadedSliceError(required, loaded); err != nil {
			b.Fatal("Unexpected error value", err)
		}
	}
}

func BenchmarkCheckNotAllUsersLoadedMapError10(b *testing.B) {
	required, loaded, err := testCheckNotAllUsersLoadedPrepareEqualData(10)
	if err != nil {
		b.Fatal(err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		if err := checkNotAllUsersLoadedMapError(required, loaded); err != nil {
			b.Fatal("Unexpected error value", err)
		}
	}
}

func BenchmarkCheckNotAllUsersLoadedMapError20(b *testing.B) {
	required, loaded, err := testCheckNotAllUsersLoadedPrepareEqualData(20)
	if err != nil {
		b.Fatal(err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		if err := checkNotAllUsersLoadedMapError(required, loaded); err != nil {
			b.Fatal("Unexpected error value", err)
		}
	}
}

func BenchmarkCheckNotAllUsersLoadedMapError30(b *testing.B) {
	required, loaded, err := testCheckNotAllUsersLoadedPrepareEqualData(30)
	if err != nil {
		b.Fatal(err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		if err := checkNotAllUsersLoadedMapError(required, loaded); err != nil {
			b.Fatal("Unexpected error value", err)
		}
	}
}

func BenchmarkCheckNotAllUsersLoadedMapError40(b *testing.B) {
	required, loaded, err := testCheckNotAllUsersLoadedPrepareEqualData(40)
	if err != nil {
		b.Fatal(err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		if err := checkNotAllUsersLoadedMapError(required, loaded); err != nil {
			b.Fatal("Unexpected error value", err)
		}
	}
}

func BenchmarkCheckNotAllUsersLoadedMapError50(b *testing.B) {
	required, loaded, err := testCheckNotAllUsersLoadedPrepareEqualData(50)
	if err != nil {
		b.Fatal(err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		if err := checkNotAllUsersLoadedMapError(required, loaded); err != nil {
			b.Fatal("Unexpected error value", err)
		}
	}
}

func BenchmarkCheckNotAllUsersLoadedSliceErrorError10(b *testing.B) {
	required, loaded, err := testCheckNotAllUsersLoadedPrepareDifferentData(10)
	if err != nil {
		b.Fatal(err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		err := checkNotAllUsersLoadedSliceError(required, loaded)

		_, ok := err.(*storage.ErrRelatedResourcesNotFound)
		if !ok {
			b.Fatal("Unexpected error value", err)
		}
	}
}

func BenchmarkCheckNotAllUsersLoadedSliceErrorError20(b *testing.B) {
	required, loaded, err := testCheckNotAllUsersLoadedPrepareDifferentData(20)
	if err != nil {
		b.Fatal(err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		err := checkNotAllUsersLoadedSliceError(required, loaded)

		_, ok := err.(*storage.ErrRelatedResourcesNotFound)
		if !ok {
			b.Fatal("Unexpected error value", err)
		}
	}
}

func BenchmarkCheckNotAllUsersLoadedSliceErrorError30(b *testing.B) {
	required, loaded, err := testCheckNotAllUsersLoadedPrepareDifferentData(30)
	if err != nil {
		b.Fatal(err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		err := checkNotAllUsersLoadedSliceError(required, loaded)

		_, ok := err.(*storage.ErrRelatedResourcesNotFound)
		if !ok {
			b.Fatal("Unexpected error value", err)
		}
	}
}

func BenchmarkCheckNotAllUsersLoadedSliceErrorError40(b *testing.B) {
	required, loaded, err := testCheckNotAllUsersLoadedPrepareDifferentData(40)
	if err != nil {
		b.Fatal(err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		err := checkNotAllUsersLoadedSliceError(required, loaded)

		_, ok := err.(*storage.ErrRelatedResourcesNotFound)
		if !ok {
			b.Fatal("Unexpected error value", err)
		}
	}
}

func BenchmarkCheckNotAllUsersLoadedSliceErrorError50(b *testing.B) {
	required, loaded, err := testCheckNotAllUsersLoadedPrepareDifferentData(50)
	if err != nil {
		b.Fatal(err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		err := checkNotAllUsersLoadedSliceError(required, loaded)

		_, ok := err.(*storage.ErrRelatedResourcesNotFound)
		if !ok {
			b.Fatal("Unexpected error value", err)
		}
	}
}

func BenchmarkCheckNotAllUsersLoadedMapErrorError10(b *testing.B) {
	required, loaded, err := testCheckNotAllUsersLoadedPrepareDifferentData(10)
	if err != nil {
		b.Fatal(err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		err := checkNotAllUsersLoadedMapError(required, loaded)

		_, ok := err.(*storage.ErrRelatedResourcesNotFound)
		if !ok {
			b.Fatal("Unexpected error value", err)
		}
	}
}

func BenchmarkCheckNotAllUsersLoadedMapErrorError20(b *testing.B) {
	required, loaded, err := testCheckNotAllUsersLoadedPrepareDifferentData(20)
	if err != nil {
		b.Fatal(err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		err := checkNotAllUsersLoadedMapError(required, loaded)

		_, ok := err.(*storage.ErrRelatedResourcesNotFound)
		if !ok {
			b.Fatal("Unexpected error value", err)
		}
	}
}

func BenchmarkCheckNotAllUsersLoadedMapErrorError30(b *testing.B) {
	required, loaded, err := testCheckNotAllUsersLoadedPrepareDifferentData(30)
	if err != nil {
		b.Fatal(err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		err := checkNotAllUsersLoadedMapError(required, loaded)

		_, ok := err.(*storage.ErrRelatedResourcesNotFound)
		if !ok {
			b.Fatal("Unexpected error value", err)
		}
	}
}

func BenchmarkCheckNotAllUsersLoadedMapErrorError40(b *testing.B) {
	required, loaded, err := testCheckNotAllUsersLoadedPrepareDifferentData(40)
	if err != nil {
		b.Fatal(err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		err := checkNotAllUsersLoadedMapError(required, loaded)

		_, ok := err.(*storage.ErrRelatedResourcesNotFound)
		if !ok {
			b.Fatal("Unexpected error value", err)
		}
	}
}

func BenchmarkCheckNotAllUsersLoadedMapErrorError50(b *testing.B) {
	required, loaded, err := testCheckNotAllUsersLoadedPrepareDifferentData(50)
	if err != nil {
		b.Fatal(err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		err := checkNotAllUsersLoadedMapError(required, loaded)

		_, ok := err.(*storage.ErrRelatedResourcesNotFound)
		if !ok {
			b.Fatal("Unexpected error value", err)
		}
	}
}

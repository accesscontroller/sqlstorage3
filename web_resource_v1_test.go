package sqlstorage3

import (
	"context"
	"crypto/rand"
	"fmt"
	"net"
	"net/url"
	"sort"
	"testing"

	"github.com/go-pg/pg/v10"
	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

type TestWebResourceV1Suite struct {
	db *pg.DB

	sqlStorage *SQLStorage

	suite.Suite
}

func (ts *TestWebResourceV1Suite) SetupTest() {
	db, err := connectDB()
	if err != nil {
		ts.FailNow("Can not connect to test DB", err)
	}

	ts.db = db
	ts.sqlStorage = &SQLStorage{db: db}

	// Create DB Tables.
	if err := recreateTables(ts.db); err != nil {
		ts.FailNow("Can not initialize DB", err)
	}

	// Create some init data.
	var webResourceCategories = WebResourceCategorySlice{
		{
			ETag:        1,
			Name:        "category 1",
			Description: "cat 1 description",
		},
		{
			ETag:        2,
			Name:        "category 2",
			Description: "cat 2 description",
		},
		{
			ETag:        3,
			Name:        "category 3",
			Description: "cat 3 description",
		},
		{
			ETag:        4,
			Name:        "category 4",
			Description: "cat 4 description",
		},
	}

	for _, c := range webResourceCategories {
		if _, err := db.Model(c).Insert(); err != nil {
			ts.FailNow("Can not Insert webResourceCategory model", err)
		}
	}

	// Web Resources.
	var webResources = WebResourceSlice{
		{
			ETag:        1,
			Name:        "web resource 1 cat 1",
			Description: "description web resource 1 cat 1",
		},
		{
			ETag:        2,
			Name:        "web resource 2 cat 2",
			Description: "description web resource 2 cat 2",
		},
		{
			ETag:        3,
			Name:        "web resource 3 cat 1",
			Description: "description web resource 3 cat 1",
		},
		{
			ETag:        4,
			Name:        "web resource 4 cat 1",
			Description: "description web resource 4 cat 1",
		},
		{
			ETag:        5,
			Name:        "web resource 5 categories 1,2",
			Description: "Description web resource 5 categories 1,2",
		},
	}

	for _, wr := range webResources {
		if _, err := db.Model(wr).Insert(); err != nil {
			ts.FailNow("Can not insert Test Web Resource", err)
		}
	}

	// Add Web Resource to WebResource Categories.
	var webResourceToCategory = []WebResourceCategoryToWebResource{
		{
			WebResourceCategoryID: webResourceCategories[0].ID,
			WebResourceID:         webResources[0].ID,
		},
		{
			WebResourceCategoryID: webResourceCategories[1].ID,
			WebResourceID:         webResources[1].ID,
		},
		{
			WebResourceCategoryID: webResourceCategories[1].ID,
			WebResourceID:         webResources[2].ID,
		},
		// Resource 5 to Categories 1,2.
		{
			WebResourceCategoryID: webResourceCategories[1].ID,
			WebResourceID:         webResources[4].ID,
		},
		{
			WebResourceCategoryID: webResourceCategories[0].ID,
			WebResourceID:         webResources[4].ID,
		},
	}

	for i := range webResourceToCategory {
		if _, err := db.Model(&webResourceToCategory[i]).Insert(); err != nil {
			ts.FailNowf("Can not Insert WebResource to WebResource Category",
				"WebResourceID %d; WebResourceCategoryID: %d; error: %s",
				&webResourceToCategory[i].WebResourceID,
				&webResourceToCategory[i].WebResourceCategoryID,
				err)
		}
	}

	// Add IPs.
	var webResourceIPs = WebResourceIPSlice{
		{
			WebResourceID: webResources[0].ID,
			IP:            net.ParseIP("10.8.100.1"),
		},
		{
			WebResourceID: webResources[0].ID,
			IP:            net.ParseIP("10.8.100.2"),
		},
		{
			WebResourceID: webResources[1].ID,
			IP:            net.ParseIP("10.8.100.3"),
		},
		{
			WebResourceID: webResources[2].ID,
			IP:            net.ParseIP("10.8.100.4"),
		},
		{
			WebResourceID: webResources[3].ID,
			IP:            net.ParseIP("10.8.100.5"),
		},
	}

	for _, ip := range webResourceIPs {
		if _, err := db.Model(ip).Insert(); err != nil {
			ts.FailNow("Can not insert Test Web Resource IP", err)
		}
	}

	// Domains.
	var webResourceDomains = WebResourceDomainSlice{
		{
			WebResourceID: webResources[0].ID,
			Domain:        "example-1-wr-1.com",
		},
		{
			WebResourceID: webResources[0].ID,
			Domain:        "example-2-wr-1.com",
		},
		{
			WebResourceID: webResources[1].ID,
			Domain:        "example-1-wr-2.com",
		},
		{
			WebResourceID: webResources[1].ID,
			Domain:        "example-2-wr-2.com",
		},
		{
			WebResourceID: webResources[2].ID,
			Domain:        "example-1-wr-3.com",
		},
	}

	for _, domain := range webResourceDomains {
		if _, err := db.Model(domain).Insert(); err != nil {
			ts.FailNow("Can not insert Test Web Resource Domain", err)
		}
	}

	// URLs.
	var webResourceURLs = WebResourceURLSlice{
		{
			WebResourceID: webResources[0].ID,
			URL:           "http://example-1-web-resource-1.com",
		},
		{
			WebResourceID: webResources[1].ID,
			URL:           "http://example-1-web-resource-2.com",
		},
		{
			WebResourceID: webResources[1].ID,
			URL:           "http://example-2-web-resource-2.com",
		},
		{
			WebResourceID: webResources[0].ID,
			URL:           "http://example-1-web-resource-3.com",
		},
		{
			WebResourceID: webResources[0].ID,
			URL:           "http://example-1-web-resource-4.com",
		},
	}

	for _, url := range webResourceURLs {
		if _, err := db.Model(url).Insert(); err != nil {
			ts.FailNow("Can not insert Test Web Resource URL", err)
		}
	}
}

func (ts *TestWebResourceV1Suite) TearDownTest() {
	if ts.db != nil {
		if err := ts.db.Close(); err != nil {
			ts.FailNow("Can not Close DB", err)
		}
	}
}

func (ts *TestWebResourceV1Suite) TestGetSuccess() {
	var tests = []struct {
		name     storage.ResourceNameT
		category *storage.ResourceNameT

		expectedResource storage.WebResourceV1
	}{
		{
			name: "web resource 1 cat 1",
			expectedResource: storage.WebResourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.WebResourceKind,
					Name:       "web resource 1 cat 1",
				},
				Data: storage.WebResourceDataV1{
					Description: "description web resource 1 cat 1",
					Domains: []storage.WebResourceDomainT{
						"example-1-wr-1.com",
						"example-2-wr-1.com",
					},
					IPs: []net.IP{
						net.ParseIP("10.8.100.1"),
						net.ParseIP("10.8.100.2"),
					},
					WebResourceCategoryNames: []storage.ResourceNameT{"category 1"},
					URLs: []url.URL{
						parseURLPanic("http://example-1-web-resource-1.com"),
						parseURLPanic("http://example-1-web-resource-3.com"),
						parseURLPanic("http://example-1-web-resource-4.com"),
					},
				},
			},
		},
		{
			name:     "web resource 1 cat 1",
			category: func() *storage.ResourceNameT { var c storage.ResourceNameT = "category 1"; return &c }(),
			expectedResource: storage.WebResourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.WebResourceKind,
					Name:       "web resource 1 cat 1",
				},
				Data: storage.WebResourceDataV1{
					Description: "description web resource 1 cat 1",
					Domains: []storage.WebResourceDomainT{
						"example-1-wr-1.com",
						"example-2-wr-1.com",
					},
					IPs: []net.IP{
						net.ParseIP("10.8.100.1"),
						net.ParseIP("10.8.100.2"),
					},
					WebResourceCategoryNames: []storage.ResourceNameT{"category 1"},
					URLs: []url.URL{
						parseURLPanic("http://example-1-web-resource-1.com"),
						parseURLPanic("http://example-1-web-resource-3.com"),
						parseURLPanic("http://example-1-web-resource-4.com"),
					},
				},
			},
		},
		{
			name: "web resource 5 categories 1,2",
			expectedResource: storage.WebResourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       5,
					Kind:       storage.WebResourceKind,
					Name:       "web resource 5 categories 1,2",
				},
				Data: storage.WebResourceDataV1{
					Description:              "Description web resource 5 categories 1,2",
					WebResourceCategoryNames: []storage.ResourceNameT{"category 1", "category 2"},
					Domains:                  []storage.WebResourceDomainT{},
					URLs:                     []url.URL{},
					IPs:                      []net.IP{},
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.GetWebResourceV1Context(context.TODO(), test.category, test.name)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		ts.Equal(test.expectedResource, *got, "Unexpected value")
	}
}

func (ts *TestWebResourceV1Suite) TestGetNotFound() {
	var tests = []struct {
		name     storage.ResourceNameT
		category *storage.ResourceNameT

		expectedError error
	}{
		{
			name: "not found web resource 1 cat 1",
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.WebResourceKind,
					Name:       "not found web resource 1 cat 1",
				},
			),
		},
		{ // Correct name but not found in a category.
			name:     "web resource 1 cat 1",
			category: func() *storage.ResourceNameT { var c storage.ResourceNameT = "incorrect category"; return &c }(),
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.WebResourceKind,
					Name:       "web resource 1 cat 1",
				},
			),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.GetWebResourceV1Context(context.TODO(), test.category, test.name)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		if !ts.Nil(got, "Must return nil result") {
			ts.FailNow("Got not nil result - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestWebResourceV1Suite) TestCreateSuccess() {
	var tests = []struct {
		getCreated bool

		resource storage.WebResourceV1
	}{
		{
			resource: storage.WebResourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       111,
					Kind:       storage.WebResourceKind,
					Name:       "new web resource 1 at category 2",
				},
				Data: storage.WebResourceDataV1{
					Description: "new web resource description",
					Domains: []storage.WebResourceDomainT{
						"domain-1.com",
						"domain-2.com",
					},
					IPs: []net.IP{
						net.ParseIP("10.8.200.1"),
						net.ParseIP("10.8.200.2"),
					},
					URLs: []url.URL{
						parseURLPanic("http://domain-1.com/path-1"),
					},
					WebResourceCategoryNames: []storage.ResourceNameT{"category 2"},
				},
			},
		},
		{
			resource: storage.WebResourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       112,
					Kind:       storage.WebResourceKind,
					Name:       "new web resource 2 at category 2",
				},
				Data: storage.WebResourceDataV1{
					Description: "new web resource description",
					Domains: []storage.WebResourceDomainT{
						"domain-3.com",
						"domain-4.com",
					},
					IPs: []net.IP{
						net.ParseIP("10.8.200.3"),
						net.ParseIP("10.8.200.4"),
					},
					URLs: []url.URL{
						parseURLPanic("http://domain-2.com/path-1"),
					},
					WebResourceCategoryNames: []storage.ResourceNameT{"category 2"},
				},
			},
			getCreated: true,
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.CreateWebResourceV1Context(context.TODO(),
			&test.resource, test.getCreated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Check created.
		var dbModel WebResource

		if err := ts.db.Model(&dbModel).
			Relation("WebResourceCategorys").
			Relation("WebResourceIPs").
			Relation("WebResourceDomains").
			Relation("WebResourceURLs").
			Where("web_resource.name = ?", test.resource.Metadata.Name).Select(); err != nil {
			ts.FailNow("Can not get a created WebResource from DB", err)
		}

		// Compare.
		ts.Equal(test.resource.Metadata.ETag, dbModel.ETag)
		ts.Equal(test.resource.Metadata.Name, dbModel.Name)
		ts.Equal(test.resource.Data.Description, dbModel.Description)

		// Check Web Resource Categories.
		if !ts.Lenf(dbModel.WebResourceCategorys, len(test.resource.Data.WebResourceCategoryNames),
			"WebResourceCategory Names Arrays Lengths does not match",
			"DBModel: %+v; Expected: %+v",
			dbModel.WebResourceCategorys, test.resource.Data.WebResourceCategoryNames) {
			ts.FailNow("Lengths do not match")
		}

		sort.Slice(test.resource.Data.WebResourceCategoryNames, func(i, j int) bool {
			return test.resource.Data.WebResourceCategoryNames[i] < test.resource.Data.WebResourceCategoryNames[j]
		})

		sort.Slice(dbModel.WebResourceCategorys, func(i, j int) bool {
			return dbModel.WebResourceCategorys[i].Name < dbModel.WebResourceCategorys[j].Name
		})

		for dbMInd := range dbModel.WebResourceCategorys {
			ts.Equal(test.resource.Data.WebResourceCategoryNames[dbMInd],
				dbModel.WebResourceCategorys[dbMInd].Name,
				"Categories must be equal")
		}

		// Check Domains.
		sort.Slice(test.resource.Data.Domains, func(i, j int) bool {
			return test.resource.Data.Domains[i] < test.resource.Data.Domains[j]
		})

		sort.Slice(dbModel.WebResourceDomains, func(i, j int) bool {
			return dbModel.WebResourceDomains[i].Domain < dbModel.WebResourceDomains[j].Domain
		})

		for domInd := range test.resource.Data.Domains {
			ts.Equal(test.resource.Data.Domains[domInd], dbModel.WebResourceDomains[domInd].Domain,
				"Domain names must be equal")
		}

		// Check IPs.
		sort.Slice(test.resource.Data.IPs, func(i, j int) bool {
			return test.resource.Data.IPs[i].String() < test.resource.Data.IPs[j].String()
		})

		sort.Slice(dbModel.WebResourceIPs, func(i, j int) bool {
			return dbModel.WebResourceIPs[i].IP.String() < dbModel.WebResourceIPs[j].IP.String()
		})

		for IPInd := range test.resource.Data.IPs {
			ts.Equal(test.resource.Data.IPs[IPInd], dbModel.WebResourceIPs[IPInd].IP,
				"IPs addresses must be equal")
		}

		// URLs.
		sort.Slice(test.resource.Data.URLs, func(i, j int) bool {
			return test.resource.Data.URLs[i].String() < test.resource.Data.URLs[j].String()
		})

		sort.Slice(dbModel.WebResourceURLs, func(i, j int) bool {
			return dbModel.WebResourceURLs[i].URL < dbModel.WebResourceURLs[j].URL
		})

		for URLInd := range test.resource.Data.URLs {
			ts.Equal(test.resource.Data.URLs[URLInd].String(), dbModel.WebResourceURLs[URLInd].URL,
				"URLs must be equal")
		}

		// Check returned result.
		if !test.getCreated {
			continue
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		// Domains.
		sort.Slice(got.Data.Domains, func(i, j int) bool {
			return got.Data.Domains[i] < got.Data.Domains[j]
		})

		// IPs.
		sort.Slice(got.Data.IPs, func(i, j int) bool {
			return got.Data.IPs[i].String() < got.Data.IPs[j].String()
		})

		// URLs.
		sort.Slice(got.Data.URLs, func(i, j int) bool {
			return got.Data.URLs[i].String() < got.Data.URLs[j].String()
		})

		ts.Equal(test.resource, *got, "Unexpected returned result")
	}
}

func (ts *TestWebResourceV1Suite) TestCreateConflict() {
	var tests = []struct {
		resource storage.WebResourceV1

		expectedError error
	}{
		{
			resource: storage.WebResourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       111,
					Kind:       storage.WebResourceKind,
					Name:       "web resource 1 cat 1", // Name conflict.
				},
				Data: storage.WebResourceDataV1{
					Description: "new web resource description",
					Domains: []storage.WebResourceDomainT{
						"domain-1.com",
						"domain-2.com",
					},
					IPs: []net.IP{
						net.ParseIP("10.8.200.1"),
						net.ParseIP("10.8.200.2"),
					},
					URLs: []url.URL{
						parseURLPanic("http://domain-1.com/path-1"),
					},
					WebResourceCategoryNames: []storage.ResourceNameT{"category 2"},
				},
			},
			expectedError: storage.NewErrResourceAlreadyExists([]interface{}{
				storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       111,
					Kind:       storage.WebResourceKind,
					Name:       "web resource 1 cat 1",
				},
			}),
		},
		{
			resource: storage.WebResourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       111,
					Kind:       storage.WebResourceKind,
					Name:       "new web resource 1 cat 2",
				},
				Data: storage.WebResourceDataV1{
					Description: "new web resource description",
					Domains: []storage.WebResourceDomainT{
						"domain-1.com",
						"domain-2.com",
						"example-1-wr-1.com", // Domain conflict.
					},
					IPs: []net.IP{
						net.ParseIP("10.8.200.1"),
						net.ParseIP("10.8.200.2"),
					},
					URLs: []url.URL{
						parseURLPanic("http://domain-1.com/path-1"),
					},
					WebResourceCategoryNames: []storage.ResourceNameT{"category 2"},
				},
			},
			expectedError: storage.NewErrResourceAlreadyExists([]interface{}{
				fmt.Sprintf("Domains %+v", []string{
					"domain-1.com",
					"domain-2.com",
					"example-1-wr-1.com", // Domain conflict.
				}),
			}),
		},
		{
			resource: storage.WebResourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       111,
					Kind:       storage.WebResourceKind,
					Name:       "new web resource 1 cat 2",
				},
				Data: storage.WebResourceDataV1{
					Description: "new web resource description",
					Domains: []storage.WebResourceDomainT{
						"domain-1.com",
						"domain-2.com",
					},
					IPs: []net.IP{
						net.ParseIP("10.8.100.1"), // IP Conflict.
						net.ParseIP("10.8.200.2"),
					},
					URLs: []url.URL{
						parseURLPanic("http://domain-1.com/path-1"),
					},
					WebResourceCategoryNames: []storage.ResourceNameT{"category 2"},
				},
			},
			expectedError: storage.NewErrResourceAlreadyExists([]interface{}{
				fmt.Sprintf("IPs %+v", []string{
					"10.8.100.1",
					"10.8.200.2",
				}),
			}),
		},
		{
			resource: storage.WebResourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       111,
					Kind:       storage.WebResourceKind,
					Name:       "new web resource 1 cat 2",
				},
				Data: storage.WebResourceDataV1{
					Description: "new web resource description",
					Domains: []storage.WebResourceDomainT{
						"domain-1.com",
						"domain-2.com",
					},
					IPs: []net.IP{
						net.ParseIP("10.8.200.2"),
					},
					URLs: []url.URL{
						parseURLPanic("http://domain-1.com/path-1"),
						parseURLPanic("http://example-1-web-resource-2.com"), // URL conflict.
					},
					WebResourceCategoryNames: []storage.ResourceNameT{"category 2"},
				},
			},
			expectedError: storage.NewErrResourceAlreadyExists([]interface{}{
				fmt.Sprintf("URLs %+v", []string{
					"http://domain-1.com/path-1",
					"http://example-1-web-resource-2.com",
				}),
			}),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.CreateWebResourceV1Context(context.TODO(), &test.resource, true)

		ts.Nil(got, "Must return nil result")

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestWebResourceV1Suite) TestCreateCategoryNotFound() {
	var tests = []struct {
		resource storage.WebResourceV1

		expectedError error
	}{
		{
			resource: storage.WebResourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       111,
					Kind:       storage.WebResourceKind,
					Name:       "new web resource 1 at category 2",
				},
				Data: storage.WebResourceDataV1{
					Description: "new web resource description",
					Domains: []storage.WebResourceDomainT{
						"domain-1.com",
						"domain-2.com",
					},
					IPs: []net.IP{
						net.ParseIP("10.8.200.1"),
						net.ParseIP("10.8.200.2"),
					},
					URLs: []url.URL{
						parseURLPanic("http://domain-1.com/path-1"),
					},
					WebResourceCategoryNames: []storage.ResourceNameT{"non-existing category 2"}, // No such a category.
				},
			},
			expectedError: storage.NewErrRelatedResourcesNotFound(
				storage.APIVersionV1Value,
				storage.WebResourceCategoryKind,
				[]storage.ResourceNameT{"non-existing category 2"},
				nil),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.CreateWebResourceV1Context(context.TODO(), &test.resource, true)

		ts.Nil(got, "Must return nil result")

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestWebResourceV1Suite) TestUpdateSuccess() {
	var tests = []struct {
		getUpdated bool

		oldName  storage.ResourceNameT
		resource storage.WebResourceV1
	}{
		{
			// No Update.
			getUpdated: true,
			oldName:    "web resource 2 cat 2",
			resource: storage.WebResourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       2,
					Kind:       storage.WebResourceKind,
					Name:       "web resource 2 cat 2",
				},
				Data: storage.WebResourceDataV1{
					Description: "description web resource 2 cat 2",
					Domains: []storage.WebResourceDomainT{
						"example-1-wr-2.com",
						"example-2-wr-2.com",
					},
					IPs: []net.IP{
						net.ParseIP("10.8.100.3"),
					},
					WebResourceCategoryNames: []storage.ResourceNameT{"category 1"},
					URLs: []url.URL{
						parseURLPanic("http://example-1-web-resource-2.com"),
						parseURLPanic("http://example-2-web-resource-2.com"),
					},
				},
			},
		},
		{
			oldName: "web resource 1 cat 1",
			resource: storage.WebResourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.WebResourceKind,
					Name:       "updated web resource 1 at category 2",
				},
				Data: storage.WebResourceDataV1{
					Description: "new web resource description",
					Domains: []storage.WebResourceDomainT{
						"example-1-wr-1.com",
						"domain-1.com",
						"domain-2.com",
					},
					IPs: []net.IP{
						net.ParseIP("10.8.100.1"),
						net.ParseIP("10.8.200.1"),
						net.ParseIP("10.8.200.2"),
					},
					URLs: []url.URL{
						parseURLPanic("http://example-1-web-resource-1.com"),
						parseURLPanic("http://domain-1.com/path-1"),
					},
					WebResourceCategoryNames: []storage.ResourceNameT{"category 2"},
				},
			},
		},
		{
			oldName: "web resource 3 cat 1",
			resource: storage.WebResourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       3,
					Kind:       storage.WebResourceKind,
					Name:       "new web resource 3 at category 1",
				},
				Data: storage.WebResourceDataV1{
					Description: "new web resource description",
					Domains: []storage.WebResourceDomainT{
						"domain-3.com",
						"domain-4.com",
					},
					IPs: []net.IP{
						net.ParseIP("10.8.200.3"),
						net.ParseIP("10.8.200.4"),
					},
					URLs: []url.URL{
						parseURLPanic("http://domain-2.com/path-1"),
					},
					WebResourceCategoryNames: []storage.ResourceNameT{"category 4"},
				},
			},
			getUpdated: true,
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.UpdateWebResourceV1Context(context.TODO(),
			test.oldName, &test.resource, test.getUpdated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Check DB status.
		var dbModel WebResource

		if err := ts.db.Model(&dbModel).
			Relation("WebResourceCategorys").
			Relation("WebResourceIPs").
			Relation("WebResourceDomains").
			Relation("WebResourceURLs").
			Where("web_resource.name = ?", test.resource.Metadata.Name).Select(); err != nil {
			ts.FailNow("Can not get a created WebResource from DB", err)
		}

		// Compare.
		// Fix ETag Increment.
		test.resource.Metadata.ETag++

		ts.Equal(test.resource.Metadata.ETag, dbModel.ETag)
		ts.Equal(test.resource.Metadata.Name, dbModel.Name)
		ts.Equal(test.resource.Data.Description, dbModel.Description)

		// Check Web Resource Categories.
		if !ts.Lenf(dbModel.WebResourceCategorys, len(test.resource.Data.WebResourceCategoryNames),
			"WebResourceCategory Names Arrays Lengths does not match",
			"DBModel: %+v; Expected: %+v",
			dbModel.WebResourceCategorys, test.resource.Data.WebResourceCategoryNames) {
			ts.FailNow("Lengths do not match")
		}

		sort.Slice(test.resource.Data.WebResourceCategoryNames, func(i, j int) bool {
			return test.resource.Data.WebResourceCategoryNames[i] < test.resource.Data.WebResourceCategoryNames[j]
		})

		sort.Slice(dbModel.WebResourceCategorys, func(i, j int) bool {
			return dbModel.WebResourceCategorys[i].Name < dbModel.WebResourceCategorys[j].Name
		})

		for dbMInd := range dbModel.WebResourceCategorys {
			ts.Equal(dbModel.WebResourceCategorys[dbMInd].Name,
				test.resource.Data.WebResourceCategoryNames[dbMInd],
				"Categories must be equal")
		}

		// Check Domains.
		if !ts.Lenf(dbModel.WebResourceDomains, len(test.resource.Data.Domains),
			"WebResourceCategory Domain Arrays Lengths does not match",
			"DBModel: %+v; Expected: %+v",
			dbModel.WebResourceDomains, test.resource.Data.Domains) {
			ts.FailNow("Lengths do not match")
		}

		sort.Slice(test.resource.Data.Domains, func(i, j int) bool {
			return test.resource.Data.Domains[i] < test.resource.Data.Domains[j]
		})

		sort.Slice(dbModel.WebResourceDomains, func(i, j int) bool {
			return dbModel.WebResourceDomains[i].Domain < dbModel.WebResourceDomains[j].Domain
		})

		for domInd := range test.resource.Data.Domains {
			ts.Equal(test.resource.Data.Domains[domInd], dbModel.WebResourceDomains[domInd].Domain,
				"Domain names must be equal")
		}

		// Check IPs.
		if !ts.Lenf(dbModel.WebResourceIPs, len(test.resource.Data.IPs),
			"WebResourceCategory IPs Arrays Lengths does not match",
			"DBModel: %+v; Expected: %+v",
			dbModel.WebResourceIPs, test.resource.Data.IPs) {
			ts.FailNow("Lengths do not match")
		}

		sort.Slice(test.resource.Data.IPs, func(i, j int) bool {
			return test.resource.Data.IPs[i].String() < test.resource.Data.IPs[j].String()
		})

		sort.Slice(dbModel.WebResourceIPs, func(i, j int) bool {
			return dbModel.WebResourceIPs[i].IP.String() < dbModel.WebResourceIPs[j].IP.String()
		})

		for IPInd := range test.resource.Data.IPs {
			ts.Equal(test.resource.Data.IPs[IPInd], dbModel.WebResourceIPs[IPInd].IP,
				"IPs addresses must be equal")
		}

		// URLs.
		if !ts.Lenf(dbModel.WebResourceURLs, len(test.resource.Data.URLs),
			"WebResourceCategory URLs Arrays Lengths does not match",
			"DBModel: %+v; Expected: %+v",
			dbModel.WebResourceURLs, test.resource.Data.URLs) {
			ts.FailNow("Lengths do not match")
		}

		sort.Slice(test.resource.Data.URLs, func(i, j int) bool {
			return test.resource.Data.URLs[i].String() < test.resource.Data.URLs[j].String()
		})

		sort.Slice(dbModel.WebResourceURLs, func(i, j int) bool {
			return dbModel.WebResourceURLs[i].URL < dbModel.WebResourceURLs[j].URL
		})

		for URLInd := range test.resource.Data.URLs {
			ts.Equal(test.resource.Data.URLs[URLInd].String(), dbModel.WebResourceURLs[URLInd].URL,
				"URLs must be equal")
		}

		// Check returned result.
		if !test.getUpdated {
			continue
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		// Domains.
		sort.Slice(got.Data.Domains, func(i, j int) bool {
			return got.Data.Domains[i] < got.Data.Domains[j]
		})

		// IPs.
		sort.Slice(got.Data.IPs, func(i, j int) bool {
			return got.Data.IPs[i].String() < got.Data.IPs[j].String()
		})

		// URLs.
		sort.Slice(got.Data.URLs, func(i, j int) bool {
			return got.Data.URLs[i].String() < got.Data.URLs[j].String()
		})

		ts.Equal(test.resource, *got, "Unexpected returned result")
	}
}

func (ts *TestWebResourceV1Suite) TestUpdateNotFound() {
	var tests = []struct {
		oldName storage.ResourceNameT

		patch storage.WebResourceV1

		expectedError error
	}{
		{
			oldName: "not found resource 1",
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.WebResourceKind,
					Name:       "not found resource 1",
				},
			),
		},
		{
			oldName: "web resource 1 cat 1",
			patch: storage.WebResourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.WebResourceKind,
					Name:       "updated web resource 1 cat 1",
				},
				Data: storage.WebResourceDataV1{
					Description:              "updated description",
					WebResourceCategoryNames: []storage.ResourceNameT{"non existing web resource category name"},
				},
			},
			expectedError: storage.NewErrRelatedResourcesNotFound(
				storage.APIVersionV1Value,
				storage.WebResourceCategoryKind,
				[]storage.ResourceNameT{"non existing web resource category name"},
				nil,
			),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.UpdateWebResourceV1Context(context.TODO(), test.oldName, &test.patch, true)

		ts.Nil(got, "Must return nil result")

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestWebResourceV1Suite) TestUpdateETagError() {
	var tests = []struct {
		oldName storage.ResourceNameT

		patch storage.WebResourceV1

		expectedError error
	}{
		{
			oldName: "web resource 1 cat 1",
			patch: storage.WebResourceV1{
				Metadata: storage.MetadataV1{
					ETag: 111,
				},
			},
			expectedError: storage.NewErrETagDoesNotMatch(1, 111),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.UpdateWebResourceV1Context(context.TODO(), test.oldName, &test.patch, true)

		ts.Nil(got, "Must return nil result")

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestWebResourceV1Suite) TestUpdateConflict() {
	var tests = []struct {
		oldName storage.ResourceNameT

		patch storage.WebResourceV1

		expectedError error
	}{
		{
			oldName: "web resource 1 cat 1",
			patch: storage.WebResourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.WebResourceKind,
					Name:       "web resource 2 cat 2", // Name conflict.
				},
				Data: storage.WebResourceDataV1{
					Description: "new web resource description",
					Domains: []storage.WebResourceDomainT{
						"domain-1.com",
						"domain-2.com",
					},
					IPs: []net.IP{
						net.ParseIP("10.8.200.1"),
						net.ParseIP("10.8.200.2"),
					},
					URLs: []url.URL{
						parseURLPanic("http://domain-1.com/path-1"),
					},
					WebResourceCategoryNames: []storage.ResourceNameT{"category 2"},
				},
			},
			expectedError: storage.NewErrResourceAlreadyExists([]interface{}{
				storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.WebResourceKind,
					Name:       "web resource 2 cat 2", // Name conflict.
				},
			}),
		},
		{
			oldName: "web resource 1 cat 1",
			patch: storage.WebResourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.WebResourceKind,
					Name:       "updated web resource 1 cat 1",
				},
				Data: storage.WebResourceDataV1{
					Description: "new web resource description",
					Domains: []storage.WebResourceDomainT{
						"domain-1.com",
						"domain-2.com",
						"example-1-wr-2.com", // Domain Conflict.
					},
					IPs: []net.IP{
						net.ParseIP("10.8.200.1"),
						net.ParseIP("10.8.200.2"),
					},
					URLs: []url.URL{
						parseURLPanic("http://domain-1.com/path-1"),
					},
					WebResourceCategoryNames: []storage.ResourceNameT{"category 2"},
				},
			},
			expectedError: storage.NewErrResourceAlreadyExists([]interface{}{
				fmt.Sprintf("WebResourceDomains: %s", []string{
					"domain-1.com",
					"domain-2.com",
					"example-1-wr-2.com",
				}),
			}),
		},
		{
			oldName: "web resource 1 cat 1",
			patch: storage.WebResourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.WebResourceKind,
					Name:       "updated web resource 1 cat 1",
				},
				Data: storage.WebResourceDataV1{
					Description: "new web resource description",
					Domains: []storage.WebResourceDomainT{
						"domain-1.com",
						"domain-2.com",
					},
					IPs: []net.IP{
						net.ParseIP("10.8.100.3"), // IP conflict.
						net.ParseIP("10.8.200.2"),
					},
					URLs: []url.URL{
						parseURLPanic("http://domain-1.com/path-1"),
					},
					WebResourceCategoryNames: []storage.ResourceNameT{"category 2"},
				},
			},
			expectedError: storage.NewErrResourceAlreadyExists([]interface{}{
				fmt.Sprintf("WebResourceIPs: %s", []string{
					"10.8.100.3", "10.8.200.2",
				}),
			}),
		},
		{
			oldName: "web resource 1 cat 1",
			patch: storage.WebResourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.WebResourceKind,
					Name:       "updated web resource 1 cat 1",
				},
				Data: storage.WebResourceDataV1{
					Description: "new web resource description",
					Domains: []storage.WebResourceDomainT{
						"domain-1.com",
						"domain-2.com",
					},
					IPs: []net.IP{
						net.ParseIP("10.8.200.2"),
					},
					URLs: []url.URL{
						parseURLPanic("http://domain-1.com/path-1"),
						parseURLPanic("http://example-2-web-resource-2.com"), // URL conflict.
					},
					WebResourceCategoryNames: []storage.ResourceNameT{"category 2"},
				},
			},
			expectedError: storage.NewErrResourceAlreadyExists([]interface{}{
				fmt.Sprintf("WebResourceURLs: %s", []string{
					"http://domain-1.com/path-1",
					"http://example-2-web-resource-2.com",
				}),
			}),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.UpdateWebResourceV1Context(context.TODO(), test.oldName, &test.patch, true)

		ts.Nil(got, "Must return nil result")

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestWebResourceV1Suite) TestDeleteSuccess() {
	var tests = []struct {
		resource storage.WebResourceV1
	}{
		{
			resource: storage.WebResourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Name:       "web resource 1 cat 1",
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		// Get The Resource ID for further checks.
		var webResourceID int64

		if err := ts.db.Model((*WebResource)(nil)).
			Where("web_resource.name = ?", test.resource.Metadata.Name).Column("id").
			Select(&webResourceID); err != nil {
			ts.FailNow("Can not get a Web Resource ID before deleting", err)
		}

		gotErr := ts.sqlStorage.DeleteWebResourceV1Context(context.TODO(), &test.resource)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Check that the Resource was deleted.
		isExists, err := ts.db.Model((*WebResource)(nil)).Where("web_resource.name = ?", test.resource.Metadata.Name).Exists()
		if err != nil {
			ts.FailNow("Can not check if a Web Resource exists", err)
		}

		ts.False(isExists, "isExists Web Resource check must return false")

		// Check Related.
		IPCount, err := ts.db.Model((*WebResourceIP)(nil)).Where("web_resource_id = ?", webResourceID).Count()
		if err != nil {
			ts.FailNow("Can not get related WebResourceIP items count", err)
		}

		ts.Equal(0, IPCount, "Returned WebResourceIP count must be 0")

		DomainCount, err := ts.db.Model((*WebResourceDomain)(nil)).Where("web_resource_id = ?", webResourceID).Count()
		if err != nil {
			ts.FailNow("Can not get related WebResourceDomain items count", err)
		}

		ts.Equal(0, DomainCount, "Returned WebResourceDomain count must be 0")

		URLCount, err := ts.db.Model((*WebResourceURL)(nil)).Where("web_resource_id = ?", webResourceID).Count()
		if err != nil {
			ts.FailNow("Can not get related WebResourceURL items count", err)
		}

		ts.Equal(0, URLCount, "Returned WebResourceURL count must be 0")
	}
}

func (ts *TestWebResourceV1Suite) TestDeleteETagError() {
	var tests = []struct {
		resource storage.WebResourceV1

		expectedError error
	}{
		{
			resource: storage.WebResourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       100, // Incorrect.
					Name:       "web resource 1 cat 1",
				},
			},
			expectedError: storage.NewErrETagDoesNotMatch(1, 100),
		},
	}

	for i := range tests {
		test := &tests[i]

		// Get The Resource ID for further checks.
		var webResourceID int64

		if err := ts.db.Model((*WebResource)(nil)).
			Where("web_resource.name = ?", test.resource.Metadata.Name).Column("id").
			Select(&webResourceID); err != nil {
			ts.FailNow("Can not get a Web Resource ID before deleting", err)
		}

		gotErr := ts.sqlStorage.DeleteWebResourceV1Context(context.TODO(), &test.resource)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		// Check Error.
		ts.Equal(test.expectedError, gotErr, "Unexpected error value")

		// Check that the Resource was deleted.
		isExists, err := ts.db.Model((*WebResource)(nil)).Where("web_resource.name = ?", test.resource.Metadata.Name).Exists()
		if err != nil {
			ts.FailNow("Can not check if a Web Resource exists", err)
		}

		ts.True(isExists, "isExists Web Resource check must return true")

		// Check Related.
		IPCount, err := ts.db.Model((*WebResourceIP)(nil)).Where("web_resource_id = ?", webResourceID).Count()
		if err != nil {
			ts.FailNow("Can not get related WebResourceIP items count", err)
		}

		ts.NotEqual(0, IPCount, "Returned WebResourceIP count must not be 0")

		DomainCount, err := ts.db.Model((*WebResourceDomain)(nil)).Where("web_resource_id = ?", webResourceID).Count()
		if err != nil {
			ts.FailNow("Can not get related WebResourceDomain items count", err)
		}

		ts.NotEqual(0, DomainCount, "Returned WebResourceDomain count must not be 0")

		URLCount, err := ts.db.Model((*WebResourceURL)(nil)).Where("web_resource_id = ?", webResourceID).Count()
		if err != nil {
			ts.FailNow("Can not get related WebResourceURL items count", err)
		}

		ts.NotEqual(0, URLCount, "Returned WebResourceURL count must not be 0")
	}
}

func (ts *TestWebResourceV1Suite) TestDeleteNotFoundError() {
	var tests = []struct {
		resource storage.WebResourceV1

		expectedError error
	}{
		{
			resource: storage.WebResourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       100,
					Name:       "not found web resource 1 cat 1", // Not Found.
					Kind:       storage.WebResourceKind,
				},
			},
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       100,
					Kind:       storage.WebResourceKind,
					Name:       "not found web resource 1 cat 1",
				},
			),
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.sqlStorage.DeleteWebResourceV1Context(context.TODO(), &test.resource)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		// Check Error.
		ts.Equal(test.expectedError, gotErr, "Unexpected error value")

		// Check that the Resource was not deleted.
		isExists, err := ts.db.Model((*WebResource)(nil)).Where("web_resource.name = ?", test.resource.Metadata.Name).Exists()
		if err != nil {
			ts.FailNow("Can not check if a Web Resource exists", err)
		}

		ts.False(isExists, "isExists Web Resource check must return false")
	}
}

func TestWebResourceV1(t *testing.T) {
	suite.Run(t, &TestWebResourceV1Suite{})
}

func benchmarkPrepareEqualDomains(count int, b *testing.B) ([]storage.WebResourceDomainT, WebResourceDomainSlice) {
	// Prepare data.
	var (
		domains   = make([]storage.WebResourceDomainT, count)
		dbDomains = make(WebResourceDomainSlice, count)
	)

	for dmInd := 0; dmInd < count; dmInd++ {
		var data = make([]byte, 500)

		if _, err := rand.Read(data); err != nil {
			b.Fatalf("Can not read random bytes: %s", err)
		}

		var domainStr = storage.WebResourceDomainT(data)

		domains[dmInd] = domainStr
		dbDomains[dmInd] = &WebResourceDomain{
			Domain: domainStr,
		}
	}

	return domains, dbDomains
}

func BenchmarkCompareDomainSlicesEqual5(b *testing.B) {
	const domainsCount = 5 // I personally do not expect far more than 5 domains per resource.

	domains, dbDomains := benchmarkPrepareEqualDomains(domainsCount, b)

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		isUpdate, add, del := computeDomainSlicesUpdate(101, dbDomains, domains)

		if isUpdate {
			b.Fatalf("computeDomainSlicesUpdate isUpdate must return false because of deliberately equal data")
		}

		if add != nil {
			b.Fatalf("computeDomainSlicesUpdate must return nil add because of deliberately equal data")
		}

		if del != nil {
			b.Fatalf("computeDomainSlicesUpdate must return nil delete because of deliberately equal data")
		}
	}
}

func BenchmarkCompareDomainSlicesEqual10(b *testing.B) {
	const domainsCount = 10

	domains, dbDomains := benchmarkPrepareEqualDomains(domainsCount, b)

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		isUpdate, add, del := computeDomainSlicesUpdate(101, dbDomains, domains)

		if isUpdate {
			b.Fatalf("computeDomainSlicesUpdate isUpdate must return false because of deliberately equal data")
		}

		if add != nil {
			b.Fatalf("computeDomainSlicesUpdate must return nil add because of deliberately equal data")
		}

		if del != nil {
			b.Fatalf("computeDomainSlicesUpdate must return nil delete because of deliberately equal data")
		}
	}
}

func BenchmarkCompareDomainSlicesEqual15(b *testing.B) {
	const domainsCount = 15

	domains, dbDomains := benchmarkPrepareEqualDomains(domainsCount, b)

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		isUpdate, add, del := computeDomainSlicesUpdate(101, dbDomains, domains)

		if isUpdate {
			b.Fatalf("computeDomainSlicesUpdate isUpdate must return false because of deliberately equal data")
		}

		if add != nil {
			b.Fatalf("computeDomainSlicesUpdate must return nil add because of deliberately equal data")
		}

		if del != nil {
			b.Fatalf("computeDomainSlicesUpdate must return nil delete because of deliberately equal data")
		}
	}
}

func BenchmarkCompareDomainSlicesEqual20(b *testing.B) {
	const domainsCount = 20

	domains, dbDomains := benchmarkPrepareEqualDomains(domainsCount, b)

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		isUpdate, add, del := computeDomainSlicesUpdate(101, dbDomains, domains)

		if isUpdate {
			b.Fatalf("computeDomainSlicesUpdate isUpdate must return false because of deliberately equal data")
		}

		if add != nil {
			b.Fatalf("computeDomainSlicesUpdate must return nil add because of deliberately equal data")
		}

		if del != nil {
			b.Fatalf("computeDomainSlicesUpdate must return nil delete because of deliberately equal data")
		}
	}
}

func benchmarkPrepareUpdateAllDomains(count int, b *testing.B) ([]storage.WebResourceDomainT, WebResourceDomainSlice) {
	// Prepare data.
	var (
		domains   = make([]storage.WebResourceDomainT, count)
		dbDomains = make(WebResourceDomainSlice, count)
	)

	for dmInd := 0; dmInd < count; dmInd++ {
		var data = make([]byte, 500)

		if _, err := rand.Read(data); err != nil {
			b.Fatalf("Can not read random bytes: %s", err)
		}

		var domainStr = storage.WebResourceDomainT(data)

		domains[dmInd] = domainStr

		if _, err := rand.Read(data); err != nil {
			b.Fatalf("Can not read random bytes: %s", err)
		}

		domainStr = storage.WebResourceDomainT(data)

		dbDomains[dmInd] = &WebResourceDomain{
			Domain: domainStr,
		}
	}

	return domains, dbDomains
}

func BenchmarkCompareDomainSlicesInequal5(b *testing.B) {
	const domainsCount = 5 // I personally do not expect far more than 5 domains per resource.

	domains, dbDomains := benchmarkPrepareUpdateAllDomains(domainsCount, b)

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		isUpdate, add, del := computeDomainSlicesUpdate(101, dbDomains, domains)

		if !isUpdate {
			b.Fatalf("computeDomainSlicesUpdate isUpdate must return true because of deliberately unequal data")
		}

		if add == nil {
			b.Fatalf("computeDomainSlicesUpdate must not return nil add because of deliberately unequal data")
		}

		if del == nil {
			b.Fatalf("computeDomainSlicesUpdate must return nil delete because of deliberately unequal data")
		}
	}
}

func BenchmarkCompareDomainSlicesInequal10(b *testing.B) {
	const domainsCount = 10

	domains, dbDomains := benchmarkPrepareUpdateAllDomains(domainsCount, b)

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		isUpdate, add, del := computeDomainSlicesUpdate(101, dbDomains, domains)

		if !isUpdate {
			b.Fatalf("computeDomainSlicesUpdate isUpdate must return true because of deliberately unequal data")
		}

		if add == nil {
			b.Fatalf("computeDomainSlicesUpdate must not return nil add because of deliberately unequal data")
		}

		if del == nil {
			b.Fatalf("computeDomainSlicesUpdate must return nil delete because of deliberately unequal data")
		}
	}
}

func BenchmarkCompareDomainSlicesInequal15(b *testing.B) {
	const domainsCount = 15

	domains, dbDomains := benchmarkPrepareUpdateAllDomains(domainsCount, b)

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		isUpdate, add, del := computeDomainSlicesUpdate(101, dbDomains, domains)

		if !isUpdate {
			b.Fatalf("computeDomainSlicesUpdate isUpdate must return true because of deliberately unequal data")
		}

		if add == nil {
			b.Fatalf("computeDomainSlicesUpdate must not return nil add because of deliberately unequal data")
		}

		if del == nil {
			b.Fatalf("computeDomainSlicesUpdate must return nil delete because of deliberately unequal data")
		}
	}
}

func BenchmarkCompareDomainSlicesInequal20(b *testing.B) {
	const domainsCount = 20

	domains, dbDomains := benchmarkPrepareUpdateAllDomains(domainsCount, b)

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		isUpdate, add, del := computeDomainSlicesUpdate(101, dbDomains, domains)

		if !isUpdate {
			b.Fatalf("computeDomainSlicesUpdate isUpdate must return true because of deliberately unequal data")
		}

		if add == nil {
			b.Fatalf("computeDomainSlicesUpdate must not return nil add because of deliberately unequal data")
		}

		if del == nil {
			b.Fatalf("computeDomainSlicesUpdate must return nil delete because of deliberately unequal data")
		}
	}
}

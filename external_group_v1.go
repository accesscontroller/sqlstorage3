package sqlstorage3

import (
	"context"
	"errors"
	"time"

	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

// GetExternalGroupV1Context loads and returns one ExternalGroupV1
// by its name and ExternalUsersGroupsSourceV1 name.
func (s *SQLStorage) GetExternalGroupV1Context(ctx context.Context, name,
	sourceName storage.ResourceNameT) (*storage.ExternalGroupV1, error) {
	var model ExternalGroup

	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		var q = tx.Model(&model)

		// Join.
		q = q.Relation("AccessGroup").Relation("ExternalUsersGroupsSource")

		// Filter.
		q = q.Where("external_users_groups_source.name = ? AND external_group.name = ?",
			sourceName, name)

		if err := q.Select(); err != nil {
			if errors.Is(err, pg.ErrNoRows) {
				return storage.NewErrResourceNotFound(
					&storage.NewExternalGroupV1(name, sourceName).Metadata)
			}

			return err
		}

		return nil
	}); err != nil {
		return nil, err
	}

	return model.toStorageExternalGroup(), nil
}

func storageExternalGroupToModel(g *storage.ExternalGroupV1, sourceID, accessGroupID int64) *ExternalGroup {
	return &ExternalGroup{
		AccessGroupID:               accessGroupID,
		ETag:                        g.Metadata.ETag,
		ExternalUsersGroupsSourceID: sourceID,
		Name:                        g.Metadata.Name,
	}
}

// CreateExternalGroupV1Context creates one ExternalGroupV1.
func (s *SQLStorage) CreateExternalGroupV1Context(ctx context.Context, //nolint:dupl
	group *storage.ExternalGroupV1, getCreated bool) (*storage.ExternalGroupV1, error) {
	var model *ExternalGroup

	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		md, err := createExternalGroupV1Tx(tx, group)
		if err != nil {
			return err
		}

		model = md

		return nil
	}); err != nil {
		return nil, err
	}

	if getCreated {
		return model.toStorageExternalGroup(), nil
	}

	return nil, nil
}

func createExternalGroupV1Tx(tx *pg.Tx, group *storage.ExternalGroupV1) (*ExternalGroup, error) {
	// Load Access Group ID.
	var (
		accessGroupID   int64
		accessGroupName storage.ResourceNameT
	)

	if len(group.Data.AccessGroupName) > 0 {
		if err := tx.Model((*AccessGroup)(nil)).Column("id", "name").
			Where("name = ?", group.Data.AccessGroupName).
			Select(&accessGroupID, &accessGroupName); err != nil {
			if errors.Is(err, pg.ErrNoRows) {
				return nil, storage.NewErrResourceNotFound(
					&storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       0,
						Kind:       storage.AccessGroupKind,
						Name:       group.Data.AccessGroupName,
					},
				)
			}

			return nil, err
		}
	}

	// Load External Source ID.
	var (
		externalSourceID   int64
		externalSourceName storage.ResourceNameT
	)

	if err := tx.Model((*ExternalUsersGroupsSource)(nil)).Column("id", "name").
		Where("name = ?", group.Metadata.ExternalSource.SourceName).
		Select(&externalSourceID, &externalSourceName); err != nil {
		if errors.Is(err, pg.ErrNoRows) {
			return nil, storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       0,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       group.Metadata.ExternalSource.SourceName,
				},
			)
		}

		return nil, err
	}

	// Try to INSERT.
	var model = storageExternalGroupToModel(group, externalSourceID, accessGroupID)

	if _, err := tx.Model(model).Insert(); err != nil {
		if typedErr, ok := err.(pg.Error); ok {
			if typedErr.IntegrityViolation() {
				return nil, storage.NewErrResourceAlreadyExists([]interface{}{*group}, typedErr)
			}
		}

		return nil, err
	}

	// Set names.
	model.AccessGroup.Name = accessGroupName
	model.ExternalUsersGroupsSource.Name = externalSourceName

	return model, nil
}

func updateExternalGroupV1Tx(tx *pg.Tx,
	oldName storage.ResourceNameT, group *storage.ExternalGroupV1) (*ExternalGroup, error) {
	// Load an old External group from DB.
	var dbGroup ExternalGroup

	if err := tx.Model(&dbGroup).
		Where("external_group.name = ? AND external_users_groups_source.name = ?",
			oldName, group.Metadata.ExternalSource.SourceName).
		Relation("AccessGroup").Relation("ExternalUsersGroupsSource").Select(); err != nil {
		if errors.Is(err, pg.ErrNoRows) {
			return nil, storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: group.Metadata.ExternalSource.SourceName,
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: oldName,
				},
			)
		}

		return nil, err
	}

	// Check ETag.
	if group.Metadata.ETag != dbGroup.ETag {
		return nil, storage.NewErrETagDoesNotMatch(dbGroup.ETag, group.Metadata.ETag)
	}

	// Check if a patch contains differences in AccessGroup
	// and Update a dbModel if required.
	if dbGroup.AccessGroup.Name != group.Data.AccessGroupName {
		if len(group.Data.AccessGroupName) == 0 {
			// Set NULL.
			dbGroup.AccessGroupID = 0
			dbGroup.AccessGroup = AccessGroup{}
		} else {
			// Load.
			var accessGroupID int64

			if err := tx.Model((*AccessGroup)(nil)).Where("name = ?", group.Data.AccessGroupName).
				Column("id").Select(&accessGroupID); err != nil {
				if errors.Is(err, pg.ErrNoRows) {
					return nil, storage.NewErrResourceNotFound(
						&storage.MetadataV1{
							APIVersion: storage.APIVersionV1Value,
							Kind:       storage.AccessGroupKind,
							Name:       group.Data.AccessGroupName,
						},
					)
				}

				return nil, err
			}

			// Set values for correct return.
			dbGroup.AccessGroupID = accessGroupID
			dbGroup.AccessGroup.Name = group.Data.AccessGroupName
		}
	}

	// Set values.
	dbGroup.ETag++
	dbGroup.Name = group.Metadata.Name
	dbGroup.UpdatedAt = time.Now()

	// Update.

	if _, err := tx.Model(&dbGroup).WherePK().Update(); err != nil {
		if typedE, ok := err.(pg.Error); ok {
			if typedE.IntegrityViolation() {
				return nil, storage.NewErrResourceAlreadyExists([]interface{}{*group}, typedE)
			}
		}

		return nil, err
	}

	return &dbGroup, nil
}

// UpdateExternalGroupV1Context updates one given ExternalGroupV1;
// Lookup is performed using oldName and ExternalUsersGroupsSourceV1
// from group.
func (s *SQLStorage) UpdateExternalGroupV1Context(ctx context.Context,
	oldName storage.ResourceNameT, group *storage.ExternalGroupV1,
	getResult bool) (*storage.ExternalGroupV1, error) {
	var model *ExternalGroup

	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		m, err := updateExternalGroupV1Tx(tx, oldName, group)
		if err != nil {
			return err
		}

		model = m

		return nil
	}); err != nil {
		return nil, err
	}

	if getResult {
		return model.toStorageExternalGroup(), nil
	}

	return nil, nil
}

func deleteExternalGroupV1Tx(tx *pg.Tx, group *storage.ExternalGroupV1) error {
	var model ExternalGroup

	// Load.
	if err := tx.Model(&model).Relation("ExternalUsersGroupsSource").
		Where("external_group.name = ? AND external_users_groups_source.name = ?",
			group.Metadata.Name, group.Metadata.ExternalSource.SourceName).Select(); err != nil {
		if errors.Is(err, pg.ErrNoRows) {
			return storage.NewErrResourceNotFound(&group.Metadata)
		}

		return err
	}

	// Check ETag.
	if model.ETag != group.Metadata.ETag {
		return storage.NewErrETagDoesNotMatch(model.ETag, group.Metadata.ETag)
	}

	// Update related ExternalUser ETags and UpdatedAt.
	// Load Relations.
	var relations []*ExternalUserToExternalGroup

	if err := tx.Model(&relations).Relation("ExternalUser",
		func(q *orm.Query) (*orm.Query, error) {
			return q.Column("external_user_id"), nil
		}).Where("external_group_id = ?", model.ID).Select(); err != nil {
		return err
	}

	if len(relations) > 0 {
		// Load users.
		var userIDs = make([]int64, len(relations))

		for i, rel := range relations {
			userIDs[i] = rel.ExternalUserID
		}

		var users ExternalUserSlice

		if err := tx.Model(&users).Column("external_user.id", "external_user.e_tag").
			WhereIn("external_user.id IN (?)", userIDs).Select(); err != nil {
			return err
		}

		// Update.
		var curTime = time.Now()

		for _, u := range users {
			u.ETag++
			u.UpdatedAt = curTime
		}

		if _, err := tx.Model(&users).Column("e_tag", "updated_at").Update(); err != nil {
			return err
		}
	}

	// Delete.
	_, err := tx.Model(&model).WherePK().Delete()

	return err
}

// DeleteExternalGroupV1Context deletes one ExternalGroupV1.
func (s *SQLStorage) DeleteExternalGroupV1Context(ctx context.Context,
	group *storage.ExternalGroupV1) error {
	return s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		return deleteExternalGroupV1Tx(tx, group)
	})
}

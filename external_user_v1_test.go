package sqlstorage3

import (
	"context"
	"testing"
	"time"

	"github.com/go-pg/pg/v10"
	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

type TestExternalUserV1Suite struct {
	db *pg.DB

	sqlStorage *SQLStorage

	suite.Suite

	openTimes, closeTimes map[storage.ResourceNameT]*time.Time
}

func (ts *TestExternalUserV1Suite) SetupTest() {
	ts.openTimes = make(map[storage.ResourceNameT]*time.Time)
	ts.closeTimes = make(map[storage.ResourceNameT]*time.Time)

	db, err := connectDB()
	if err != nil {
		ts.FailNow("Can not connect to test DB", err)
	}

	ts.db = db
	ts.sqlStorage = &SQLStorage{db: db}

	// Create DB Tables.
	if err := recreateTables(ts.db); err != nil {
		ts.FailNow("Can not initialize DB", err)
	}

	// Create some init data.

	// ExternalUsersGroupsSource.
	var externalUsersGroupsSources = []ExternalUsersGroupsSource{
		{
			ETag:              1,
			GetMode:           storage.ExternalUsersGroupsGetModePassive,
			GroupsGetSettings: "get groups settings",
			GroupsToLoad:      []string{"group1", "group2"},
			UsersGetSettings:  "get users settings",
			Type:              storage.ExternalUsersGroupsSourceTypeLDAP,
			Name:              "source 1",
			PollInterval:      121,
		},
		{
			ETag:              1,
			GetMode:           storage.ExternalUsersGroupsGetModePassive,
			GroupsGetSettings: "get groups settings",
			GroupsToLoad:      []string{"group1", "group2"},
			UsersGetSettings:  "get users settings",
			Type:              storage.ExternalUsersGroupsSourceTypeLDAP,
			Name:              "source 2",
			PollInterval:      122,
		},
	}

	for i := range externalUsersGroupsSources {
		if _, err := db.Model(&externalUsersGroupsSources[i]).Insert(); err != nil {
			ts.FailNow("Can not create init data", err.Error())
		}
	}

	// Session Sources.
	var sessionSources = []ExternalSessionsSource{
		{
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[0].ID,
			ETag:                        1,
			GetMode:                     storage.ExternalSessionsGetModePassive,
			Name:                        "sessions source 1",
			Settings:                    []byte(`{"Name": "source 1", "Key1": "Value1", "Key2": "Value2"}`),
			Type:                        storage.ExternalSessionsSourceTypeMSADEvents,
		},
		{
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[1].ID,
			ETag:                        2,
			GetMode:                     storage.ExternalSessionsGetModePoll,
			Name:                        "sessions source 2",
			Settings:                    []byte(`{"Name": "source 2", "Key2": "Value2", "Key3": "Value3"}`),
			Type:                        storage.ExternalSessionsSourceTypeMSADEvents,
		},
	}

	for i := range sessionSources {
		if _, err := db.Model(&sessionSources[i]).Insert(); err != nil {
			ts.FailNow("Can not create init data", err.Error())
		}
	}

	// External Users.
	var externalUsers = []ExternalUser{
		{
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[0].ID,
			ETag:                        1,
			Name:                        "user 1 at source 1",
		},
		{
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[0].ID,
			ETag:                        2,
			Name:                        "user 2 at source 1",
		},
		{
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[1].ID,
			ETag:                        3,
			Name:                        "user 1 at source 2",
		},
		{
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[1].ID,
			ETag:                        4,
			Name:                        "user 2 at source 2",
		},
	}

	for i := range externalUsers {
		if _, err := db.Model(&externalUsers[i]).Insert(); err != nil {
			ts.FailNow("Can not insert ExternalUser", err)
		}
	}

	// External Groups.
	var externalGroups = ExternalGroupSlice{
		{
			ID:                          1,
			ETag:                        1,
			Name:                        "group 1 at source 1",
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[0].ID,
		},
	}

	for _, g := range externalGroups {
		if _, err := db.Model(g).Insert(); err != nil {
			ts.FailNow("Can not insert ExternalGroup", err)
		}
	}

	// Make External Group to External User Relations.
	var user2Groups = []*ExternalUserToExternalGroup{
		{
			ExternalGroupID: 1,
			ExternalUserID:  1,
			CreatedAt:       time.Now(),
		},
	}

	for _, rel := range user2Groups {
		if _, err := db.Model(rel).Insert(); err != nil {
			ts.FailNow("Can not insert ExternalUserToExternalGroup", err)
		}
	}
}

func (ts *TestExternalUserV1Suite) TearDownTest() {
	if ts.db != nil {
		if err := ts.db.Close(); err != nil {
			ts.FailNow("Can not Close DB", err)
		}
	}
}

func (ts *TestExternalUserV1Suite) TestGetSuccess() {
	var tests = []struct {
		source storage.ResourceNameT
		name   storage.ResourceNameT

		expectedUser storage.ExternalUserV1
	}{
		{
			source: "source 1",
			name:   "user 1 at source 1",
			expectedUser: storage.ExternalUserV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "user 1 at source 1",
				},
				Data: storage.ExternalUserDataV1{},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.GetExternalUserV1Context(context.TODO(),
			test.name, test.source)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		ts.Equal(test.expectedUser, *got, "Unexpected result value")
	}
}

func (ts *TestExternalUserV1Suite) TestGetNotFound() {
	var tests = []struct {
		source storage.ResourceNameT
		name   storage.ResourceNameT

		expectedError error
	}{
		{
			source: "source 1",
			name:   "non existing user at source 1",
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             0,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "non existing user at source 1",
				},
			),
		},
		{
			source: "incorrect source 1",
			name:   "user 1 at source 1",
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             0,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "incorrect source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "user 1 at source 1",
				},
			),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.GetExternalUserV1Context(context.TODO(),
			test.name, test.source)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		if !ts.Nil(got, "Must return nil result") {
			ts.FailNow("Got not nil result - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error result value")
	}
}

func (ts *TestExternalUserV1Suite) TestCreateSuccess() {
	var tests = []struct {
		getCreated bool

		user storage.ExternalUserV1
	}{
		{
			getCreated: false,
			user: storage.ExternalUserV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "user 5 at source 1",
				},
				Data: storage.ExternalUserDataV1{},
			},
		},
		{
			getCreated: true,
			user: storage.ExternalUserV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             10,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 2",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "user 5 at source 2",
				},
				Data: storage.ExternalUserDataV1{},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.CreateExternalUserV1Context(context.TODO(),
			&test.user, test.getCreated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error, can not continue")
		}

		// Check created User.
		var dbUser ExternalUser

		if err := ts.db.Model(&dbUser).Relation("ExternalUsersGroupsSource").
			Where("external_users_groups_source.name = ? AND external_user.name = ?",
				test.user.Metadata.ExternalSource.SourceName, test.user.Metadata.Name).Select(); err != nil {
			ts.FailNow("Can not get a User from DB", err)
		}

		// Compare.
		ts.Equal(test.user.Metadata.ETag, dbUser.ETag)
		ts.Equal(test.user.Metadata.ExternalSource.SourceName, dbUser.ExternalUsersGroupsSource.Name)
		ts.Equal(test.user.Metadata.Name, dbUser.Name)
		ts.True(test.user.Metadata.ExternalResource)
		ts.WithinDuration(time.Now(), dbUser.CreatedAt, time.Minute)

		// If returned value.
		if !test.getCreated {
			continue
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("nil result - can not continue")
		}

		ts.Equal(test.user, *got, "Unexpected value")
	}
}

func (ts *TestExternalUserV1Suite) TestCreateConflict() {
	var tests = []struct {
		user storage.ExternalUserV1

		expectedError error
	}{
		{
			user: storage.ExternalUserV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             10,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 2",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "user 1 at source 2", // Name conflict.
				},
				Data: storage.ExternalUserDataV1{},
			},
			expectedError: storage.NewErrResourceAlreadyExists([]interface{}{
				storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             10,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 2",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "user 1 at source 2", // Name conflict.
				},
			}),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.CreateExternalUserV1Context(context.TODO(),
			&test.user, true)

		ts.Nil(got, "Must return nil result")

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("No error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalUserV1Suite) TestCreateSourceNotFound() {
	var tests = []struct {
		user storage.ExternalUserV1

		expectedError error
	}{
		{
			user: storage.ExternalUserV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             10,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "non existing source 2", // Not such a source.
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "user 100 at source 2",
				},
				Data: storage.ExternalUserDataV1{},
			},
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Name:       "non existing source 2",
					Kind:       storage.ExternalUsersGroupsSourceKind,
				},
			),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.CreateExternalUserV1Context(context.TODO(),
			&test.user, true)

		ts.Nil(got, "Must return nil result")

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("No error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalUserV1Suite) TestUpdateSuccess() {
	var tests = []struct {
		getResult bool
		oldName   storage.ResourceNameT

		patch storage.ExternalUserV1
	}{
		{
			oldName: "user 1 at source 1",
			patch: storage.ExternalUserV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "renamed user 1 at source 1",
				},
			},
		},
		{
			getResult: true,
			oldName:   "user 1 at source 2",
			patch: storage.ExternalUserV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             3,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 2",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "renamed user 1 at source 2",
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.UpdateExternalUserV1Context(context.TODO(),
			test.oldName, &test.patch, test.getResult)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Check DB.
		var dbUser ExternalUser

		if err := ts.db.Model(&dbUser).Relation("ExternalUsersGroupsSource").
			Where("external_users_groups_source.name = ? AND external_user.name = ?",
				test.patch.Metadata.ExternalSource.SourceName, test.patch.Metadata.Name).Select(); err != nil {
			ts.FailNow("Can not get an Updated User from DB", err)
		}

		// Compare.
		test.patch.Metadata.ETag++ // To account the changed ETag.
		ts.Equal(test.patch.Metadata.ETag, dbUser.ETag)
		ts.Equal(test.patch.Metadata.ExternalSource.SourceName, dbUser.ExternalUsersGroupsSource.Name)
		ts.Equal(test.patch.Metadata.Name, dbUser.Name)
		ts.WithinDuration(time.Now(), dbUser.UpdatedAt, time.Minute)

		if !test.getResult {
			continue
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		ts.Equal(test.patch, *got, "Unexpected result value")
	}
}

func (ts *TestExternalUserV1Suite) TestUpdateETagError() {
	var tests = []struct {
		oldName storage.ResourceNameT

		patch storage.ExternalUserV1

		expectedError error
	}{
		{
			oldName: "user 1 at source 1",
			patch: storage.ExternalUserV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             101,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "renamed user 1 at source 1",
				},
			},
			expectedError: storage.NewErrETagDoesNotMatch(1, 101),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.UpdateExternalUserV1Context(context.TODO(),
			test.oldName, &test.patch, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		// Compare error.
		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalUserV1Suite) TestUpdateNotFound() {
	var tests = []struct {
		oldName storage.ResourceNameT

		patch storage.ExternalUserV1

		expectedError error
	}{
		{
			oldName: "not found user 1 at source 1",
			patch: storage.ExternalUserV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             101,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "renamed user 1 at source 1",
				},
			},
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "not found user 1 at source 1",
				},
			),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.UpdateExternalUserV1Context(context.TODO(),
			test.oldName, &test.patch, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		// Compare error.
		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalUserV1Suite) TestUpdateConflict() {
	var tests = []struct {
		oldName storage.ResourceNameT

		patch storage.ExternalUserV1

		expectedError error
	}{
		{
			oldName: "user 1 at source 1",
			patch: storage.ExternalUserV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "user 2 at source 1", // New Name conflict.
				},
			},
			expectedError: storage.NewErrResourceAlreadyExists([]interface{}{
				storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "user 2 at source 1", // New Name conflict.
				},
			}),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.UpdateExternalUserV1Context(context.TODO(),
			test.oldName, &test.patch, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		// Compare error.
		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalUserV1Suite) TestDeleteSuccess() {
	var tests = []struct {
		user storage.ExternalUserV1
	}{
		{
			user: storage.ExternalUserV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "user 1 at source 1",
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.sqlStorage.DeleteExternalUserV1Context(context.TODO(), &test.user)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Check that the user is deleted.
		exists, err := ts.db.Model((*ExternalUser)(nil)).
			Relation("ExternalUsersGroupsSource").
			Where("external_users_groups_source.name = ? AND external_user.name = ?",
				test.user.Metadata.ExternalSource.SourceName, test.user.Metadata.Name).Exists()
		if err != nil {
			ts.FailNow("Can not check a User existence in DB", err)
		}

		ts.False(exists, "Must return Exists == false")
	}
}

func (ts *TestExternalUserV1Suite) TestDeleteSuccessCheckMemberGroupETag() {
	var tests = []struct {
		user               storage.ExternalUserV1
		expectedGroupETags map[storage.ResourceNameT]storage.ETagT
	}{
		{
			user: storage.ExternalUserV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "user 1 at source 1",
				},
			},
			expectedGroupETags: map[storage.ResourceNameT]storage.ETagT{
				"group 1 at source 1": 2,
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.sqlStorage.DeleteExternalUserV1Context(context.TODO(), &test.user)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Load Groups.
		var groupNames = make([]storage.ResourceNameT, 0, len(test.expectedGroupETags))

		for name := range test.expectedGroupETags {
			groupNames = append(groupNames, name)
		}

		var dbGroups ExternalGroupSlice

		if err := ts.db.Model(&dbGroups).Relation("ExternalUsersGroupsSource").
			WhereIn("external_group.name IN (?)", groupNames).
			Column("external_group.name", "external_group.e_tag").
			Where("external_users_groups_source.name = ?", test.user.Metadata.ExternalSource.SourceName).
			Select(); err != nil {
			ts.FailNowf("Can not ExternalGroups for a ETag check",
				"Source: %s, Names: %s, Error: %s", test.user.Metadata.ExternalSource.SourceName, groupNames, err)
		}

		// Check ETags.
		for _, dbGr := range dbGroups {
			ts.Equalf(test.expectedGroupETags[dbGr.Name], dbGr.ETag,
				"Unexpected ExternalGroup ETag value. Group: %s, Source: %s",
				dbGr.Name, test.user.Metadata.ExternalSource.SourceName)
		}
	}
}

func (ts *TestExternalUserV1Suite) TestDeleteNotFound() {
	var tests = []struct {
		user storage.ExternalUserV1

		expectedError error
	}{
		{
			user: storage.ExternalUserV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "not existing user 1 at source 1", // No such a User.
				},
			},
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					ETag: 1,
					Kind: storage.ExternalUserKind,
					Name: "not existing user 1 at source 1",
				},
			),
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.sqlStorage.DeleteExternalUserV1Context(context.TODO(), &test.user)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		// Check that the user is not exists.
		exists, err := ts.db.Model((*ExternalUser)(nil)).
			Relation("ExternalUsersGroupsSource").
			Where("external_users_groups_source.name = ? AND external_user.name = ?",
				test.user.Metadata.ExternalSource.SourceName, test.user.Metadata.Name).Exists()
		if err != nil {
			ts.FailNow("Can not check a User existence in DB", err)
		}

		ts.False(exists, "Must return Exists == false")

		// Check Error.
		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalUserV1Suite) TestDeleteETagIncorrect() {
	var tests = []struct {
		user storage.ExternalUserV1

		expectedError error
	}{
		{
			user: storage.ExternalUserV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             101,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "user 1 at source 1", // No such a User.
				},
			},
			expectedError: storage.NewErrETagDoesNotMatch(1, 101),
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.sqlStorage.DeleteExternalUserV1Context(context.TODO(), &test.user)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		// Check that the user is exists.
		exists, err := ts.db.Model((*ExternalUser)(nil)).
			Relation("ExternalUsersGroupsSource").
			Where("external_users_groups_source.name = ? AND external_user.name = ?",
				test.user.Metadata.ExternalSource.SourceName, test.user.Metadata.Name).Exists()
		if err != nil {
			ts.FailNow("Can not check a User existence in DB", err)
		}

		ts.True(exists, "Must return Exists == True")

		// Check Error.
		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func TestExternalUserV1(t *testing.T) {
	suite.Run(t, &TestExternalUserV1Suite{})
}

package sqlstorage3

import (
	"context"

	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func getWebResourceModelsTx(tx *pg.Tx,
	category *storage.ResourceNameT, filters []storage.WebResourceFilterV1) (WebResourceSlice, error) {
	var models WebResourceSlice

	q := tx.Model(&models)

	q = q.Relation("WebResourceCategorys")
	q = q.Relation("WebResourceIPs")
	q = q.Relation("WebResourceDomains")
	q = q.Relation("WebResourceURLs")

	// q = q.Join("LEFT JOIN web_resource_ips AS web_resource_ip ON web_resource.id = web_resource_ip.web_resource_id")
	// q = q.Join("LEFT JOIN web_resource_domains AS web_resource_domain ON web_resource_domain.web_resource_id = web_resource.id")
	// q = q.Join("LEFT JOIN web_resource_urls AS web_resource_url ON web_resource_url.web_resource_id = web_resource.id")
	// q = q.Join("JOIN web_resource_categories AS web_resource_category ON web_resource_category.id = web_resource.web_resource_category_id")

	for i := range filters {
		fltExpr := &filters[i]

		q = q.WhereOrGroup(func(iq *orm.Query) (*orm.Query, error) {
			if !isZero(fltExpr.NameSubstringMatch) {
				iq = iq.Where("web_resource.name LIKE ?",
					"%"+fltExpr.NameSubstringMatch+"%")
			}

			if !isZero(fltExpr.DescriptionSubstringMatch) {
				iq = iq.Where("web_resource.description LIKE ?",
					"%"+fltExpr.DescriptionSubstringMatch+"%")
			}

			// if !isZero(fltExpr.Domains) {
			// 	iq = iq.WhereIn("web_resource_domain.domain = ?",
			// 		fltExpr.Domains)
			// }

			// if !isZero(fltExpr.IPs) {
			// 	iq = iq.WhereIn("web_resource_IP.IP = ?",
			// 		fltExpr.IPs)
			// }

			// if !isZero(fltExpr.URLs) {
			// 	iq = iq.WhereIn("web_resource_url.url = ?",
			// 		fltExpr.URLs)
			// }

			return iq, nil
		})
	}

	// Filter Category.
	if category != nil && len(*category) != 0 {
		// Load Category ID.
		catID, err := loadWebResourceCategoryIDByNameTx(tx, *category)
		if err != nil {
			return nil, err
		}

		q = q.Where("web_resource_category_id = ?", catID).
			Join("INNER JOIN web_resource_category_to_web_resource ON web_resource_category_to_web_resource.web_resource_id = web_resource.id")
	}

	// Select.
	if err := q.Select(); err != nil {
		return nil, err
	}

	return models, nil
}

func (s *SQLStorage) GetWebResourceV1sContext(ctx context.Context, category *storage.ResourceNameT,
	filters []storage.WebResourceFilterV1) ([]storage.WebResourceV1, error) {
	var models WebResourceSlice

	if err := s.db.RunInTransaction(ctx, func(t *pg.Tx) error {
		ms, err := getWebResourceModelsTx(t, category, filters)
		if err != nil {
			return err
		}

		models = ms

		return nil
	}); err != nil {
		return nil, err
	}

	return models.toStorageWebResources()
}

func (s *SQLStorage) CreateWebResourceV1sContext(ctx context.Context,
	resources []storage.WebResourceV1,
	getCreated bool) ([]storage.WebResourceV1, error) {
	var models WebResourceSlice

	if getCreated {
		models = make(WebResourceSlice, 0, len(resources))
	}

	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		for i := range resources {
			var nRes = &resources[i]

			m, err := createWebResourceV1Tx(tx, nRes)
			if err != nil {
				return err
			}

			if getCreated {
				models = append(models, m)
			}
		}

		return nil
	}); err != nil {
		return nil, err
	}

	if getCreated {
		return models.toStorageWebResources()
	}

	return nil, nil
}

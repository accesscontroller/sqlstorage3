package sqlstorage3

import (
	"context"
	"encoding/json"
	"errors"
	"time"

	"github.com/go-pg/pg/v10"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

// GetExternalSessionsSourceV1Context loads and returns one ExternalSessionsSourceV1.
func (s *SQLStorage) GetExternalSessionsSourceV1Context(
	ctx context.Context, name storage.ResourceNameT) (*storage.ExternalSessionsSourceV1, error) {
	var model = &ExternalSessionsSource{}

	// Load.
	if err := s.db.ModelContext(ctx, model).Relation("ExternalUsersGroupsSource").
		Where("external_sessions_source.name = ?", name).Select(); err != nil {
		if errors.Is(err, pg.ErrNoRows) {
			return nil, storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       0,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       name,
				},
			)
		}

		return nil, err
	}

	conv, err := model.toStorageExternalSessionsSource()
	if err != nil {
		return nil, err
	}

	return conv, nil
}

func createExternalSessionsSourceV1Tx(tx *pg.Tx,
	source *storage.ExternalSessionsSourceV1) (*ExternalSessionsSource, error) {
	// Serialize settings.
	settings, err := json.Marshal(source.Data.Settings)
	if err != nil {
		return nil, err
	}

	// Lookup ExternalUsersGroupsSource.
	var externalUsersGroupsSource ExternalUsersGroupsSource

	if err := tx.Model(&externalUsersGroupsSource).
		Where("name = ?", source.Data.ExternalUsersSourceName).Select(); err != nil {
		if errors.Is(err, pg.ErrNoRows) {
			return nil, storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       source.Data.ExternalUsersSourceName,
				},
			)
		}

		return nil, err
	}

	// Try to create.
	var model = &ExternalSessionsSource{
		ExternalUsersGroupsSourceID: externalUsersGroupsSource.ID,
		ETag:                        source.Metadata.ETag,
		ExternalUsersGroupsSource:   externalUsersGroupsSource,
		GetMode:                     source.Data.GetMode,
		Settings:                    settings,
		Name:                        source.Metadata.Name,
		Type:                        source.Data.Type,
	}

	if _, err := tx.Model(model).Insert(); err != nil {
		if typedErr, ok := err.(pg.Error); ok {
			if typedErr.IntegrityViolation() {
				return nil, storage.NewErrResourceAlreadyExists([]interface{}{*source}, typedErr)
			}
		}

		return nil, err
	}

	// Result.
	return model, nil
}

// CreateExternalSessionsSourceV1Context creates one ExternalSessionsSourceV1.
func (s *SQLStorage) CreateExternalSessionsSourceV1Context(ctx context.Context,
	source *storage.ExternalSessionsSourceV1, getCreated bool) (*storage.ExternalSessionsSourceV1, error) {
	var model *ExternalSessionsSource

	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		md, err := createExternalSessionsSourceV1Tx(tx, source)
		if err != nil {
			return err
		}

		model = md

		return nil
	}); err != nil {
		return nil, err
	}

	if getCreated {
		return model.toStorageExternalSessionsSource()
	}

	return nil, nil
}

// UpdateExternalSessionsSourceV1Context updates existing ExternalSessionsSourceV1.
func (s *SQLStorage) UpdateExternalSessionsSourceV1Context(ctx context.Context, name storage.ResourceNameT,
	source *storage.ExternalSessionsSourceV1, getUpdated bool) (*storage.ExternalSessionsSourceV1, error) {
	var model = &ExternalSessionsSource{}

	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		// Load an existing SessionsSource.
		if err := tx.Model(model).Where("external_sessions_source.name = ?", name).
			Relation("ExternalUsersGroupsSource").Select(); err != nil {
			if errors.Is(err, pg.ErrNoRows) {
				return storage.NewErrResourceNotFound(
					&storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						Kind:       storage.ExternalSessionsSourceKind,
						Name:       name,
					},
				)
			}

			return err
		}

		// Check ETag.
		if model.ETag != source.Metadata.ETag {
			return storage.NewErrETagDoesNotMatch(model.ETag, source.Metadata.ETag)
		}

		// Check UsersGroupsSource.
		if model.ExternalUsersGroupsSource.Name != source.Data.ExternalUsersSourceName {
			return storage.NewErrForbiddenUpdateReadOnlyField(source.Metadata, "Data#ExternalUsersSourceName")
		}

		// Update fields.
		model.ETag++
		model.UpdatedAt = time.Now()
		model.Name = source.Metadata.Name
		model.Type = source.Data.Type
		model.GetMode = source.Data.GetMode

		// Dump.
		bts, err := json.Marshal(source.Data.Settings)
		if err != nil {
			return err
		}

		model.Settings = bts

		// Update.
		if _, err := tx.Model(model).WherePK().Update(); err != nil {
			if typedE, ok := err.(pg.Error); ok {
				if typedE.IntegrityViolation() {
					return storage.NewErrResourceAlreadyExists([]interface{}{*source}, typedE)
				}
			}

			return err
		}

		return nil
	}); err != nil {
		return nil, err
	}

	if getUpdated {
		return model.toStorageExternalSessionsSource()
	}

	return nil, nil
}

// DeleteExternalSessionsSourceV1Context deletes one existing ExternalSessionsSourceV1.
func (s *SQLStorage) DeleteExternalSessionsSourceV1Context(ctx context.Context,
	source *storage.ExternalSessionsSourceV1) error {
	return s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		// Lookup.
		var model ExternalSessionsSource

		if err := tx.Model(&model).
			Where("external_sessions_source.name = ?", source.Metadata.Name).Select(); err != nil {
			if errors.Is(err, pg.ErrNoRows) {
				return storage.NewErrResourceNotFound(&source.Metadata)
			}

			return err
		}

		// Check ETag.
		if source.Metadata.ETag != model.ETag {
			return storage.NewErrETagDoesNotMatch(model.ETag, source.Metadata.ETag)
		}

		// Try to delete.
		_, err := tx.Model(&model).WherePK().Delete()

		return err
	})
}

package sqlstorage3

import (
	"context"
	"encoding/json"
	"testing"
	"time"

	"github.com/go-pg/pg/v10"
	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

type TestExternalUsersGroupsSourceV1Suite struct {
	db *pg.DB

	sqlStorage *SQLStorage

	suite.Suite
}

func (ts *TestExternalUsersGroupsSourceV1Suite) SetupTest() {
	db, err := connectDB()
	if err != nil {
		ts.FailNow("Can not connect to test DB", err)
	}

	ts.db = db
	ts.sqlStorage = &SQLStorage{db: db}

	// Create DB Tables.
	if err := recreateTables(ts.db); err != nil {
		ts.FailNow("Can not initialize DB", err)
	}

	// Create some init data.

	// ExternalUsersGroupsSource.
	var externalUsersGroupsSources = []ExternalUsersGroupsSource{
		{
			ETag:              1,
			GetMode:           storage.ExternalUsersGroupsGetModePassive,
			GroupsGetSettings: `{ "source 1": "groups" }`,
			GroupsToLoad:      []string{"group1", "group2"},
			UsersGetSettings:  `{ "source 1": "users" }`,
			Type:              storage.ExternalUsersGroupsSourceTypeLDAP,
			Name:              "source 1",
			PollInterval:      121,
		},
		{
			ETag:              2,
			GetMode:           storage.ExternalUsersGroupsGetModePoll,
			GroupsGetSettings: `{ "source 2": "groups" }`,
			GroupsToLoad:      []string{"group1", "group2"},
			UsersGetSettings:  `{ "source 2": "users" }`,
			Type:              storage.ExternalUsersGroupsSourceTypeLDAP,
			Name:              "source 2",
			PollInterval:      122,
		},
		{
			ETag:              3,
			GetMode:           storage.ExternalUsersGroupsGetModePassive,
			GroupsGetSettings: `{ "source 3": "groups" }`,
			GroupsToLoad:      []string{"group4", "group41"},
			UsersGetSettings:  `{ "source 3": "users" }`,
			Type:              storage.ExternalUsersGroupsSourceTypeLDAP,
			Name:              "source 3",
			PollInterval:      123,
		},
		{
			ETag:              4,
			GetMode:           storage.ExternalUsersGroupsGetModePoll,
			GroupsGetSettings: `{ "source 4": "groups" }`,
			GroupsToLoad:      []string{"group4", "group5"},
			UsersGetSettings:  `{ "source 4": "users" }`,
			Type:              storage.ExternalUsersGroupsSourceTypeLDAP,
			Name:              "source 4",
			PollInterval:      124,
		},
	}

	for i := range externalUsersGroupsSources {
		if _, err := db.Model(&externalUsersGroupsSources[i]).Insert(); err != nil {
			ts.FailNow("Can not create init data", err.Error())
		}
	}
}

func (ts *TestExternalUsersGroupsSourceV1Suite) TearDownTest() {
	if ts.db != nil {
		if err := ts.db.Close(); err != nil {
			ts.FailNow("Can not Close DB", err)
		}
	}
}

func (ts *TestExternalUsersGroupsSourceV1Suite) TestGetSuccess() {
	var tests = []struct {
		name storage.ResourceNameT

		expectedSource storage.ExternalUsersGroupsSourceV1
	}{
		{
			name: "source 3",
			expectedSource: storage.ExternalUsersGroupsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       3,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       "source 3",
				},
				Data: storage.ExternalUsersGroupsSourceDataV1{
					PollIntervalSec:      123,
					GetMode:              storage.ExternalUsersGroupsGetModePassive,
					Type:                 storage.ExternalUsersGroupsSourceTypeLDAP,
					GroupsToMonitor:      []string{"group4", "group41"},
					GroupsSourceSettings: map[string]string{"source 3": "groups"},
					UsersSourceSettings:  map[string]string{"source 3": "users"},
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.GetExternalUsersGroupsSourceV1Context(
			context.TODO(), test.name)

		ts.NoError(gotErr, "Unexpected error")

		if !ts.NotNil(got, "Must not return nil as result") {
			ts.FailNow("nil result - can not continue")
		}

		ts.Equal(test.expectedSource, *got, "Unexpected source value")
	}
}

func (ts *TestExternalUsersGroupsSourceV1Suite) TestGetNotFound() {
	var tests = []struct {
		name storage.ResourceNameT

		expectedError error
	}{
		{
			name: "non existing source 111",
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       0,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       "non existing source 111",
				},
			),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.GetExternalUsersGroupsSourceV1Context(
			context.TODO(), test.name)

		ts.Nil(got, "Must return nil as result")

		if !ts.Error(gotErr, "Unexpected error") {
			ts.FailNow("nil error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalUsersGroupsSourceV1Suite) TestCreateSuccess() {
	var tests = []struct {
		getResult bool

		sourceToCreate storage.ExternalUsersGroupsSourceV1
	}{
		{
			getResult: false,
			sourceToCreate: storage.ExternalUsersGroupsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       112,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       "new source 1",
				},
				Data: storage.ExternalUsersGroupsSourceDataV1{
					PollIntervalSec: 11022,
					GetMode:         storage.ExternalUsersGroupsGetModePassive,
					GroupsSourceSettings: map[string]string{
						"key 1": "value 1",
					},
					GroupsToMonitor: []string{"group 1", "group 2"},
					UsersSourceSettings: map[string]string{
						"key 2": "value 2",
					},
					Type: storage.ExternalUsersGroupsSourceTypeLDAP,
				},
			},
		},
		{
			getResult: true,
			sourceToCreate: storage.ExternalUsersGroupsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       113,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       "new source 2",
				},
				Data: storage.ExternalUsersGroupsSourceDataV1{
					PollIntervalSec: 1162,
					GetMode:         storage.ExternalUsersGroupsGetModePoll,
					GroupsSourceSettings: map[string]string{
						"key 12": "value 1",
					},
					GroupsToMonitor: []string{"group 1", "group 2"},
					UsersSourceSettings: map[string]string{
						"key 22": "value 2",
					},
					Type: storage.ExternalUsersGroupsSourceTypeLDAP,
				},
			},
		},
		{
			getResult: true,
			sourceToCreate: storage.ExternalUsersGroupsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       113,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       "new source 3",
				},
				Data: storage.ExternalUsersGroupsSourceDataV1{
					PollIntervalSec: 1162,
					GetMode:         storage.ExternalUsersGroupsGetModePoll,
					GroupsSourceSettings: map[string]string{
						"key 12": "value 1",
					},
					// GroupsToMonitor: []string{},
					UsersSourceSettings: map[string]string{
						"key 22": "value 2",
					},
					Type: storage.ExternalUsersGroupsSourceTypeLDAP,
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		// Create.
		got, gotErr := ts.sqlStorage.CreateExternalUsersGroupsSourceV1Context(
			context.TODO(), &test.sourceToCreate, test.getResult)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("got error - can not continue")
		}

		// Get from DB.
		var dbModel = &ExternalUsersGroupsSource{}

		if err := ts.db.Model(dbModel).
			Where("name = ?", test.sourceToCreate.Metadata.Name).Select(); err != nil {
			ts.FailNow("Can not load a Source from DB: ", err)
		}

		// Compare.
		ts.Equal(test.sourceToCreate.Metadata.Name, dbModel.Name)
		ts.Equal(test.sourceToCreate.Metadata.ETag, dbModel.ETag, "ETag must be 1")

		var expGGS, err = json.Marshal(test.sourceToCreate.Data.GroupsSourceSettings)
		if err != nil {
			ts.FailNow("Got json.Marshal error, can not continue: ", err)
		}

		ts.JSONEq(string(expGGS), dbModel.GroupsGetSettings)

		expUGS, err := json.Marshal(test.sourceToCreate.Data.UsersSourceSettings)
		if err != nil {
			ts.FailNow("Got json.Marshal error, can not continue: ", err)
		}

		ts.JSONEq(string(expUGS), dbModel.UsersGetSettings)

		ts.Equal(test.sourceToCreate.Data.GetMode, dbModel.GetMode)
		ts.Equal(test.sourceToCreate.Data.GroupsToMonitor, dbModel.GroupsToLoad)
		ts.Equal(test.sourceToCreate.Data.PollIntervalSec, dbModel.PollInterval)
		ts.Equal(test.sourceToCreate.Data.Type, dbModel.Type)

		if test.getResult {
			if !ts.NotNil(got, "Must not return nil result") {
				ts.FailNow("nil result - can not continue")
			}

			ts.Equal(test.sourceToCreate, *got, "Unexpected created value")
		}
	}
}

func (ts *TestExternalUsersGroupsSourceV1Suite) TestCreateConflict() {
	var tests = []struct {
		expectedError error

		source storage.ExternalUsersGroupsSourceV1
	}{
		{
			expectedError: storage.NewErrResourceAlreadyExists([]interface{}{
				storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       "source 1",
				},
			}),
			source: storage.ExternalUsersGroupsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       "source 1",
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.CreateExternalUsersGroupsSourceV1Context(context.TODO(),
			&test.source, true)

		ts.Nil(got, "Must return nil result")

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("nil error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalUsersGroupsSourceV1Suite) TestUpdateSuccess() {
	var tests = []struct {
		getUpdated bool

		name  storage.ResourceNameT
		patch storage.ExternalUsersGroupsSourceV1
	}{
		{
			name: "source 1",
			patch: storage.ExternalUsersGroupsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       "updated source 1",
				},
				Data: storage.ExternalUsersGroupsSourceDataV1{
					PollIntervalSec: 11022,
					GetMode:         storage.ExternalUsersGroupsGetModePassive,
					GroupsSourceSettings: map[string]string{
						"key 1": "value 1",
					},
					GroupsToMonitor: []string{"group 1", "group 2"},
					UsersSourceSettings: map[string]string{
						"key 2": "value 2",
					},
					Type: storage.ExternalUsersGroupsSourceTypeLDAP,
				},
			},
		},
		{
			getUpdated: true,
			name:       "source 2",
			patch: storage.ExternalUsersGroupsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       2,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       "updated source 2",
				},
				Data: storage.ExternalUsersGroupsSourceDataV1{
					PollIntervalSec: 11022,
					GetMode:         storage.ExternalUsersGroupsGetModePassive,
					GroupsSourceSettings: map[string]string{
						"key 1": "value 1",
					},
					GroupsToMonitor: []string{"group 1", "group 2"},
					UsersSourceSettings: map[string]string{
						"key 2": "value 2",
					},
					Type: storage.ExternalUsersGroupsSourceTypeLDAP,
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.UpdateExternalUsersGroupsSourceV1Context(context.TODO(),
			test.name, &test.patch, test.getUpdated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue", gotErr)
		}

		// Check Update.
		var dbModel ExternalUsersGroupsSource

		if err := ts.db.Model(&dbModel).Where("name = ?", test.patch.Metadata.Name).
			Select(); err != nil {
			ts.FailNow("Can not load a Source from DB", err)
		}

		// Compare.
		ts.Equal(test.patch.Metadata.ETag, dbModel.ETag-1)
		ts.Equal(test.patch.Metadata.Name, dbModel.Name)
		ts.Equal(test.patch.Data.GetMode, dbModel.GetMode)
		ts.Equal(test.patch.Data.GroupsToMonitor, dbModel.GroupsToLoad)
		ts.Equal(test.patch.Data.PollIntervalSec, dbModel.PollInterval)
		ts.Equal(test.patch.Data.Type, dbModel.Type)

		ggs, err := json.Marshal(test.patch.Data.GroupsSourceSettings)
		if err != nil {
			ts.FailNow("Can not json.Marshal", err)
		}

		ugs, err := json.Marshal(test.patch.Data.UsersSourceSettings)
		if err != nil {
			ts.FailNow("Can not json.Marshal", err)
		}

		ts.JSONEq(string(ggs), dbModel.GroupsGetSettings)
		ts.JSONEq(string(ugs), dbModel.UsersGetSettings)

		ts.WithinDuration(time.Now(), dbModel.UpdatedAt, time.Minute)

		if test.getUpdated {
			if !ts.NotNil(got, "Must not return nil result") {
				ts.FailNow("nil result - can not continue")
			}

			// Correct ETag.
			got.Metadata.ETag--

			ts.Equal(test.patch, *got, "Unexpected result value")
		}
	}
}

func (ts *TestExternalUsersGroupsSourceV1Suite) TestUpdateNotFound() {
	var tests = []struct {
		name  storage.ResourceNameT
		patch storage.ExternalUsersGroupsSourceV1

		expectedError error
	}{
		{
			name: "not existing source 1",
			patch: storage.ExternalUsersGroupsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       "updated source 1",
				},
				Data: storage.ExternalUsersGroupsSourceDataV1{
					PollIntervalSec: 11022,
					GetMode:         storage.ExternalUsersGroupsGetModePassive,
					GroupsSourceSettings: map[string]string{
						"key 1": "value 1",
					},
					GroupsToMonitor: []string{"group 1", "group 2"},
					UsersSourceSettings: map[string]string{
						"key 2": "value 2",
					},
					Type: storage.ExternalUsersGroupsSourceTypeLDAP,
				},
			},
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       "not existing source 1",
				},
			),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.UpdateExternalUsersGroupsSourceV1Context(context.TODO(),
			test.name, &test.patch, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got error - can not continue", gotErr)
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalUsersGroupsSourceV1Suite) TestUpdateConflict() {
	var tests = []struct {
		name  storage.ResourceNameT
		patch storage.ExternalUsersGroupsSourceV1

		expectedError error
	}{
		{
			name: "source 1",
			patch: storage.ExternalUsersGroupsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       "source 2", // Conflict.
				},
				Data: storage.ExternalUsersGroupsSourceDataV1{
					PollIntervalSec: 11022,
					GetMode:         storage.ExternalUsersGroupsGetModePassive,
					GroupsSourceSettings: map[string]string{
						"key 1": "value 1",
					},
					GroupsToMonitor: []string{"group 1", "group 2"},
					UsersSourceSettings: map[string]string{
						"key 2": "value 2",
					},
					Type: storage.ExternalUsersGroupsSourceTypeLDAP,
				},
			},
			expectedError: storage.NewErrResourceAlreadyExists([]interface{}{
				storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       "source 2", // Conflict.
				},
			}),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.UpdateExternalUsersGroupsSourceV1Context(context.TODO(),
			test.name, &test.patch, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got error - can not continue", gotErr)
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalUsersGroupsSourceV1Suite) TestDeleteSuccess() {
	var tests = []struct {
		source storage.ExternalUsersGroupsSourceV1
	}{
		{
			source: storage.ExternalUsersGroupsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       "source 1",
					ETag:       1,
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.sqlStorage.DeleteExternalUsersGroupsSourceV1Context(context.TODO(),
			&test.source)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("got error - can not continue")
		}

		// Check that there is no more the source.
		exists, err := ts.db.Model((*ExternalUsersGroupsSource)(nil)).
			Where("name = ?", test.source.Metadata.Name).Exists()
		if err != nil {
			ts.FailNow("Can not get existence status of a Source from DB")
		}

		ts.False(exists, "Must not get exists == true")
	}
}

func (ts *TestExternalUsersGroupsSourceV1Suite) TestDeleteNotFound() {
	var tests = []struct {
		source storage.ExternalUsersGroupsSourceV1

		expectedError error
	}{
		{
			source: storage.ExternalUsersGroupsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       "not existing source 1",
					ETag:       1,
				},
			},
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       "not existing source 1",
					ETag:       1,
				},
			),
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.sqlStorage.DeleteExternalUsersGroupsSourceV1Context(context.TODO(),
			&test.source)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("got nil error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")

		// Check that there is no more the source.
		exists, err := ts.db.Model((*ExternalUsersGroupsSource)(nil)).
			Where("name = ?", test.source.Metadata.Name).Exists()
		if err != nil {
			ts.FailNow("Can not get existence status of a Source from DB")
		}

		ts.False(exists, "Must not get exists == true")
	}
}

func (ts *TestExternalUsersGroupsSourceV1Suite) TestDeleteETagError() {
	var tests = []struct {
		source storage.ExternalUsersGroupsSourceV1

		expectedError error
	}{
		{
			source: storage.ExternalUsersGroupsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       "source 1",
					ETag:       100,
				},
			},
			expectedError: storage.NewErrETagDoesNotMatch(1, 100),
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.sqlStorage.DeleteExternalUsersGroupsSourceV1Context(context.TODO(),
			&test.source)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("got nil error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")

		// Check that there is the source.
		exists, err := ts.db.Model((*ExternalUsersGroupsSource)(nil)).
			Where("name = ?", test.source.Metadata.Name).Exists()
		if err != nil {
			ts.FailNow("Can not get existence status of a Source from DB")
		}

		ts.True(exists, "Must exists == true")
	}
}

func TestExternalUsersGroupsSourceV1(t *testing.T) {
	suite.Run(t, &TestExternalUsersGroupsSourceV1Suite{})
}

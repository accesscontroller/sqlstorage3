package sqlstorage3

import (
	"encoding/json"
	"net"
	"net/url"
	"time"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

type ExternalUsersGroupsSource struct {
	ID int64

	Name              storage.ResourceNameT                     `pg:",unique,notnull,type:varchar(1024)"`
	ETag              storage.ETagT                             `pg:",notnull"`
	GetMode           storage.ExternalUsersGroupsSourceGetModeT `pg:",notnull,type:varchar(64)"`
	Type              storage.ExternalUsersGroupsSourceTypeT    `pg:",notnull,type:varchar(64)"`
	UsersGetSettings  string                                    `pg:""`
	GroupsGetSettings string                                    `pg:""`
	PollInterval      int                                       `pg:",notnull"`
	GroupsToLoad      []string                                  `pg:",array"`

	CreatedAt time.Time `pg:"default:now()"`
	UpdatedAt time.Time

	ExternalGroups ExternalGroupSlice `pg:"rel:has-many"`
	ExternalUsers  ExternalUserSlice  `pg:"rel:has-many"`
}

func (s *ExternalUsersGroupsSource) toStorageExternalUsersGroupsSource() (*storage.ExternalUsersGroupsSourceV1, error) {
	var result = storage.NewExternalUsersGroupsSourceV1(s.Name)

	var (
		groupSettings, userSettings map[string]string
	)

	if err := json.Unmarshal([]byte(s.GroupsGetSettings), &groupSettings); err != nil {
		return nil, err
	}

	if err := json.Unmarshal([]byte(s.UsersGetSettings), &userSettings); err != nil {
		return nil, err
	}

	result.Metadata.ETag = s.ETag
	result.Data.GetMode = s.GetMode
	result.Data.GroupsToMonitor = s.GroupsToLoad
	result.Data.PollIntervalSec = s.PollInterval
	result.Data.Type = s.Type
	result.Data.GroupsSourceSettings = groupSettings
	result.Data.UsersSourceSettings = userSettings

	return result, nil
}

type ExternalUsersGroupsSourceSlice []*ExternalUsersGroupsSource

func (ess ExternalUsersGroupsSourceSlice) toStorageExternalUsersGroupsSources() (
	[]storage.ExternalUsersGroupsSourceV1, error) {
	var result = make([]storage.ExternalUsersGroupsSourceV1, 0, len(ess))

	for i := range ess {
		s, err := (ess[i]).toStorageExternalUsersGroupsSource()
		if err != nil {
			return nil, err
		}

		result = append(result, *s)
	}

	return result, nil
}

type ExternalUser struct {
	ID int64

	Name storage.ResourceNameT `pg:",unique:name__external_users_groups_source_id,notnull,type:varchar(1024)"`
	ETag storage.ETagT         `pg:",notnull"`

	ExternalUsersGroupsSourceID int64                     `pg:",unique:name__external_users_groups_source_id,notnull,on_delete:CASCADE,on_update:CASCADE"` // nolint:lll
	ExternalUsersGroupsSource   ExternalUsersGroupsSource `pg:"rel:has-one"`

	CreatedAt time.Time `pg:"default:now()"`
	UpdatedAt time.Time

	ExternalGroups ExternalGroupSlice `pg:"many2many:external_user_to_external_group"`
}

func (u *ExternalUser) toStorageExternalUser() *storage.ExternalUserV1 {
	var uRes = storage.NewExternalUserV1(u.Name, u.ExternalUsersGroupsSource.Name)

	uRes.Metadata.ETag = u.ETag

	return uRes
}

type ExternalUserSlice []*ExternalUser

func (us ExternalUserSlice) toStorageExternalUsers() []storage.ExternalUserV1 {
	var result = make([]storage.ExternalUserV1, 0, len(us))

	for _, v := range us {
		result = append(result, *v.toStorageExternalUser())
	}

	return result
}

type ExternalGroup struct {
	ID int64

	Name storage.ResourceNameT `pg:",notnull,unique:name__external_users_groups_source_id,type:varchar(1024)"`
	ETag storage.ETagT         `pg:",notnull"`

	AccessGroupID int64       `pg:"on_update:CASCADE,on_delete:SET NULL"`
	AccessGroup   AccessGroup `pg:"rel:has-one"`

	CreatedAt time.Time `pg:"default:now()"`
	UpdatedAt time.Time

	ExternalUsersGroupsSourceID int64                     `pg:",notnull,unique:name__external_users_groups_source_id,on_delete:CASCADE,on_update:CASCADE"` // nolint:lll
	ExternalUsersGroupsSource   ExternalUsersGroupsSource `pg:"rel:has-one"`

	ExternalUsers ExternalUserSlice `pg:"many2many:external_user_to_external_group"`
}

func (e *ExternalGroup) toStorageExternalGroup() *storage.ExternalGroupV1 {
	var result = storage.NewExternalGroupV1(e.Name, e.ExternalUsersGroupsSource.Name)

	result.Metadata.ETag = e.ETag
	result.Data.AccessGroupName = e.AccessGroup.Name

	return result
}

type ExternalGroupSlice []*ExternalGroup

func (s ExternalGroupSlice) toStorageExternalGroups() []storage.ExternalGroupV1 {
	var result = make([]storage.ExternalGroupV1, 0, len(s))

	for _, g := range s {
		result = append(result, *g.toStorageExternalGroup())
	}

	return result
}

type ExternalUserToExternalGroup struct {
	tableName struct{} `pg:"external_user_to_external_group,alias:g"` // nolint:structcheck,unused

	ExternalUserID  int64 `pg:",pk,unique:user__group,on_delete:CASCADE,on_update:CASCADE"`
	ExternalGroupID int64 `pg:",pk,unique:user__group,on_delete:CASCADE,on_update:CASCADE"`

	CreatedAt time.Time `pg:"default:now()"`

	ExternalUser  *ExternalUser  `pg:"rel:has-one"`
	ExternalGroup *ExternalGroup `pg:"rel:has-one"`
}

type AccessGroup struct {
	ID int64

	Name storage.ResourceNameT `pg:",notnull,unique,type:varchar(1024)"`
	ETag storage.ETagT         `pg:",notnull"`
	// DefaultPolicy - action for resources not listed in DefaultPolicyExceptions.
	DefaultPolicy storage.DefaultAccessPolicyT `pg:",notnull"`
	// OrderInAccessRulesList - order of applying this policy.
	OrderInAccessRulesList int64 `pg:",notnull,unique"`
	// TotalGroupSpeedLimitBps - speed limit for all people who are under the group (summary).
	TotalGroupSpeedLimitBps int64                           `pg:",notnull"`
	UserGroupSpeedLimitBps  int64                           `pg:",notnull"` // limit for every user in the group
	ExtraAccessDenyMessage  storage.ExtraAccessDenyMessageT // message to show a user in case of blocking access

	CreatedAt time.Time `pg:"default:now()"`
	UpdatedAt time.Time

	// ExternalGroups groups to match user from them.
	ExternalGroups ExternalGroupSlice `pg:"rel:has-many"`
	// WebResourceCategorys - web resource categories to exclude them from DefaultPolicy.
	WebResourceCategorys WebResourceCategorySlice `pg:"many2many:web_resource_category_to_access_group"`
}

func (ag *AccessGroup) toStorageAccessGroup() *storage.AccessGroupV1 {
	var sag = storage.NewAccessGroupV1(ag.Name)

	sag.Metadata.ETag = ag.ETag
	sag.Data.DefaultPolicy = ag.DefaultPolicy
	sag.Data.ExtraAccessDenyMessage = ag.ExtraAccessDenyMessage
	sag.Data.OrderInAccessRulesList = ag.OrderInAccessRulesList
	sag.Data.TotalGroupSpeedLimitBps = ag.TotalGroupSpeedLimitBps
	sag.Data.UserGroupSpeedLimitBps = ag.UserGroupSpeedLimitBps

	return sag
}

type AccessGroupSlice []*AccessGroup

func (s AccessGroupSlice) toStorageAccessGroups() []storage.AccessGroupV1 {
	var result = make([]storage.AccessGroupV1, 0, len(s))

	for _, v := range s {
		result = append(result, *v.toStorageAccessGroup())
	}

	return result
}

type WebResourceCategoryToAccessGroup struct {
	tableName struct{} `pg:"web_resource_category_to_access_group,alias:g"` // nolint:structcheck,unused

	WebResourceCategoryID int64 `pg:",pk,unique:web_resource_category_to_access_group,on_delete:CASCADE,on_update:CASCADE"`
	AccessGroupID         int64 `pg:",pk,unique:web_resource_category_to_access_group,on_delete:CASCADE,on_update:CASCADE"`

	CreatedAt time.Time `pg:"default:now()"`

	WebResourceCategory *WebResourceCategory `pg:"rel:has-one"`
	AccessGroup         *AccessGroup         `pg:"rel:has-one"`
}

type ExternalSessionsSource struct {
	ID int64

	Name     storage.ResourceNameT                  `pg:",notnull,unique,type:varchar(1024)"`
	ETag     storage.ETagT                          `pg:",notnull"`
	GetMode  storage.ExternalSessionsSourceGetModeT `pg:",notnull,type:varchar(32)"`
	Type     storage.ExternalSessionsSourceTypeT    `pg:",notnull,type:varchar(32)"`
	Settings []byte                                 `pg:",notnull"`

	CreatedAt time.Time `pg:"default:now()"`
	UpdatedAt time.Time

	ExternalUsersGroupsSourceID int64                     `pg:",notnull,on_delete:CASCADE,on_update:CASCADE"`
	ExternalUsersGroupsSource   ExternalUsersGroupsSource `pg:"rel:has-one"`

	ExternalSessions ExternalUserSessionSlice `pg:"rel:has-many"`
}

func (s *ExternalSessionsSource) toStorageExternalSessionsSource() (*storage.ExternalSessionsSourceV1, error) {
	var res = storage.NewExternalSessionsSourceV1(s.Name)

	res.Metadata.ETag = s.ETag

	res.Data.GetMode = s.GetMode
	res.Data.Type = s.Type

	var sett = make(map[string]string)

	if err := json.Unmarshal(s.Settings, &sett); err != nil {
		return nil, err
	}

	res.Data.Settings = sett

	res.Data.ExternalUsersSourceName = s.ExternalUsersGroupsSource.Name

	return res, nil
}

type ExternalSessionsSourceSlice []*ExternalSessionsSource

func (s ExternalSessionsSourceSlice) toStorageExternalSessionsSources() ([]storage.ExternalSessionsSourceV1, error) {
	var result = make([]storage.ExternalSessionsSourceV1, 0, len(s))

	for _, ess := range s {
		c, err := ess.toStorageExternalSessionsSource()
		if err != nil {
			return nil, err
		}

		result = append(result, *c)
	}

	return result, nil
}

type ExternalUserSession struct {
	ID int64

	Name   storage.ResourceNameT `pg:",notnull,unique:name__external_sessions_source_id,type:varchar(1024)"`
	ETag   storage.ETagT         `pg:",notnull"`
	Closed bool                  `pg:",notnull,use_zero"`

	OpenTimestamp  time.Time `pg:",notnull"`
	CloseTimestamp time.Time `pg:",soft_delete"`
	UpdatedAt      time.Time

	HostName  string
	IPAddress net.IP `pg:",notnull"`

	ExternalUserID int64        `pg:",notnull,on_delete:CASCADE,on_update:CASCADE"`
	ExternalUser   ExternalUser `pg:"rel:has-one"`

	ExternalSessionsSourceID int64                  `pg:",notnull,unique:name__external_sessions_source_id,on_delete:CASCADE,on_update:CASCADE"` // nolint:lll
	ExternalSessionsSource   ExternalSessionsSource `pg:"rel:has-one"`
}

func (us *ExternalUserSession) toStorageExternalUserSession() *storage.ExternalUserSessionV1 {
	var userSession = storage.NewExternalUserSessionV1(us.Name, us.ExternalSessionsSource.Name)

	userSession.Metadata.ETag = us.ETag
	userSession.Data.Closed = us.Closed

	if us.Closed {
		userSession.Data.CloseTimestamp = &us.CloseTimestamp
	}

	userSession.Data.ExternalUserName = us.ExternalUser.Name
	userSession.Data.ExternalUsersGroupsSourceName = us.ExternalUser.ExternalUsersGroupsSource.Name
	userSession.Data.Hostname = us.HostName
	userSession.Data.IPAddress = us.IPAddress
	userSession.Data.OpenTimestamp = &us.OpenTimestamp

	return userSession
}

type ExternalUserSessionSlice []*ExternalUserSession

func (uss ExternalUserSessionSlice) toStorageExternalUserSessions() []storage.ExternalUserSessionV1 {
	var result = make([]storage.ExternalUserSessionV1, 0, len(uss))

	for _, s := range uss {
		result = append(result, *s.toStorageExternalUserSession())
	}

	return result
}

type WebResourceCategoryToWebResource struct {
	tableName struct{} `pg:"web_resource_category_to_web_resource,alias:wrcwr"` // nolint:structcheck,unused

	WebResourceCategoryID int64 `pg:",pk,unique:web_resource_category_to_web_resource,on_delete:CASCADE,on_update:CASCADE"`
	WebResourceID         int64 `pg:",pk,unique:web_resource_category_to_web_resource,on_delete:CASCADE,on_update:CASCADE"`

	CreatedAt time.Time `pg:"default:now()"`

	WebResourceCategory *WebResourceCategory `pg:"rel:has-one"`
	WebResource         *WebResource         `pg:"rel:has-one"`
}

type WebResourceCategoryToWebResourceSlice []WebResourceCategoryToWebResource

type WebResourceCategory struct {
	ID int64

	Name        storage.ResourceNameT                   `pg:",unique,notnull,type:varchar(1024)"`
	ETag        storage.ETagT                           `pg:",notnull"`
	Description storage.WebResourceCategoryDescriptionT `pg:",notnull"`

	CreatedAt time.Time `pg:"default:now()"`
	UpdatedAt time.Time

	WebResources WebResourceSlice `pg:"many2many:web_resource_category_to_web_resource"`

	AccessGroups AccessGroupSlice `pg:"many2many:web_resource_category_to_access_group"`
}

func (w *WebResourceCategory) toStorageWebResourceCategory() *storage.WebResourceCategoryV1 {
	var result = storage.NewWebResourceCategoryV1(w.Name)

	result.Metadata.ETag = w.ETag
	result.Data.Description = w.Description

	return result
}

type WebResourceCategorySlice []*WebResourceCategory

func (w WebResourceCategorySlice) toStorageWebResourceCategories() []storage.WebResourceCategoryV1 {
	var result = make([]storage.WebResourceCategoryV1, 0, len(w))

	for _, c := range w {
		result = append(result, *c.toStorageWebResourceCategory())
	}

	return result
}

type WebResource struct {
	ID int64

	Name        storage.ResourceNameT           `pg:",unique,notnull,type:varchar(1024)"`
	ETag        storage.ETagT                   `pg:",notnull"`
	Description storage.WebResourceDescriptionT `pg:",notnull"`

	CreatedAt time.Time `pg:"default:now()"`
	UpdatedAt time.Time

	WebResourceCategorys WebResourceCategorySlice `pg:"many2many:web_resource_category_to_web_resource"`

	WebResourceIPs     WebResourceIPSlice     `pg:"rel:has-many"`
	WebResourceDomains WebResourceDomainSlice `pg:"rel:has-many"`
	WebResourceURLs    WebResourceURLSlice    `pg:"rel:has-many"`
}

func (w *WebResource) toStorageWebResource() (*storage.WebResourceV1, error) {
	var result = storage.NewWebResourceV1(w.Name)

	result.Metadata.ETag = w.ETag
	result.Data.Description = w.Description

	// URLs.
	urls, err := w.WebResourceURLs.toURLs()
	if err != nil {
		return nil, err
	}

	result.Data.URLs = urls

	// Domains.
	result.Data.Domains = w.WebResourceDomains.toDomains()

	// IPs.
	result.Data.IPs = w.WebResourceIPs.toIPs()

	// WebResourceCategory Names.
	result.Data.WebResourceCategoryNames = make([]storage.ResourceNameT, 0, len(w.WebResourceCategorys))

	for i := range w.WebResourceCategorys {
		result.Data.WebResourceCategoryNames = append(result.Data.WebResourceCategoryNames, w.WebResourceCategorys[i].Name)
	}

	return result, nil
}

type WebResourceSlice []*WebResource

func (s WebResourceSlice) toStorageWebResources() ([]storage.WebResourceV1, error) {
	var result = make([]storage.WebResourceV1, 0, len(s))

	for _, r := range s {
		res, err := r.toStorageWebResource()
		if err != nil {
			return nil, err
		}

		result = append(result, *res)
	}

	return result, nil
}

type WebResourceIP struct {
	ID int64

	IP net.IP `pg:",notnull,unique"`

	CreatedAt time.Time `pg:"default:now()"`
	UpdatedAt time.Time

	WebResourceID int64       `pg:",notnull,on_delete:CASCADE,on_update:CASCADE"`
	WebResource   WebResource `pg:"rel:has-one"`
}

func (ip *WebResourceIP) String() string {
	return ip.IP.String()
}

type WebResourceIPSlice []*WebResourceIP

func (ips WebResourceIPSlice) toIPs() []net.IP {
	var result = make([]net.IP, 0, len(ips))

	for _, ip := range ips {
		result = append(result, ip.IP)
	}

	return result
}

type WebResourceDomain struct {
	ID int64

	Domain storage.WebResourceDomainT `pg:",notnull,type:varchar(1024),unique"`

	CreatedAt time.Time `pg:"default:now()"`
	UpdatedAt time.Time

	WebResourceID int64       `pg:",notnull,on_delete:CASCADE,on_update:CASCADE"`
	WebResource   WebResource `pg:"rel:has-one"`
}

func (d *WebResourceDomain) String() string {
	return string(d.Domain)
}

type WebResourceDomainSlice []*WebResourceDomain

func (domains WebResourceDomainSlice) toDomains() []storage.WebResourceDomainT {
	var result = make([]storage.WebResourceDomainT, 0, len(domains))

	for _, d := range domains {
		result = append(result, d.Domain)
	}

	return result
}

type WebResourceURL struct {
	ID int64

	URL string `pg:",notnull,type:varchar(1024),unique"`

	CreatedAt time.Time `pg:"default:now()"`
	UpdatedAt time.Time

	WebResourceID int64       `pg:",notnull,on_delete:CASCADE,on_update:CASCADE"`
	WebResource   WebResource `pg:"rel:has-one"`
}

func (u *WebResourceURL) String() string {
	return u.URL
}

type WebResourceURLSlice []*WebResourceURL

func (urls WebResourceURLSlice) toURLs() ([]url.URL, error) {
	var result = make([]url.URL, 0, len(urls))

	for _, u := range urls {
		parsedU, err := url.Parse(u.URL)
		if err != nil {
			return nil, err
		}

		result = append(result, *parsedU)
	}

	return result, nil
}

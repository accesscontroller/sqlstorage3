package sqlstorage3

import (
	"context"
	"errors"
	"time"

	"github.com/go-pg/pg/v10"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

// GetExternalUserSessionV1Context loads and return one ExternalUserSessionV1.
func (s *SQLStorage) GetExternalUserSessionV1Context(ctx context.Context,
	externalSessionsSourceName, name storage.ResourceNameT) (*storage.ExternalUserSessionV1, error) {
	var model = &ExternalUserSession{}

	if err := s.db.WithContext(ctx).Model(model).Relation("ExternalSessionsSource").
		Relation("ExternalUser.name").Relation("ExternalUser.ExternalUsersGroupsSource.name").
		AllWithDeleted().
		Where("external_user_session.name = ? AND external_sessions_source.name = ?",
			name, externalSessionsSourceName).Select(); err != nil {
		if errors.Is(err, pg.ErrNoRows) {
			return nil, storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: externalSessionsSourceName,
						Kind:       storage.ExternalSessionsSourceKind,
					},
					Kind: storage.ExternalUserSessionKind,
					Name: name,
				},
			)
		}

		return nil, err
	}

	return model.toStorageExternalUserSession(), nil
}

func openExternalUserSessionV1Tx(tx *pg.Tx, session *storage.ExternalUserSessionV1,
	getOpened bool) (*ExternalUserSession, error) {
	var model = &ExternalUserSession{}

	// Lookup ExternalSessions Source.
	var sourceID int64
	if err := tx.Model((*ExternalSessionsSource)(nil)).
		Column("external_sessions_source.id").
		Where("name = ?", session.Metadata.ExternalSource.SourceName).
		Select(&sourceID); err != nil {
		if errors.Is(err, pg.ErrNoRows) {
			return nil, storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       0,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       session.Metadata.ExternalSource.SourceName,
				},
			)
		}

		return nil, err
	}

	// Lookup External User.
	var userID int64
	if err := tx.Model((*ExternalUser)(nil)).
		Column("external_user.id").
		Relation("ExternalUsersGroupsSource._").
		Where("external_user.name = ? AND external_users_groups_source.name = ?",
			session.Data.ExternalUserName, session.Data.ExternalUsersGroupsSourceName).
		Select(&userID); err != nil {
		if errors.Is(err, pg.ErrNoRows) {
			return nil, storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             0,
					Kind:             storage.ExternalUserKind,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: session.Data.ExternalUsersGroupsSourceName,
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Name: session.Data.ExternalUserName,
				},
			)
		}

		return nil, err
	}

	// Insert.
	model.ETag = session.Metadata.ETag
	model.ExternalSessionsSourceID = sourceID
	model.ExternalUserID = userID
	model.HostName = session.Data.Hostname
	model.IPAddress = session.Data.IPAddress
	model.Name = session.Metadata.Name
	model.OpenTimestamp = *session.Data.OpenTimestamp

	if _, err := tx.Model(model).Insert(); err != nil {
		if typedE, ok := err.(pg.Error); ok {
			if typedE.IntegrityViolation() {
				return nil, storage.NewErrResourceAlreadyExists([]interface{}{session.Metadata}, typedE)
			}
		}

		return nil, err
	}

	if getOpened {
		// Set extra values.
		model.ExternalSessionsSource.Name = session.Metadata.ExternalSource.SourceName
		model.ExternalUser.Name = session.Data.ExternalUserName
		model.ExternalUser.ExternalUsersGroupsSource.Name = session.Data.ExternalUsersGroupsSourceName

		return model, nil
	}

	return nil, nil
}

// OpenExternalUserSessionV1Context opens a new ExternalUserSessionV1.
func (s *SQLStorage) OpenExternalUserSessionV1Context(ctx context.Context,
	session *storage.ExternalUserSessionV1, getOpened bool) (*storage.ExternalUserSessionV1, error) {
	var model *ExternalUserSession

	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		m, err := openExternalUserSessionV1Tx(tx, session, getOpened)
		if err != nil {
			return err
		}

		model = m

		return nil
	}); err != nil {
		return nil, err
	}

	if getOpened {
		return model.toStorageExternalUserSession(), nil
	}

	return nil, nil
}

func (s *SQLStorage) CloseExternalUserSessionV1Context(ctx context.Context,
	externalSessionsSourceName storage.ResourceNameT,
	session *storage.ExternalUserSessionV1,
	getClosed bool) (*storage.ExternalUserSessionV1, error) {
	var model = &ExternalUserSession{}

	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		// Lookup.
		if err := tx.Model(model).Relation("ExternalSessionsSource").
			Where("external_user_session.name = ? AND external_sessions_source.name = ?",
				session.Metadata.Name, session.Metadata.ExternalSource.SourceName).
			Select(); err != nil {
			if errors.Is(err, pg.ErrNoRows) {
				return storage.NewErrResourceNotFound(&session.Metadata)
			}

			return err
		}

		// Check ETag.
		if model.ETag != session.Metadata.ETag {
			return storage.NewErrETagDoesNotMatch(model.ETag, session.Metadata.ETag)
		}

		// Close.
		model.CloseTimestamp = time.Now()
		model.Closed = true

		_, err := tx.Model(model).WherePK().Update()

		return err
	}); err != nil {
		return nil, err
	}

	if getClosed {
		return model.toStorageExternalUserSession(), nil
	}

	return nil, nil
}

package sqlstorage3

import (
	"context"
	"sort"
	"testing"

	"github.com/go-pg/pg/v10"
	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

type TestExternalGroupV1sSuite struct {
	db *pg.DB

	sqlStorage *SQLStorage

	suite.Suite
}

func (ts *TestExternalGroupV1sSuite) SetupTest() {
	db, err := connectDB()
	if err != nil {
		ts.FailNow("Can not connect to test DB", err)
	}

	ts.db = db
	ts.sqlStorage = &SQLStorage{db: db}

	// Create DB Tables.
	if err := recreateTables(ts.db); err != nil {
		ts.FailNow("Can not initialize DB", err)
	}

	// Create some init data.

	// ExternalUsersGroupsSource.
	var externalUsersGroupsSources = []ExternalUsersGroupsSource{
		{
			ETag:              1,
			GetMode:           storage.ExternalUsersGroupsGetModePassive,
			GroupsGetSettings: "get groups settings",
			GroupsToLoad:      []string{"group1", "group2"},
			UsersGetSettings:  "get users settings",
			Type:              storage.ExternalUsersGroupsSourceTypeLDAP,
			Name:              "source 1",
			PollInterval:      121,
		},
		{
			ETag:              1,
			GetMode:           storage.ExternalUsersGroupsGetModePassive,
			GroupsGetSettings: "get groups settings",
			GroupsToLoad:      []string{"group1", "group2"},
			UsersGetSettings:  "get users settings",
			Type:              storage.ExternalUsersGroupsSourceTypeLDAP,
			Name:              "source 2",
			PollInterval:      122,
		},
	}

	for i := range externalUsersGroupsSources {
		if _, err := db.Model(&externalUsersGroupsSources[i]).Insert(); err != nil {
			ts.FailNow("Can not create init data", err.Error())
		}
	}

	// AccessGroup.
	var accessGroups = []AccessGroup{
		{
			Name:                    "access group 1",
			DefaultPolicy:           storage.DefaultAccessPolicyAllow,
			ETag:                    1,
			OrderInAccessRulesList:  10,
			TotalGroupSpeedLimitBps: -1,
			UserGroupSpeedLimitBps:  -1,
		},
		{
			Name:                    "access group 2",
			DefaultPolicy:           storage.DefaultAccessPolicyAllow,
			ETag:                    1,
			OrderInAccessRulesList:  11,
			TotalGroupSpeedLimitBps: 1_000_000_000,
			UserGroupSpeedLimitBps:  1_000_000,
		},
	}

	for i := range accessGroups {
		if _, err := db.Model(&accessGroups[i]).Insert(); err != nil {
			ts.FailNow("Can not create init data", err.Error())
		}
	}

	// ExternalGroup.
	var extGroups = []ExternalGroup{
		{
			AccessGroupID:               accessGroups[0].ID,
			ETag:                        1,
			Name:                        "external group 1 at source 1",
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[0].ID,
		},
		{
			AccessGroupID:               accessGroups[0].ID,
			ETag:                        1,
			Name:                        "external group 2 at source 1",
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[0].ID,
		},
		{
			AccessGroupID:               accessGroups[1].ID,
			ETag:                        1,
			Name:                        "external group 1 at source 2",
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[1].ID,
		},
		{
			ETag:                        1,
			Name:                        "external group 2 at source 2",
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[1].ID,
		},
	}

	for i := range extGroups {
		if _, err := db.Model(&extGroups[i]).Insert(); err != nil {
			ts.FailNow("Can not create init data", err.Error())
		}
	}
}

func (ts *TestExternalGroupV1sSuite) TearDownTest() {
	if ts.db != nil {
		if err := ts.db.Close(); err != nil {
			ts.FailNow("Can not Close DB", err)
		}
	}
}

func (ts *TestExternalGroupV1sSuite) TestGet() {
	var tests = []struct {
		filter []storage.ExternalGroupFilterV1
		source storage.ResourceNameT

		expectedResult []storage.ExternalGroupV1
	}{
		{
			source: "source 1",
			filter: []storage.ExternalGroupFilterV1{
				{
					Name: "external group 1 at source 1",
				},
			},
			expectedResult: []storage.ExternalGroupV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 1",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalGroupKind,
						Name: "external group 1 at source 1",
					},
					Data: storage.ExternalGroupDataV1{
						AccessGroupName: "access group 1",
					},
				},
			},
		},
		{ // Filter contains names of groups from different sources.
			// Must return only a group from "source 1".
			source: "source 1",
			filter: []storage.ExternalGroupFilterV1{
				{
					Name: "external group 1 at source 1",
				},
				{
					Name: "external group 1 at source 2",
				},
			},
			expectedResult: []storage.ExternalGroupV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 1",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalGroupKind,
						Name: "external group 1 at source 1",
					},
					Data: storage.ExternalGroupDataV1{
						AccessGroupName: "access group 1",
					},
				},
			},
		},
		{
			source: "source 1",
			filter: []storage.ExternalGroupFilterV1{
				{
					Name: "external group 1 at source 1",
				},
				{
					Name: "external group 2 at source 1",
				},
			},
			expectedResult: []storage.ExternalGroupV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 1",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalGroupKind,
						Name: "external group 1 at source 1",
					},
					Data: storage.ExternalGroupDataV1{
						AccessGroupName: "access group 1",
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 1",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalGroupKind,
						Name: "external group 2 at source 1",
					},
					Data: storage.ExternalGroupDataV1{
						AccessGroupName: "access group 1",
					},
				},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.sqlStorage.GetExternalGroupV1sContext(context.TODO(),
			test.filter, test.source)

		ts.NoError(gotErr, "Unexpected error")

		if !ts.NotNil(got, "nil result") {
			ts.FailNow("nil result - can not continue")
		}

		// Sort.
		sort.Slice(test.expectedResult, func(i, j int) bool {
			return test.expectedResult[i].Metadata.Name < test.expectedResult[j].Metadata.Name // nolint:scopelint
		})
		sort.Slice(got, func(i, j int) bool {
			return got[i].Metadata.Name < got[j].Metadata.Name
		})

		ts.Equal(test.expectedResult, got, "Unexpected result")
	}
}

func (ts *TestExternalGroupV1sSuite) TestCreateSuccess() {
	var tests = []struct {
		groups []storage.ExternalGroupV1
		source storage.ResourceNameT

		getCreated bool
	}{
		{
			source:     "source 1",
			getCreated: true,
			groups: []storage.ExternalGroupV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1,
						ExternalResource: true,
						Kind:             storage.ExternalGroupKind,
						Name:             "external group 5 at source 1",
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 1",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
					},
					Data: storage.ExternalGroupDataV1{
						AccessGroupName: "access group 1",
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1,
						ExternalResource: true,
						Kind:             storage.ExternalGroupKind,
						Name:             "external group 6 at source 1",
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 1",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
					},
					Data: storage.ExternalGroupDataV1{},
				},
			},
		},
		{
			source:     "source 2",
			getCreated: false,
			groups: []storage.ExternalGroupV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1,
						ExternalResource: true,
						Kind:             storage.ExternalGroupKind,
						Name:             "external group 5 at source 2",
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 2",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
					},
					Data: storage.ExternalGroupDataV1{
						AccessGroupName: "access group 1",
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1,
						ExternalResource: true,
						Kind:             storage.ExternalGroupKind,
						Name:             "external group 6 at source 2",
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 2",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
					},
					Data: storage.ExternalGroupDataV1{},
				},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.sqlStorage.CreateExternalGroupV1sContext(context.TODO(),
			test.groups, test.getCreated)

		ts.NoError(gotErr, "Unexpected error", gotErr)

		// Check existence.
		for _, g := range test.groups {
			var dbGroup ExternalGroup

			if err := ts.db.Model(&dbGroup).Relation("AccessGroup").Relation("ExternalUsersGroupsSource").
				Where("external_group.name = ? AND external_users_groups_source.name = ?",
					g.Metadata.Name, test.source).Select(); err != nil {
				ts.FailNow("Can not load a Group from DB", err)
			}

			ts.Equal(g.Metadata.Name, dbGroup.Name)
			ts.Equal(g.Metadata.ExternalSource.SourceName, dbGroup.ExternalUsersGroupsSource.Name)
			ts.Equal(g.Metadata.ETag, dbGroup.ETag)
			ts.Equal(g.Data.AccessGroupName, dbGroup.AccessGroup.Name)
		}

		if !test.getCreated {
			continue
		}

		ts.NotNil(got, "Unexpected nil result")
		ts.Equal(test.groups, got, "Unexpected result")
	}
}

func (ts *TestExternalGroupV1sSuite) TestErrorNoSource() {
	var tests = []struct {
		groups []storage.ExternalGroupV1

		expectedError error
	}{
		{
			// Source does not exist.
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       0,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       "not created source 2",
				},
			),
			groups: []storage.ExternalGroupV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1,
						ExternalResource: true,
						Kind:             storage.ExternalGroupKind,
						Name:             "external group 5 at source 2",
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "not created source 2",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
					},
					Data: storage.ExternalGroupDataV1{
						AccessGroupName: "access group 1",
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1,
						ExternalResource: true,
						Kind:             storage.ExternalGroupKind,
						Name:             "external group 6 at source 2",
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 2",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
					},
					Data: storage.ExternalGroupDataV1{},
				},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.sqlStorage.CreateExternalGroupV1sContext(context.TODO(), test.groups, true)

		ts.Nil(got, "result must be nil")

		ts.Error(gotErr, "must return error")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalGroupV1sSuite) TestErrorNoAccessGroup() {
	var tests = []struct {
		groups []storage.ExternalGroupV1

		expectedError error
	}{
		{
			// AccessGroup does not exist.
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       0,
					Kind:       storage.AccessGroupKind,
					Name:       "not created access group 1",
				},
			),
			groups: []storage.ExternalGroupV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1,
						ExternalResource: true,
						Kind:             storage.ExternalGroupKind,
						Name:             "external group 5 at source 2",
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 2",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
					},
					Data: storage.ExternalGroupDataV1{
						AccessGroupName: "not created access group 1",
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1,
						ExternalResource: true,
						Kind:             storage.ExternalGroupKind,
						Name:             "external group 6 at source 2",
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 2",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
					},
					Data: storage.ExternalGroupDataV1{},
				},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.sqlStorage.CreateExternalGroupV1sContext(context.TODO(), test.groups, true)

		ts.Nil(got, "result must be nil")

		ts.Error(gotErr, "must return error")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalGroupV1sSuite) TestErrorConflict() {
	var tests = []struct {
		groups []storage.ExternalGroupV1

		expectedError error
	}{
		{
			// Name conflict.
			expectedError: storage.NewErrResourceAlreadyExists([]interface{}{
				storage.ExternalGroupV1{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1,
						ExternalResource: true,
						Kind:             storage.ExternalGroupKind,
						Name:             "external group 1 at source 2", // Conflict.
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 2",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
					},
					Data: storage.ExternalGroupDataV1{
						AccessGroupName: "access group 1",
					},
				},
			}),
			groups: []storage.ExternalGroupV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1,
						ExternalResource: true,
						Kind:             storage.ExternalGroupKind,
						Name:             "external group 1 at source 2", // Conflict.
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 2",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
					},
					Data: storage.ExternalGroupDataV1{
						AccessGroupName: "access group 1",
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1,
						ExternalResource: true,
						Kind:             storage.ExternalGroupKind,
						Name:             "external group 6 at source 2",
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 2",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
					},
					Data: storage.ExternalGroupDataV1{},
				},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.sqlStorage.CreateExternalGroupV1sContext(context.TODO(), test.groups, true)

		ts.Nil(got, "result must be nil")

		ts.Error(gotErr, "must return error")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalGroupV1sSuite) TestUpdateSuccess() {
	var tests = []struct {
		patch []storage.ExternalGroupV1

		getResult bool
	}{
		{
			patch: []storage.ExternalGroupV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 1",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalGroupKind,
						Name: "external group 1 at source 1",
					},
					Data: storage.ExternalGroupDataV1{
						AccessGroupName: "access group 2",
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 1",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalGroupKind,
						Name: "external group 2 at source 1",
					},
					Data: storage.ExternalGroupDataV1{
						AccessGroupName: "access group 1",
					},
				},
			},
			getResult: false,
		},
		{
			patch: []storage.ExternalGroupV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 2",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalGroupKind,
						Name: "external group 1 at source 2",
					},
					Data: storage.ExternalGroupDataV1{},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 2",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalGroupKind,
						Name: "external group 2 at source 2",
					},
					Data: storage.ExternalGroupDataV1{
						AccessGroupName: "access group 2",
					},
				},
			},
			getResult: true,
		},
	}

	for _, test := range tests {
		got, gotErr := ts.sqlStorage.UpdateExternalGroupV1sContext(context.TODO(), test.patch, test.getResult)

		ts.NoError(gotErr, "Unexpected error")

		// Check update.
		for i := range test.patch {
			var dbGroup ExternalGroup

			if err := ts.db.Model(&dbGroup).Where(
				"external_group.name = ? AND external_users_groups_source.name = ?",
				test.patch[i].Metadata.Name, test.patch[i].Metadata.ExternalSource.SourceName).
				Relation("AccessGroup").Relation("ExternalUsersGroupsSource").Select(); err != nil {
				ts.FailNow("Can not load an ExternalGroup from DB: ", err)
			}

			ts.Equal(test.patch[i].Metadata.Name, dbGroup.Name)
			ts.Equal(test.patch[i].Metadata.ETag+1, dbGroup.ETag)
			ts.Equal(test.patch[i].Metadata.ExternalSource.SourceName, dbGroup.ExternalUsersGroupsSource.Name)
			ts.Equal(test.patch[i].Data.AccessGroupName, dbGroup.AccessGroup.Name)

			// Check result.
			if !test.getResult {
				ts.Nil(got, "must return nil result")

				continue
			}

			if !ts.NotNil(got, "nil result") {
				ts.FailNow("Can not continue - nil result")
			}

			test.patch[i].Metadata.ETag++ // To make equal with updated value.
			ts.Equal(test.patch[i], got[i], "Unexpected result")
		}
	}
}

func (ts *TestExternalGroupV1sSuite) TestDeleteSuccess() {
	var tests = []struct {
		group storage.ExternalGroupV1
	}{
		{
			group: storage.ExternalGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: "external group 1 at source 1",
				},
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.sqlStorage.DeleteExternalGroupV1Context(context.TODO(), &test.group)

		if !ts.NoError(gotErr) {
			ts.FailNow("error. Can not continue")
		}

		// Check that a group is deleted.
		exists, err := ts.db.Model((*ExternalGroup)(nil)).Relation("ExternalUsersGroupsSource").
			Where("external_group.name = ? AND external_users_groups_source.name = ?",
				test.group.Metadata.Name, test.group.Metadata.ExternalSource.SourceName).Exists()
		if err != nil {
			ts.FailNow("Can not load an ExternalGroup from DB")
		}

		ts.False(exists, "ExternalGroup exists must be false")
	}
}

func (ts *TestExternalGroupV1sSuite) TestDeleteNotFound() {
	var tests = []struct {
		groups []storage.ExternalGroupV1
	}{
		{
			groups: []storage.ExternalGroupV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 1",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalGroupKind,
						Name: "non existing external group 1 at source 1",
					},
				},
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.sqlStorage.DeleteExternalGroupV1sContext(context.TODO(), test.groups)

		if !ts.Error(gotErr) {
			ts.FailNow("no error. Can not continue")
		}

		ts.Equal(storage.NewErrResourceNotFound(&test.groups[0].Metadata), gotErr, "Unexpected error")
	}
}

func (ts *TestExternalGroupV1sSuite) TestDeleteETagNotMatch() {
	var tests = []struct {
		groups []storage.ExternalGroupV1

		expectedError error
	}{
		{
			expectedError: storage.NewErrETagDoesNotMatch(1, 2),
			groups: []storage.ExternalGroupV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             2,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 1",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalGroupKind,
						Name: "external group 1 at source 1",
					},
				},
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.sqlStorage.DeleteExternalGroupV1sContext(context.TODO(), test.groups)

		if !ts.Error(gotErr) {
			ts.FailNow("no error. Can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error")
	}
}

func TestExternalGroupV1s(t *testing.T) {
	suite.Run(t, &TestExternalGroupV1sSuite{})
}

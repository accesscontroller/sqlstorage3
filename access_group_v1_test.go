package sqlstorage3

import (
	"context"
	"testing"
	"time"

	"github.com/go-pg/pg/v10"
	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

type TestAccessGroupSuite struct {
	db *pg.DB

	sqlStorage *SQLStorage

	suite.Suite
}

func (ts *TestAccessGroupSuite) checkHelper(groupsToDelete []struct {
	group               storage.AccessGroupV1
	expectedError       error
	isRecordExistsAfter bool
}) {
	for i := range groupsToDelete {
		gd := &groupsToDelete[i]

		got := ts.sqlStorage.DeleteAccessGroupV1Context(context.TODO(), &gd.group)

		// Check error.
		ts.Assert().Equal(gd.expectedError, got, "Unexpected result")

		// Try to lookup.
		var lookupM = &AccessGroup{}

		exists, err := ts.db.Model(lookupM).Where("name = ?", gd.group.Metadata.Name).Exists()
		if err != nil {
			ts.FailNow("Unexpected DB error", err)
		}

		ts.Assert().Equal(gd.isRecordExistsAfter, exists, "Unexpected Record Existence status")
	}
}

func (ts *TestAccessGroupSuite) SetupTest() {
	db, err := connectDB()
	if err != nil {
		ts.FailNow("Can not connect to test DB", err)
	}

	ts.db = db
	ts.sqlStorage = &SQLStorage{db: db}

	// Create DB Tables.
	if err := recreateTables(ts.db); err != nil {
		ts.FailNow("Can not initialize DB", err)
	}

	// Create some init data.
	var groups = []*AccessGroup{
		{
			Name:          "access group 1",
			DefaultPolicy: storage.DefaultAccessPolicyAllow,
			WebResourceCategorys: []*WebResourceCategory{
				{
					Name: "web resource category 1",
				},
			},
			ETag: 1,
			ExternalGroups: []*ExternalGroup{
				{
					Name: "external group 1 at source 1",
					ExternalUsersGroupsSource: ExternalUsersGroupsSource{
						Name: "source 1",
					},
				},
			},
			OrderInAccessRulesList:  10,
			TotalGroupSpeedLimitBps: -1,
			UserGroupSpeedLimitBps:  -1,
		},
		{
			Name:          "access group 2",
			DefaultPolicy: storage.DefaultAccessPolicyAllow,
			WebResourceCategorys: []*WebResourceCategory{
				{
					Name: "web resource category 1",
				},
			},
			ETag: 1,
			ExternalGroups: []*ExternalGroup{
				{
					Name: "external group 1 at source 1",
					ExternalUsersGroupsSource: ExternalUsersGroupsSource{
						Name: "source 1",
					},
				},
			},
			OrderInAccessRulesList:  11,
			TotalGroupSpeedLimitBps: -1,
			UserGroupSpeedLimitBps:  -1,
		},
		{
			Name:          "access group 3",
			DefaultPolicy: storage.DefaultAccessPolicyAllow,
			WebResourceCategorys: []*WebResourceCategory{
				{
					Name: "web resource category 1",
				},
			},
			ETag: 1,
			ExternalGroups: []*ExternalGroup{
				{
					Name: "external group 1 at source 1",
					ExternalUsersGroupsSource: ExternalUsersGroupsSource{
						Name: "source 1",
					},
				},
			},
			OrderInAccessRulesList:  12,
			TotalGroupSpeedLimitBps: -1,
			UserGroupSpeedLimitBps:  -1,
		},
	}

	for _, ag := range groups {
		if _, err := db.Model(ag).Insert(); err != nil {
			ts.FailNow("Can not create init data", err.Error())
		}
	}
}

func (ts *TestAccessGroupSuite) TearDownTest() {
	if ts.db != nil {
		if err := ts.db.Close(); err != nil {
			ts.FailNow("Can not Close DB", err)
		}
	}
}

func (ts *TestAccessGroupSuite) TestDeleteSuccess() {
	var groupsToDelete = []struct {
		group               storage.AccessGroupV1
		expectedError       error
		isRecordExistsAfter bool
	}{
		{
			// Success.
			group: storage.AccessGroupV1{
				Metadata: storage.MetadataV1{
					ETag: 1,
					Name: "access group 1",
				},
				Data: storage.AccessGroupDataV1{},
			},
			expectedError:       nil,
			isRecordExistsAfter: false,
		},
	}

	ts.checkHelper(groupsToDelete)
}

func (ts *TestAccessGroupSuite) TestDeleteNotFound() {
	var groupsToDelete = []struct {
		group               storage.AccessGroupV1
		expectedError       error
		isRecordExistsAfter bool
	}{
		{
			// Not found.
			group: storage.AccessGroupV1{
				Metadata: storage.MetadataV1{
					ETag: 1,
					Name: "non-existing access group 2",
				},
				Data: storage.AccessGroupDataV1{},
			},
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					ETag: 1,
					Name: "non-existing access group 2",
				},
			),
			isRecordExistsAfter: false,
		},
	}

	ts.checkHelper(groupsToDelete)
}

func (ts *TestAccessGroupSuite) TestDeleteETagError() {
	var groupsToDelete = []struct {
		group               storage.AccessGroupV1
		expectedError       error
		isRecordExistsAfter bool
	}{
		{
			// Incorrect ETag.
			group: storage.AccessGroupV1{
				Metadata: storage.MetadataV1{
					ETag: 10,
					Name: "access group 1",
				},
				Data: storage.AccessGroupDataV1{},
			},
			expectedError:       storage.NewErrETagDoesNotMatch(1, 10),
			isRecordExistsAfter: true,
		},
	}

	ts.checkHelper(groupsToDelete)
}

func (ts *TestAccessGroupSuite) TestGetSuccess() {
	var tests = []struct {
		Name          storage.ResourceNameT
		ExpectedGroup storage.AccessGroupV1
	}{
		{
			Name: "access group 1",
			ExpectedGroup: storage.AccessGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.AccessGroupKind,
					Name:       "access group 1",
				},
				Data: storage.AccessGroupDataV1{
					DefaultPolicy:           storage.DefaultAccessPolicyAllow,
					ExtraAccessDenyMessage:  "",
					OrderInAccessRulesList:  10,
					TotalGroupSpeedLimitBps: -1,
					UserGroupSpeedLimitBps:  -1,
				},
			},
		},
	}

	for i := range tests {
		test := tests[i]

		got, gotErr := ts.sqlStorage.GetAccessGroupV1Context(context.TODO(), test.Name)

		ts.Assert().NoError(gotErr, "Unexpected error")

		if !ts.Assert().NotNil(got) {
			ts.FailNow("got nil")
		}

		ts.Assert().Equal(test.ExpectedGroup, *got, "Unexpected result")
	}
}

func (ts *TestAccessGroupSuite) TestGetNotFound() {
	var tests = []struct {
		Name storage.ResourceNameT
	}{
		{
			Name: "access group 10",
		},
	}

	for _, test := range tests {
		got, gotErr := ts.sqlStorage.GetAccessGroupV1Context(context.TODO(), test.Name)

		ts.Assert().Nil(got, "must be nil")
		ts.Assert().Error(gotErr, "must be error")
		ts.Assert().IsType(&storage.ErrResourceNotFound{}, gotErr, "Unexpected error type")
	}
}

func (ts *TestAccessGroupSuite) TestCreateSuccess() {
	var tests = []struct {
		group storage.AccessGroupV1

		getResult bool
	}{
		{
			group: storage.AccessGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.AccessGroupKind,
					Name:       "access group 12",
				},
				Data: storage.AccessGroupDataV1{
					DefaultPolicy:           storage.DefaultAccessPolicyAllow,
					ExtraAccessDenyMessage:  "deny msg",
					OrderInAccessRulesList:  103,
					TotalGroupSpeedLimitBps: 12343,
					UserGroupSpeedLimitBps:  32,
				},
			},
			getResult: false,
		},
		{
			group: storage.AccessGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       12,
					Kind:       storage.AccessGroupKind,
					Name:       "access group 13",
				},
				Data: storage.AccessGroupDataV1{
					DefaultPolicy:           storage.DefaultAccessPolicyAllow,
					ExtraAccessDenyMessage:  "deny msg",
					OrderInAccessRulesList:  105,
					TotalGroupSpeedLimitBps: 12343,
					UserGroupSpeedLimitBps:  32,
				},
			},
			getResult: true,
		},
	}

	for _, test := range tests {
		got, gotErr := ts.sqlStorage.CreateAccessGroupV1Context(context.TODO(), &test.group, test.getResult)

		ts.NoError(gotErr, "Unexpected error")

		// Check existence.
		var dbm AccessGroup
		exists, err := ts.db.Model(&dbm).Where("name = ?", test.group.Metadata.Name).Exists()

		ts.NoError(err, "DB query error")
		ts.True(exists, "No record in DB")

		if test.getResult {
			ts.NotNil(got, "nil result")

			ts.Equal(test.group, *got, "Unexpected result")
		}
	}
}

func (ts *TestAccessGroupSuite) TestCreateError() {
	var tests = []struct {
		group storage.AccessGroupV1

		expectedError error
	}{
		{ // Name conflict
			group: storage.AccessGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.AccessGroupKind,
					Name:       "access group 1",
				},
				Data: storage.AccessGroupDataV1{
					DefaultPolicy:           storage.DefaultAccessPolicyAllow,
					ExtraAccessDenyMessage:  "deny msg",
					OrderInAccessRulesList:  103,
					TotalGroupSpeedLimitBps: 12343,
					UserGroupSpeedLimitBps:  32,
				},
			},
			expectedError: &storage.ErrResourceAlreadyExists{},
		},
		{ // Order conflict
			group: storage.AccessGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.AccessGroupKind,
					Name:       "access group 223",
				},
				Data: storage.AccessGroupDataV1{
					DefaultPolicy:           storage.DefaultAccessPolicyAllow,
					ExtraAccessDenyMessage:  "deny msg",
					OrderInAccessRulesList:  10,
					TotalGroupSpeedLimitBps: 12343,
					UserGroupSpeedLimitBps:  32,
				},
			},
			expectedError: &storage.ErrResourceAlreadyExists{},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.CreateAccessGroupV1Context(context.TODO(), &test.group, false)

		ts.Error(gotErr, "nil error")
		ts.Nil(got, "not nil result")
		ts.IsType(test.expectedError, gotErr, "Wrong error type")
	}
}

func (ts *TestAccessGroupSuite) TestUpdateSuccess() {
	var tests = []struct {
		oldName   storage.ResourceNameT
		group     storage.AccessGroupV1
		getResult bool
	}{
		{
			oldName:   "access group 1",
			getResult: false,
			group: storage.AccessGroupV1{
				Metadata: storage.MetadataV1{
					Name:       "renamed access group 1",
					ETag:       1,
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroupKind,
				},
				Data: storage.AccessGroupDataV1{
					DefaultPolicy:           storage.DefaultAccessPolicyDeny,
					ExtraAccessDenyMessage:  "new access deny message",
					OrderInAccessRulesList:  1000,
					TotalGroupSpeedLimitBps: 874365,
					UserGroupSpeedLimitBps:  7643,
				},
			},
		},
		{
			oldName:   "access group 2",
			getResult: true,
			group: storage.AccessGroupV1{
				Metadata: storage.MetadataV1{
					Name:       "renamed access group 2",
					ETag:       1,
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroupKind,
				},
				Data: storage.AccessGroupDataV1{
					DefaultPolicy:           storage.DefaultAccessPolicyDeny,
					ExtraAccessDenyMessage:  "new access deny message",
					OrderInAccessRulesList:  1001,
					TotalGroupSpeedLimitBps: 874365,
					UserGroupSpeedLimitBps:  7643,
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotError := ts.sqlStorage.UpdateAccessGroupV1Context(context.TODO(),
			test.oldName, &test.group, test.getResult)

		ts.NoError(gotError, "unexpected error")

		// Check Update.
		var dbAG AccessGroup
		if err := ts.sqlStorage.db.Model(&dbAG).
			Where("name = ?", test.group.Metadata.Name).Select(); err != nil {
			ts.FailNow("DB Select error", err)
		}

		ts.Equal(test.group.Metadata.Name, dbAG.Name)
		ts.Equal(test.group.Metadata.ETag+1, dbAG.ETag)
		ts.Equal(test.group.Data.DefaultPolicy, dbAG.DefaultPolicy)
		ts.Equal(test.group.Data.ExtraAccessDenyMessage, dbAG.ExtraAccessDenyMessage)
		ts.Equal(test.group.Data.OrderInAccessRulesList, dbAG.OrderInAccessRulesList)
		ts.Equal(test.group.Data.TotalGroupSpeedLimitBps, dbAG.TotalGroupSpeedLimitBps)
		ts.Equal(test.group.Data.UserGroupSpeedLimitBps, dbAG.UserGroupSpeedLimitBps)

		ts.WithinDuration(time.Now(), dbAG.UpdatedAt, time.Minute)

		if test.getResult {
			test.group.Metadata.ETag++
			ts.Equal(test.group, *got, "unexpected result")
		}
	}
}

func (ts *TestAccessGroupSuite) TestUpdateErrorConflict() {
	var tests = []struct {
		oldName       storage.ResourceNameT
		group         storage.AccessGroupV1
		expectedError error
	}{
		{ // Name conflict.
			oldName: "access group 1",
			group: storage.AccessGroupV1{
				Metadata: storage.MetadataV1{
					Name:       "access group 2",
					ETag:       1,
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroupKind,
				},
				Data: storage.AccessGroupDataV1{
					DefaultPolicy:           storage.DefaultAccessPolicyDeny,
					ExtraAccessDenyMessage:  "new access deny message",
					OrderInAccessRulesList:  1000,
					TotalGroupSpeedLimitBps: 874365,
					UserGroupSpeedLimitBps:  7643,
				},
			},
			expectedError: storage.NewErrResourceAlreadyExists([]interface{}{
				storage.AccessGroupV1{
					Metadata: storage.MetadataV1{
						Name:       "access group 2",
						ETag:       1,
						APIVersion: storage.APIVersionV1Value,
						Kind:       storage.AccessGroupKind,
					},
					Data: storage.AccessGroupDataV1{
						DefaultPolicy:           storage.DefaultAccessPolicyDeny,
						ExtraAccessDenyMessage:  "new access deny message",
						OrderInAccessRulesList:  1000,
						TotalGroupSpeedLimitBps: 874365,
						UserGroupSpeedLimitBps:  7643,
					},
				},
			}),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotError := ts.sqlStorage.UpdateAccessGroupV1Context(context.TODO(),
			test.oldName, &test.group, true)

		ts.Nil(got, "must return nil result")

		if !ts.Error(gotError, "must return error") {
			ts.FailNow("nil error - can not continue")
		}

		ts.Equal(test.expectedError, gotError, "Unexpected error value")
	}
}

func (ts *TestAccessGroupSuite) TestUpdateNotFound() {
	var tests = []struct {
		oldName       storage.ResourceNameT
		group         storage.AccessGroupV1
		expectedError error
	}{
		{
			oldName: "not found access group 1",
			group: storage.AccessGroupV1{
				Metadata: storage.MetadataV1{
					Name:       "access group 2",
					ETag:       1,
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroupKind,
				},
				Data: storage.AccessGroupDataV1{
					DefaultPolicy:           storage.DefaultAccessPolicyDeny,
					ExtraAccessDenyMessage:  "new access deny message",
					OrderInAccessRulesList:  1000,
					TotalGroupSpeedLimitBps: 874365,
					UserGroupSpeedLimitBps:  7643,
				},
			},
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					Name:       "not found access group 1",
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroupKind,
				},
			),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotError := ts.sqlStorage.UpdateAccessGroupV1Context(context.TODO(),
			test.oldName, &test.group, true)

		ts.Nil(got, "must return nil result")

		if !ts.Error(gotError, "must return error") {
			ts.FailNow("nil error - can not continue")
		}

		ts.Equal(test.expectedError, gotError, "Unexpected error value")
	}
}

func TestAccessGroup(t *testing.T) {
	suite.Run(t, &TestAccessGroupSuite{})
}

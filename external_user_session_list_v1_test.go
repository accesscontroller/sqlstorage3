package sqlstorage3

import (
	"context"
	"net"
	"testing"
	"time"

	"github.com/go-pg/pg/v10"
	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

type TestExternalUserSessionV1sSuite struct {
	db *pg.DB

	sqlStorage *SQLStorage

	suite.Suite

	openTimes  map[storage.ResourceNameT]*time.Time // key is a session name.
	closeTimes map[storage.ResourceNameT]*time.Time // key is a session name.
}

func (ts *TestExternalUserSessionV1sSuite) SetupTest() {
	ts.openTimes = make(map[storage.ResourceNameT]*time.Time)
	ts.closeTimes = make(map[storage.ResourceNameT]*time.Time)

	db, err := connectDB()
	if err != nil {
		ts.FailNow("Can not connect to test DB", err)
	}

	ts.db = db
	ts.sqlStorage = &SQLStorage{db: db}

	// Create DB Tables.
	if err := recreateTables(ts.db); err != nil {
		ts.FailNow("Can not initialize DB", err)
	}

	// Create some init data.

	// ExternalUsersGroupsSource.
	var externalUsersGroupsSources = []ExternalUsersGroupsSource{
		{
			ETag:              1,
			GetMode:           storage.ExternalUsersGroupsGetModePassive,
			GroupsGetSettings: "get groups settings",
			GroupsToLoad:      []string{"group1", "group2"},
			UsersGetSettings:  "get users settings",
			Type:              storage.ExternalUsersGroupsSourceTypeLDAP,
			Name:              "source 1",
			PollInterval:      121,
		},
		{
			ETag:              1,
			GetMode:           storage.ExternalUsersGroupsGetModePassive,
			GroupsGetSettings: "get groups settings",
			GroupsToLoad:      []string{"group1", "group2"},
			UsersGetSettings:  "get users settings",
			Type:              storage.ExternalUsersGroupsSourceTypeLDAP,
			Name:              "source 2",
			PollInterval:      122,
		},
	}

	for i := range externalUsersGroupsSources {
		if _, err := db.Model(&externalUsersGroupsSources[i]).Insert(); err != nil {
			ts.FailNow("Can not create init data", err.Error())
		}
	}

	// Session Sources.
	var sessionSources = []ExternalSessionsSource{
		{
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[0].ID,
			ETag:                        1,
			GetMode:                     storage.ExternalSessionsGetModePassive,
			Name:                        "sessions source 1",
			Settings:                    []byte(`{"Name": "source 1", "Key1": "Value1", "Key2": "Value2"}`),
			Type:                        storage.ExternalSessionsSourceTypeMSADEvents,
		},
		{
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[1].ID,
			ETag:                        2,
			GetMode:                     storage.ExternalSessionsGetModePoll,
			Name:                        "sessions source 2",
			Settings:                    []byte(`{"Name": "source 2", "Key2": "Value2", "Key3": "Value3"}`),
			Type:                        storage.ExternalSessionsSourceTypeMSADEvents,
		},
	}

	for i := range sessionSources {
		if _, err := db.Model(&sessionSources[i]).Insert(); err != nil {
			ts.FailNow("Can not create init data", err.Error())
		}
	}

	// External Users.
	var externalUsers = []ExternalUser{
		{
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[0].ID,
			ETag:                        1,
			Name:                        "user 1 at source 1",
		},
		{
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[0].ID,
			ETag:                        1,
			Name:                        "user 2 at source 1",
		},
		{
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[1].ID,
			ETag:                        1,
			Name:                        "user 1 at source 2",
		},
		{
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[1].ID,
			ETag:                        1,
			Name:                        "user 2 at source 2",
		},
	}

	for i := range externalUsers {
		if _, err := db.Model(&externalUsers[i]).Insert(); err != nil {
			ts.FailNow("Can not insert ExternalUser", err)
		}
	}

	// External User Sessions.
	var externalUserSessions = []ExternalUserSession{
		{
			ExternalSessionsSourceID: sessionSources[0].ID,
			ETag:                     1,
			ExternalUserID:           externalUsers[0].ID,
			HostName:                 "hostname-1",
			IPAddress:                net.ParseIP("10.0.0.1"),
			Name:                     "session 1 source 1",
			OpenTimestamp:            time.Now().Truncate(time.Microsecond),
		},
		{
			ExternalSessionsSourceID: sessionSources[1].ID,
			ETag:                     1,
			ExternalUserID:           externalUsers[1].ID,
			HostName:                 "hostname-2",
			IPAddress:                net.ParseIP("10.0.0.2"),
			Name:                     "session 1 source 2",
			OpenTimestamp:            time.Now().Truncate(time.Microsecond),
		},
		{
			ExternalSessionsSourceID: sessionSources[0].ID,
			ETag:                     1,
			ExternalUserID:           externalUsers[0].ID,
			HostName:                 "hostname-3",
			IPAddress:                net.ParseIP("10.0.0.3"),
			Name:                     "session 2 source 1",
			OpenTimestamp:            time.Now().Truncate(time.Microsecond),
		},
		{
			ExternalSessionsSourceID: sessionSources[1].ID,
			ETag:                     1,
			ExternalUserID:           externalUsers[1].ID,
			HostName:                 "hostname-1",
			IPAddress:                net.ParseIP("10.0.0.4"),
			Name:                     "closed session 2 source 2",
			OpenTimestamp:            time.Now().Add(-time.Hour).Truncate(time.Microsecond),
			CloseTimestamp:           time.Now().Truncate(time.Microsecond),
			Closed:                   true,
		},
	}

	for i := range externalUserSessions {
		ps := &externalUserSessions[i]

		ts.openTimes[ps.Name] = &ps.OpenTimestamp

		if ps.Closed {
			ts.closeTimes[ps.Name] = &ps.CloseTimestamp
		}

		if _, err := db.Model(ps).Insert(); err != nil {
			ts.FailNow("Can not insert ExternalUserSession", err)
		}
	}
}

func (ts *TestExternalUserSessionV1sSuite) TearDownTest() {
	if ts.db != nil {
		if err := ts.db.Close(); err != nil {
			ts.FailNow("Can not Close DB", err)
		}
	}
}

func (ts *TestExternalUserSessionV1sSuite) TestGetSessions() {
	var tests = []struct {
		source storage.ResourceNameT

		filters []storage.ExternalUserSessionFilterV1

		expectedSessions []storage.ExternalUserSessionV1
	}{
		{
			source: "sessions source 2",
			filters: []storage.ExternalUserSessionFilterV1{
				{
					Closed: func() *bool { var b = true; return &b }(),
				},
			},
			expectedSessions: []storage.ExternalUserSessionV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "sessions source 2",
							Kind:       storage.ExternalSessionsSourceKind,
						},
						Kind: storage.ExternalUserSessionKind,
						Name: "closed session 2 source 2",
					},
					Data: storage.ExternalUserSessionDataV1{
						Closed:                        true,
						CloseTimestamp:                ts.closeTimes["closed session 2 source 2"],
						ExternalUserName:              "user 2 at source 1",
						ExternalUsersGroupsSourceName: "source 1",
						Hostname:                      "hostname-1",
						IPAddress:                     net.ParseIP("10.0.0.4"),
						OpenTimestamp:                 ts.openTimes["closed session 2 source 2"],
					},
				},
			},
		},
		{
			source: "sessions source 2",
			filters: []storage.ExternalUserSessionFilterV1{
				{
					Name: "session 1 source 2",
				},
			},
			expectedSessions: []storage.ExternalUserSessionV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "sessions source 2",
							Kind:       storage.ExternalSessionsSourceKind,
						},
						Kind: storage.ExternalUserSessionKind,
						Name: "session 1 source 2",
					},
					Data: storage.ExternalUserSessionDataV1{
						Closed:                        false,
						CloseTimestamp:                ts.closeTimes["session 1 source 2"],
						ExternalUserName:              "user 2 at source 1",
						ExternalUsersGroupsSourceName: "source 1",
						Hostname:                      "hostname-2",
						IPAddress:                     net.ParseIP("10.0.0.2"),
						OpenTimestamp:                 ts.openTimes["session 1 source 2"],
					},
				},
			},
		},
		{
			source: "sessions source 2",
			filters: []storage.ExternalUserSessionFilterV1{
				{
					Closed: func() *bool { var b = true; return &b }(),
				},
			},
			expectedSessions: []storage.ExternalUserSessionV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "sessions source 2",
							Kind:       storage.ExternalSessionsSourceKind,
						},
						Kind: storage.ExternalUserSessionKind,
						Name: "closed session 2 source 2",
					},
					Data: storage.ExternalUserSessionDataV1{
						CloseTimestamp:                ts.closeTimes["closed session 2 source 2"],
						OpenTimestamp:                 ts.openTimes["closed session 2 source 2"],
						ExternalUserName:              "user 2 at source 1",
						ExternalUsersGroupsSourceName: "source 1",
						Hostname:                      "hostname-1",
						Closed:                        true,
						IPAddress:                     net.ParseIP("10.0.0.4"),
					},
				},
			},
		},
	}

	for i := range tests {
		pt := &tests[i]

		got, gotErr := ts.sqlStorage.GetExternalUserSessionV1sContext(context.TODO(),
			pt.source, pt.filters)

		ts.NoError(gotErr, "must not return error")

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("nil result can not continue")
		}

		ts.Equal(pt.expectedSessions, got, "Unexpected result")
	}
}

func (ts *TestExternalUserSessionV1sSuite) TestOpenSuccess() {
	var openTimes = map[string]time.Time{
		"new session 3 at source 1": time.Now(),
		"new session 4 at source 1": time.Now(),
		"new session 5 at source 1": time.Now(),
		"new session 6 at source 1": time.Now(),
	}

	var tests = []struct {
		getResult bool

		sessionsToOpen []storage.ExternalUserSessionV1
	}{
		{
			getResult: true,
			sessionsToOpen: []storage.ExternalUserSessionV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "sessions source 1",
							Kind:       storage.ExternalSessionsSourceKind,
						},
						Kind: storage.ExternalUserSessionKind,
						Name: "new session 3 at source 1",
					},
					Data: storage.ExternalUserSessionDataV1{
						ExternalUserName:              "user 1 at source 1",
						ExternalUsersGroupsSourceName: "source 1",
						Hostname:                      "hostname-10",
						IPAddress:                     net.ParseIP("10.1.0.1"),
						OpenTimestamp: func() *time.Time {
							var t = openTimes["new session 3 at source 1"]
							return &t
						}(),
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "sessions source 1",
							Kind:       storage.ExternalSessionsSourceKind,
						},
						Kind: storage.ExternalUserSessionKind,
						Name: "new session 4 at source 1",
					},
					Data: storage.ExternalUserSessionDataV1{
						ExternalUserName:              "user 1 at source 1",
						ExternalUsersGroupsSourceName: "source 1",
						Hostname:                      "hostname-10",
						IPAddress:                     net.ParseIP("10.1.0.2"),
						OpenTimestamp: func() *time.Time {
							var t = openTimes["new session 4 at source 1"]
							return &t
						}(),
					},
				},
			},
		},
		{
			getResult: false,
			sessionsToOpen: []storage.ExternalUserSessionV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "sessions source 1",
							Kind:       storage.ExternalSessionsSourceKind,
						},
						Kind: storage.ExternalUserSessionKind,
						Name: "new session 5 at source 1",
					},
					Data: storage.ExternalUserSessionDataV1{
						ExternalUserName:              "user 2 at source 1",
						ExternalUsersGroupsSourceName: "source 1",
						Hostname:                      "hostname-10",
						IPAddress:                     net.ParseIP("10.1.0.5"),
						OpenTimestamp: func() *time.Time {
							var t = openTimes["new session 5 at source 1"]
							return &t
						}(),
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "sessions source 1",
							Kind:       storage.ExternalSessionsSourceKind,
						},
						Kind: storage.ExternalUserSessionKind,
						Name: "new session 6 at source 1",
					},
					Data: storage.ExternalUserSessionDataV1{
						ExternalUserName:              "user 2 at source 1",
						ExternalUsersGroupsSourceName: "source 1",
						Hostname:                      "hostname-10",
						IPAddress:                     net.ParseIP("10.1.0.6"),
						OpenTimestamp: func() *time.Time {
							var t = openTimes["new session 6 at source 1"]
							return &t
						}(),
					},
				},
			},
		},
	}

	for i := range tests {
		pt := &tests[i]

		got, gotErr := ts.sqlStorage.OpenExternalUserSessionV1sContext(context.TODO(),
			pt.sessionsToOpen, pt.getResult)

		if !ts.NoError(gotErr, "Unexpected error") {
			ts.FailNow("error - can not continue")
		}

		// Check created.
		for expSInd := range pt.sessionsToOpen {
			var expS = &pt.sessionsToOpen[expSInd]

			var dbModel ExternalUserSession

			if err := ts.db.Model(&dbModel).
				Relation("ExternalSessionsSource").
				Relation("ExternalUser.name").Relation("ExternalUser.ExternalUsersGroupsSource.name").
				Where("external_user_session.name = ? AND external_sessions_source.name = ?",
					expS.Metadata.Name, expS.Metadata.ExternalSource.SourceName).
				Select(); err != nil {
				ts.FailNow("Can not get value from DB: ", err)
			}

			// Compare values.
			ts.Equal(expS.Metadata.Name, dbModel.Name)
			ts.Equal(expS.Metadata.ETag, dbModel.ETag)
			ts.True(expS.Metadata.ExternalResource)
			ts.Equal(expS.Metadata.ExternalSource.SourceName, dbModel.ExternalSessionsSource.Name)
			ts.Nil(expS.Data.CloseTimestamp, nil)
			ts.False(expS.Data.Closed)
			ts.Equal(expS.Data.ExternalUserName, dbModel.ExternalUser.Name)
			ts.Equal(expS.Data.ExternalUsersGroupsSourceName, dbModel.ExternalUser.ExternalUsersGroupsSource.Name)
			ts.Equal(expS.Data.Hostname, dbModel.HostName)
			ts.Equal(expS.Data.IPAddress, dbModel.IPAddress)
			ts.WithinDuration(*expS.Data.OpenTimestamp, dbModel.OpenTimestamp, time.Second)

			// Check result.
			if pt.getResult {
				if !ts.NotNil(got, "Must not return nil result") {
					ts.FailNow("nil result - can not continue")
				}

				ts.Equal(*expS, got[expSInd], "Unexpected value")
			}
		}
	}
}

func (ts *TestExternalUserSessionV1sSuite) TestOpenConflictName() {
	var tests = []struct {
		sessionsToOpen []storage.ExternalUserSessionV1

		expectedError error
	}{
		{
			expectedError: storage.NewErrResourceAlreadyExists([]interface{}{
				storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "sessions source 1",
						Kind:       storage.ExternalSessionsSourceKind,
					},
					Kind: storage.ExternalUserSessionKind,
					Name: "session 1 source 1",
				},
			}),
			sessionsToOpen: []storage.ExternalUserSessionV1{{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "sessions source 1",
						Kind:       storage.ExternalSessionsSourceKind,
					},
					Kind: storage.ExternalUserSessionKind,
					Name: "session 1 source 1",
				},
				Data: storage.ExternalUserSessionDataV1{
					ExternalUserName:              "user 2 at source 1",
					ExternalUsersGroupsSourceName: "source 1",
					Hostname:                      "hostname-10",
					IPAddress:                     net.ParseIP("10.1.0.2"),
					OpenTimestamp: func() *time.Time {
						var t = time.Now()
						return &t
					}(),
				},
			}},
		},
	}

	for i := range tests {
		pt := &tests[i]

		got, gotErr := ts.sqlStorage.OpenExternalUserSessionV1sContext(context.TODO(),
			pt.sessionsToOpen, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("nil error - can not continue")
		}

		ts.Nil(got, "Must return nil as result")

		ts.Equal(pt.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalUserSessionV1sSuite) TestOpenConflictIP() {
	var tests = []struct {
		sessionsToOpen []storage.ExternalUserSessionV1

		expectedError error
	}{
		{
			expectedError: storage.NewErrResourceAlreadyExists([]interface{}{
				storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "sessions source 1",
						Kind:       storage.ExternalSessionsSourceKind,
					},
					Kind: storage.ExternalUserSessionKind,
					Name: "new session 3 source 1",
				},
			}),
			sessionsToOpen: []storage.ExternalUserSessionV1{{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "sessions source 1",
						Kind:       storage.ExternalSessionsSourceKind,
					},
					Kind: storage.ExternalUserSessionKind,
					Name: "new session 3 source 1",
				},
				Data: storage.ExternalUserSessionDataV1{
					ExternalUserName:              "user 2 at source 1",
					ExternalUsersGroupsSourceName: "source 1",
					Hostname:                      "hostname-10",
					IPAddress:                     net.ParseIP("10.0.0.1"), // Conflict IP.
					OpenTimestamp: func() *time.Time {
						var t = time.Now()
						return &t
					}(),
				},
			}},
		},
	}

	for i := range tests {
		pt := &tests[i]

		got, gotErr := ts.sqlStorage.OpenExternalUserSessionV1sContext(context.TODO(),
			pt.sessionsToOpen, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("nil error - can not continue")
		}

		ts.Nil(got, "Must return nil as result")

		ts.Equal(pt.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalUserSessionV1sSuite) TestOpenSourceNotFound() {
	var tests = []struct {
		sessionsToOpen []storage.ExternalUserSessionV1

		expectedError error
	}{
		{
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       0,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "non existing sessions source 1",
				},
			),
			sessionsToOpen: []storage.ExternalUserSessionV1{{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "non existing sessions source 1", // Does not exist.
						Kind:       storage.ExternalSessionsSourceKind,
					},
					Kind: storage.ExternalUserSessionKind,
					Name: "new session 3 source 1",
				},
				Data: storage.ExternalUserSessionDataV1{
					ExternalUserName:              "user 2 at source 1",
					ExternalUsersGroupsSourceName: "source 1",
					Hostname:                      "hostname-10",
					IPAddress:                     net.ParseIP("10.1.0.1"),
					OpenTimestamp: func() *time.Time {
						var t = time.Now()
						return &t
					}(),
				},
			}},
		},
	}

	for i := range tests {
		pt := &tests[i]

		got, gotErr := ts.sqlStorage.OpenExternalUserSessionV1sContext(context.TODO(),
			pt.sessionsToOpen, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("nil error - can not continue")
		}

		ts.Nil(got, "Must return nil as result")

		ts.Equal(pt.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalUserSessionV1sSuite) TestOpenExternalUserNotFound() {
	var tests = []struct {
		sessionsToOpen []storage.ExternalUserSessionV1

		expectedError error
	}{
		{
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             0,
					Kind:             storage.ExternalUserKind,
					Name:             "non existing external user 1",
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
				},
			),
			sessionsToOpen: []storage.ExternalUserSessionV1{{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "sessions source 1",
						Kind:       storage.ExternalSessionsSourceKind,
					},
					Kind: storage.ExternalUserSessionKind,
					Name: "new session 3 source 1",
				},
				Data: storage.ExternalUserSessionDataV1{
					ExternalUserName:              "non existing external user 1", // Does not exist.
					ExternalUsersGroupsSourceName: "source 1",
					Hostname:                      "hostname-10",
					IPAddress:                     net.ParseIP("10.1.0.1"),
					OpenTimestamp: func() *time.Time {
						var t = time.Now()
						return &t
					}(),
				},
			}},
		},
	}

	for i := range tests {
		pt := &tests[i]

		got, gotErr := ts.sqlStorage.OpenExternalUserSessionV1sContext(context.TODO(),
			pt.sessionsToOpen, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("nil error - can not continue")
		}

		ts.Nil(got, "Must return nil as result")

		ts.Equal(pt.expectedError, gotErr, "Unexpected error value")
	}
}

func TestExternalUserSessionV1s(t *testing.T) {
	suite.Run(t, &TestExternalUserSessionV1sSuite{})
}

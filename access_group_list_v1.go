package sqlstorage3

import (
	"context"

	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

// AccessGroupV1sGetter - Returns AccessGroupV1s using given filter.
func (s *SQLStorage) GetAccessGroupV1sContext(ctx context.Context,
	flt []storage.AccessGroupFilterV1) ([]storage.AccessGroupV1, error) {
	var result []storage.AccessGroupV1

	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		var (
			groups AccessGroupSlice

			q = tx.Model(&groups)
		)

		for i := range flt {
			f := &flt[i]

			q = q.WhereOrGroup(func(q *orm.Query) (*orm.Query, error) {
				if !isZero(f.Name) {
					q = q.Where("name = ?", f.Name)
				}

				if !isZero(f.DefaultPolicy) {
					q = q.Where("default_policy = ?", f.DefaultPolicy)
				}

				if !isZero(f.OrderInAccessRulesList) {
					q = q.Where("order_in_access_rules_list  = ?", f.OrderInAccessRulesList)
				}

				return q, nil
			})
		}

		if err := q.Select(); err != nil {
			return err
		}

		result = groups.toStorageAccessGroups()

		return nil
	}); err != nil {
		return nil, err
	}

	return result, nil
}

func storageAccessGroupV1sToModel(src []storage.AccessGroupV1) AccessGroupSlice {
	var result = make(AccessGroupSlice, 0, len(src))

	for i := range src {
		p := &src[i]

		result = append(result, storageAccessGroupV1ToModel(p))
	}

	return result
}

// CreateAccessGroupV1sContext - Creates multiple AccessGroupRules from AccessGroupV1s.
func (s *SQLStorage) CreateAccessGroupV1sContext(ctx context.Context,
	groups []storage.AccessGroupV1, getCreated bool) ([]storage.AccessGroupV1, error) {
	var result []storage.AccessGroupV1

	// Create.
	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		var models = storageAccessGroupV1sToModel(groups)
		q := tx.Model(&models)

		if getCreated {
			q = q.Returning("*")
		}

		if _, err := q.Insert(); err != nil {
			typedE, ok := err.(pg.Error)
			if ok {
				if typedE.IntegrityViolation() {
					return storage.NewErrResourceAlreadyExists(
						makeAccessGroupV1sConflicts(groups),
						typedE,
					)
				}
			}

			return err
		}

		if getCreated {
			result = models.toStorageAccessGroups()
		}

		return nil
	}); err != nil {
		return nil, err
	}

	return result, nil
}

func makeAccessGroupV1sConflicts(groups []storage.AccessGroupV1) []interface{} {
	var conflicts = make([]interface{}, len(groups))

	for i := range groups {
		conflicts[i] = groups[i]
	}

	return conflicts
}

package sqlstorage3

import (
	"context"

	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func (s *SQLStorage) GetExternalUserV1sContext(ctx context.Context, filters []storage.ExternalUserFilterV1,
	sourceName storage.ResourceNameT) ([]storage.ExternalUserV1, error) {
	var models ExternalUserSlice

	// Make filter.
	var q = s.db.ModelContext(ctx, &models).Relation("ExternalUsersGroupsSource").
		Where("external_users_groups_source.name = ?", sourceName)

	q = q.WhereGroup(func(wGSQ *orm.Query) (*orm.Query, error) {
		// WhereGroupSubQuery.
		for i := range filters {
			flt := &filters[i]

			// WhereOrSubQuery.
			wGSQ = wGSQ.WhereOrGroup(func(wOSQ *orm.Query) (*orm.Query, error) {
				if !isZero(flt.Name) {
					wOSQ = wOSQ.Where("external_user.name = ?", flt.Name)
				}

				if !isZero(flt.NameSubstringMatch) {
					wOSQ = wOSQ.Where("external_user.name LIKE ?", "%"+flt.NameSubstringMatch+"%")
				}

				return wOSQ, nil
			})
		}
		return wGSQ, nil
	})

	// Load.
	if err := q.Select(); err != nil {
		return nil, err
	}

	return models.toStorageExternalUsers(), nil
}

func (s *SQLStorage) CreateExternalUserV1sContext(ctx context.Context,
	users []storage.ExternalUserV1, getCreated bool) ([]storage.ExternalUserV1, error) {
	if len(users) == 0 {
		return nil, nil
	}

	var models ExternalUserSlice

	if getCreated {
		models = make(ExternalUserSlice, 0, len(users))
	}

	var firstSource = users[0].Metadata.ExternalSource.SourceName

	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		for i := range users {
			u := &users[i]

			// Check Source.
			if u.Metadata.ExternalSource.SourceName != firstSource {
				return storage.NewErrIncorrectExternalSource(firstSource, u.Metadata)
			}

			m, err := createExternalUserV1Tx(tx, u)
			if err != nil {
				return err
			}

			if getCreated {
				models = append(models, m)
			}
		}

		return nil
	}); err != nil {
		return nil, err
	}

	if getCreated {
		return models.toStorageExternalUsers(), nil
	}

	return nil, nil
}

func (s *SQLStorage) DeleteExternalUserV1sContext(
	ctx context.Context, users []storage.ExternalUserV1) error {
	return s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		for i := range users {
			u := &users[i]

			if err := deleteExternalUserV1Tx(tx, u); err != nil {
				return err
			}
		}

		return nil
	})
}

package sqlstorage3

import (
	"context"
	"testing"
	"time"

	"github.com/go-pg/pg/v10"
	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

type TestExternalGroupV1Suite struct {
	db *pg.DB

	sqlStorage *SQLStorage

	suite.Suite
}

func (ts *TestExternalGroupV1Suite) SetupTest() {
	db, err := connectDB()
	if err != nil {
		ts.FailNow("Can not connect to test DB", err)
	}

	ts.db = db
	ts.sqlStorage = &SQLStorage{db: db}

	// Create DB Tables.
	if err := recreateTables(ts.db); err != nil {
		ts.FailNow("Can not initialize DB", err)
	}

	// Create some init data.

	// ExternalUsersGroupsSource.
	var externalUsersGroupsSources = []ExternalUsersGroupsSource{
		{
			ETag:              1,
			GetMode:           storage.ExternalUsersGroupsGetModePassive,
			GroupsGetSettings: "get groups settings",
			GroupsToLoad:      []string{"group1", "group2"},
			UsersGetSettings:  "get users settings",
			Type:              storage.ExternalUsersGroupsSourceTypeLDAP,
			Name:              "source 1",
			PollInterval:      121,
		},
		{
			ETag:              1,
			GetMode:           storage.ExternalUsersGroupsGetModePassive,
			GroupsGetSettings: "get groups settings",
			GroupsToLoad:      []string{"group1", "group2"},
			UsersGetSettings:  "get users settings",
			Type:              storage.ExternalUsersGroupsSourceTypeLDAP,
			Name:              "source 2",
			PollInterval:      122,
		},
	}

	for i := range externalUsersGroupsSources {
		if _, err := db.Model(&externalUsersGroupsSources[i]).Insert(); err != nil {
			ts.FailNow("Can not create init data", err.Error())
		}
	}

	// AccessGroup.
	var accessGroups = []AccessGroup{
		{
			Name:                    "access group 1",
			DefaultPolicy:           storage.DefaultAccessPolicyAllow,
			ETag:                    1,
			OrderInAccessRulesList:  10,
			TotalGroupSpeedLimitBps: -1,
			UserGroupSpeedLimitBps:  -1,
		},
		{
			Name:                    "access group 2",
			DefaultPolicy:           storage.DefaultAccessPolicyAllow,
			ETag:                    1,
			OrderInAccessRulesList:  11,
			TotalGroupSpeedLimitBps: 1_000_000_000,
			UserGroupSpeedLimitBps:  1_000_000,
		},
	}

	for i := range accessGroups {
		if _, err := db.Model(&accessGroups[i]).Insert(); err != nil {
			ts.FailNow("Can not create init data", err.Error())
		}
	}

	// ExternalGroup.
	var extGroups = []ExternalGroup{
		{
			ID:                          11102,
			AccessGroupID:               accessGroups[0].ID,
			ETag:                        1,
			Name:                        "external group 1 at source 1",
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[0].ID,
		},
		{
			AccessGroupID:               accessGroups[0].ID,
			ETag:                        1,
			Name:                        "external group 2 at source 1",
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[0].ID,
		},
		{
			AccessGroupID:               accessGroups[1].ID,
			ETag:                        1,
			Name:                        "external group 1 at source 2",
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[1].ID,
		},
	}

	for i := range extGroups {
		if _, err := db.Model(&extGroups[i]).Insert(); err != nil {
			ts.FailNow("Can not create init data", err.Error())
		}
	}

	// ExternalUser.
	var externalUsers = ExternalUserSlice{
		{
			ID:                          121,
			ETag:                        10,
			Name:                        "user 1 at source 1",
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[0].ID,
		},
	}

	for _, u := range externalUsers {
		if _, err := db.Model(u).Insert(); err != nil {
			ts.FailNowf("Can not create init data: ExternalUsers",
				"Name: %s, Error: %s", u.Name, err)
		}
	}

	// Make relations.
	var extUser2Groups = []*ExternalUserToExternalGroup{
		{
			ExternalGroupID: 11102,
			ExternalUserID:  121,
		},
	}

	for _, rel := range extUser2Groups {
		if _, err := db.Model(rel).Insert(); err != nil {
			ts.FailNowf("Can not create init data: ExternalUser to ExternalGroup relation",
				"Relation: %+v, Error: %s", rel, err)
		}
	}
}

func (ts *TestExternalGroupV1Suite) TearDownTest() {
	if ts.db != nil {
		if err := ts.db.Close(); err != nil {
			ts.FailNow("Can not Close DB", err)
		}
	}
}

func (ts *TestExternalGroupV1Suite) TestGetSuccess() {
	var tests = []struct {
		name   storage.ResourceNameT
		source storage.ResourceNameT

		expectedGroup storage.ExternalGroupV1
	}{
		{
			name:   "external group 1 at source 1",
			source: "source 1",
			expectedGroup: storage.ExternalGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: "external group 1 at source 1",
				},
				Data: storage.ExternalGroupDataV1{
					AccessGroupName: "access group 1",
				},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.sqlStorage.GetExternalGroupV1Context(context.TODO(), test.name, test.source)

		ts.NoError(gotErr, "unexpected error")

		if !ts.NotNil(got) {
			ts.FailNow("nil result - can not continue")
		}

		ts.Equal(test.expectedGroup, *got, "Unexpected result")
	}
}

func (ts *TestExternalGroupV1Suite) TestGetNotFound() {
	var tests = []struct {
		name   storage.ResourceNameT
		source storage.ResourceNameT

		expectedError error
	}{
		{ // No a Group with a name in a Source.
			name:          "external group 1 at source 1",
			source:        "source 2",
			expectedError: &storage.ErrResourceNotFound{},
		},
		{ // No a Group at all.
			name:          "non existing external group 1 at source 1",
			source:        "source 1",
			expectedError: &storage.ErrResourceNotFound{},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.sqlStorage.GetExternalGroupV1Context(context.TODO(), test.name, test.source)

		ts.Error(gotErr, "must be error error")

		ts.Nil(got, "must be nil result")

		ts.IsType(test.expectedError, gotErr, "Unexpected error type")
	}
}

func (ts *TestExternalGroupV1Suite) TestCreateSuccess() {
	var tests = []struct {
		group storage.ExternalGroupV1

		getCreated bool
	}{
		{
			group: storage.ExternalGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: "external group 3 at source 3",
				},
				Data: storage.ExternalGroupDataV1{
					AccessGroupName: "access group 1",
				},
			},
			getCreated: true,
		},
		{
			group: storage.ExternalGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 2",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: "external group 3 at source 2",
				},
				Data: storage.ExternalGroupDataV1{},
			},
		},
		{
			group: storage.ExternalGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 2",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: "external group 4 at source 2",
				},
				Data: storage.ExternalGroupDataV1{},
			},
			getCreated: true,
		},
	}

	for _, test := range tests {
		got, gotErr := ts.sqlStorage.CreateExternalGroupV1Context(context.TODO(), &test.group, test.getCreated)

		ts.NoError(gotErr, "Unexpected error")

		// Check existence.
		var dbGroup ExternalGroup

		if err := ts.db.Model(&dbGroup).Relation("AccessGroup").Relation("ExternalUsersGroupsSource").
			Where("external_group.name = ? AND external_users_groups_source.name = ?",
				test.group.Metadata.Name, test.group.Metadata.ExternalSource.SourceName).Select(); err != nil {
			ts.FailNow("Can not select an ExternalGroup from DB")
		}

		ts.Equal(test.group.Metadata.Name, dbGroup.Name)
		ts.Equal(test.group.Metadata.ETag, dbGroup.ETag)
		ts.Equal(test.group.Metadata.ExternalSource.SourceName, dbGroup.ExternalUsersGroupsSource.Name)
		ts.Equal(test.group.Data.AccessGroupName, dbGroup.AccessGroup.Name)

		if !test.getCreated {
			continue
		}

		if ts.NotNil(got, "Unexpected nil result") {
			ts.Equal(test.group, *got, "Unexpected result")
		}
	}
}

func (ts *TestExternalGroupV1Suite) TestUpdateSuccess() {
	var tests = []struct {
		oldName storage.ResourceNameT

		patch storage.ExternalGroupV1

		getResult bool
	}{
		{
			oldName: "external group 1 at source 1",
			patch: storage.ExternalGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: "renamed external group 1 at source 1",
				},
				Data: storage.ExternalGroupDataV1{
					AccessGroupName: "access group 2",
				},
			},
		},
		{
			oldName: "external group 1 at source 2",
			patch: storage.ExternalGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 2",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: "renamed external group 1 at source 2",
				},
				Data: storage.ExternalGroupDataV1{
					AccessGroupName: "access group 1",
				},
			},
			getResult: true,
		},
		{
			oldName: "external group 2 at source 1",
			patch: storage.ExternalGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: "renamed external group 2 at source 1",
				},
				Data: storage.ExternalGroupDataV1{},
			},
			getResult: true,
		},
	}

	for _, test := range tests {
		got, gotErr := ts.sqlStorage.UpdateExternalGroupV1Context(context.TODO(), test.oldName, &test.patch, test.getResult)

		ts.NoError(gotErr, "Unexpected error")

		// Check update.
		var dbGroup ExternalGroup

		if err := ts.db.Model(&dbGroup).Where(
			"external_group.name = ? AND external_users_groups_source.name = ?",
			test.patch.Metadata.Name, test.patch.Metadata.ExternalSource.SourceName).
			Relation("AccessGroup").Relation("ExternalUsersGroupsSource").Select(); err != nil {
			ts.FailNow("Can not load an ExternalGroup from DB: ", err)
		}

		ts.Equal(test.patch.Metadata.Name, dbGroup.Name)
		ts.Equal(test.patch.Metadata.ETag+1, dbGroup.ETag)
		ts.Equal(test.patch.Metadata.ExternalSource.SourceName, dbGroup.ExternalUsersGroupsSource.Name)
		ts.Equal(test.patch.Data.AccessGroupName, dbGroup.AccessGroup.Name)
		ts.WithinDuration(time.Now(), dbGroup.UpdatedAt, time.Minute)

		// Check result.
		if !test.getResult {
			ts.Nil(got, "must return nil result")

			continue
		}

		if !ts.NotNil(got, "nil result") {
			ts.FailNow("Can not continue - nil result")
		}

		test.patch.Metadata.ETag++ // To make equal with updated value.
		ts.Equal(test.patch, *got, "Unexpected result")
	}
}

func (ts *TestExternalGroupV1Suite) TestUpdateGroupNotFoundError() {
	var tests = []struct {
		oldName storage.ResourceNameT

		patch storage.ExternalGroupV1

		expectedError error

		TestName string
	}{
		{ // Group not found.
			TestName: "ExternalGroup not found",
			oldName:  "not found external group 1 at source 1",
			patch: storage.ExternalGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: "renamed external group 1 at source 1",
				},
				Data: storage.ExternalGroupDataV1{
					AccessGroupName: "access group 2",
				},
			},
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             0,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: "not found external group 1 at source 1",
				},
			),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.UpdateExternalGroupV1Context(context.TODO(), test.oldName, &test.patch, true)

		ts.Errorf(gotErr, "must return error", "Test Name %s", test.TestName)
		ts.Nilf(got, "must return nil result", "Test Name %s", test.TestName)

		ts.Equalf(test.expectedError, gotErr, "unexpected error value", "Test Name %s", test.TestName)
	}
}

func (ts *TestExternalGroupV1Suite) TestUpdateNewNameConflictError() {
	var tests = []struct {
		oldName storage.ResourceNameT

		patch storage.ExternalGroupV1

		expectedError error

		TestName string
	}{
		{ // New name conflict.
			TestName: "ExternalGroup Name conflict",
			oldName:  "external group 1 at source 1",
			patch: storage.ExternalGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: "external group 2 at source 1",
				},
				Data: storage.ExternalGroupDataV1{
					AccessGroupName: "access group 1",
				},
			},
			expectedError: storage.NewErrResourceAlreadyExists([]interface{}{
				storage.ExternalGroupV1{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 1",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalGroupKind,
						Name: "external group 2 at source 1",
					},
					Data: storage.ExternalGroupDataV1{
						AccessGroupName: "access group 1",
					},
				},
			}),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.UpdateExternalGroupV1Context(context.TODO(), test.oldName, &test.patch, true)

		ts.Errorf(gotErr, "must return error", "Test Name %s", test.TestName)
		ts.Nilf(got, "must return nil result", "Test Name %s", test.TestName)

		ts.Equalf(test.expectedError, gotErr, "unexpected error value", "Test Name %s", test.TestName)
	}
}

func (ts *TestExternalGroupV1Suite) TestUpdateETagError() {
	var tests = []struct {
		oldName storage.ResourceNameT

		patch storage.ExternalGroupV1

		expectedError error

		TestName string
	}{
		{
			// ETag does not match.
			TestName: "ETag does not match",
			oldName:  "external group 2 at source 1",
			patch: storage.ExternalGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             10,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: "renamed external group 2 at source 1",
				},
				Data: storage.ExternalGroupDataV1{},
			},
			expectedError: storage.NewErrETagDoesNotMatch(1, 10),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.UpdateExternalGroupV1Context(context.TODO(), test.oldName, &test.patch, true)

		ts.Errorf(gotErr, "must return error", "Test Name %s", test.TestName)
		ts.Nilf(got, "must return nil result", "Test Name %s", test.TestName)

		ts.Equalf(test.expectedError, gotErr, "unexpected error value", "Test Name %s", test.TestName)
	}
}

func (ts *TestExternalGroupV1Suite) TestUpdateAccessGroupNotFoundError() {
	var tests = []struct {
		oldName storage.ResourceNameT

		patch storage.ExternalGroupV1

		expectedError error

		TestName string
	}{
		{
			// AccessGroup not found.
			TestName: "AccessGroup not found",
			oldName:  "external group 2 at source 1",
			patch: storage.ExternalGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: "renamed external group 2 at source 1",
				},
				Data: storage.ExternalGroupDataV1{
					AccessGroupName: "non existing access group 1",
				},
			},
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: "v1",
					Kind:       "AccessGroup",
					Name:       "non existing access group 1",
					ETag:       0,
				},
			),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.UpdateExternalGroupV1Context(context.TODO(), test.oldName, &test.patch, true)

		ts.Errorf(gotErr, "must return error", "Test Name %s", test.TestName)
		ts.Nilf(got, "must return nil result", "Test Name %s", test.TestName)

		ts.Equalf(test.expectedError, gotErr, "unexpected error value", "Test Name %s", test.TestName)
	}
}

func (ts *TestExternalGroupV1Suite) TestDeleteSuccess() {
	var tests = []struct {
		group storage.ExternalGroupV1
	}{
		{
			group: storage.ExternalGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: "external group 1 at source 1",
				},
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.sqlStorage.DeleteExternalGroupV1Context(context.TODO(), &test.group)

		if !ts.NoError(gotErr) {
			ts.FailNow("error. Can not continue")
		}

		// Check that a group is deleted.
		exists, err := ts.db.Model((*ExternalGroup)(nil)).Relation("ExternalUsersGroupsSource").
			Where("external_group.name = ? AND external_users_groups_source.name = ?",
				test.group.Metadata.Name, test.group.Metadata.ExternalSource.SourceName).Exists()
		if err != nil {
			ts.FailNow("Can not load an ExternalGroup from DB")
		}

		ts.False(exists, "ExternalGroup exists must be false")
	}
}

func (ts *TestExternalGroupV1Suite) TestDeleteSuccessCheckUsersETagUpdatedAt() {
	var tests = []struct {
		group storage.ExternalGroupV1

		expectedUserETags map[storage.ResourceNameT]storage.ETagT
	}{
		{
			group: storage.ExternalGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: "external group 1 at source 1",
				},
			},

			expectedUserETags: map[storage.ResourceNameT]storage.ETagT{
				"user 1 at source 1": 11,
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.sqlStorage.DeleteExternalGroupV1Context(context.TODO(), &test.group)

		if !ts.NoError(gotErr) {
			ts.FailNow("error. Can not continue")
		}

		// Check ETag and UpdatedAt.
		// Load Users.
		var userNames = make([]storage.ResourceNameT, 0, len(test.expectedUserETags))

		for name := range test.expectedUserETags {
			userNames = append(userNames, name)
		}

		var dbUsers ExternalUserSlice

		if err := ts.db.Model(&dbUsers).Column("external_user.name", "external_user.e_tag").
			Relation("ExternalUsersGroupsSource").
			Where("external_users_groups_source.name = ?", test.group.Metadata.ExternalSource.SourceName).
			WhereIn("external_user.name IN (?)", userNames).Select(); err != nil {
			ts.FailNowf("Can not load ExternalUsers from DB",
				"Source: %s, Names: %+v, Error: %s",
				test.group.Metadata.ExternalSource.SourceName,
				userNames, err)
		}

		// Check.
		for _, dbU := range dbUsers {
			ts.Equal(test.expectedUserETags[dbU.Name], dbU.ETag, "Unexpected Related ExternalUser ETag value",
				"Source: %s, User: %s", test.group.Metadata.ExternalSource.SourceName, dbU.Name)
		}
	}
}

func (ts *TestExternalGroupV1Suite) TestDeleteNotFound() {
	var tests = []struct {
		group storage.ExternalGroupV1
	}{
		{
			group: storage.ExternalGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: "non existing external group 1 at source 1",
				},
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.sqlStorage.DeleteExternalGroupV1Context(context.TODO(), &test.group)

		if !ts.Error(gotErr) {
			ts.FailNow("no error. Can not continue")
		}

		ts.Equal(storage.NewErrResourceNotFound(&test.group.Metadata), gotErr, "Unexpected error")
	}
}

func (ts *TestExternalGroupV1Suite) TestDeleteETagNotMatch() {
	var tests = []struct {
		group storage.ExternalGroupV1

		expectedError error
	}{
		{
			expectedError: storage.NewErrETagDoesNotMatch(1, 2),
			group: storage.ExternalGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             2,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: "external group 1 at source 1",
				},
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.sqlStorage.DeleteExternalGroupV1Context(context.TODO(), &test.group)

		if !ts.Error(gotErr) {
			ts.FailNow("no error. Can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error")
	}
}

func TestExternalGroupV1(t *testing.T) {
	suite.Run(t, &TestExternalGroupV1Suite{})
}

package sqlstorage3

import (
	"context"
	"sort"
	"testing"
	"time"

	"github.com/go-pg/pg/v10"
	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

type TestAccessGroupWebResourceCategoryListV1RelationsSuite struct {
	db *pg.DB

	sqlStorage *SQLStorage

	suite.Suite
}

func (ts *TestAccessGroupWebResourceCategoryListV1RelationsSuite) SetupTest() {
	db, err := connectDB()
	if err != nil {
		ts.FailNow("Can not connect to test DB", err)
	}

	ts.db = db
	ts.sqlStorage = &SQLStorage{db: db}

	// Create DB Tables.
	if err := recreateTables(ts.db); err != nil {
		ts.FailNow("Can not initialize DB", err)
	}

	// Create some init data.

	// Access Groups.
	var grps = []AccessGroup{
		{
			Name:                    "access group 1",
			DefaultPolicy:           storage.DefaultAccessPolicyAllow,
			ETag:                    1,
			OrderInAccessRulesList:  11,
			TotalGroupSpeedLimitBps: -1,
			UserGroupSpeedLimitBps:  -1,
		},
		{
			Name:                    "access group 2",
			DefaultPolicy:           storage.DefaultAccessPolicyAllow,
			ETag:                    1,
			OrderInAccessRulesList:  12,
			TotalGroupSpeedLimitBps: 1_000_000_000,
			UserGroupSpeedLimitBps:  1_000_000,
		},
		{
			Name:                    "access group 3",
			DefaultPolicy:           storage.DefaultAccessPolicyAllow,
			ETag:                    3,
			OrderInAccessRulesList:  13,
			TotalGroupSpeedLimitBps: 1_000_000_000,
			UserGroupSpeedLimitBps:  1_000_000,
		},
	}

	for i := range grps {
		gr := &grps[i]

		if _, err := db.Model(gr).Insert(); err != nil {
			ts.FailNow("Can not create init data", err.Error())
		}
	}

	// Web Resource Categories.
	var webResourceCats = WebResourceCategorySlice{
		{
			CreatedAt:   time.Now(),
			ETag:        1,
			Name:        "category 1",
			Description: "description category 1",
		},
		{
			CreatedAt:   time.Now(),
			ETag:        2,
			Name:        "category 2",
			Description: "description category 2",
		},
		{
			CreatedAt:   time.Now(),
			ETag:        3,
			Name:        "category 3",
			Description: "description category 3",
		},
		{
			CreatedAt:   time.Now(),
			ETag:        4,
			Name:        "category 4",
			Description: "description category 4",
		},
	}

	for _, cat := range webResourceCats {
		if _, err := db.Model(cat).Insert(); err != nil {
			ts.FailNow("Can not insert Test WebResourceCategory", err)
		}
	}

	// Bind WebResourceCategory to AccessGroup.
	var accessGroupWebResourceCats = []*WebResourceCategoryToAccessGroup{
		{
			AccessGroupID:         grps[0].ID,
			CreatedAt:             time.Now(),
			WebResourceCategoryID: webResourceCats[0].ID,
		},
		{
			AccessGroupID:         grps[0].ID,
			CreatedAt:             time.Now(),
			WebResourceCategoryID: webResourceCats[1].ID,
		},
		{
			AccessGroupID:         grps[1].ID,
			CreatedAt:             time.Now(),
			WebResourceCategoryID: webResourceCats[2].ID,
		},
	}

	for _, cB := range accessGroupWebResourceCats {
		if _, err := db.Model(cB).Insert(); err != nil {
			ts.FailNow("Can not Test Bind AccessGroup to WebResourceCategory", err)
		}
	}
}

func (ts *TestAccessGroupWebResourceCategoryListV1RelationsSuite) TearDownTest() {
	if ts.db != nil {
		if err := ts.db.Close(); err != nil {
			ts.FailNow("Can not Close DB", err)
		}
	}
}

func (ts *TestAccessGroupWebResourceCategoryListV1RelationsSuite) TestGetSuccess() {
	var tests = []struct {
		accessGroup storage.ResourceNameT

		expectedCategories []storage.ResourceNameT
	}{
		{
			accessGroup:        "access group 1",
			expectedCategories: []storage.ResourceNameT{"category 1", "category 2"},
		},
		{
			accessGroup:        "access group 3",
			expectedCategories: []storage.ResourceNameT{},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.GetAccessGroupWebResourceCategoryV1sContext(context.TODO(), test.accessGroup)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		ts.Equal(test.expectedCategories, got, "Unexpected result")
	}
}

func (ts *TestAccessGroupWebResourceCategoryListV1RelationsSuite) TestGetNoAccessGroup() {
	var tests = []struct {
		accessGroup storage.ResourceNameT

		expectedError error
	}{
		{
			accessGroup: "non existing access group 1",
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroupKind,
					Name:       "non existing access group 1",
				},
			),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.GetAccessGroupWebResourceCategoryV1sContext(context.TODO(), test.accessGroup)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestAccessGroupWebResourceCategoryListV1RelationsSuite) TestBindSuccess() {
	var tests = []struct {
		accessGroup storage.ResourceNameT

		webResourceCategories []storage.ResourceNameT

		expectedBindedWebResourceCategories []storage.ResourceNameT
	}{
		{
			accessGroup: "access group 2",
			webResourceCategories: []storage.ResourceNameT{
				"category 4",
				"category 2",
			},
			expectedBindedWebResourceCategories: []storage.ResourceNameT{
				"category 2",
				"category 3",
				"category 4",
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.sqlStorage.BindAccessGroup2WebResourceCategoryV1sContext(context.TODO(),
			test.accessGroup, test.webResourceCategories)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Load existing bindings.
		var accessGroup AccessGroup

		if err := ts.db.Model(&accessGroup).Relation("WebResourceCategorys").
			Where("access_group.name = ?", test.accessGroup).Select(); err != nil {
			ts.FailNow("Can not load an AccessGroup from DB", err)
		}

		var dbBindedWRCs = make([]storage.ResourceNameT, 0,
			len(accessGroup.WebResourceCategorys))

		for _, c := range accessGroup.WebResourceCategorys {
			dbBindedWRCs = append(dbBindedWRCs, c.Name)
		}

		// Compare bindings with expected.
		sort.Slice(test.expectedBindedWebResourceCategories, func(i, j int) bool {
			return test.expectedBindedWebResourceCategories[i] < test.expectedBindedWebResourceCategories[j]
		})

		sort.Slice(dbBindedWRCs, func(i, j int) bool {
			return dbBindedWRCs[i] < dbBindedWRCs[j]
		})

		ts.Equal(test.expectedBindedWebResourceCategories, dbBindedWRCs,
			"Unexpected binded WebResourceCategories slice")
	}
}

func (ts *TestAccessGroupWebResourceCategoryListV1RelationsSuite) TestBindNoAccessGroup() {
	var tests = []struct {
		accessGroup storage.ResourceNameT

		webResourceCategories []storage.ResourceNameT

		expectedError error
	}{
		{
			accessGroup: "not-found access group 2",
			webResourceCategories: []storage.ResourceNameT{
				"category 4",
				"category 2",
			},
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroupKind,
					Name:       "not-found access group 2",
				},
			),
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.sqlStorage.BindAccessGroup2WebResourceCategoryV1sContext(context.TODO(),
			test.accessGroup, test.webResourceCategories)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestAccessGroupWebResourceCategoryListV1RelationsSuite) TestBindNoWebResourceCategory() {
	var tests = []struct {
		accessGroup storage.ResourceNameT

		webResourceCategories []storage.ResourceNameT

		expectedError error
	}{
		{
			accessGroup: "access group 2",
			webResourceCategories: []storage.ResourceNameT{
				"not-existing category 4",
				"category 2",
			},
			expectedError: storage.NewErrRelatedResourcesNotFound(storage.APIVersionV1Value,
				storage.WebResourceCategoryKind, []storage.ResourceNameT{"not-existing category 4"}, nil),
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.sqlStorage.BindAccessGroup2WebResourceCategoryV1sContext(context.TODO(),
			test.accessGroup, test.webResourceCategories)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestAccessGroupWebResourceCategoryListV1RelationsSuite) TestUnbindSuccess() {
	var tests = []struct {
		accessGroup storage.ResourceNameT

		webResourceCategories []storage.ResourceNameT

		expectedBindedWebResourceCategories []storage.ResourceNameT
	}{
		{
			accessGroup: "access group 2",
			webResourceCategories: []storage.ResourceNameT{
				"category 4",
				"category 2",
			},
			expectedBindedWebResourceCategories: []storage.ResourceNameT{
				"category 3",
			},
		},
		{
			accessGroup: "access group 1",
			webResourceCategories: []storage.ResourceNameT{
				"category 4",
				"category 2",
			},
			expectedBindedWebResourceCategories: []storage.ResourceNameT{
				"category 1",
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.sqlStorage.UnBindAccessGroup2WebResourceCategoryV1sContext(context.TODO(),
			test.accessGroup, test.webResourceCategories)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Load existing bindings.
		var accessGroup AccessGroup

		if err := ts.db.Model(&accessGroup).Relation("WebResourceCategorys").
			Where("access_group.name = ?", test.accessGroup).Select(); err != nil {
			ts.FailNow("Can not load an AccessGroup from DB", err)
		}

		var dbBindedWRCs = make([]storage.ResourceNameT, 0,
			len(accessGroup.WebResourceCategorys))

		for _, c := range accessGroup.WebResourceCategorys {
			dbBindedWRCs = append(dbBindedWRCs, c.Name)
		}

		// Compare bindings with expected.
		sort.Slice(test.expectedBindedWebResourceCategories, func(i, j int) bool {
			return test.expectedBindedWebResourceCategories[i] < test.expectedBindedWebResourceCategories[j]
		})

		sort.Slice(dbBindedWRCs, func(i, j int) bool {
			return dbBindedWRCs[i] < dbBindedWRCs[j]
		})

		ts.Equal(test.expectedBindedWebResourceCategories, dbBindedWRCs,
			"Unexpected binded WebResourceCategories slice")
	}
}

func (ts *TestAccessGroupWebResourceCategoryListV1RelationsSuite) TestUnBindNoAccessGroup() {
	var tests = []struct {
		accessGroup storage.ResourceNameT

		webResourceCategories []storage.ResourceNameT

		expectedError error
	}{
		{
			accessGroup: "not-found access group 2",
			webResourceCategories: []storage.ResourceNameT{
				"category 4",
				"category 2",
			},
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroupKind,
					Name:       "not-found access group 2",
				},
			),
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.sqlStorage.UnBindAccessGroup2WebResourceCategoryV1sContext(context.TODO(),
			test.accessGroup, test.webResourceCategories)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestAccessGroupWebResourceCategoryListV1RelationsSuite) TestUnBindNoWebResourceCategory() {
	var tests = []struct {
		accessGroup storage.ResourceNameT

		webResourceCategories []storage.ResourceNameT

		expectedError error
	}{
		{
			accessGroup: "access group 2",
			webResourceCategories: []storage.ResourceNameT{
				"not-existing category 4",
				"category 2",
			},
			expectedError: storage.NewErrRelatedResourcesNotFound(storage.APIVersionV1Value,
				storage.WebResourceCategoryKind, []storage.ResourceNameT{"not-existing category 4"}, nil),
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.sqlStorage.UnBindAccessGroup2WebResourceCategoryV1sContext(context.TODO(),
			test.accessGroup, test.webResourceCategories)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func TestAccessGroupWebResourceCategoryListV1Relations(t *testing.T) {
	suite.Run(t, &TestAccessGroupWebResourceCategoryListV1RelationsSuite{})
}

type TestCheckNotAllWebResCategoriesLoadedErrorSuite struct {
	suite.Suite
}

func (ts *TestCheckNotAllWebResCategoriesLoadedErrorSuite) TestError() {
	var tests = []struct {
		required []storage.ResourceNameT

		loaded WebResourceCategorySlice

		expectedError error
	}{
		{
			required: []storage.ResourceNameT{"cat 1", "cat 2"},
			loaded: WebResourceCategorySlice{
				{
					Name: "cat 1",
				},
			},
			expectedError: storage.NewErrRelatedResourcesNotFound(storage.APIVersionV1Value,
				storage.WebResourceCategoryKind, []storage.ResourceNameT{"cat 2"}, nil),
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := checkNotAllWebResCategoriesLoadedError(test.required, test.loaded)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestCheckNotAllWebResCategoriesLoadedErrorSuite) TestNoError() {
	var tests = []struct {
		required []storage.ResourceNameT

		loaded WebResourceCategorySlice
	}{
		{
			required: []storage.ResourceNameT{"cat 1", "cat 2", "cat 1"},
			loaded: WebResourceCategorySlice{
				{
					Name: "cat 2",
				},
				{
					Name: "cat 1",
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := checkNotAllWebResCategoriesLoadedError(test.required, test.loaded)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}
	}
}

func TestCheckNotAllWebResCategoriesLoadedError(t *testing.T) {
	suite.Run(t, &TestCheckNotAllWebResCategoriesLoadedErrorSuite{})
}

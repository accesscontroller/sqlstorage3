package sqlstorage3

import (
	"context"
	"errors"
	"fmt"
	"net"
	"net/url"
	"time"

	"github.com/go-pg/pg/v10"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func (s *SQLStorage) GetWebResourceV1Context(
	ctx context.Context, category *storage.ResourceNameT,
	name storage.ResourceNameT) (*storage.WebResourceV1, error) {
	var (
		model = &WebResource{}

		q = s.db.ModelContext(ctx, model)
	)

	q = q.Relation("WebResourceCategorys")
	q = q.Relation("WebResourceIPs")
	q = q.Relation("WebResourceDomains")
	q = q.Relation("WebResourceURLs")

	q = q.Where("web_resource.name = ?", name)

	if err := q.Select(); err != nil {
		if errors.Is(err, pg.ErrNoRows) {
			return nil, storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.WebResourceKind,
					Name:       name,
				},
			)
		}

		return nil, err
	}

	// Filter by Category.
	if category != nil {
		var found bool

		for i := range model.WebResourceCategorys {
			if *category == model.WebResourceCategorys[i].Name {
				found = true

				break
			}
		}

		if !found {
			return nil, storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.WebResourceKind,
					Name:       name,
				},
			)
		}
	}

	return model.toStorageWebResource()
}

func insertDomainsTx(tx *pg.Tx, webResourceID int64, // nolint:dupl
	domains []storage.WebResourceDomainT) (WebResourceDomainSlice, error) {
	// Do nothing on empty.
	if len(domains) == 0 {
		return WebResourceDomainSlice{}, nil
	}

	var domainModels = make(WebResourceDomainSlice, len(domains))

	for i := range domains {
		domainModels[i] = &WebResourceDomain{
			WebResourceID: webResourceID,
			CreatedAt:     time.Now(),
			Domain:        domains[i],
		}
	}

	if _, err := tx.Model(&domainModels).Insert(); err != nil {
		if typedErr, ok := err.(pg.Error); ok {
			if typedErr.IntegrityViolation() {
				return nil, storage.NewErrResourceAlreadyExists([]interface{}{
					fmt.Sprintf("Domains %+v", domainModels),
				}, typedErr)
			}
		}

		return nil, err
	}

	return domainModels, nil
}

func insertIPsTx(tx *pg.Tx, webResourceID int64, ips []net.IP) (WebResourceIPSlice, error) { // nolint:dupl
	// Do nothing on empty.
	if len(ips) == 0 {
		return WebResourceIPSlice{}, nil
	}

	var IPModels = make(WebResourceIPSlice, len(ips))

	for i := range ips {
		IPModels[i] = &WebResourceIP{
			WebResourceID: webResourceID,
			CreatedAt:     time.Now(),
			IP:            ips[i],
		}
	}

	if _, err := tx.Model(&IPModels).Insert(); err != nil {
		if typedErr, ok := err.(pg.Error); ok {
			if typedErr.IntegrityViolation() {
				return nil, storage.NewErrResourceAlreadyExists([]interface{}{
					fmt.Sprintf("IPs %+v", IPModels),
				}, typedErr)
			}
		}

		return nil, err
	}

	return IPModels, nil
}

func insertURLsTx(tx *pg.Tx, webResourceID int64, urls []url.URL) (WebResourceURLSlice, error) {
	// Do nothing on empty.
	if len(urls) == 0 {
		return WebResourceURLSlice{}, nil
	}

	var URLModels = make(WebResourceURLSlice, len(urls))

	for i := range urls {
		URLModels[i] = &WebResourceURL{
			WebResourceID: webResourceID,
			CreatedAt:     time.Now(),
			URL:           urls[i].String(),
		}
	}

	if _, err := tx.Model(&URLModels).Insert(); err != nil {
		if typedErr, ok := err.(pg.Error); ok {
			if typedErr.IntegrityViolation() {
				var eURLs = make([]string, len(urls))
				for i := range urls {
					eURLs[i] = urls[i].String()
				}

				return nil, storage.NewErrResourceAlreadyExists([]interface{}{
					fmt.Sprintf("URLs %+v", eURLs),
				}, typedErr)
			}
		}

		return nil, err
	}

	return URLModels, nil
}

func createWebResourceV1Tx(tx *pg.Tx, resource *storage.WebResourceV1) (*WebResource, error) {
	// Insert resource.
	var model = &WebResource{}

	model.CreatedAt = time.Now()
	model.Description = resource.Data.Description
	model.ETag = resource.Metadata.ETag
	model.Name = resource.Metadata.Name

	if _, err := tx.Model(model).Insert(); err != nil {
		if typedErr, ok := err.(pg.Error); ok {
			if typedErr.IntegrityViolation() {
				return nil, storage.NewErrResourceAlreadyExists([]interface{}{
					resource.Metadata,
				}, typedErr)
			}
		}

		return nil, err
	}

	// Insert WebResourceCategories.
	if err := bindWebResourceCategoriesToWebResourceTx(
		tx, model, resource.Data.WebResourceCategoryNames); err != nil {
		return nil, err
	}

	// Reload Categories.
	dbCats, err := loadWebResourceCategoriesForWebResourceTx(tx, model.ID)
	if err != nil {
		return nil, err
	}

	model.WebResourceCategorys = dbCats

	// Insert Domains.
	dms, err := insertDomainsTx(tx, model.ID, resource.Data.Domains) //
	if err != nil {
		return nil, err
	}

	model.WebResourceDomains = dms

	// Insert IPs.
	IPs, err := insertIPsTx(tx, model.ID, resource.Data.IPs)
	if err != nil {
		return nil, err
	}

	model.WebResourceIPs = IPs

	// Insert URLs.
	URLs, err := insertURLsTx(tx, model.ID, resource.Data.URLs)
	if err != nil {
		return nil, err
	}

	model.WebResourceURLs = URLs

	return model, nil
}

func (s *SQLStorage) CreateWebResourceV1Context(ctx context.Context,
	resource *storage.WebResourceV1, getCreated bool) (*storage.WebResourceV1, error) {
	var model *WebResource

	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		m, err := createWebResourceV1Tx(tx, resource)
		if err != nil {
			return err
		}

		model = m

		return nil
	}); err != nil {
		return nil, err
	}

	if getCreated {
		return model.toStorageWebResource()
	}

	return nil, nil
}

func computeDomainSlicesUpdate(webResourceID int64,
	dbDomains WebResourceDomainSlice, domains []storage.WebResourceDomainT) (
	isUpdate bool, add, del WebResourceDomainSlice) {
	// Check simple cases so as to skip loops if possible.
	if len(dbDomains) == len(domains) && len(domains) == 0 {
		return false, nil, nil
	}

	// Check that all the DB domains exist in domains or
	// find the domains to delete.
	for _, d := range dbDomains {
		var isFound bool

		for i := range domains {
			if domains[i] == d.Domain {
				isFound = true

				break
			}
		}

		if !isFound {
			isUpdate = true

			del = append(del, d)
		}
	}

	// Check that all the domains exist in DB domains or
	// find domains to add.
	for _, d := range domains {
		var isFound bool

		for _, d2 := range dbDomains {
			if d2.Domain == d {
				isFound = true

				break
			}
		}

		if !isFound {
			isUpdate = true

			add = append(add, &WebResourceDomain{
				WebResourceID: webResourceID,
				CreatedAt:     time.Now(),
				Domain:        d,
			})
		}
	}

	return isUpdate, add, del
}

func updateDomainsTx(tx *pg.Tx, webResourceID int64, dbDomains WebResourceDomainSlice, // nolint:dupl
	domains []storage.WebResourceDomainT, getResult bool) (WebResourceDomainSlice, error) {
	// Get Updates.
	isUpdate, adds, deletes := computeDomainSlicesUpdate(webResourceID, dbDomains, domains)

	if !isUpdate {
		return dbDomains, nil
	}

	// Delete.
	if len(deletes) > 0 {
		if _, err := tx.Model(&deletes).WherePK().Delete(); err != nil {
			return nil, err
		}
	}

	// Add.
	if len(adds) > 0 { // nolint:nestif
		if _, err := tx.Model(&adds).Insert(); err != nil {
			if typedErr, ok := err.(pg.Error); ok {
				if typedErr.IntegrityViolation() {
					return nil, storage.NewErrResourceAlreadyExists([]interface{}{
						fmt.Sprintf("WebResourceDomains: %s", adds),
					}, typedErr)
				}
			}

			return nil, err
		}
	}

	var models WebResourceDomainSlice

	if getResult {
		if err := tx.Model(&models).
			Where("web_resource_domain.web_resource_id = ?", webResourceID).
			Select(); err != nil {
			return nil, err
		}
	}

	return models, nil
}

func computeURLSlicesUpdate(webResourceID int64, dbURLs WebResourceURLSlice, urls []url.URL) (
	isUpdate bool, add, del WebResourceURLSlice) {
	// Check simple cases so as to skip loops if possible.
	if len(dbURLs) == len(urls) && len(urls) == 0 {
		return false, nil, nil
	}

	// Check for URLs to add.
	var URLStr = make([]string, len(urls)) // Used to store string converted urls.

	for i := range urls {
		var isFound bool

		uS := urls[i].String()

		URLStr[i] = uS

		for _, dbU := range dbURLs {
			if dbU.URL == uS {
				isFound = true

				break
			}
		}

		if !isFound {
			isUpdate = true

			add = append(add, &WebResourceURL{
				WebResourceID: webResourceID,
				CreatedAt:     time.Now(),
				URL:           urls[i].String(),
			})
		}
	}

	// Check for URLs to delete.
	for _, dbU := range dbURLs {
		var isFound bool

		for _, uS := range URLStr {
			if uS == dbU.URL {
				isFound = true

				break
			}
		}

		if !isFound {
			isUpdate = true

			del = append(del, dbU)
		}
	}

	return isUpdate, add, del
}

func updateURLsTx(tx *pg.Tx, webResourceID int64, // nolint:dupl
	dbURLs WebResourceURLSlice, urls []url.URL, getResult bool) (WebResourceURLSlice, error) {
	// Get Updates.
	isUpdate, adds, deletes := computeURLSlicesUpdate(webResourceID, dbURLs, urls)

	if !isUpdate {
		return dbURLs, nil
	}

	// Delete.
	if len(deletes) > 0 {
		if _, err := tx.Model(&deletes).WherePK().Delete(); err != nil {
			return nil, err
		}
	}

	// Insert.
	if len(adds) > 0 { // nolint:nestif
		if _, err := tx.Model(&adds).Insert(); err != nil {
			if typedErr, ok := err.(pg.Error); ok {
				if typedErr.IntegrityViolation() {
					return nil, storage.NewErrResourceAlreadyExists([]interface{}{
						fmt.Sprintf("WebResourceURLs: %s", adds),
					}, typedErr)
				}
			}

			return nil, err
		}
	}

	var models WebResourceURLSlice

	if getResult {
		if err := tx.Model(&models).
			Where("web_resource_url.web_resource_id = ?", webResourceID).Select(); err != nil {
			return nil, err
		}
	}

	return models, nil
}

func computeIPSlicesUpdate(webResourceID int64,
	dbIPs WebResourceIPSlice, ips []net.IP) (
	isUpdate bool, add, del WebResourceIPSlice) {
	// Check simple cases so as to skip loops if possible.
	if len(dbIPs) == len(ips) && len(ips) == 0 {
		return false, nil, nil
	}

	// Check for IPs to add.
	for i := range ips {
		var isFound bool

		pIP := &ips[i]

		for _, dbIP := range dbIPs {
			if pIP.Equal(dbIP.IP) {
				isFound = true

				break
			}
		}

		if !isFound {
			isUpdate = true

			add = append(add, &WebResourceIP{
				WebResourceID: webResourceID,
				CreatedAt:     time.Now(),
				IP:            *pIP,
			})
		}
	}

	// Check for IPs to delete.
	for _, dbIP := range dbIPs {
		var isFound bool

		for i := range ips {
			if dbIP.IP.Equal(ips[i]) {
				isFound = true

				break
			}
		}

		if !isFound {
			isUpdate = true

			del = append(del, dbIP)
		}
	}

	return isUpdate, add, del
}

func updateIPsTx(tx *pg.Tx, webResourceID int64, dbIPs WebResourceIPSlice, // nolint:dupl
	ips []net.IP, getResult bool) (WebResourceIPSlice, error) {
	// Get Updates.
	isUpdate, adds, deletes := computeIPSlicesUpdate(webResourceID, dbIPs, ips)

	if !isUpdate {
		return dbIPs, nil
	}

	// Delete.
	if len(deletes) > 0 {
		if _, err := tx.Model(&deletes).WherePK().Delete(); err != nil {
			return nil, err
		}
	}

	// Insert.
	if len(adds) > 0 { // nolint:nestif
		if _, err := tx.Model(&adds).Insert(); err != nil {
			if typedErr, ok := err.(pg.Error); ok {
				if typedErr.IntegrityViolation() {
					return nil, storage.NewErrResourceAlreadyExists([]interface{}{
						fmt.Sprintf("WebResourceIPs: %s", adds),
					}, typedErr)
				}
			}

			return nil, err
		}
	}

	var models WebResourceIPSlice

	if getResult {
		if err := tx.Model(&models).
			Where("web_resource_ip.web_resource_id = ?", webResourceID).
			Select(); err != nil {
			return nil, err
		}
	}

	return models, nil
}

// bindWebResourceCategoriesToWebResourceTx binds WebResource to given WebResourceCategoryV1s.
func bindWebResourceCategoriesToWebResourceTx(tx *pg.Tx, model *WebResource, cats []storage.ResourceNameT) error {
	// Bind.
	bindModelIDs, err := loadWebResourceCategoryIDsByNamesTx(tx, cats)
	if err != nil {
		return err
	}

	currentTS := time.Now()
	bindings := make([]WebResourceCategoryToWebResource, 0, len(bindModelIDs))

	for i := range bindModelIDs {
		bindings = append(bindings, WebResourceCategoryToWebResource{
			WebResourceCategoryID: bindModelIDs[i],
			WebResourceID:         model.ID,
			CreatedAt:             currentTS,
		})
	}

	if _, err := tx.Model(&bindings).OnConflict("DO NOTHING").Insert(); err != nil {
		return err
	}

	return nil
}

// unBindWebResourceCategoriesFromWebResourceTx unbinds given WebResourceCategoryV1s from WebResourceV1.
func unBindWebResourceCategoriesFromWebResourceTx(tx *pg.Tx, model *WebResource, cats []storage.ResourceNameT) error {
	unBindModelIDs, err := loadWebResourceCategoryIDsByNamesTx(tx, cats)
	if err != nil {
		return err
	}

	if _, err := tx.Model((*WebResourceCategoryToWebResource)(nil)).
		Where("web_resource_id = ?", model.ID).
		WhereIn("web_resource_category_id IN (?)", unBindModelIDs).Delete(); err != nil {
		return err
	}

	return nil
}

func loadWebResourceCategoriesForWebResourceTx(tx *pg.Tx, webResourceID int64) (WebResourceCategorySlice, error) {
	var webResource WebResource

	if err := tx.Model(&webResource).
		Relation("WebResourceCategorys").
		Where("web_resource.id = ?", webResourceID).Select(); err != nil {
		return nil, err
	}

	return webResource.WebResourceCategorys, nil
}

func updateWebResourceCategoriesTx(tx *pg.Tx, model *WebResource, refCats []storage.ResourceNameT) (*WebResource, error) {
	// Make Categories Names.
	var modelCats = make([]storage.ResourceNameT, 0, len(model.WebResourceCategorys))

	for _, c := range model.WebResourceCategorys {
		modelCats = append(modelCats, c.Name)
	}

	// Compare and make update.
	modelCatsM := storage.ResourceNamesToSet(modelCats)
	refCatsM := storage.ResourceNamesToSet(refCats)

	// Make updates.
	var (
		toBind   = make([]storage.ResourceNameT, 0)
		toUnBind = make([]storage.ResourceNameT, 0)
	)

	for refName := range refCatsM {
		_, exists := modelCatsM[refName]
		if !exists {
			toBind = append(toBind, refName)
		}
	}

	for modName := range modelCatsM {
		_, exists := refCatsM[modName]
		if !exists {
			toUnBind = append(toUnBind, modName)
		}
	}

	// Perform Updates.
	// Bind.
	if err := bindWebResourceCategoriesToWebResourceTx(tx, model, toBind); err != nil {
		return nil, err
	}

	// Unbind.
	if err := unBindWebResourceCategoriesFromWebResourceTx(tx, model, toUnBind); err != nil {
		return nil, err
	}

	// Get Updated WebResourceCategories.
	dbCats, err := loadWebResourceCategoriesForWebResourceTx(tx, model.ID)
	if err != nil {
		return nil, err
	}

	model.WebResourceCategorys = dbCats

	return model, nil
}

func updateWebResourceV1Tx(tx *pg.Tx, name storage.ResourceNameT,
	resource *storage.WebResourceV1, getResult bool) (*WebResource, error) {
	var model = &WebResource{}

	// Lookup.
	if err := tx.Model(model).
		Relation("WebResourceCategorys").
		Relation("WebResourceIPs").
		Relation("WebResourceDomains").
		Relation("WebResourceURLs").Where("web_resource.name = ?", name).Select(); err != nil {
		if errors.Is(err, pg.ErrNoRows) {
			return nil, storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.WebResourceKind,
					Name:       name,
				},
			)
		}

		return nil, err
	}

	// Check ETag.
	if model.ETag != resource.Metadata.ETag {
		return nil, storage.NewErrETagDoesNotMatch(model.ETag, resource.Metadata.ETag)
	}

	// Update model.
	model.ETag++
	model.UpdatedAt = time.Now()
	model.Description = resource.Data.Description
	model.Name = resource.Metadata.Name

	if _, err := tx.Model(model).WherePK().Update(); err != nil {
		if typedErr, ok := err.(pg.Error); ok {
			if typedErr.IntegrityViolation() {
				return nil, storage.NewErrResourceAlreadyExists([]interface{}{
					resource.Metadata,
				}, typedErr)
			}
		}

		return nil, err
	}

	// Update IP addresses.
	nIPs, err := updateIPsTx(tx, model.ID, model.WebResourceIPs, resource.Data.IPs, getResult)
	if err != nil {
		return nil, err
	}

	model.WebResourceIPs = nIPs

	// Update Domains.
	nDomains, err := updateDomainsTx(tx, model.ID, model.WebResourceDomains, resource.Data.Domains, getResult)
	if err != nil {
		return nil, err
	}

	model.WebResourceDomains = nDomains

	// Update URLs.
	nURLs, err := updateURLsTx(tx, model.ID, model.WebResourceURLs, resource.Data.URLs, getResult)
	if err != nil {
		return nil, err
	}

	model.WebResourceURLs = nURLs

	// If Web Resource Category is changed - lookup for a new.
	model, err = updateWebResourceCategoriesTx(
		tx, model, resource.Data.WebResourceCategoryNames)
	if err != nil {
		return nil, err
	}

	return model, nil
}

func (s *SQLStorage) UpdateWebResourceV1Context(ctx context.Context,
	name storage.ResourceNameT, resource *storage.WebResourceV1,
	getUpdated bool) (*storage.WebResourceV1, error) {
	var model *WebResource

	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		updated, err := updateWebResourceV1Tx(tx, name, resource, getUpdated)
		if err != nil {
			return err
		}

		model = updated

		return nil
	}); err != nil {
		return nil, err
	}

	if getUpdated {
		return model.toStorageWebResource()
	}

	return nil, nil
}

func (s *SQLStorage) DeleteWebResourceV1Context(ctx context.Context, resource *storage.WebResourceV1) error {
	return s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		var model WebResource

		// Lookup.
		if err := tx.Model(&model).Where("web_resource.name = ?", resource.Metadata.Name).Select(); err != nil {
			if errors.Is(err, pg.ErrNoRows) {
				return storage.NewErrResourceNotFound(&resource.Metadata)
			}

			return err
		}

		// Check ETag.
		if model.ETag != resource.Metadata.ETag {
			return storage.NewErrETagDoesNotMatch(model.ETag, resource.Metadata.ETag)
		}

		// Delete related.
		if _, err := tx.Model((*WebResourceDomain)(nil)).
			Where("web_resource_domain.web_resource_id = ?", model.ID).Delete(); err != nil {
			return err
		}

		if _, err := tx.Model((*WebResourceURL)(nil)).
			Where("web_resource_url.web_resource_id = ?", model.ID).Delete(); err != nil {
			return err
		}

		if _, err := tx.Model((*WebResourceIP)(nil)).
			Where("web_resource_ip.web_resource_id = ?", model.ID).Delete(); err != nil {
			return err
		}

		if _, err := tx.Model(&model).WherePK().Delete(); err != nil {
			return err
		}

		return nil
	})
}

package sqlstorage3

import (
	"context"
	"errors"
	"time"

	"github.com/go-pg/pg/v10"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

// DeleteAccessGroupV1Context - Deletes AccessGroup.
func (s *SQLStorage) DeleteAccessGroupV1Context(ctx context.Context, group *storage.AccessGroupV1) error {
	return s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		// Lookup an AccessGroup.
		var agm = &AccessGroup{}

		if err := tx.Model(agm).Where(
			"name = ?", group.Metadata.Name).Select(); err != nil {
			if errors.Is(err, pg.ErrNoRows) {
				return storage.NewErrResourceNotFound(&group.Metadata)
			}

			return err
		}

		if agm.ETag != group.Metadata.ETag {
			return storage.NewErrETagDoesNotMatch(agm.ETag, group.Metadata.ETag)
		}

		_, err := tx.Model(agm).WherePK().Delete()

		return err
	})
}

// GetAccessGroupV1Context -  Returns one AccessGroup by its name.
func (s *SQLStorage) GetAccessGroupV1Context(ctx context.Context, name storage.ResourceNameT) (*storage.AccessGroupV1, error) {
	var agm AccessGroup

	if err := s.db.WithContext(ctx).Model(&agm).Where("name = ?", name).Select(); err != nil {
		if errors.Is(err, pg.ErrNoRows) {
			return nil, storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       0,
					Kind:       storage.AccessGroupKind,
					Name:       name,
				},
			)
		}

		return nil, err
	}

	return agm.toStorageAccessGroup(), nil
}

func storageAccessGroupV1ToModel(p *storage.AccessGroupV1) *AccessGroup {
	return &AccessGroup{
		OrderInAccessRulesList:  p.Data.OrderInAccessRulesList,
		DefaultPolicy:           p.Data.DefaultPolicy,
		ETag:                    p.Metadata.ETag,
		ExtraAccessDenyMessage:  p.Data.ExtraAccessDenyMessage,
		Name:                    p.Metadata.Name,
		TotalGroupSpeedLimitBps: p.Data.TotalGroupSpeedLimitBps,
		UserGroupSpeedLimitBps:  p.Data.UserGroupSpeedLimitBps,
	}
}

// CreateAccessGroupV1Context - Creates a new AccessGroupV1;
// Returns created AccessGroupV1 if getResult is true.
func (s *SQLStorage) CreateAccessGroupV1Context(ctx context.Context,
	group *storage.AccessGroupV1, getResult bool) (*storage.AccessGroupV1, error) {
	var agm *AccessGroup

	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		agm = storageAccessGroupV1ToModel(group)

		if _, err := tx.Model(agm).Insert(); err != nil {
			typedE, ok := err.(pg.Error)
			if ok {
				if typedE.IntegrityViolation() {
					return storage.NewErrResourceAlreadyExists(
						[]interface{}{group},
						typedE)
				}
			}

			return err
		}

		return nil
	}); err != nil {
		return nil, err
	}

	if getResult {
		return agm.toStorageAccessGroup(), nil
	}

	return nil, nil
}

// UpdateAccessGroupV1Context - modifies AccessGroupV1 with possible renaming;
// Returns updated AccessGroupV1 if getResult is true.
func (s *SQLStorage) UpdateAccessGroupV1Context(ctx context.Context,
	groupNameToUpdate storage.ResourceNameT,
	group *storage.AccessGroupV1, getResult bool) (*storage.AccessGroupV1, error) {
	var agm *AccessGroup

	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		agm = storageAccessGroupV1ToModel(group)

		var dbModel AccessGroup
		if err := tx.Model(&dbModel).Where("name = ?", groupNameToUpdate).Select(); err != nil {
			if errors.Is(err, pg.ErrNoRows) {
				return storage.NewErrResourceNotFound(
					&storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						Kind:       storage.AccessGroupKind,
						Name:       groupNameToUpdate,
					},
				)
			}

			return err
		}

		if dbModel.ETag != agm.ETag {
			return storage.NewErrETagDoesNotMatch(dbModel.ETag, agm.ETag)
		}

		// Set ETag and ID.
		agm.ETag++
		agm.ID = dbModel.ID
		agm.UpdatedAt = time.Now()

		if _, err := tx.Model(agm).WherePK().Update(); err != nil {
			if typedE, ok := err.(pg.Error); ok {
				if typedE.IntegrityViolation() {
					return storage.NewErrResourceAlreadyExists(
						[]interface{}{*group},
						typedE)
				}
			}

			return err
		}

		return nil
	}); err != nil {
		return nil, err
	}

	if getResult {
		return agm.toStorageAccessGroup(), nil
	}

	return nil, nil
}

func loadAccessGroupIDTx(tx *pg.Tx, accessGroup storage.ResourceNameT) (*AccessGroup, error) {
	// Lookup Access Group.
	var accessGroupModel AccessGroup

	if err := tx.Model(&accessGroupModel).
		Where("access_group.name = ?", accessGroup).
		Select(); err != nil {
		if errors.Is(err, pg.ErrNoRows) {
			return nil, storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroupKind,
					Name:       accessGroup,
				},
			)
		}

		return nil, err
	}

	return &accessGroupModel, nil
}

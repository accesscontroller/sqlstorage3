module gitlab.com/accesscontroller/sqlstorage3

go 1.15

require (
	github.com/go-pg/pg/v10 v10.7.3
	github.com/kr/text v0.2.0 // indirect
	github.com/nxadm/tail v1.4.6 // indirect
	github.com/onsi/gomega v1.10.4 // indirect
	github.com/stretchr/testify v1.6.1
	github.com/vmihailenco/msgpack/v5 v5.1.3 // indirect
	gitlab.com/accesscontroller/accesscontroller/controller/storage v0.0.0-20201227151802-89abbe05265a
	go.opentelemetry.io/otel v0.15.0 // indirect
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad // indirect
	golang.org/x/net v0.0.0-20201224014010-6772e930b67b // indirect
	golang.org/x/sys v0.0.0-20201223074533-0d417f636930 // indirect
	golang.org/x/text v0.3.4 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)

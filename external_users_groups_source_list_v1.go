package sqlstorage3

import (
	"context"

	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func (s *SQLStorage) GetExternalUsersGroupsSourceV1sContext(
	ctx context.Context,
	filters []storage.ExternalUsersGroupsSourceFilterV1) (
	[]storage.ExternalUsersGroupsSourceV1, error) {
	var models ExternalUsersGroupsSourceSlice

	var q = s.db.ModelContext(ctx, &models)

	for i := range filters {
		fp := &filters[i]

		q = q.WhereOrGroup(func(iq *orm.Query) (*orm.Query, error) {
			if !isZero(fp.GetMode) {
				iq = iq.Where("external_users_groups_source.get_mode = ?", fp.GetMode)
			}

			if !isZero(fp.Name) {
				iq = iq.Where("external_users_groups_source.name = ?", fp.Name)
			}

			if !isZero(fp.NameSubstringMatch) {
				iq = iq.Where("external_users_groups_source.name LIKE ?", "%"+fp.NameSubstringMatch+"%")
			}

			if !isZero(fp.Type) {
				iq = iq.Where("external_users_groups_source.type = ?", fp.Type)
			}

			return iq, nil
		})
	}

	// Select.
	if err := q.Select(); err != nil {
		return nil, err
	}

	return models.toStorageExternalUsersGroupsSources()
}

func (s *SQLStorage) CreateExternalUsersGroupsSourceV1sContext(ctx context.Context,
	sources []storage.ExternalUsersGroupsSourceV1, getCreated bool) ([]storage.ExternalUsersGroupsSourceV1, error) {
	var models ExternalUsersGroupsSourceSlice

	if getCreated {
		models = make(ExternalUsersGroupsSourceSlice, 0, len(sources))
	}

	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		for i := range sources {
			src := &sources[i]

			s, err := createExternalUsersGroupsSourceV1Tx(tx, src)
			if err != nil {
				return err
			}

			if getCreated {
				models = append(models, s)
			}
		}

		return nil
	}); err != nil {
		return nil, err
	}

	if getCreated {
		return models.toStorageExternalUsersGroupsSources()
	}

	return nil, nil
}

package sqlstorage3

import (
	"context"

	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

//  GetExternalUserSessionV1sContext loads and returns filtered sessions list;
// if filter is nil - returns all sessions.
func (s *SQLStorage) GetExternalUserSessionV1sContext(ctx context.Context,
	sessionsSourceName storage.ResourceNameT,
	filters []storage.ExternalUserSessionFilterV1) ([]storage.ExternalUserSessionV1, error) {
	var models ExternalUserSessionSlice

	// Query.
	var q = s.db.ModelContext(ctx, &models).Relation("ExternalSessionsSource").
		AllWithDeleted().
		Relation("ExternalUser.name").Relation("ExternalUser.ExternalUsersGroupsSource.name").
		Where("external_sessions_source.name = ?", sessionsSourceName)

	// Filters sub WHERE groups.
	q = q.WhereGroup(func(fltGrps *orm.Query) (*orm.Query, error) {
		return makeExternalUserSessionV1sFilterExpr(fltGrps, filters), nil
	})

	// Load.
	if err := q.Select(); err != nil {
		return nil, err
	}

	return models.toStorageExternalUserSessions(), nil
}

func makeExternalUserSessionV1sFilterExpr(
	fltGrps *orm.Query, filters []storage.ExternalUserSessionFilterV1) *orm.Query {
	// Add filters.
	for i := range filters {
		pf := &filters[i]

		fltGrps = fltGrps.WhereOrGroup(func(iq *orm.Query) (*orm.Query, error) {
			if !isZero(pf.CloseTimestamp) {
				iq = iq.Where("external_user_session.close_timestamp = ?", pf.CloseTimestamp)
				iq = iq.AllWithDeleted()
			}

			if !isZero(pf.Closed) {
				iq = iq.Where("external_user_session.closed = ?", pf.Closed)
				iq = iq.AllWithDeleted()
			}

			if !isZero(pf.ExternalSessionsSourceName) {
				iq = iq.Where("external_sessions_source__name = ?", pf.ExternalSessionsSourceName)
			}

			if !isZero(pf.ExternalUserName) {
				iq = iq.Where("external_user.name = ?", pf.ExternalUserName)
			}

			if !isZero(pf.Hostname) {
				iq = iq.Where("external_user_session.hostname = ?", pf.Hostname)
			}

			if !isZero(pf.IPAddress) {
				iq = iq.Where("external_user_session.ip_address = ?", pf.IPAddress)
			}

			if !isZero(pf.Name) {
				iq = iq.Where("external_user_session.name = ?", pf.Name)
			}

			if !isZero(pf.OpenTimestamp) {
				iq = iq.Where("external_user_session.open_timestamp = ?", pf.OpenTimestamp)
			}

			return iq, nil
		})
	}

	return fltGrps
}

// OpenExternalUserSessionV1sContext opens ExternalUserSessionV1s from given list.
// All given ExternalUserSessionV1s must be from the same ExternalUserSessionSourceV1.
func (s *SQLStorage) OpenExternalUserSessionV1sContext(ctx context.Context,
	sessions []storage.ExternalUserSessionV1, getOpened bool) ([]storage.ExternalUserSessionV1, error) {
	var result ExternalUserSessionSlice

	if getOpened {
		result = make(ExternalUserSessionSlice, 0, len(sessions))
	}

	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		for i := range sessions {
			ps := &sessions[i]

			m, err := openExternalUserSessionV1Tx(tx, ps, getOpened)
			if err != nil {
				return err
			}

			if getOpened {
				result = append(result, m)
			}
		}

		return nil
	}); err != nil {
		return nil, err
	}

	if getOpened {
		return result.toStorageExternalUserSessions(), nil
	}

	return nil, nil
}

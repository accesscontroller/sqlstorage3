package sqlstorage3

import (
	"context"
	"errors"
	"net/url"
	"os"
	"strings"

	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
)

const (
	dbConnectionDSNEnv string = "SQLSTORAGE3_DB_DSN"
)

var (
	ErrCanNotFindDBConSettings = errors.New("os.Env[SQLSTORAGE3_DB_DSN] is not set")
)

func connectDB() (*pg.DB, error) {
	dsn, ok := os.LookupEnv(dbConnectionDSNEnv)
	if !ok {
		return nil, ErrCanNotFindDBConSettings
	}

	u, err := url.Parse(dsn)
	if err != nil {
		return nil, err
	}

	pwd, _ := u.User.Password()

	return pg.Connect(&pg.Options{
		Addr:     u.Host,
		Database: strings.TrimLeft(u.Path, "/"),
		User:     u.User.Username(),
		Password: pwd,
	}), nil
}

// recreateTables recreates tables for test suite.
func recreateTables(db *pg.DB) error {
	return db.RunInTransaction(context.TODO(), func(tx *pg.Tx) error {
		// Register Many to Many Models.
		orm.RegisterTable(&WebResourceCategoryToAccessGroup{})
		orm.RegisterTable(&ExternalUserToExternalGroup{})
		orm.RegisterTable(&WebResourceCategoryToWebResource{})

		// Delete Tables.
		for i := range dbTables {
			if err := tx.Model(dbTables[i]).DropTable(&orm.DropTableOptions{
				Cascade:  true,
				IfExists: true,
			}); err != nil {
				return err
			}
		}

		// Create Tables.
		for i := range dbTables {
			if err := tx.Model(dbTables[i]).CreateTable(&orm.CreateTableOptions{
				FKConstraints: true,
				IfNotExists:   true,
				Temp:          true,
			}); err != nil {
				return err
			}
		}

		// Make External Session IP Constraint.
		if _, err := tx.Exec(
			`CREATE UNIQUE INDEX unique_ip_address_open_sessions
			ON external_user_sessions USING btree
			(ip_address ASC NULLS LAST)
			TABLESPACE pg_default
			WHERE closed = false;`); err != nil {
			return err
		}

		return nil
	})
}

package sqlstorage3

import (
	"context"

	"github.com/go-pg/pg/v10"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func (s *SQLStorage) GetAccessGroupExternalGroupV1sContext(ctx context.Context,
	accessGroup storage.ResourceNameT) (*storage.AccessGroup2ExternalGroupListRelationsV1, error) {
	var relations *storage.AccessGroup2ExternalGroupListRelationsV1

	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		// Load Data.
		var externalGroups ExternalGroupSlice

		if err := tx.Model(&externalGroups).
			Relation("ExternalUsersGroupsSource").
			Relation("AccessGroup.name").
			Where("access_group.name = ?", accessGroup).Select(); err != nil {
			return err
		}

		// Check Access Group existence.
		if len(externalGroups) == 0 {
			isExists, err := tx.Model((*AccessGroup)(nil)).Where("access_group.name = ?", accessGroup).Exists()
			if err != nil {
				return err
			}

			if !isExists {
				return storage.NewErrResourceNotFound(&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroupKind,
					Name:       accessGroup,
				})
			}
		}

		// Make result.
		relations = storage.NewAccessGroup2ExternalGroupListV1Relations(accessGroup)

		relations.Data.AccessGroup = accessGroup

		for _, g := range externalGroups {
			relations.Data.Relations[g.ExternalUsersGroupsSource.Name] = append(
				relations.Data.Relations[g.ExternalUsersGroupsSource.Name], g.Name)
		}

		return nil
	}); err != nil {
		return nil, err
	}

	return relations, nil
}

// checkNotAllGroupsLoadedError returns ErrRelatedResourcesNotFound if
// there are at least one required group which is not in loaded.
func checkNotAllGroupsLoadedError(required []storage.ResourceNameT, loaded ExternalGroupSlice) error {
	var notFoundGroups = make([]storage.ResourceNameT, 0, len(required)-len(loaded))

	for _, rg := range required {
		var isFound bool

		for _, ld := range loaded {
			if ld.Name == rg {
				isFound = true

				continue
			}
		}

		if !isFound {
			notFoundGroups = append(notFoundGroups, rg)
		}
	}

	if len(notFoundGroups) > 0 {
		return storage.NewErrRelatedResourcesNotFound(storage.APIVersionV1Value,
			storage.ExternalGroupKind, notFoundGroups, nil)
	}

	return nil
}

func bindUnbindAccessGroup2ExternalGroupV1sTx(tx *pg.Tx,
	relations *storage.AccessGroup2ExternalGroupListRelationsV1, isBind bool) error {
	// Load AccessGroup.
	accessGroup, err := loadAccessGroupIDTx(tx, relations.Data.AccessGroup)
	if err != nil {
		return err
	}

	// Update ETag.
	accessGroup.ETag++

	if _, err := tx.Model(accessGroup).WherePK().Column("e_tag").Update(); err != nil {
		return err
	}

	// Lookup groups for every source in relations.
	for src, groups := range relations.Data.Relations {
		var groupModels = make(ExternalGroupSlice, 0, len(groups))

		// Load all groups from the source.
		if err := tx.Model(&groupModels).
			Relation("ExternalUsersGroupsSource").
			Where("external_users_groups_source.name = ?", src).
			WhereIn("external_group.name IN (?)", groups).Select(); err != nil {
			return err
		}

		// Check that all the requested groups are loaded.
		if err := checkNotAllGroupsLoadedError(groups, groupModels); err != nil {
			return err
		}

		// Bind.
		for _, dbGroup := range groupModels {
			if isBind {
				dbGroup.AccessGroupID = accessGroup.ID
			} else {
				dbGroup.AccessGroupID = 0
			}

			dbGroup.ETag++
		}

		if _, err := tx.Model(&groupModels).WherePK().
			Column("e_tag", "access_group_id").Update(); err != nil {
			return err
		}
	}

	return nil
}

func (s *SQLStorage) BindAccessGroup2ExternalGroupV1sContext(ctx context.Context,
	relations *storage.AccessGroup2ExternalGroupListRelationsV1) error {
	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		return bindUnbindAccessGroup2ExternalGroupV1sTx(tx, relations, true)
	}); err != nil {
		return err
	}

	return nil
}

func (s *SQLStorage) UnBindAccessGroup2ExternalGroupV1sContext(ctx context.Context,
	relations *storage.AccessGroup2ExternalGroupListRelationsV1) error {
	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		return bindUnbindAccessGroup2ExternalGroupV1sTx(tx, relations, false)
	}); err != nil {
		return err
	}

	return nil
}

package sqlstorage3

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/url"
	"sort"
	"testing"

	"github.com/go-pg/pg/v10"
	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func parseURLPanic(raw string) url.URL {
	u, err := url.Parse(raw)
	if err != nil {
		log.Fatalln(err)
	}

	return *u
}

type TestWebResourceV1sSuite struct {
	db *pg.DB

	sqlStorage *SQLStorage

	suite.Suite
}

func (ts *TestWebResourceV1sSuite) SetupTest() {
	db, err := connectDB()
	if err != nil {
		ts.FailNow("Can not connect to test DB", err)
	}

	ts.db = db
	ts.sqlStorage = &SQLStorage{db: db}

	// Create DB Tables.
	if err := recreateTables(ts.db); err != nil {
		ts.FailNow("Can not initialize DB", err)
	}

	// Create some init data.
	var webResourceCategories = WebResourceCategorySlice{
		{
			ETag:        1,
			Name:        "category 1",
			Description: "cat 1 description",
		},
		{
			ETag:        2,
			Name:        "category 2",
			Description: "cat 2 description",
		},
		{
			ETag:        3,
			Name:        "category 3",
			Description: "cat 3 description",
		},
		{
			ETag:        4,
			Name:        "category 4",
			Description: "cat 4 description",
		},
	}

	for _, c := range webResourceCategories {
		if _, err := db.Model(c).Insert(); err != nil {
			ts.FailNow("Can not Insert webResourceCategory model", err)
		}
	}

	// Web Resources.
	var webResources = WebResourceSlice{
		{
			ETag:        1,
			Name:        "web resource 1 cat 1",
			Description: "description web resource 1 cat 1",
		},
		{
			ETag:        2,
			Name:        "web resource 2 cat 2",
			Description: "description web resource 2 cat 2",
		},
		{
			ETag:        3,
			Name:        "web resource 3 cat 1",
			Description: "description web resource 3 cat 1",
		},
		{
			ETag:        4,
			Name:        "web resource 4 cat 1",
			Description: "description web resource 4 cat 1",
		},
	}

	for _, wr := range webResources {
		if _, err := db.Model(wr).Insert(); err != nil {
			ts.FailNow("Can not insert Test Web Resource", err)
		}
	}

	// Add Web Resource to WebResource Categories.
	var webResourceToCategory = []WebResourceCategoryToWebResource{
		{
			WebResourceCategoryID: webResourceCategories[0].ID,
			WebResourceID:         webResources[0].ID,
		},
		{
			WebResourceCategoryID: webResourceCategories[1].ID,
			WebResourceID:         webResources[1].ID,
		},
		{
			WebResourceCategoryID: webResourceCategories[1].ID,
			WebResourceID:         webResources[2].ID,
		},
	}

	for i := range webResourceToCategory {
		var wrwrc = &webResourceToCategory[i]

		if _, err := db.Model(wrwrc).Insert(); err != nil {
			ts.FailNowf("Can not Insert WebResource to WebResource Category",
				"WebResourceID %d; WebResourceCategoryID: %d; error: %s",
				wrwrc.WebResourceID, wrwrc.WebResourceCategoryID, err)
		}
	}

	// Add IPs.
	var webResourceIPs = WebResourceIPSlice{
		{
			WebResourceID: webResources[0].ID,
			IP:            net.ParseIP("10.8.100.1"),
		},
		{
			WebResourceID: webResources[0].ID,
			IP:            net.ParseIP("10.8.100.2"),
		},
		{
			WebResourceID: webResources[1].ID,
			IP:            net.ParseIP("10.8.100.3"),
		},
		{
			WebResourceID: webResources[2].ID,
			IP:            net.ParseIP("10.8.100.4"),
		},
		{
			WebResourceID: webResources[3].ID,
			IP:            net.ParseIP("10.8.100.5"),
		},
	}

	for _, ip := range webResourceIPs {
		if _, err := db.Model(ip).Insert(); err != nil {
			ts.FailNow("Can not insert Test Web Resource IP", err)
		}
	}

	// Domains.
	var webResourceDomains = WebResourceDomainSlice{
		{
			WebResourceID: webResources[0].ID,
			Domain:        "example-1-wr-1.com",
		},
		{
			WebResourceID: webResources[0].ID,
			Domain:        "example-2-wr-1.com",
		},
		{
			WebResourceID: webResources[1].ID,
			Domain:        "example-1-wr-2.com",
		},
		{
			WebResourceID: webResources[1].ID,
			Domain:        "example-2-wr-2.com",
		},
		{
			WebResourceID: webResources[2].ID,
			Domain:        "example-1-wr-3.com",
		},
	}

	for _, domain := range webResourceDomains {
		if _, err := db.Model(domain).Insert(); err != nil {
			ts.FailNow("Can not insert Test Web Resource Domain", err)
		}
	}

	// URLs.
	var webResourceURLs = WebResourceURLSlice{
		{
			WebResourceID: webResources[0].ID,
			URL:           "http://example-1-web-resource-1.com",
		},
		{
			WebResourceID: webResources[1].ID,
			URL:           "http://example-1-web-resource-2.com",
		},
		{
			WebResourceID: webResources[1].ID,
			URL:           "http://example-2-web-resource-2.com",
		},
		{
			WebResourceID: webResources[0].ID,
			URL:           "http://example-1-web-resource-3.com",
		},
		{
			WebResourceID: webResources[0].ID,
			URL:           "http://example-1-web-resource-4.com",
		},
	}

	for _, url := range webResourceURLs {
		if _, err := db.Model(url).Insert(); err != nil {
			ts.FailNow("Can not insert Test Web Resource URL", err)
		}
	}
}

func (ts *TestWebResourceV1sSuite) TearDownTest() {
	if ts.db != nil {
		if err := ts.db.Close(); err != nil {
			ts.FailNow("Can not Close DB", err)
		}
	}
}

func (ts *TestWebResourceV1sSuite) TestGet() {
	var tests = []struct {
		category *storage.ResourceNameT
		filters  []storage.WebResourceFilterV1

		expectedResources []storage.WebResourceV1
	}{
		{
			filters: []storage.WebResourceFilterV1{
				{
					NameSubstringMatch: "web resource 1",
				},
			},
			expectedResources: []storage.WebResourceV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       1,
						Kind:       storage.WebResourceKind,
						Name:       "web resource 1 cat 1",
					},
					Data: storage.WebResourceDataV1{
						Description: "description web resource 1 cat 1",
						Domains: []storage.WebResourceDomainT{
							"example-1-wr-1.com",
							"example-2-wr-1.com",
						},
						IPs: []net.IP{
							net.ParseIP("10.8.100.1"),
							net.ParseIP("10.8.100.2"),
						},
						WebResourceCategoryNames: []storage.ResourceNameT{"category 1"},
						URLs: []url.URL{
							parseURLPanic("http://example-1-web-resource-1.com"),
							parseURLPanic("http://example-1-web-resource-3.com"),
							parseURLPanic("http://example-1-web-resource-4.com"),
						},
					},
				},
			},
		},
		{
			// Check Empty but defined value case.
			category: func() *storage.ResourceNameT {
				var s = storage.ResourceNameT("")

				return &s
			}(),
			filters: []storage.WebResourceFilterV1{
				{
					NameSubstringMatch: "web resource 1",
				},
			},
			expectedResources: []storage.WebResourceV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       1,
						Kind:       storage.WebResourceKind,
						Name:       "web resource 1 cat 1",
					},
					Data: storage.WebResourceDataV1{
						Description: "description web resource 1 cat 1",
						Domains: []storage.WebResourceDomainT{
							"example-1-wr-1.com",
							"example-2-wr-1.com",
						},
						IPs: []net.IP{
							net.ParseIP("10.8.100.1"),
							net.ParseIP("10.8.100.2"),
						},
						WebResourceCategoryNames: []storage.ResourceNameT{"category 1"},
						URLs: []url.URL{
							parseURLPanic("http://example-1-web-resource-1.com"),
							parseURLPanic("http://example-1-web-resource-3.com"),
							parseURLPanic("http://example-1-web-resource-4.com"),
						},
					},
				},
			},
		},
		{
			filters: []storage.WebResourceFilterV1{
				{
					DescriptionSubstringMatch: "web resource 1",
				},
			},
			expectedResources: []storage.WebResourceV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       1,
						Kind:       storage.WebResourceKind,
						Name:       "web resource 1 cat 1",
					},
					Data: storage.WebResourceDataV1{
						Description: "description web resource 1 cat 1",
						Domains: []storage.WebResourceDomainT{
							"example-1-wr-1.com",
							"example-2-wr-1.com",
						},
						IPs: []net.IP{
							net.ParseIP("10.8.100.1"),
							net.ParseIP("10.8.100.2"),
						},
						WebResourceCategoryNames: []storage.ResourceNameT{"category 1"},
						URLs: []url.URL{
							parseURLPanic("http://example-1-web-resource-1.com"),
							parseURLPanic("http://example-1-web-resource-3.com"),
							parseURLPanic("http://example-1-web-resource-4.com"),
						},
					},
				},
			},
		},
		{
			// No "web resource 1" in "category 2".
			category: func() *storage.ResourceNameT { var c storage.ResourceNameT = "category 2"; return &c }(),
			filters: []storage.WebResourceFilterV1{
				{
					NameSubstringMatch: "web resource 1",
				},
			},
			expectedResources: []storage.WebResourceV1{},
		},
		// {
		// 	filters: []storage.WebResourceFilterV1{
		// 		{
		// 			IPs: []net.IP{
		// 				net.ParseIP("10.8.100.1"),
		// 			},
		// 		},
		// 	},
		// 	expectedResources: []storage.WebResourceV1{
		// 		{
		// 			Metadata: storage.MetadataV1{
		// 				APIVersion: storage.APIVersionV1Value,
		// 				ETag:       1,
		// 				Kind:       storage.WebResourceKind,
		// 				Name:       "web resource 1 cat 1",
		// 			},
		// 			Data: storage.WebResourceDataV1{
		// 				Description: "description web resource 1 cat 1",
		// 				Domains: []storage.WebResourceDomainT{
		// 					"example-1-wr-1.com",
		// 					"example-2-wr-1.com",
		// 				},
		// 				IPs: []net.IP{
		// 					net.ParseIP("10.8.100.1"),
		// 					net.ParseIP("10.8.100.2"),
		// 				},
		// 				WebResourceCategoryName: "category 1",
		// 				URLs: []url.URL{
		// 					parseURLPanic("http://example-1-web-resource-1.com"),
		// 					parseURLPanic("http://example-1-web-resource-3.com"),
		// 					parseURLPanic("http://example-1-web-resource-4.com"),
		// 				},
		// 			},
		// 		},
		// 	},
		// },
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.GetWebResourceV1sContext(context.TODO(), test.category, test.filters)

		if !ts.NoError(gotErr, "Unexpected error") {
			ts.FailNow("Got error - can not continue")
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		sort.Slice(test.expectedResources, func(i, j int) bool {
			return test.expectedResources[i].Metadata.Name < test.expectedResources[j].Metadata.Name
		})

		sort.Slice(got, func(i, j int) bool {
			return got[i].Metadata.Name < got[j].Metadata.Name
		})

		ts.Equal(test.expectedResources, got, "Unexpected result")
	}
}

func (ts *TestWebResourceV1sSuite) TestCreateSuccess() { // nolint:gocognit
	var tests = []struct {
		getCreated bool

		resources []storage.WebResourceV1
	}{
		{
			resources: []storage.WebResourceV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       111,
						Kind:       storage.WebResourceKind,
						Name:       "new web resource 1 at category 2",
					},
					Data: storage.WebResourceDataV1{
						Description: "new web resource description",
						Domains: []storage.WebResourceDomainT{
							"domain-1.com",
							"domain-2.com",
						},
						IPs: []net.IP{
							net.ParseIP("10.8.200.1"),
							net.ParseIP("10.8.200.2"),
						},
						URLs: []url.URL{
							parseURLPanic("http://domain-1.com/path-1"),
						},
						WebResourceCategoryNames: []storage.ResourceNameT{"category 2"},
					},
				},
			},
		},
		{
			resources: []storage.WebResourceV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       112,
						Kind:       storage.WebResourceKind,
						Name:       "new web resource 2 at category 2",
					},
					Data: storage.WebResourceDataV1{
						Description: "new web resource description",
						Domains: []storage.WebResourceDomainT{
							"domain-3.com",
							"domain-4.com",
						},
						IPs: []net.IP{
							net.ParseIP("10.8.200.3"),
							net.ParseIP("10.8.200.4"),
						},
						URLs: []url.URL{
							parseURLPanic("http://domain-2.com/path-1"),
						},
						WebResourceCategoryNames: []storage.ResourceNameT{"category 2"},
					},
				},
			},
			getCreated: true,
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.CreateWebResourceV1sContext(context.TODO(),
			test.resources, test.getCreated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Check created.
		for wResInd := range test.resources {
			var expectedResource = &test.resources[wResInd]

			var dbModel WebResource

			if err := ts.db.Model(&dbModel).
				Relation("WebResourceCategorys").
				Relation("WebResourceIPs").
				Relation("WebResourceDomains").
				Relation("WebResourceURLs").
				Where("web_resource.name = ?", expectedResource.Metadata.Name).Select(); err != nil {
				ts.FailNow("Can not get a created WebResource from DB", err)
			}

			// Compare.
			ts.Equal(expectedResource.Metadata.ETag, dbModel.ETag)
			ts.Equal(expectedResource.Metadata.Name, dbModel.Name)
			ts.Equal(expectedResource.Data.Description, dbModel.Description)

			// Check Web Resource Categories.
			if !ts.Lenf(dbModel.WebResourceCategorys, len(expectedResource.Data.WebResourceCategoryNames),
				"WebResourceCategory Names Arrays Lengths does not match",
				"DBModel: %+v; Expected: %+v",
				dbModel.WebResourceCategorys, expectedResource.Data.WebResourceCategoryNames) {
				ts.FailNow("Lengths do not match")
			}

			sort.Slice(expectedResource.Data.WebResourceCategoryNames, func(i, j int) bool {
				return expectedResource.Data.WebResourceCategoryNames[i] < expectedResource.Data.WebResourceCategoryNames[j]
			})

			sort.Slice(dbModel.WebResourceCategorys, func(i, j int) bool {
				return dbModel.WebResourceCategorys[i].Name < dbModel.WebResourceCategorys[j].Name
			})

			for dbMInd := range dbModel.WebResourceCategorys {
				ts.Equal(dbModel.WebResourceCategorys[dbMInd].Name,
					expectedResource.Data.WebResourceCategoryNames[dbMInd],
					"Categories must be equal")
			}

			// Check Domains.
			if !ts.Lenf(dbModel.WebResourceDomains, len(expectedResource.Data.Domains),
				"WebResourceCategory Domain Arrays Lengths does not match",
				"DBModel: %+v; Expected: %+v",
				dbModel.WebResourceDomains, expectedResource.Data.Domains) {
				ts.FailNow("Lengths do not match")
			}

			sort.Slice(expectedResource.Data.Domains, func(i, j int) bool {
				return expectedResource.Data.Domains[i] < expectedResource.Data.Domains[j]
			})

			sort.Slice(dbModel.WebResourceDomains, func(i, j int) bool {
				return dbModel.WebResourceDomains[i].Domain < dbModel.WebResourceDomains[j].Domain
			})

			for domInd := range expectedResource.Data.Domains {
				ts.Equal(expectedResource.Data.Domains[domInd], dbModel.WebResourceDomains[domInd].Domain,
					"Domain names must be equal")
			}

			// Check IPs.
			if !ts.Lenf(dbModel.WebResourceIPs, len(expectedResource.Data.IPs),
				"WebResourceCategory IPs Arrays Lengths does not match",
				"DBModel: %+v; Expected: %+v",
				dbModel.WebResourceIPs, expectedResource.Data.IPs) {
				ts.FailNow("Lengths do not match")
			}

			sort.Slice(expectedResource.Data.IPs, func(i, j int) bool {
				return expectedResource.Data.IPs[i].String() < expectedResource.Data.IPs[j].String()
			})

			sort.Slice(dbModel.WebResourceIPs, func(i, j int) bool {
				return dbModel.WebResourceIPs[i].IP.String() < dbModel.WebResourceIPs[j].IP.String()
			})

			for IPInd := range expectedResource.Data.IPs {
				ts.Equal(expectedResource.Data.IPs[IPInd], dbModel.WebResourceIPs[IPInd].IP,
					"IPs addresses must be equal")
			}

			// URLs.
			if !ts.Lenf(dbModel.WebResourceURLs, len(expectedResource.Data.URLs),
				"WebResourceCategory URLs Arrays Lengths does not match",
				"DBModel: %+v; Expected: %+v",
				dbModel.WebResourceURLs, expectedResource.Data.URLs) {
				ts.FailNow("Lengths do not match")
			}

			sort.Slice(expectedResource.Data.URLs, func(i, j int) bool {
				return expectedResource.Data.URLs[i].String() < expectedResource.Data.URLs[j].String()
			})

			sort.Slice(dbModel.WebResourceURLs, func(i, j int) bool {
				return dbModel.WebResourceURLs[i].URL < dbModel.WebResourceURLs[j].URL
			})

			for URLInd := range expectedResource.Data.URLs {
				ts.Equal(expectedResource.Data.URLs[URLInd].String(), dbModel.WebResourceURLs[URLInd].URL,
					"URLs must be equal")
			}

			// Check returned result.
			if !test.getCreated {
				continue
			}

			if !ts.NotNil(got, "Must not return nil result") {
				ts.FailNow("Got nil result - can not continue")
			}

			// Domains.
			sort.Slice(got[wResInd].Data.Domains, func(i, j int) bool {
				return got[wResInd].Data.Domains[i] < got[wResInd].Data.Domains[j] // nolint:scopelint
			})

			// IPs.
			sort.Slice(got[wResInd].Data.IPs, func(i, j int) bool {
				return got[wResInd].Data.IPs[i].String() < got[wResInd].Data.IPs[j].String() // nolint:scopelint
			})

			// URLs.
			sort.Slice(got[wResInd].Data.URLs, func(i, j int) bool {
				return got[wResInd].Data.URLs[i].String() < got[wResInd].Data.URLs[j].String() // nolint:scopelint
			})

			ts.Equal(*expectedResource, got[wResInd], "Unexpected returned result")
		}
	}
}

func (ts *TestWebResourceV1sSuite) TestCreateConflict() {
	var tests = []struct {
		resources []storage.WebResourceV1

		expectedError error
	}{
		{
			resources: []storage.WebResourceV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       111,
						Kind:       storage.WebResourceKind,
						Name:       "web resource 1 cat 1", // Name conflict.
					},
					Data: storage.WebResourceDataV1{
						Description: "new web resource description",
						Domains: []storage.WebResourceDomainT{
							"domain-1.com",
							"domain-2.com",
						},
						IPs: []net.IP{
							net.ParseIP("10.8.200.1"),
							net.ParseIP("10.8.200.2"),
						},
						URLs: []url.URL{
							parseURLPanic("http://domain-1.com/path-1"),
						},
						WebResourceCategoryNames: []storage.ResourceNameT{"category 2"},
					},
				},
			},
			expectedError: storage.NewErrResourceAlreadyExists([]interface{}{
				storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       111,
					Kind:       storage.WebResourceKind,
					Name:       "web resource 1 cat 1",
				},
			}),
		},
		{
			resources: []storage.WebResourceV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       111,
						Kind:       storage.WebResourceKind,
						Name:       "new web resource 1 cat 2",
					},
					Data: storage.WebResourceDataV1{
						Description: "new web resource description",
						Domains: []storage.WebResourceDomainT{
							"domain-1.com",
							"domain-2.com",
							"example-1-wr-1.com", // Domain conflict.
						},
						IPs: []net.IP{
							net.ParseIP("10.8.200.1"),
							net.ParseIP("10.8.200.2"),
						},
						URLs: []url.URL{
							parseURLPanic("http://domain-1.com/path-1"),
						},
						WebResourceCategoryNames: []storage.ResourceNameT{"category 2"},
					},
				},
			},
			expectedError: storage.NewErrResourceAlreadyExists([]interface{}{
				fmt.Sprintf("Domains %+v", []string{
					"domain-1.com",
					"domain-2.com",
					"example-1-wr-1.com", // Domain conflict.
				}),
			}),
		},
		{
			resources: []storage.WebResourceV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       111,
						Kind:       storage.WebResourceKind,
						Name:       "new web resource 1 cat 2",
					},
					Data: storage.WebResourceDataV1{
						Description: "new web resource description",
						Domains: []storage.WebResourceDomainT{
							"domain-1.com",
							"domain-2.com",
						},
						IPs: []net.IP{
							net.ParseIP("10.8.100.1"), // IP Conflict.
							net.ParseIP("10.8.200.2"),
						},
						URLs: []url.URL{
							parseURLPanic("http://domain-1.com/path-1"),
						},
						WebResourceCategoryNames: []storage.ResourceNameT{"category 2"},
					},
				},
			},
			expectedError: storage.NewErrResourceAlreadyExists([]interface{}{
				fmt.Sprintf("IPs %+v", []string{
					"10.8.100.1",
					"10.8.200.2",
				}),
			}),
		},
		{
			resources: []storage.WebResourceV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       111,
						Kind:       storage.WebResourceKind,
						Name:       "new web resource 1 cat 2",
					},
					Data: storage.WebResourceDataV1{
						Description: "new web resource description",
						Domains: []storage.WebResourceDomainT{
							"domain-1.com",
							"domain-2.com",
						},
						IPs: []net.IP{
							net.ParseIP("10.8.200.2"),
						},
						URLs: []url.URL{
							parseURLPanic("http://domain-1.com/path-1"),
							parseURLPanic("http://example-1-web-resource-2.com"), // URL conflict.
						},
						WebResourceCategoryNames: []storage.ResourceNameT{"category 2"},
					},
				},
			},
			expectedError: storage.NewErrResourceAlreadyExists([]interface{}{
				fmt.Sprintf("URLs %+v", []string{
					"http://domain-1.com/path-1",
					"http://example-1-web-resource-2.com",
				}),
			}),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.CreateWebResourceV1sContext(context.TODO(),
			test.resources, true)

		ts.Nil(got, "Must return nil result")

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestWebResourceV1sSuite) TestCreateCategoryNotFound() {
	var tests = []struct {
		resources []storage.WebResourceV1

		expectedError error
	}{
		{
			resources: []storage.WebResourceV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       111,
						Kind:       storage.WebResourceKind,
						Name:       "new web resource 1 at category 2",
					},
					Data: storage.WebResourceDataV1{
						Description: "new web resource description",
						Domains: []storage.WebResourceDomainT{
							"domain-1.com",
							"domain-2.com",
						},
						IPs: []net.IP{
							net.ParseIP("10.8.200.1"),
							net.ParseIP("10.8.200.2"),
						},
						URLs: []url.URL{
							parseURLPanic("http://domain-1.com/path-1"),
						},
						WebResourceCategoryNames: []storage.ResourceNameT{"non-existing category 2"}, // No such a category.
					},
				},
			},
			expectedError: storage.NewErrRelatedResourcesNotFound(
				storage.APIVersionV1Value,
				storage.WebResourceCategoryKind,
				[]storage.ResourceNameT{"non-existing category 2"},
				nil,
			),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.CreateWebResourceV1sContext(context.TODO(),
			test.resources, true)

		ts.Nil(got, "Must return nil result")

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func TestWebResourceV1s(t *testing.T) {
	suite.Run(t, &TestWebResourceV1sSuite{})
}

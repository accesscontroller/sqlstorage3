package sqlstorage3

import (
	"context"
	"encoding/json"
	"testing"
	"time"

	"github.com/go-pg/pg/v10"
	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

type TestExternalSessionsSourceV1Suite struct {
	db *pg.DB

	sqlStorage *SQLStorage

	suite.Suite
}

func (ts *TestExternalSessionsSourceV1Suite) SetupTest() {
	db, err := connectDB()
	if err != nil {
		ts.FailNow("Can not connect to test DB", err)
	}

	ts.db = db
	ts.sqlStorage = &SQLStorage{db: db}

	// Create DB Tables.
	if err := recreateTables(ts.db); err != nil {
		ts.FailNow("Can not initialize DB", err)
	}

	// Create some init data.

	// ExternalUsersGroupsSource.
	var externalUsersGroupsSources = []ExternalUsersGroupsSource{
		{
			ETag:              1,
			GetMode:           storage.ExternalUsersGroupsGetModePassive,
			GroupsGetSettings: "get groups settings",
			GroupsToLoad:      []string{"group1", "group2"},
			UsersGetSettings:  "get users settings",
			Type:              storage.ExternalUsersGroupsSourceTypeLDAP,
			Name:              "source 1",
			PollInterval:      121,
		},
		{
			ETag:              1,
			GetMode:           storage.ExternalUsersGroupsGetModePassive,
			GroupsGetSettings: "get groups settings",
			GroupsToLoad:      []string{"group1", "group2"},
			UsersGetSettings:  "get users settings",
			Type:              storage.ExternalUsersGroupsSourceTypeLDAP,
			Name:              "source 2",
			PollInterval:      122,
		},
	}

	for i := range externalUsersGroupsSources {
		if _, err := db.Model(&externalUsersGroupsSources[i]).Insert(); err != nil {
			ts.FailNow("Can not create init data", err.Error())
		}
	}

	// Session Sources.
	var sessionSources = []ExternalSessionsSource{
		{
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[0].ID,
			ETag:                        1,
			GetMode:                     storage.ExternalSessionsGetModePassive,
			Name:                        "sessions source 1",
			Settings:                    []byte(`{"Name": "source 1", "Key1": "Value1", "Key2": "Value2"}`),
			Type:                        storage.ExternalSessionsSourceTypeMSADEvents,
		},
		{
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[1].ID,
			ETag:                        2,
			GetMode:                     storage.ExternalSessionsGetModePoll,
			Name:                        "sessions source 2",
			Settings:                    []byte(`{"Name": "source 2", "Key2": "Value2", "Key3": "Value3"}`),
			Type:                        storage.ExternalSessionsSourceTypeMSADEvents,
		},
	}

	for i := range sessionSources {
		if _, err := db.Model(&sessionSources[i]).Insert(); err != nil {
			ts.FailNow("Can not create init data", err.Error())
		}
	}
}

func (ts *TestExternalSessionsSourceV1Suite) TearDownTest() {
	if ts.db != nil {
		if err := ts.db.Close(); err != nil {
			ts.FailNow("Can not Close DB", err)
		}
	}
}

func (ts *TestExternalSessionsSourceV1Suite) TestGetSuccess() {
	var tests = []struct {
		name storage.ResourceNameT

		source storage.ExternalSessionsSourceV1
	}{
		{
			name: "sessions source 1",
			source: storage.ExternalSessionsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "sessions source 1",
				},
				Data: storage.ExternalSessionsSourceDataV1{
					GetMode:                 storage.ExternalSessionsGetModePassive,
					ExternalUsersSourceName: "source 1",
					Type:                    storage.ExternalSessionsSourceTypeMSADEvents,
					Settings: map[string]string{
						"Name": "source 1",
						"Key1": "Value1",
						"Key2": "Value2",
					},
				},
			},
		},
		{
			name: "sessions source 2",
			source: storage.ExternalSessionsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       2,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "sessions source 2",
				},
				Data: storage.ExternalSessionsSourceDataV1{
					GetMode:                 storage.ExternalSessionsGetModePoll,
					ExternalUsersSourceName: "source 2",
					Type:                    storage.ExternalSessionsSourceTypeMSADEvents,
					Settings: map[string]string{
						"Name": "source 2",
						"Key3": "Value3",
						"Key2": "Value2",
					},
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.GetExternalSessionsSourceV1Context(context.TODO(), test.name)

		ts.NoError(gotErr, "Must not return error")

		if !ts.NotNil(got, "nil result") {
			ts.FailNow("nil result - can not continue")
		}

		ts.Equal(test.source, *got, "Unexpected result")
	}
}

func (ts *TestExternalSessionsSourceV1Suite) TestGetNotFound() {
	var tests = []struct {
		name storage.ResourceNameT

		expectedError error
	}{
		{
			name: "non existing sessions source 1",

			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "non existing sessions source 1",
				},
			),
		},
	}

	for _, test := range tests {
		got, gotErr := ts.sqlStorage.GetExternalSessionsSourceV1Context(context.TODO(), test.name)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("nil error - can not continue")
		}

		ts.Nil(got, "must be nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalSessionsSourceV1Suite) TestCreateSuccess() {
	var tests = []struct {
		getResult bool

		source storage.ExternalSessionsSourceV1
	}{
		{
			source: storage.ExternalSessionsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "external sessions source 10",
				},
				Data: storage.ExternalSessionsSourceDataV1{
					GetMode:                 storage.ExternalSessionsGetModePassive,
					ExternalUsersSourceName: "source 1",
					Settings: map[string]string{
						"key 10": "value 10",
					},
					Type: storage.ExternalSessionsSourceTypeMSADEvents,
				},
			},
		},
		{
			getResult: true,
			source: storage.ExternalSessionsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "external sessions source 11",
				},
				Data: storage.ExternalSessionsSourceDataV1{
					GetMode:                 storage.ExternalSessionsGetModePoll,
					ExternalUsersSourceName: "source 2",
					Settings: map[string]string{
						"key 11": "value 11",
					},
					Type: storage.ExternalSessionsSourceTypeMSADEvents,
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.CreateExternalSessionsSourceV1Context(context.TODO(), &test.source, test.getResult)

		if !ts.NoError(gotErr, "must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Check that the Source was created.
		var dbSource = &ExternalSessionsSource{}

		if err := ts.db.Model(dbSource).Relation("ExternalUsersGroupsSource").
			Where("external_sessions_source.name = ?", test.source.Metadata.Name).
			Select(); err != nil {
			ts.FailNow("Can not get created ExternalSessionsSource from DB", err)
		}

		// Compare.
		dbConverted, err := dbSource.toStorageExternalSessionsSource()
		if err != nil {
			ts.FailNow("Can not convert DB Model to ExternalSessionsSource", err)
		}

		ts.Equal(test.source, *dbConverted, "Unexpected DB ExternalSessionsSource")

		// Check result
		if !test.getResult {
			continue
		}

		if !ts.NotNil(got, "Must not return nil ExternalSessionsSourceV1") {
			ts.FailNow("nil as ExternalSessionsSourceV1 - can not continue")
		}

		ts.Equal(test.source, *got, "Unexpected result")
	}
}

func (ts *TestExternalSessionsSourceV1Suite) TestCreateExternalUsersGroupsSourceNotFound() {
	var tests = []struct {
		expectedError error

		source storage.ExternalSessionsSourceV1
	}{
		{
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       "non existing source 1",
				},
			),
			source: storage.ExternalSessionsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "external sessions source 10",
				},
				Data: storage.ExternalSessionsSourceDataV1{
					GetMode:                 storage.ExternalSessionsGetModePassive,
					ExternalUsersSourceName: "non existing source 1",
					Settings: map[string]string{
						"key 10": "value 10",
					},
					Type: storage.ExternalSessionsSourceTypeMSADEvents,
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.CreateExternalSessionsSourceV1Context(context.TODO(), &test.source, true)

		// Check that the Source was not created.
		var dbSource = &ExternalSessionsSource{}

		created, err := ts.db.Model(dbSource).Relation("ExternalUsersGroupsSource").
			Where("external_sessions_source.name = ?", test.source.Metadata.Name).
			Exists()
		if err != nil {
			ts.FailNow("Can not check is created ExternalSessionsSource is created", err)
		}

		ts.False(created, "Must return created==false")

		// Check error.
		if !ts.Error(gotErr, "must return error") {
			ts.FailNow("nil error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")

		ts.Nil(got, "Must return nil result")
	}
}

func (ts *TestExternalSessionsSourceV1Suite) TestCreateConflict() {
	var tests = []struct {
		expectedError error

		source storage.ExternalSessionsSourceV1
	}{
		{
			expectedError: storage.NewErrResourceAlreadyExists([]interface{}{
				storage.ExternalSessionsSourceV1{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       1,
						Kind:       storage.ExternalSessionsSourceKind,
						Name:       "sessions source 1",
					},
					Data: storage.ExternalSessionsSourceDataV1{
						GetMode:                 storage.ExternalSessionsGetModePassive,
						ExternalUsersSourceName: "source 1",
						Settings: map[string]string{
							"key 10": "value 10",
						},
						Type: storage.ExternalSessionsSourceTypeMSADEvents,
					},
				},
			}),
			source: storage.ExternalSessionsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "sessions source 1",
				},
				Data: storage.ExternalSessionsSourceDataV1{
					GetMode:                 storage.ExternalSessionsGetModePassive,
					ExternalUsersSourceName: "source 1",
					Settings: map[string]string{
						"key 10": "value 10",
					},
					Type: storage.ExternalSessionsSourceTypeMSADEvents,
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.CreateExternalSessionsSourceV1Context(context.TODO(), &test.source, true)

		// Check error.
		if !ts.Error(gotErr, "must return error") {
			ts.FailNow("nil error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")

		ts.Nil(got, "Must return nil result")
	}
}

func (ts *TestExternalSessionsSourceV1Suite) TestUpdateSuccess() {
	var tests = []struct {
		getResult bool

		oldName storage.ResourceNameT

		patch storage.ExternalSessionsSourceV1
	}{
		{
			getResult: true,
			oldName:   "sessions source 1",
			patch: storage.ExternalSessionsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "renamed external sessions source 1",
				},
				Data: storage.ExternalSessionsSourceDataV1{
					GetMode:                 storage.ExternalSessionsGetModePoll,
					ExternalUsersSourceName: "source 1",
					Settings: map[string]string{
						"New Key 1": "New Value 1",
					},
					Type: storage.ExternalSessionsSourceTypeMSADEvents,
				},
			},
		},
		{
			getResult: false,
			oldName:   "sessions source 2",
			patch: storage.ExternalSessionsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       2,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "renamed external sessions source 2",
				},
				Data: storage.ExternalSessionsSourceDataV1{
					GetMode:                 storage.ExternalSessionsGetModePassive,
					ExternalUsersSourceName: "source 2",
					Settings: map[string]string{
						"New Key 1": "New Value 1",
					},
					Type: storage.ExternalSessionsSourceTypeMSADEvents,
				},
			},
		},
	}

	for i := range tests {
		tp := &tests[i]

		got, gotErr := ts.sqlStorage.UpdateExternalSessionsSourceV1Context(context.TODO(), tp.oldName, &tp.patch, tp.getResult)

		if !ts.NoError(gotErr, "must not return error") {
			ts.FailNow("Update error - can not continue")
		}

		// Check change.
		var dbSource ExternalSessionsSource

		if err := ts.db.Model(&dbSource).Where("external_sessions_source.name = ?", tp.patch.Metadata.Name).
			Relation("ExternalUsersGroupsSource").Select(); err != nil {
			ts.FailNow("Can not load an updated ExternalSessionsSource from DB", err)
		}

		ts.Equal(tp.patch.Metadata.Name, dbSource.Name)
		ts.Equal(tp.patch.Metadata.ETag+1, dbSource.ETag)
		ts.Equal(tp.patch.Data.ExternalUsersSourceName, dbSource.ExternalUsersGroupsSource.Name)
		ts.Equal(tp.patch.Data.GetMode, dbSource.GetMode)
		ts.Equal(tp.patch.Data.Type, dbSource.Type)

		expB, err := json.Marshal(tp.patch.Data.Settings)
		if err != nil {
			ts.FailNow("Can not json.Marshal expected Settings")
		}

		ts.Equal(expB, dbSource.Settings)
		ts.WithinDuration(time.Now(), dbSource.UpdatedAt, time.Minute)

		if !tp.getResult {
			continue
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("nil result - can not continue")
		}

		// Correct ETag.
		got.Metadata.ETag--

		ts.Equal(tp.patch, *got, "Unexpected value")
	}
}

func (ts *TestExternalSessionsSourceV1Suite) TestUpdateNotFound() {
	var tests = []struct {
		getResult bool

		oldName storage.ResourceNameT

		patch storage.ExternalSessionsSourceV1

		expectedError error
	}{
		{
			getResult: true,
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       0,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "not found sessions source 1",
				},
			),
			oldName: "not found sessions source 1",
			patch: storage.ExternalSessionsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "renamed external sessions source 1",
				},
				Data: storage.ExternalSessionsSourceDataV1{
					GetMode:                 storage.ExternalSessionsGetModePoll,
					ExternalUsersSourceName: "source 1",
					Settings: map[string]string{
						"New Key 1": "New Value 1",
					},
					Type: storage.ExternalSessionsSourceTypeMSADEvents,
				},
			},
		},
	}

	for i := range tests {
		tp := &tests[i]

		got, gotErr := ts.sqlStorage.UpdateExternalSessionsSourceV1Context(context.TODO(), tp.oldName, &tp.patch, tp.getResult)

		if !ts.Error(gotErr, "must return error") {
			ts.FailNow("no Update error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(tp.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalSessionsSourceV1Suite) TestUpdateETagError() {
	var tests = []struct {
		getResult bool

		oldName storage.ResourceNameT

		patch storage.ExternalSessionsSourceV1

		expectedError error
	}{
		{
			getResult:     true,
			expectedError: storage.NewErrETagDoesNotMatch(1, 1111),
			oldName:       "sessions source 1",
			patch: storage.ExternalSessionsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1111, // Incorrect.
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "renamed external sessions source 1",
				},
				Data: storage.ExternalSessionsSourceDataV1{
					GetMode:                 storage.ExternalSessionsGetModePoll,
					ExternalUsersSourceName: "source 1",
					Settings: map[string]string{
						"New Key 1": "New Value 1",
					},
					Type: storage.ExternalSessionsSourceTypeMSADEvents,
				},
			},
		},
	}

	for i := range tests {
		tp := &tests[i]

		got, gotErr := ts.sqlStorage.UpdateExternalSessionsSourceV1Context(context.TODO(), tp.oldName, &tp.patch, tp.getResult)

		if !ts.Error(gotErr, "must return error") {
			ts.FailNow("no Update error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(tp.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalSessionsSourceV1Suite) TestUpdateForbiddenField() {
	var tests = []struct {
		getResult bool

		oldName storage.ResourceNameT

		patch storage.ExternalSessionsSourceV1

		expectedError error
	}{
		{
			getResult: true,
			expectedError: storage.NewErrForbiddenUpdateReadOnlyField(storage.MetadataV1{
				APIVersion: storage.APIVersionV1Value,
				ETag:       1,
				Kind:       storage.ExternalSessionsSourceKind,
				Name:       "renamed external sessions source 1",
			},
				"Data#ExternalUsersSourceName"),
			oldName: "sessions source 1",
			patch: storage.ExternalSessionsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "renamed external sessions source 1",
				},
				Data: storage.ExternalSessionsSourceDataV1{
					GetMode:                 storage.ExternalSessionsGetModePoll,
					ExternalUsersSourceName: "forbidden update source 1",
					Settings: map[string]string{
						"New Key 1": "New Value 1",
					},
					Type: storage.ExternalSessionsSourceTypeMSADEvents,
				},
			},
		},
	}

	for i := range tests {
		tp := &tests[i]

		got, gotErr := ts.sqlStorage.UpdateExternalSessionsSourceV1Context(context.TODO(), tp.oldName, &tp.patch, tp.getResult)

		if !ts.Error(gotErr, "must return error") {
			ts.FailNow("no Update error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(tp.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalSessionsSourceV1Suite) TestUpdateNewNameConflict() {
	var tests = []struct {
		getResult bool

		oldName storage.ResourceNameT

		patch storage.ExternalSessionsSourceV1

		expectedError error
	}{
		{
			getResult: true,
			expectedError: storage.NewErrResourceAlreadyExists([]interface{}{
				storage.ExternalSessionsSourceV1{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       1,
						Kind:       storage.ExternalSessionsSourceKind,
						Name:       "sessions source 2",
					},
					Data: storage.ExternalSessionsSourceDataV1{
						GetMode:                 storage.ExternalSessionsGetModePoll,
						ExternalUsersSourceName: "source 1",
						Settings: map[string]string{
							"New Key 1": "New Value 1",
						},
						Type: storage.ExternalSessionsSourceTypeMSADEvents,
					},
				},
			}),
			oldName: "sessions source 1",
			patch: storage.ExternalSessionsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "sessions source 2", // Name conflict.
				},
				Data: storage.ExternalSessionsSourceDataV1{
					GetMode:                 storage.ExternalSessionsGetModePoll,
					ExternalUsersSourceName: "source 1",
					Settings: map[string]string{
						"New Key 1": "New Value 1",
					},
					Type: storage.ExternalSessionsSourceTypeMSADEvents,
				},
			},
		},
	}

	for i := range tests {
		tp := &tests[i]

		got, gotErr := ts.sqlStorage.UpdateExternalSessionsSourceV1Context(context.TODO(), tp.oldName, &tp.patch, tp.getResult)

		if !ts.Error(gotErr, "must return error") {
			ts.FailNow("no Update error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(tp.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalSessionsSourceV1Suite) TestDeleteSuccess() {
	var tests = []struct {
		source storage.ExternalSessionsSourceV1
	}{
		{
			source: storage.ExternalSessionsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "sessions source 1",
				}, // Data section is not important.
			},
		},
	}

	for i := range tests {
		pt := &tests[i]

		gotErr := ts.sqlStorage.DeleteExternalSessionsSourceV1Context(context.TODO(), &pt.source)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("got error - can not continue")
		}

		// Check that there is no more the source.
		var dbSource ExternalSessionsSource

		exists, err := ts.db.Model(&dbSource).Where("external_sessions_source.name = ?", pt.source.Metadata.Name).Exists()
		if err != nil {
			ts.FailNow("Can not check if the Source exists", err)
		}

		ts.False(exists, "Must return is exists == false")
	}
}

func (ts *TestExternalSessionsSourceV1Suite) TestDeleteNotFound() {
	var tests = []struct {
		expectedError error

		source storage.ExternalSessionsSourceV1
	}{
		{
			source: storage.ExternalSessionsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "not existing sessions source 1",
				}, // Data section is not important.
			},
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "not existing sessions source 1",
				},
			),
		},
	}

	for i := range tests {
		pt := &tests[i]

		gotErr := ts.sqlStorage.DeleteExternalSessionsSourceV1Context(context.TODO(), &pt.source)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("got no error - can not continue")
		}

		// Check that there is no the source with the name.
		var dbSource ExternalSessionsSource

		exists, err := ts.db.Model(&dbSource).Where("external_sessions_source.name = ?", pt.source.Metadata.Name).Exists()
		if err != nil {
			ts.FailNow("Can not check if the Source exists", err)
		}

		ts.False(exists, "Must return is exists == false")

		// Check error.
		ts.Equal(pt.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalSessionsSourceV1Suite) TestDeleteETagError() {
	var tests = []struct {
		expectedError error

		source storage.ExternalSessionsSourceV1
	}{
		{
			source: storage.ExternalSessionsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       111, // Incorrect ETag.
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "sessions source 1",
				}, // Data section is not important.
			},
			expectedError: storage.NewErrETagDoesNotMatch(1, 111),
		},
	}

	for i := range tests {
		pt := &tests[i]

		gotErr := ts.sqlStorage.DeleteExternalSessionsSourceV1Context(context.TODO(), &pt.source)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("got no error - can not continue")
		}

		// Check that there is the source with the name.
		var dbSource ExternalSessionsSource

		exists, err := ts.db.Model(&dbSource).Where("external_sessions_source.name = ?", pt.source.Metadata.Name).Exists()
		if err != nil {
			ts.FailNow("Can not check if the Source exists", err)
		}

		ts.True(exists, "Must return is exists == true")

		// Check error.
		ts.Equal(pt.expectedError, gotErr, "Unexpected error value")
	}
}

func TestExternalSessionsSourceV1(t *testing.T) {
	suite.Run(t, &TestExternalSessionsSourceV1Suite{})
}

package sqlstorage3

import (
	"context"
	"net"
	"testing"
	"time"

	"github.com/go-pg/pg/v10"
	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

type TestExternalUserV1sSuite struct {
	db *pg.DB

	sqlStorage *SQLStorage

	suite.Suite

	openTimes, closeTimes map[storage.ResourceNameT]*time.Time
}

func (ts *TestExternalUserV1sSuite) SetupTest() {
	ts.openTimes = make(map[storage.ResourceNameT]*time.Time)
	ts.closeTimes = make(map[storage.ResourceNameT]*time.Time)

	db, err := connectDB()
	if err != nil {
		ts.FailNow("Can not connect to test DB", err)
	}

	ts.db = db
	ts.sqlStorage = &SQLStorage{db: db}

	// Create tables.
	if err := recreateTables(ts.db); err != nil {
		ts.FailNow("Can not initialize DB", err)
	}

	// Create some init data.

	// ExternalUsersGroupsSource.
	var externalUsersGroupsSources = []ExternalUsersGroupsSource{
		{
			ETag:              1,
			GetMode:           storage.ExternalUsersGroupsGetModePassive,
			GroupsGetSettings: "get groups settings",
			GroupsToLoad:      []string{"group1", "group2"},
			UsersGetSettings:  "get users settings",
			Type:              storage.ExternalUsersGroupsSourceTypeLDAP,
			Name:              "source 1",
			PollInterval:      121,
		},
		{
			ETag:              1,
			GetMode:           storage.ExternalUsersGroupsGetModePassive,
			GroupsGetSettings: "get groups settings",
			GroupsToLoad:      []string{"group1", "group2"},
			UsersGetSettings:  "get users settings",
			Type:              storage.ExternalUsersGroupsSourceTypeLDAP,
			Name:              "source 2",
			PollInterval:      122,
		},
	}

	for i := range externalUsersGroupsSources {
		if _, err := db.Model(&externalUsersGroupsSources[i]).Insert(); err != nil {
			ts.FailNow("Can not create init data", err.Error())
		}
	}

	// Session Sources.
	var sessionSources = []ExternalSessionsSource{
		{
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[0].ID,
			ETag:                        1,
			GetMode:                     storage.ExternalSessionsGetModePassive,
			Name:                        "sessions source 1",
			Settings:                    []byte(`{"Name": "source 1", "Key1": "Value1", "Key2": "Value2"}`),
			Type:                        storage.ExternalSessionsSourceTypeMSADEvents,
		},
		{
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[1].ID,
			ETag:                        2,
			GetMode:                     storage.ExternalSessionsGetModePoll,
			Name:                        "sessions source 2",
			Settings:                    []byte(`{"Name": "source 2", "Key2": "Value2", "Key3": "Value3"}`),
			Type:                        storage.ExternalSessionsSourceTypeMSADEvents,
		},
	}

	for i := range sessionSources {
		if _, err := db.Model(&sessionSources[i]).Insert(); err != nil {
			ts.FailNow("Can not create init data", err.Error())
		}
	}

	// External Users.
	var externalUsers = []ExternalUser{
		{
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[0].ID,
			ETag:                        1,
			Name:                        "user 1 at source 1",
		},
		{
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[0].ID,
			ETag:                        2,
			Name:                        "user 2 at source 1",
		},
		{
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[1].ID,
			ETag:                        3,
			Name:                        "user 1 at source 2",
		},
		{
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[1].ID,
			ETag:                        4,
			Name:                        "user 2 at source 2",
		},
	}

	for i := range externalUsers {
		if _, err := db.Model(&externalUsers[i]).Insert(); err != nil {
			ts.FailNow("Can not insert ExternalUser", err)
		}
	}

	// External User Sessions.
	var externalUserSessions = []ExternalUserSession{
		{
			ExternalSessionsSourceID: sessionSources[0].ID,
			ETag:                     1,
			ExternalUserID:           externalUsers[0].ID,
			HostName:                 "hostname-1",
			IPAddress:                net.ParseIP("10.0.0.1"),
			Name:                     "session 1 source 1",
			OpenTimestamp:            time.Now().Truncate(time.Microsecond),
		},
		{
			ExternalSessionsSourceID: sessionSources[1].ID,
			ETag:                     1,
			ExternalUserID:           externalUsers[1].ID,
			HostName:                 "hostname-2",
			IPAddress:                net.ParseIP("10.0.0.2"),
			Name:                     "session 1 source 2",
			OpenTimestamp:            time.Now().Truncate(time.Microsecond),
		},
		{
			ExternalSessionsSourceID: sessionSources[0].ID,
			ETag:                     1,
			ExternalUserID:           externalUsers[0].ID,
			HostName:                 "hostname-3",
			IPAddress:                net.ParseIP("10.0.0.3"),
			Name:                     "session 2 source 1",
			OpenTimestamp:            time.Now().Truncate(time.Microsecond),
		},
		{
			ExternalSessionsSourceID: sessionSources[1].ID,
			ETag:                     1,
			ExternalUserID:           externalUsers[1].ID,
			HostName:                 "hostname-1",
			IPAddress:                net.ParseIP("10.0.0.4"),
			Name:                     "closed session 2 source 2",
			OpenTimestamp:            time.Now().Add(-time.Hour).Truncate(time.Microsecond),
			CloseTimestamp:           time.Now().Truncate(time.Microsecond),
			Closed:                   true,
		},
	}

	for i := range externalUserSessions {
		ps := &externalUserSessions[i]

		ts.openTimes[ps.Name] = &ps.OpenTimestamp

		if ps.Closed {
			ts.closeTimes[ps.Name] = &ps.CloseTimestamp
		}

		if _, err := db.Model(ps).Insert(); err != nil {
			ts.FailNow("Can not insert ExternalUserSession", err)
		}
	}
}

func (ts *TestExternalUserV1sSuite) TearDownTest() {
	if ts.db != nil {
		if err := ts.db.Close(); err != nil {
			ts.FailNow("Can not Close DB", err)
		}
	}
}

func (ts *TestExternalUserV1sSuite) TestGet() {
	var tests = []struct {
		source storage.ResourceNameT

		filters []storage.ExternalUserFilterV1

		expectedUsers []storage.ExternalUserV1
	}{
		{
			source: "source 1",
			filters: []storage.ExternalUserFilterV1{
				{
					Name: "user 1 at source 1",
				},
				{
					Name: "user 1 at source 2",
				},
			},
			expectedUsers: []storage.ExternalUserV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       1,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 1",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						ExternalResource: true,
						Kind:             storage.ExternalUserKind,
						Name:             "user 1 at source 1",
					},
					Data: storage.ExternalUserDataV1{},
				},
			},
		},
		{
			source: "source 1",
			filters: []storage.ExternalUserFilterV1{
				{
					NameSubstringMatch: "user 1",
				},
				{
					Name: "user 1 at source 2",
				},
			},
			expectedUsers: []storage.ExternalUserV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       1,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 1",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						ExternalResource: true,
						Kind:             storage.ExternalUserKind,
						Name:             "user 1 at source 1",
					},
					Data: storage.ExternalUserDataV1{},
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.GetExternalUserV1sContext(context.TODO(),
			test.filters, test.source)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("got error - can not continue")
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("nil result - can not continue")
		}

		ts.Equal(test.expectedUsers, got, "Unexpected result")
	}
}

func (ts *TestExternalUserV1sSuite) TestCreateSuccess() {
	var tests = []struct {
		getCreated bool

		users []storage.ExternalUserV1
	}{
		{
			getCreated: false,
			users: []storage.ExternalUserV1{{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "user 5 at source 1",
				},
				Data: storage.ExternalUserDataV1{},
			},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             10,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 1",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalUserKind,
						Name: "user 6 at source 1",
					},
					Data: storage.ExternalUserDataV1{},
				},
			},
		},
		{
			getCreated: true,
			users: []storage.ExternalUserV1{{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 2",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "user 6 at source 2",
				},
				Data: storage.ExternalUserDataV1{},
			},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             10,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 2",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalUserKind,
						Name: "user 7 at source 2",
					},
					Data: storage.ExternalUserDataV1{},
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.CreateExternalUserV1sContext(context.TODO(),
			test.users, test.getCreated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error, can not continue")
		}

		// Check created Users one by one.
		for euI := range test.users {
			expEU := &test.users[euI]

			var dbUser ExternalUser

			if err := ts.db.Model(&dbUser).Relation("ExternalUsersGroupsSource").
				Where("external_users_groups_source.name = ? AND external_user.name = ?",
					expEU.Metadata.ExternalSource.SourceName, expEU.Metadata.Name).Select(); err != nil {
				ts.FailNow("Can not get a User from DB", err)
			}

			// Compare.
			ts.Equal(expEU.Metadata.ETag, dbUser.ETag)
			ts.Equal(expEU.Metadata.ExternalSource.SourceName, dbUser.ExternalUsersGroupsSource.Name)
			ts.Equal(expEU.Metadata.Name, dbUser.Name)
			ts.True(expEU.Metadata.ExternalResource)
			ts.WithinDuration(time.Now(), dbUser.CreatedAt, time.Minute)

			// If returned value.
			if !test.getCreated {
				continue
			}

			if !ts.NotNil(got, "Must not return nil result") {
				ts.FailNow("nil result - can not continue")
			}

			ts.Equal(*expEU, got[euI], "Unexpected value")
		}
	}
}

func (ts *TestExternalUserV1sSuite) TestCreateConflict() {
	var tests = []struct {
		users []storage.ExternalUserV1

		expectedError error
	}{
		{
			users: []storage.ExternalUserV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             10,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 2",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalUserKind,
						Name: "user 1 at source 2", // Name conflict.
					},
					Data: storage.ExternalUserDataV1{},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             10,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 2",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalUserKind,
						Name: "user 1000 at source 2", // No Conflict.
					},
					Data: storage.ExternalUserDataV1{},
				},
			},
			expectedError: storage.NewErrResourceAlreadyExists([]interface{}{
				storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             10,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 2",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "user 1 at source 2", // Name conflict.
				},
			}),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.CreateExternalUserV1sContext(context.TODO(),
			test.users, true)

		ts.Nil(got, "Must return nil result")

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("No error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalUserV1sSuite) TestCreateSourceNotFound() {
	var tests = []struct {
		source storage.ResourceNameT
		users  []storage.ExternalUserV1

		expectedError error
	}{
		{
			source: "non existing source",
			users: []storage.ExternalUserV1{{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             10,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "non existing source", // Not such a source.
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "user 100 at source 2",
				},
				Data: storage.ExternalUserDataV1{},
			}},
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Name:       "non existing source",
					Kind:       storage.ExternalUsersGroupsSourceKind,
				},
			),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.CreateExternalUserV1sContext(context.TODO(),
			test.users, true)

		ts.Nil(got, "Must return nil result")

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("No error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalUserV1sSuite) TestCreateIncorrectSource() {
	var tests = []struct {
		users []storage.ExternalUserV1

		expectedError error
	}{
		{
			users: []storage.ExternalUserV1{{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             10,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 2", // Different source.
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "user 100 at source 2",
				},
				Data: storage.ExternalUserDataV1{},
			},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             10,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 1", // Different source.
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalUserKind,
						Name: "user 101 at source 1",
					},
					Data: storage.ExternalUserDataV1{},
				},
			},
			expectedError: storage.NewErrIncorrectExternalSource("source 2",
				storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             10,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1", // Different source.
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "user 101 at source 1",
				}),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.CreateExternalUserV1sContext(context.TODO(),
			test.users, true)

		ts.Nil(got, "Must return nil result")

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("No error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalUserV1sSuite) TestDeleteSuccess() {
	var tests = []struct {
		users []storage.ExternalUserV1
	}{
		{
			users: []storage.ExternalUserV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 1",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalUserKind,
						Name: "user 1 at source 1",
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             2,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 1",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalUserKind,
						Name: "user 2 at source 1",
					},
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.sqlStorage.DeleteExternalUserV1sContext(context.TODO(), test.users)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Check that the user is deleted.
		for uID := range test.users {
			userCh := &test.users[uID]

			exists, err := ts.db.Model((*ExternalUser)(nil)).
				Relation("ExternalUsersGroupsSource").
				Where("external_users_groups_source.name = ? AND external_user.name = ?",
					userCh.Metadata.ExternalSource.SourceName, userCh.Metadata.Name).Exists()
			if err != nil {
				ts.FailNow("Can not check a User existence in DB", err)
			}

			ts.False(exists, "Must return Exists == false")
		}
	}
}

func (ts *TestExternalUserV1sSuite) TestDeleteNotFound() {
	var tests = []struct {
		users []storage.ExternalUserV1

		expectedError error
	}{
		{
			users: []storage.ExternalUserV1{{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "not existing user 1 at source 1", // No such a User.
				}},
			},
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					ETag: 1,
					Kind: storage.ExternalUserKind,
					Name: "not existing user 1 at source 1",
				},
			),
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.sqlStorage.DeleteExternalUserV1sContext(context.TODO(), test.users)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		// Check that the user is not exists.
		for uID := range test.users {
			userCh := &test.users[uID]

			exists, err := ts.db.Model((*ExternalUser)(nil)).
				Relation("ExternalUsersGroupsSource").
				Where("external_users_groups_source.name = ? AND external_user.name = ?",
					userCh.Metadata.ExternalSource.SourceName, userCh.Metadata.Name).Exists()
			if err != nil {
				ts.FailNow("Can not check a User existence in DB", err)
			}

			ts.False(exists, "Must return Exists == false")
		}

		// Check Error.
		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalUserV1sSuite) TestDeleteETagIncorrect() {
	var tests = []struct {
		users []storage.ExternalUserV1

		expectedError error
	}{
		{
			users: []storage.ExternalUserV1{{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             101,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "user 1 at source 1", // No such a User.
				},
			}},
			expectedError: storage.NewErrETagDoesNotMatch(1, 101),
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.sqlStorage.DeleteExternalUserV1sContext(context.TODO(), test.users)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		// Check that the users are in DB.
		for uID := range test.users {
			userCh := &test.users[uID]

			exists, err := ts.db.Model((*ExternalUser)(nil)).
				Relation("ExternalUsersGroupsSource").
				Where("external_users_groups_source.name = ? AND external_user.name = ?",
					userCh.Metadata.ExternalSource.SourceName, userCh.Metadata.Name).Exists()
			if err != nil {
				ts.FailNow("Can not check a User existence in DB", err)
			}

			ts.True(exists, "Must return Exists == True")
		}

		// Check Error.
		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func TestExternalUserV1s(t *testing.T) {
	suite.Run(t, &TestExternalUserV1sSuite{})
}

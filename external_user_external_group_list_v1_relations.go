package sqlstorage3

import (
	"context"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func (s *SQLStorage) GetExternalUserExternalGroupV1sContext(_ context.Context,
	externalGroupSource, externalUser storage.ResourceNameT) (*storage.ExternalUser2ExternalGroupListRelationsV1, error) {
	return nil, ErrorNotImplemented
}

func (s *SQLStorage) BindExternalUser2ExternalGroupV1sContext(_ context.Context,
	relations *storage.ExternalUser2ExternalGroupListRelationsV1) error {
	return ErrorNotImplemented
}

func (s *SQLStorage) UnBindExternalUser2ExternalGroupV1sContext(_ context.Context,
	relations *storage.ExternalUser2ExternalGroupListRelationsV1) error {
	return ErrorNotImplemented
}

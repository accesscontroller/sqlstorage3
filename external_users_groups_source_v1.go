package sqlstorage3

import (
	"context"
	"encoding/json"
	"errors"
	"time"

	"github.com/go-pg/pg/v10"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func (s *SQLStorage) GetExternalUsersGroupsSourceV1Context(ctx context.Context,
	name storage.ResourceNameT) (*storage.ExternalUsersGroupsSourceV1, error) {
	var model = &ExternalUsersGroupsSource{}

	if err := s.db.ModelContext(ctx, model).
		Where("external_users_groups_source.name = ?", name).Select(); err != nil {
		if errors.Is(err, pg.ErrNoRows) {
			return nil, storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       name,
				},
			)
		}

		return nil, err
	}

	return model.toStorageExternalUsersGroupsSource()
}

func createExternalUsersGroupsSourceV1Tx(tx *pg.Tx,
	source *storage.ExternalUsersGroupsSourceV1) (*ExternalUsersGroupsSource, error) {
	var model = &ExternalUsersGroupsSource{}

	// Set values.
	ggs, err := json.Marshal(source.Data.GroupsSourceSettings)
	if err != nil {
		return nil, err
	}

	ugs, err := json.Marshal(source.Data.UsersSourceSettings)
	if err != nil {
		return nil, err
	}

	model.ETag = source.Metadata.ETag
	model.GetMode = source.Data.GetMode
	model.GroupsGetSettings = string(ggs)
	model.GroupsToLoad = source.Data.GroupsToMonitor
	model.Name = source.Metadata.Name
	model.PollInterval = source.Data.PollIntervalSec
	model.Type = source.Data.Type
	model.UsersGetSettings = string(ugs)

	// Insert.
	if _, err := tx.Model(model).Insert(); err != nil {
		if typedErr, ok := err.(pg.Error); ok {
			if typedErr.IntegrityViolation() {
				return nil, storage.NewErrResourceAlreadyExists([]interface{}{source.Metadata}, typedErr)
			}
		}

		return nil, err
	}

	return model, nil
}

func (s *SQLStorage) CreateExternalUsersGroupsSourceV1Context(ctx context.Context,
	source *storage.ExternalUsersGroupsSourceV1, getCreated bool) (*storage.ExternalUsersGroupsSourceV1, error) {
	var model *ExternalUsersGroupsSource

	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		m, err := createExternalUsersGroupsSourceV1Tx(tx, source)
		if err != nil {
			return err
		}

		model = m

		return nil
	}); err != nil {
		return nil, err
	}

	if getCreated {
		return model.toStorageExternalUsersGroupsSource()
	}

	return nil, nil
}

func (s *SQLStorage) UpdateExternalUsersGroupsSourceV1Context(ctx context.Context, name storage.ResourceNameT,
	patch *storage.ExternalUsersGroupsSourceV1, getCreated bool) (*storage.ExternalUsersGroupsSourceV1, error) {
	var model *ExternalUsersGroupsSource

	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		// Lookup in DB.
		var dbM ExternalUsersGroupsSource

		if err := tx.Model(&dbM).Where("external_users_groups_source.name = ?", name).
			Select(); err != nil {
			if errors.Is(err, pg.ErrNoRows) {
				return storage.NewErrResourceNotFound(
					&storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						Kind:       storage.ExternalUsersGroupsSourceKind,
						Name:       name,
					},
				)
			}

			return err
		}

		// Check ETag.
		if dbM.ETag != patch.Metadata.ETag {
			return storage.NewErrETagDoesNotMatch(dbM.ETag, patch.Metadata.ETag)
		}

		// Update.
		ggs, err := json.Marshal(patch.Data.GroupsSourceSettings)
		if err != nil {
			return err
		}

		ugs, err := json.Marshal(patch.Data.UsersSourceSettings)
		if err != nil {
			return err
		}

		dbM.ETag++
		dbM.GetMode = patch.Data.GetMode
		dbM.GroupsGetSettings = string(ggs)
		dbM.GroupsToLoad = patch.Data.GroupsToMonitor
		dbM.Name = patch.Metadata.Name
		dbM.PollInterval = patch.Data.PollIntervalSec
		dbM.Type = patch.Data.Type
		dbM.UsersGetSettings = string(ugs)
		dbM.UpdatedAt = time.Now()

		if _, err := tx.Model(&dbM).WherePK().Update(); err != nil {
			if typedErr, ok := err.(pg.Error); ok {
				if typedErr.IntegrityViolation() {
					return storage.NewErrResourceAlreadyExists([]interface{}{patch.Metadata}, typedErr)
				}
			}

			return err
		}

		model = &dbM

		return nil
	}); err != nil {
		return nil, err
	}

	if getCreated {
		return model.toStorageExternalUsersGroupsSource()
	}

	return nil, nil
}

func (s *SQLStorage) DeleteExternalUsersGroupsSourceV1Context(ctx context.Context,
	source *storage.ExternalUsersGroupsSourceV1) error {
	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		// Lookup.
		var model ExternalUsersGroupsSource

		if err := tx.Model(&model).Where(
			"external_users_groups_source.name = ?", source.Metadata.Name).Select(); err != nil {
			if errors.Is(err, pg.ErrNoRows) {
				return storage.NewErrResourceNotFound(&source.Metadata)
			}

			return err
		}

		// Check ETag.
		if model.ETag != source.Metadata.ETag {
			return storage.NewErrETagDoesNotMatch(model.ETag, source.Metadata.ETag)
		}

		// Delete.
		_, err := tx.Model(&model).WherePK().Delete()

		return err
	}); err != nil {
		return err
	}

	return nil
}

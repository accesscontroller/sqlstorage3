package sqlstorage3

import (
	"context"
	"testing"
	"time"

	"github.com/go-pg/pg/v10"
	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

type TestWebResourceCategoryV1Suite struct {
	db *pg.DB

	sqlStorage *SQLStorage

	suite.Suite
}

func (ts *TestWebResourceCategoryV1Suite) SetupTest() {
	db, err := connectDB()
	if err != nil {
		ts.FailNow("Can not connect to test DB", err)
	}

	ts.db = db
	ts.sqlStorage = &SQLStorage{db: db}

	// Create DB Tables.
	if err := recreateTables(ts.db); err != nil {
		ts.FailNow("Can not initialize DB", err)
	}

	// Create some init data.
	var webResourceCategories = WebResourceCategorySlice{
		{
			ETag:        1,
			Name:        "category 1",
			Description: "cat 1 description",
		},
		{
			ETag:        2,
			Name:        "category 2",
			Description: "cat 2 description",
		},
		{
			ETag:        3,
			Name:        "category 3",
			Description: "cat 3 description",
		},
		{
			ETag:        4,
			Name:        "category 4",
			Description: "cat 4 description",
		},
	}

	for _, c := range webResourceCategories {
		if _, err := db.Model(c).Insert(); err != nil {
			ts.FailNow("Can not Insert webResourceCategory model", err)
		}
	}
}

func (ts *TestWebResourceCategoryV1Suite) TearDownTest() {
	if ts.db != nil {
		if err := ts.db.Close(); err != nil {
			ts.FailNow("Can not Close DB", err)
		}
	}
}

func (ts *TestWebResourceCategoryV1Suite) TestGet() {
	var tests = []struct {
		name storage.ResourceNameT

		expected storage.WebResourceCategoryV1
	}{
		{
			name: "category 4",
			expected: storage.WebResourceCategoryV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       4,
					Kind:       storage.WebResourceCategoryKind,
					Name:       "category 4",
				},
				Data: storage.WebResourceCategoryDataV1{
					Description: "cat 4 description",
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.GetWebResourceCategoryV1Context(
			context.TODO(), test.name)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		ts.Equal(test.expected, *got, "Unexpected result value")
	}
}

func (ts *TestWebResourceCategoryV1Suite) TestCreateSuccess() {
	var tests = []struct {
		getCreated bool

		category storage.WebResourceCategoryV1
	}{
		{
			getCreated: true,
			category: storage.WebResourceCategoryV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       101,
					Kind:       storage.WebResourceCategoryKind,
					Name:       "category 101",
				},
				Data: storage.WebResourceCategoryDataV1{
					Description: "category 101 description",
				},
			},
		},
		{
			category: storage.WebResourceCategoryV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       102,
					Kind:       storage.WebResourceCategoryKind,
					Name:       "category 102",
				},
				Data: storage.WebResourceCategoryDataV1{
					Description: "category 102 description",
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.CreateWebResourceCategoryV1Context(context.TODO(),
			&test.category, test.getCreated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Check that the Category is created.
		var dbCat WebResourceCategory

		if err := ts.db.Model(&dbCat).Where(
			"web_resource_category.name = ?", test.category.Metadata.Name).
			Select(); err != nil {
			ts.FailNow("Can not get a created Category from DB", err)
		}

		ts.Equal(test.category.Metadata.Name, dbCat.Name)
		ts.Equal(test.category.Metadata.ETag, dbCat.ETag)
		ts.Equal(test.category.Data.Description, dbCat.Description)
		ts.WithinDuration(time.Now(), dbCat.CreatedAt, time.Minute)

		if !test.getCreated {
			continue
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		ts.Equal(test.category, *got, "Unexpected result value")
	}
}

func (ts *TestWebResourceCategoryV1Suite) TestCreateConflict() {
	var tests = []struct {
		category storage.WebResourceCategoryV1

		expectedError error
	}{
		{
			category: storage.WebResourceCategoryV1{
				Metadata: storage.MetadataV1{
					Name:       "category 1",
					APIVersion: storage.APIVersionV1Value,
					ETag:       112,
					Kind:       storage.WebResourceCategoryKind,
				},
				Data: storage.WebResourceCategoryDataV1{
					Description: "description cat 100",
				},
			},
			expectedError: storage.NewErrResourceAlreadyExists([]interface{}{
				storage.MetadataV1{
					Name:       "category 1",
					APIVersion: storage.APIVersionV1Value,
					ETag:       112,
					Kind:       storage.WebResourceCategoryKind,
				},
			}),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.CreateWebResourceCategoryV1Context(context.TODO(),
			&test.category, true)

		ts.Nil(got, "Must return nil result")

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestWebResourceCategoryV1Suite) TestCreateIntegrityError() {
	var tests = []struct {
		category storage.WebResourceCategoryV1

		expectedError error
	}{
		{
			category: storage.WebResourceCategoryV1{
				Metadata: storage.MetadataV1{
					Name:       "category 1",
					APIVersion: storage.APIVersionV1Value,
					ETag:       112,
					Kind:       storage.WebResourceCategoryKind,
				},
				Data: storage.WebResourceCategoryDataV1{
					Description: "", // Empty
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.CreateWebResourceCategoryV1Context(context.TODO(),
			&test.category, true)

		ts.Nil(got, "Must return nil result")

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		tpdErr, ok := gotErr.(pg.Error)
		if !ok {
			ts.FailNow("Error must be of pg.Error type")
		}

		ts.True(tpdErr.IntegrityViolation(), "tpdErr.IntegrityViolation() must return true")
	}
}

func (ts *TestWebResourceCategoryV1Suite) TestUpdateSuccess() {
	var tests = []struct {
		getUpdated bool
		name       storage.ResourceNameT

		patch storage.WebResourceCategoryV1
	}{
		{
			getUpdated: true,
			name:       "category 1",
			patch: storage.WebResourceCategoryV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.WebResourceCategoryKind,
					Name:       "renamed category 1",
				},
				Data: storage.WebResourceCategoryDataV1{
					Description: "new description for cat 1",
				},
			},
		},
		{
			name: "category 2",
			patch: storage.WebResourceCategoryV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       2,
					Kind:       storage.WebResourceCategoryKind,
					Name:       "renamed category 2",
				},
				Data: storage.WebResourceCategoryDataV1{
					Description: "new description for cat 2",
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.UpdateWebResourceCategoryV1Context(context.TODO(),
			test.name, &test.patch, test.getUpdated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Load an updated Category from DB.
		var dbCat WebResourceCategory

		if err := ts.db.Model(&dbCat).Where("web_resource_category.name = ?",
			test.patch.Metadata.Name).Select(); err != nil {
			ts.FailNow("Can not get an updated Category from DB", err)
		}

		// Compare.
		ts.Equal(test.patch.Metadata.Name, dbCat.Name)
		dbCat.ETag-- // Account for ETag increment.
		ts.Equal(test.patch.Metadata.ETag, dbCat.ETag)
		ts.Equal(test.patch.Data.Description, dbCat.Description)
		ts.WithinDuration(time.Now(), dbCat.UpdatedAt, time.Minute)

		if !test.getUpdated {
			continue
		}

		ts.NotNil(got, "Must not return nil result")

		// Correct ETag.
		got.Metadata.ETag--

		ts.Equal(test.patch, *got, "Unexpected result value")
	}
}

func (ts *TestWebResourceCategoryV1Suite) TestUpdateNotFound() {
	var tests = []struct {
		name storage.ResourceNameT

		patch storage.WebResourceCategoryV1

		expectedError error
	}{
		{
			name: "not found category 1",
			patch: storage.WebResourceCategoryV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.WebResourceCategoryKind,
					Name:       "renamed category 1",
				},
				Data: storage.WebResourceCategoryDataV1{
					Description: "new description for cat 1",
				},
			},
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.WebResourceCategoryKind,
					Name:       "not found category 1",
				},
			),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.UpdateWebResourceCategoryV1Context(context.TODO(),
			test.name, &test.patch, true)

		ts.Nil(got, "Must return nil result")

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		// Check that there is no a Category with given name.
		isExists, err := ts.db.Model((*WebResourceCategory)(nil)).
			Where("web_resource_category.name = ?",
				test.name).Exists()
		if err != nil {
			ts.FailNow("Can not get an updated Category from DB", err)
		}

		ts.False(isExists, "Must return isExists check == false")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestWebResourceCategoryV1Suite) TestUpdateConflict() {
	var tests = []struct {
		name storage.ResourceNameT

		patch storage.WebResourceCategoryV1

		expectedError error
	}{
		{
			name: "category 1",
			patch: storage.WebResourceCategoryV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.WebResourceCategoryKind,
					Name:       "category 2", // Name conflict.
				},
				Data: storage.WebResourceCategoryDataV1{
					Description: "new description for cat 1",
				},
			},
			expectedError: storage.NewErrResourceAlreadyExists([]interface{}{
				storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.WebResourceCategoryKind,
					Name:       "category 2",
				},
			}),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.UpdateWebResourceCategoryV1Context(context.TODO(),
			test.name, &test.patch, true)

		ts.Nil(got, "Must return nil result")

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestWebResourceCategoryV1Suite) TestUpdateETagError() {
	var tests = []struct {
		name storage.ResourceNameT

		patch storage.WebResourceCategoryV1

		expectedError error
	}{
		{
			name: "category 1",
			patch: storage.WebResourceCategoryV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       101, // Incorrect ETag.
					Kind:       storage.WebResourceCategoryKind,
					Name:       "renamed category 1", // Name conflict.
				},
				Data: storage.WebResourceCategoryDataV1{
					Description: "new description for cat 1",
				},
			},
			expectedError: storage.NewErrETagDoesNotMatch(1, 101),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.UpdateWebResourceCategoryV1Context(context.TODO(),
			test.name, &test.patch, true)

		ts.Nil(got, "Must return nil result")

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestWebResourceCategoryV1Suite) TestDeleteSuccess() {
	var tests = []struct {
		category storage.WebResourceCategoryV1
	}{
		{
			category: storage.WebResourceCategoryV1{
				Metadata: storage.MetadataV1{
					ETag: 1,
					Name: "category 1",
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.sqlStorage.DeleteWebResourceCategoryV1Context(
			context.TODO(), &test.category)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Check that the Category was deleted.
		exists, err := ts.db.Model((*WebResourceCategory)(nil)).
			Where("web_resource_category.name = ?", test.category.Metadata.Name).Exists()
		if err != nil {
			ts.FailNow("Can not check if a Category was deleted", err)
		}

		ts.False(exists, "Must return is exists == false")
	}
}

func (ts *TestWebResourceCategoryV1Suite) TestDeleteNotFound() {
	var tests = []struct {
		patch storage.WebResourceCategoryV1

		expectedError error
	}{
		{
			patch: storage.WebResourceCategoryV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.WebResourceCategoryKind,
					Name:       "not found category 1",
				},
				Data: storage.WebResourceCategoryDataV1{
					Description: "new description for cat 1",
				},
			},
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.WebResourceCategoryKind,
					Name:       "not found category 1",
					ETag:       1,
				},
			),
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.sqlStorage.DeleteWebResourceCategoryV1Context(
			context.TODO(), &test.patch)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestWebResourceCategoryV1Suite) TestDeleteETagError() {
	var tests = []struct {
		patch storage.WebResourceCategoryV1

		expectedError error
	}{
		{
			patch: storage.WebResourceCategoryV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       101, // Incorrect ETag.
					Kind:       storage.WebResourceCategoryKind,
					Name:       "category 1", // Name conflict.
				},
				Data: storage.WebResourceCategoryDataV1{
					Description: "new description for cat 1",
				},
			},
			expectedError: storage.NewErrETagDoesNotMatch(1, 101),
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.sqlStorage.DeleteWebResourceCategoryV1Context(context.TODO(),
			&test.patch)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func TestWebResourceCategoryV1(t *testing.T) {
	suite.Run(t, &TestWebResourceCategoryV1Suite{})
}

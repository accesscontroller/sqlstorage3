package sqlstorage3

import (
	"context"
	"sort"
	"testing"
	"time"

	"github.com/go-pg/pg/v10"
	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

type TestAccessGroupExternalGroupListV1RelationsSuite struct {
	db *pg.DB

	sqlStorage *SQLStorage

	suite.Suite
}

func (ts *TestAccessGroupExternalGroupListV1RelationsSuite) SetupTest() {
	db, err := connectDB()
	if err != nil {
		ts.FailNow("Can not connect to test DB", err)
	}

	ts.db = db
	ts.sqlStorage = &SQLStorage{db: db}

	// Create DB Tables.
	if err := recreateTables(ts.db); err != nil {
		ts.FailNow("Can not initialize DB", err)
	}

	// Create some init data.
	// Sources.
	var sources = ExternalUsersGroupsSourceSlice{
		{
			CreatedAt:         time.Now(),
			ETag:              1,
			GetMode:           storage.ExternalUsersGroupsGetModePassive,
			Name:              "source 1",
			GroupsGetSettings: `{}`,
			UsersGetSettings:  `{}`,
			GroupsToLoad:      []string{"group1", "group2"},
			PollInterval:      1800,
			Type:              storage.ExternalUsersGroupsSourceTypeLDAP,
		},
		{
			CreatedAt:         time.Now(),
			ETag:              2,
			GetMode:           storage.ExternalUsersGroupsGetModePassive,
			Name:              "source 2",
			GroupsGetSettings: `{}`,
			UsersGetSettings:  `{}`,
			GroupsToLoad:      []string{"group1", "group2"},
			PollInterval:      1800,
			Type:              storage.ExternalUsersGroupsSourceTypeLDAP,
		},
	}

	for _, gs := range sources {
		if _, err := db.Model(gs).Insert(); err != nil {
			ts.FailNow("Can not insert Test Data ExternalUsersGroupsSource", err)
		}
	}

	// Access Groups.
	var grps = []AccessGroup{
		{
			Name:                    "access group 1",
			DefaultPolicy:           storage.DefaultAccessPolicyAllow,
			ETag:                    1,
			OrderInAccessRulesList:  11,
			TotalGroupSpeedLimitBps: -1,
			UserGroupSpeedLimitBps:  -1,
		},
		{
			Name:                    "access group 2",
			DefaultPolicy:           storage.DefaultAccessPolicyAllow,
			ETag:                    1,
			OrderInAccessRulesList:  12,
			TotalGroupSpeedLimitBps: 1_000_000_000,
			UserGroupSpeedLimitBps:  1_000_000,
		},
		{
			Name:                    "access group 3",
			DefaultPolicy:           storage.DefaultAccessPolicyAllow,
			ETag:                    3,
			OrderInAccessRulesList:  13,
			TotalGroupSpeedLimitBps: 1_000_000_000,
			UserGroupSpeedLimitBps:  1_000_000,
		},
	}

	for i := range grps {
		gr := &grps[i]

		if _, err := db.Model(gr).Insert(); err != nil {
			ts.FailNow("Can not create init data", err.Error())
		}
	}

	// Groups.
	var groups = ExternalGroupSlice{
		{
			AccessGroupID:               grps[0].ID,
			ETag:                        1,
			CreatedAt:                   time.Now(),
			Name:                        "group 1 at source 1",
			ExternalUsersGroupsSourceID: sources[0].ID,
		},
		{
			AccessGroupID:               grps[1].ID,
			ETag:                        2,
			CreatedAt:                   time.Now(),
			Name:                        "group 2 at source 1",
			ExternalUsersGroupsSourceID: sources[0].ID,
		},
		{
			AccessGroupID:               grps[1].ID,
			ETag:                        3,
			CreatedAt:                   time.Now(),
			Name:                        "group 3 at source 1",
			ExternalUsersGroupsSourceID: sources[0].ID,
		},
		{
			AccessGroupID:               grps[0].ID,
			ETag:                        1,
			CreatedAt:                   time.Now(),
			Name:                        "group 1 at source 2",
			ExternalUsersGroupsSourceID: sources[1].ID,
		},
		{
			ETag:                        2,
			CreatedAt:                   time.Now(),
			Name:                        "group 2 at source 2",
			ExternalUsersGroupsSourceID: sources[1].ID,
		},
		{
			ETag:                        3,
			CreatedAt:                   time.Now(),
			Name:                        "group 3 at source 2",
			ExternalUsersGroupsSourceID: sources[1].ID,
		},
	}

	for _, g := range groups {
		if _, err := db.Model(g).Insert(); err != nil {
			ts.FailNow("Can not insert Test Data ExternalGroup", err)
		}
	}
}

func (ts *TestAccessGroupExternalGroupListV1RelationsSuite) TearDownTest() {
	if ts.db != nil {
		if err := ts.db.Close(); err != nil {
			ts.FailNow("Can not Close DB", err)
		}
	}
}

func (ts *TestAccessGroupExternalGroupListV1RelationsSuite) TestGet() {
	var tests = []struct {
		accessGroup storage.ResourceNameT

		expectedResult storage.AccessGroup2ExternalGroupListRelationsV1
	}{
		{
			accessGroup: "access group 1",
			expectedResult: storage.AccessGroup2ExternalGroupListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroup2ExternalGroupListV1RelationsKind,
					Name:       "access group 1",
				},
				Data: storage.AccessGroup2ExternalGroupListRelationsDataV1{
					AccessGroup: "access group 1",
					Relations: map[storage.ResourceNameT][]storage.ResourceNameT{
						"source 1": {
							"group 1 at source 1",
						},
						"source 2": {
							"group 1 at source 2",
						},
					},
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.GetAccessGroupExternalGroupV1sContext(context.TODO(),
			test.accessGroup)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		ts.Equal(test.expectedResult, *got, "Unexpected result value")
	}
}

func (ts *TestAccessGroupExternalGroupListV1RelationsSuite) TestGetAccessGroupNotFound() {
	var tests = []struct {
		accessGroup storage.ResourceNameT

		expectedError error
	}{
		{
			accessGroup: "not found access group",
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroupKind,
					Name:       "not found access group",
				},
			),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.GetAccessGroupExternalGroupV1sContext(context.TODO(),
			test.accessGroup)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got not error - can not continue")
		}

		if !ts.Nil(got, "Must return nil result") {
			ts.FailNow("Got not nil result - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestAccessGroupExternalGroupListV1RelationsSuite) TestBindSuccess() {
	var tests = []struct {
		relations storage.AccessGroup2ExternalGroupListRelationsV1

		expectedAccessGroupExternalGroups ExternalGroupSlice
	}{
		{
			relations: storage.AccessGroup2ExternalGroupListRelationsV1{
				Data: storage.AccessGroup2ExternalGroupListRelationsDataV1{
					AccessGroup: "access group 1",
					Relations: map[storage.ResourceNameT][]storage.ResourceNameT{
						"source 2": {
							"group 2 at source 2",
							"group 3 at source 2",
						},
						"source 1": {
							"group 3 at source 1",
						},
					},
				},
			},
			expectedAccessGroupExternalGroups: ExternalGroupSlice{
				&ExternalGroup{
					AccessGroup: AccessGroup{
						Name: "access group 1",
					},
					Name: "group 1 at source 1",
					ExternalUsersGroupsSource: ExternalUsersGroupsSource{
						Name: "source 1",
					},
				},
				&ExternalGroup{
					AccessGroup: AccessGroup{
						Name: "access group 1",
					},
					Name: "group 1 at source 2",
					ExternalUsersGroupsSource: ExternalUsersGroupsSource{
						Name: "source 2",
					},
				},
				&ExternalGroup{
					AccessGroup: AccessGroup{
						Name: "access group 1",
					},
					Name: "group 2 at source 2",
					ExternalUsersGroupsSource: ExternalUsersGroupsSource{
						Name: "source 2",
					},
				},
				&ExternalGroup{
					AccessGroup: AccessGroup{
						Name: "access group 1",
					},
					Name: "group 3 at source 2",
					ExternalUsersGroupsSource: ExternalUsersGroupsSource{
						Name: "source 2",
					},
				},
				&ExternalGroup{
					AccessGroup: AccessGroup{
						Name: "access group 1",
					},
					Name: "group 3 at source 1",
					ExternalUsersGroupsSource: ExternalUsersGroupsSource{
						Name: "source 1",
					},
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.sqlStorage.BindAccessGroup2ExternalGroupV1sContext(context.TODO(), &test.relations)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Check that the update happened.
		var groupModels ExternalGroupSlice

		if err := ts.db.Model(&groupModels).Relation("AccessGroup").
			Relation("ExternalUsersGroupsSource").
			Where("access_group.name = ?", test.relations.Data.AccessGroup).
			Select(); err != nil {
			ts.FailNow("Can not load ExternalGroups from DB", err)
		}

		// Check that the ExternalGroup slice contains required groups.
		sort.Slice(test.expectedAccessGroupExternalGroups, func(i, j int) bool {
			return test.expectedAccessGroupExternalGroups[i].Name < test.expectedAccessGroupExternalGroups[j].Name
		})

		sort.Slice(groupModels, func(i, j int) bool {
			return groupModels[i].Name < groupModels[j].Name
		})

		if !ts.Equal(len(test.expectedAccessGroupExternalGroups), len(groupModels),
			"Expected and Loaded ExternalGroup Slice lengths must be equal") {
			ts.FailNow("Different expected and loaded ExternalGroup slice lengths")
		}

		for grInd, ldGr := range groupModels {
			exp := test.expectedAccessGroupExternalGroups[grInd]

			ts.Equalf(exp.AccessGroup.Name, ldGr.AccessGroup.Name,
				"Comparison of Group %s, AccessGroup Names must be equal", exp.Name)
			ts.Equalf(exp.Name, ldGr.Name,
				"Comparison of Group %s, Names must be equal", exp.Name)
			ts.Equalf(exp.ExternalUsersGroupsSource.Name, ldGr.ExternalUsersGroupsSource.Name,
				"Comparison of Group %s, ExternalUsersGroupsSource Names must be equal", exp.Name)
		}
	}
}

func (ts *TestAccessGroupExternalGroupListV1RelationsSuite) TestBindAccessGroupNotFoundError() {
	var tests = []struct {
		relations storage.AccessGroup2ExternalGroupListRelationsV1

		unExpectedAccessGroupExternalGroups ExternalGroupSlice // Would be equal if not error.

		expectedError error
	}{
		{
			relations: storage.AccessGroup2ExternalGroupListRelationsV1{
				Data: storage.AccessGroup2ExternalGroupListRelationsDataV1{
					AccessGroup: "not found access group 1",
					Relations: map[storage.ResourceNameT][]storage.ResourceNameT{
						"source 2": {
							"group 2 at source 2",
						},
						"source 1": {
							"group 3 at source 1",
						},
					},
				},
			},
			unExpectedAccessGroupExternalGroups: ExternalGroupSlice{
				&ExternalGroup{
					AccessGroup: AccessGroup{
						Name: "not found access group 1",
					},
					Name: "group 1 at source 1",
					ExternalUsersGroupsSource: ExternalUsersGroupsSource{
						Name: "source 1",
					},
				},
				&ExternalGroup{
					AccessGroup: AccessGroup{
						Name: "not found access group 1",
					},
					Name: "group 1 at source 2",
					ExternalUsersGroupsSource: ExternalUsersGroupsSource{
						Name: "source 2",
					},
				},
				&ExternalGroup{
					AccessGroup: AccessGroup{
						Name: "not found access group 1",
					},
					Name: "group 2 at source 2",
					ExternalUsersGroupsSource: ExternalUsersGroupsSource{
						Name: "source 2",
					},
				},
				&ExternalGroup{
					AccessGroup: AccessGroup{
						Name: "not found access group 1",
					},
					Name: "group 3 at source 1",
					ExternalUsersGroupsSource: ExternalUsersGroupsSource{
						Name: "source 1",
					},
				},
			},
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroupKind,
					Name:       "not found access group 1",
				},
			),
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.sqlStorage.BindAccessGroup2ExternalGroupV1sContext(context.TODO(), &test.relations)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("No error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")

		// Check that the update did not happen.
		var groupModels ExternalGroupSlice

		if err := ts.db.Model(&groupModels).Relation("AccessGroup").
			Relation("ExternalUsersGroupsSource").
			Where("access_group.name = ?", test.relations.Data.AccessGroup).
			Select(); err != nil {
			ts.FailNow("Can not load ExternalGroups from DB", err)
		}

		// Check that the ExternalGroup slice contains required groups.
		sort.Slice(test.unExpectedAccessGroupExternalGroups, func(i, j int) bool {
			return test.unExpectedAccessGroupExternalGroups[i].Name < test.unExpectedAccessGroupExternalGroups[j].Name
		})

		sort.Slice(groupModels, func(i, j int) bool {
			return groupModels[i].Name < groupModels[j].Name
		})

		if !ts.NotEqual(len(test.unExpectedAccessGroupExternalGroups), len(groupModels),
			"Expected and Loaded ExternalGroup Slice lengths must not be equal") {
			ts.FailNow("The same expected and loaded ExternalGroup slice lengths - can not continue")
		}
	}
}

func (ts *TestAccessGroupExternalGroupListV1RelationsSuite) TestBindExternalGroupNotFoundError() {
	var tests = []struct {
		relations storage.AccessGroup2ExternalGroupListRelationsV1

		unExpectedAccessGroupExternalGroups ExternalGroupSlice // Would be equal if not error.

		expectedError error
	}{
		{
			relations: storage.AccessGroup2ExternalGroupListRelationsV1{
				Data: storage.AccessGroup2ExternalGroupListRelationsDataV1{
					AccessGroup: "access group 1",
					Relations: map[storage.ResourceNameT][]storage.ResourceNameT{
						"source 2": {
							"not found group 2 at source 2",
						},
						"source 1": {
							"group 3 at source 1",
						},
					},
				},
			},
			unExpectedAccessGroupExternalGroups: ExternalGroupSlice{
				&ExternalGroup{
					AccessGroup: AccessGroup{
						Name: "access group 1",
					},
					Name: "group 1 at source 1",
					ExternalUsersGroupsSource: ExternalUsersGroupsSource{
						Name: "source 1",
					},
				},
				&ExternalGroup{
					AccessGroup: AccessGroup{
						Name: "access group 1",
					},
					Name: "group 1 at source 2",
					ExternalUsersGroupsSource: ExternalUsersGroupsSource{
						Name: "source 2",
					},
				},
				&ExternalGroup{
					AccessGroup: AccessGroup{
						Name: "access group 1",
					},
					Name: "group 2 at source 2",
					ExternalUsersGroupsSource: ExternalUsersGroupsSource{
						Name: "source 2",
					},
				},
				&ExternalGroup{
					AccessGroup: AccessGroup{
						Name: "access group 1",
					},
					Name: "group 3 at source 1",
					ExternalUsersGroupsSource: ExternalUsersGroupsSource{
						Name: "source 1",
					},
				},
			},
			expectedError: storage.NewErrRelatedResourcesNotFound(storage.APIVersionV1Value,
				storage.ExternalGroupKind, []storage.ResourceNameT{"not found group 2 at source 2"}, nil),
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.sqlStorage.BindAccessGroup2ExternalGroupV1sContext(context.TODO(), &test.relations)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("No error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")

		// Check that the update did not happen.
		var groupModels ExternalGroupSlice

		if err := ts.db.Model(&groupModels).Relation("AccessGroup").
			Relation("ExternalUsersGroupsSource").
			Where("access_group.name = ?", test.relations.Data.AccessGroup).
			Select(); err != nil {
			ts.FailNow("Can not load ExternalGroups from DB", err)
		}

		// Check that the ExternalGroup slice contains required groups.
		sort.Slice(test.unExpectedAccessGroupExternalGroups, func(i, j int) bool {
			return test.unExpectedAccessGroupExternalGroups[i].Name < test.unExpectedAccessGroupExternalGroups[j].Name
		})

		sort.Slice(groupModels, func(i, j int) bool {
			return groupModels[i].Name < groupModels[j].Name
		})

		if !ts.NotEqual(len(test.unExpectedAccessGroupExternalGroups), len(groupModels),
			"Expected and Loaded ExternalGroup Slice lengths must not be equal") {
			ts.FailNow("The same expected and loaded ExternalGroup slice lengths - can not continue")
		}
	}
}

func (ts *TestAccessGroupExternalGroupListV1RelationsSuite) TestUnBindSuccess() {
	var tests = []struct {
		relations storage.AccessGroup2ExternalGroupListRelationsV1

		expectedAccessGroupExternalGroups ExternalGroupSlice
	}{
		{
			relations: storage.AccessGroup2ExternalGroupListRelationsV1{
				Data: storage.AccessGroup2ExternalGroupListRelationsDataV1{
					AccessGroup: "access group 1",
					Relations: map[storage.ResourceNameT][]storage.ResourceNameT{
						"source 1": {
							"group 1 at source 1",
							"group 2 at source 1", // No such a binding but checks correctness.
						},
					},
				},
			},
			expectedAccessGroupExternalGroups: ExternalGroupSlice{
				&ExternalGroup{
					AccessGroup: AccessGroup{
						Name: "access group 1",
					},
					Name: "group 1 at source 2",
					ExternalUsersGroupsSource: ExternalUsersGroupsSource{
						Name: "source 2",
					},
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.sqlStorage.UnBindAccessGroup2ExternalGroupV1sContext(context.TODO(), &test.relations)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Check that the update happened.
		var groupModels ExternalGroupSlice

		if err := ts.db.Model(&groupModels).Relation("AccessGroup").
			Relation("ExternalUsersGroupsSource").
			Where("access_group.name = ?", test.relations.Data.AccessGroup).
			Select(); err != nil {
			ts.FailNow("Can not load ExternalGroups from DB", err)
		}

		// Check that the ExternalGroup slice contains required groups.
		sort.Slice(test.expectedAccessGroupExternalGroups, func(i, j int) bool {
			return test.expectedAccessGroupExternalGroups[i].Name < test.expectedAccessGroupExternalGroups[j].Name
		})

		sort.Slice(groupModels, func(i, j int) bool {
			return groupModels[i].Name < groupModels[j].Name
		})

		if !ts.Equal(len(test.expectedAccessGroupExternalGroups), len(groupModels),
			"Expected and Loaded ExternalGroup Slice lengths must be equal") {
			ts.FailNow("Different expected and loaded ExternalGroup slice lengths")
		}

		for grInd, ldGr := range groupModels {
			exp := test.expectedAccessGroupExternalGroups[grInd]

			ts.Equalf(exp.AccessGroup.Name, ldGr.AccessGroup.Name,
				"Comparison of Group %s, AccessGroup Names must be equal", exp.Name)
			ts.Equalf(exp.Name, ldGr.Name,
				"Comparison of Group %s, Names must be equal", exp.Name)
			ts.Equalf(exp.ExternalUsersGroupsSource.Name, ldGr.ExternalUsersGroupsSource.Name,
				"Comparison of Group %s, ExternalUsersGroupsSource Names must be equal", exp.Name)
		}
	}
}

func (ts *TestAccessGroupExternalGroupListV1RelationsSuite) TestUnBindAccessGroupNotFoundError() {
	var tests = []struct {
		relations storage.AccessGroup2ExternalGroupListRelationsV1

		unExpectedAccessGroupExternalGroups ExternalGroupSlice // Would be equal if not error.

		expectedError error
	}{
		{
			relations: storage.AccessGroup2ExternalGroupListRelationsV1{
				Data: storage.AccessGroup2ExternalGroupListRelationsDataV1{
					AccessGroup: "not found access group 1",
					Relations: map[storage.ResourceNameT][]storage.ResourceNameT{
						"source 2": {
							"group 2 at source 2",
						},
						"source 1": {
							"group 3 at source 1",
						},
					},
				},
			},
			unExpectedAccessGroupExternalGroups: ExternalGroupSlice{
				&ExternalGroup{
					AccessGroup: AccessGroup{
						Name: "not found access group 1",
					},
					Name: "group 1 at source 1",
					ExternalUsersGroupsSource: ExternalUsersGroupsSource{
						Name: "source 1",
					},
				},
				&ExternalGroup{
					AccessGroup: AccessGroup{
						Name: "not found access group 1",
					},
					Name: "group 1 at source 2",
					ExternalUsersGroupsSource: ExternalUsersGroupsSource{
						Name: "source 2",
					},
				},
				&ExternalGroup{
					AccessGroup: AccessGroup{
						Name: "not found access group 1",
					},
					Name: "group 2 at source 2",
					ExternalUsersGroupsSource: ExternalUsersGroupsSource{
						Name: "source 2",
					},
				},
				&ExternalGroup{
					AccessGroup: AccessGroup{
						Name: "not found access group 1",
					},
					Name: "group 3 at source 1",
					ExternalUsersGroupsSource: ExternalUsersGroupsSource{
						Name: "source 1",
					},
				},
			},
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroupKind,
					Name:       "not found access group 1",
				},
			),
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.sqlStorage.UnBindAccessGroup2ExternalGroupV1sContext(context.TODO(), &test.relations)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("No error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")

		// Check that the update did not happen.
		var groupModels ExternalGroupSlice

		if err := ts.db.Model(&groupModels).Relation("AccessGroup").
			Relation("ExternalUsersGroupsSource").
			Where("access_group.name = ?", test.relations.Data.AccessGroup).
			Select(); err != nil {
			ts.FailNow("Can not load ExternalGroups from DB", err)
		}

		// Check that the ExternalGroup slice contains required groups.
		sort.Slice(test.unExpectedAccessGroupExternalGroups, func(i, j int) bool {
			return test.unExpectedAccessGroupExternalGroups[i].Name < test.unExpectedAccessGroupExternalGroups[j].Name
		})

		sort.Slice(groupModels, func(i, j int) bool {
			return groupModels[i].Name < groupModels[j].Name
		})

		if !ts.NotEqual(len(test.unExpectedAccessGroupExternalGroups), len(groupModels),
			"Expected and Loaded ExternalGroup Slice lengths must not be equal") {
			ts.FailNow("The same expected and loaded ExternalGroup slice lengths - can not continue")
		}
	}
}

func TestAccessGroupExternalGroupListV1Relations(t *testing.T) {
	suite.Run(t, &TestAccessGroupExternalGroupListV1RelationsSuite{})
}

type TestCheckNotAllGroupsLoadedErrorSuite struct {
	suite.Suite
}

func (ts *TestCheckNotAllGroupsLoadedErrorSuite) TestSuccess() {
	var tests = []struct {
		groups []storage.ResourceNameT

		dbGroups ExternalGroupSlice
	}{
		{
			groups: []storage.ResourceNameT{"group 1", "group 2"},
			dbGroups: ExternalGroupSlice{
				&ExternalGroup{
					Name: "group 2",
				},
				&ExternalGroup{
					Name: "group 1",
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := checkNotAllGroupsLoadedError(test.groups, test.dbGroups)

		ts.NoError(gotErr, "Must not return error")
	}
}

func (ts *TestCheckNotAllGroupsLoadedErrorSuite) TestError() {
	var tests = []struct {
		groups []storage.ResourceNameT

		dbGroups ExternalGroupSlice

		expectedError error
	}{
		{
			groups: []storage.ResourceNameT{"group 1", "group 2"},
			dbGroups: ExternalGroupSlice{
				&ExternalGroup{
					Name: "group 2",
				},
			},
			expectedError: storage.NewErrRelatedResourcesNotFound(storage.APIVersionV1Value,
				storage.ExternalGroupKind, []storage.ResourceNameT{"group 1"}, nil),
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := checkNotAllGroupsLoadedError(test.groups, test.dbGroups)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}
	}
}

func TestCheckNotAllGroupsLoadedError(t *testing.T) {
	suite.Run(t, &TestCheckNotAllGroupsLoadedErrorSuite{})
}

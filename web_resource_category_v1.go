package sqlstorage3

import (
	"context"
	"errors"
	"time"

	"github.com/go-pg/pg/v10"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func loadWebResourceCategoryModelByNameTx(tx *pg.Tx, name storage.ResourceNameT) (*WebResourceCategory, error) {
	var wrCat WebResourceCategory

	if err := tx.Model(&wrCat).
		Where("web_resource_category.name = ?", name).Select(); err != nil {
		if errors.Is(err, pg.ErrNoRows) {
			return nil, storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.WebResourceCategoryKind,
				},
			)
		}

		return nil, err
	}

	return &wrCat, nil
}

func loadWebResourceCategoryIDsByNamesTx(tx *pg.Tx, cats []storage.ResourceNameT) ([]int64, error) {
	var models WebResourceCategorySlice

	if err := tx.Model(&models).WhereIn("name IN (?)", cats).Column("id", "name").Select(); err != nil {
		return nil, err
	}

	var vals = make([]int64, 0, len(models))

	for i := range models {
		vals = append(vals, models[i].ID)
	}

	// Check if all the requested resources are found.
	if err := checkNotAllWebResCategoriesLoadedError(cats, models); err != nil {
		return nil, err
	}

	return vals, nil
}

func loadWebResourceCategoryIDByNameTx(tx *pg.Tx, category storage.ResourceNameT) (int64, error) {
	var model WebResourceCategory

	if err := tx.Model(&model).Where("name = ?", category).Column("id").Select(); err != nil {
		if errors.Is(err, pg.ErrNoRows) {
			return 0, storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.WebResourceCategoryKind,
					Name:       category,
				},
			)
		}

		return 0, err
	}

	return model.ID, nil
}

func (s *SQLStorage) GetWebResourceCategoryV1Context(ctx context.Context,
	name storage.ResourceNameT) (*storage.WebResourceCategoryV1, error) {
	var model *WebResourceCategory

	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		m, err := loadWebResourceCategoryModelByNameTx(tx, name)
		if err != nil {
			return err
		}

		model = m

		return nil
	}); err != nil {
		return nil, err
	}

	return model.toStorageWebResourceCategory(), nil
}

func createWebResourceCategoryV1Tx(tx *pg.Tx,
	category *storage.WebResourceCategoryV1) (*WebResourceCategory, error) {
	var model = &WebResourceCategory{}

	model.ETag = category.Metadata.ETag
	model.CreatedAt = time.Now()
	model.Description = category.Data.Description
	model.Name = category.Metadata.Name

	// Insert.
	if _, err := tx.Model(model).Insert(); err != nil {
		if typedErr, ok := err.(pg.Error); ok && typedErr.Field(_PGErrorCodeField) == UniqueConstraintViolation {
			return nil, storage.NewErrResourceAlreadyExists([]interface{}{category.Metadata}, typedErr)
		}

		return nil, err
	}

	return model, nil
}

func (s *SQLStorage) CreateWebResourceCategoryV1Context(ctx context.Context,
	category *storage.WebResourceCategoryV1, getCreated bool) (
	*storage.WebResourceCategoryV1, error) {
	var model *WebResourceCategory

	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		m, err := createWebResourceCategoryV1Tx(tx, category)
		if err != nil {
			return err
		}

		model = m

		return nil
	}); err != nil {
		return nil, err
	}

	if getCreated {
		return model.toStorageWebResourceCategory(), nil
	}

	return nil, nil
}

func (s *SQLStorage) UpdateWebResourceCategoryV1Context(ctx context.Context,
	name storage.ResourceNameT, category *storage.WebResourceCategoryV1,
	getUpdated bool) (*storage.WebResourceCategoryV1, error) {
	var model = &WebResourceCategory{}

	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		// Lookup DB.
		if err := tx.Model(model).Where("web_resource_category.name = ?", name).Select(); err != nil {
			if errors.Is(err, pg.ErrNoRows) {
				return storage.NewErrResourceNotFound(
					&storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						Kind:       storage.WebResourceCategoryKind,
						Name:       name,
					},
				)
			}

			return err
		}

		// Check ETag.
		if model.ETag != category.Metadata.ETag {
			return storage.NewErrETagDoesNotMatch(model.ETag, category.Metadata.ETag)
		}

		model.UpdatedAt = time.Now()
		model.Description = category.Data.Description
		model.ETag++
		model.Name = category.Metadata.Name

		if _, err := tx.Model(model).WherePK().Update(); err != nil {
			if typedErr, ok := err.(pg.Error); ok {
				if typedErr.IntegrityViolation() {
					return storage.NewErrResourceAlreadyExists([]interface{}{
						storage.MetadataV1{
							APIVersion: storage.APIVersionV1Value,
							Kind:       storage.WebResourceCategoryKind,
							Name:       category.Metadata.Name,
						},
					}, typedErr)
				}
			}

			return err
		}

		return nil
	}); err != nil {
		return nil, err
	}

	if getUpdated {
		return model.toStorageWebResourceCategory(), nil
	}

	return nil, nil
}

func (s *SQLStorage) DeleteWebResourceCategoryV1Context(ctx context.Context,
	category *storage.WebResourceCategoryV1) error {
	return s.db.RunInTransaction(ctx, func(tx *pg.Tx) error { // nolint:dupl
		var model WebResourceCategory

		if err := tx.Model(&model).
			Where("web_resource_category.name = ?", category.Metadata.Name).
			Select(); err != nil {
			if errors.Is(err, pg.ErrNoRows) {
				return storage.NewErrResourceNotFound(&category.Metadata)
			}

			return err
		}

		// ETag.
		if model.ETag != category.Metadata.ETag {
			return storage.NewErrETagDoesNotMatch(model.ETag, category.Metadata.ETag)
		}

		// Delete.
		_, err := tx.Model(&model).WherePK().Delete()

		return err
	})
}

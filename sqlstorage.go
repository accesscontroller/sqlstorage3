package sqlstorage3

import (
	"context"
	"errors"
	"reflect"

	"github.com/go-pg/pg/v10"
)

const (
	_PGErrorCodeField byte = 'C'
)

const (
	// UniqueConstraintViolation defines error code for unique constraint violation.
	UniqueConstraintViolation string = "23505"
)

type SQLStorage struct {
	db *pg.DB
}

func NewContext(ctx context.Context, login, password, hostname, database string) (*SQLStorage, error) {
	pgDB := pg.Connect(&pg.Options{
		Addr:     hostname,
		Database: database,
		User:     login,
		Password: password,
	})

	// Create DB.
	if err := createModelsContext(ctx, pgDB); err != nil {
		return nil, err
	}

	return &SQLStorage{
		db: pgDB,
	}, nil
}

func New(login, password, hostname, database string) (*SQLStorage, error) {
	return NewContext(context.TODO(), login, password, hostname, database)
}

var (
	ErrorNotImplemented = errors.New("not implemented yet")
)

func isZero(v interface{}) bool {
	return reflect.ValueOf(v).IsZero()
}

func (s *SQLStorage) CheckHealthContext(ctx context.Context) error {
	if _, err := s.db.WithContext(ctx).Exec("SELECT 1"); err != nil {
		return err
	}

	return nil
}

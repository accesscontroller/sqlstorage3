package sqlstorage3

import (
	"context"
	"testing"

	"github.com/go-pg/pg/v10"
	"github.com/stretchr/testify/suite"
)

type TestCheckHealthSuite struct {
	db *pg.DB

	sqlStorage *SQLStorage

	suite.Suite
}

func (ts *TestCheckHealthSuite) SetupTest() {
	db, err := connectDB()
	if err != nil {
		ts.FailNow("Can not connect to test DB", err)
	}

	ts.db = db
	ts.sqlStorage = &SQLStorage{db: db}
}

func (ts *TestCheckHealthSuite) TearDownTest() {}

func (ts *TestCheckHealthSuite) TestCheckHealthSuccess() {
	defer ts.db.Close()

	ts.NoError(ts.sqlStorage.CheckHealthContext(context.TODO()), "Must not return error")
}

func (ts *TestCheckHealthSuite) TestCheckHealthFailure() {
	ts.db.Close() // To imitate DB failure.

	ts.Error(ts.sqlStorage.CheckHealthContext(context.TODO()), "Must return error")
}

func TestCheckHealth(t *testing.T) {
	suite.Run(t, &TestCheckHealthSuite{})
}

package sqlstorage3

import (
	"context"
	"strings"

	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
)

var (
	// nolint:gochecknoglobals
	dbTables = []interface{}{
		&AccessGroup{},

		&ExternalUsersGroupsSource{},
		&ExternalSessionsSource{},

		&ExternalGroup{},
		&ExternalUser{},

		&ExternalUserSession{},

		&WebResourceCategory{},

		&WebResource{},

		&WebResourceDomain{},
		&WebResourceIP{},
		&WebResourceURL{},

		&ExternalUserToExternalGroup{},
		&WebResourceCategoryToAccessGroup{},
		&WebResourceCategoryToWebResource{},
	}

	// nolint:gochecknoglobals
	dbIndexes = []struct {
		Name    string
		Table   interface{}
		Field   string
		Unique  bool
		Descend bool
	}{
		{
			Name:   "external_users_groups_source__name",
			Table:  (*ExternalUsersGroupsSource)(nil),
			Field:  "name",
			Unique: true,
		},
	}
)

func createModelsContext(ctx context.Context, db *pg.DB) error {
	// Create Tables.
	if err := db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		// Register Many-to-Many Tables.
		orm.RegisterTable(&WebResourceCategoryToAccessGroup{})
		orm.RegisterTable(&ExternalUserToExternalGroup{})
		orm.RegisterTable(&WebResourceCategoryToWebResource{})

		// Make Tables.
		for _, table := range dbTables {
			if err := tx.Model(table).CreateTable(&orm.CreateTableOptions{
				IfNotExists:   true,
				FKConstraints: true,
			}); err != nil {
				return err
			}
		}

		return nil
	}); err != nil {
		return err
	}

	// Make Indexes.
	for _, ind := range dbIndexes {
		var bld = &strings.Builder{}

		bld.WriteString("CREATE ")

		if ind.Unique {
			bld.WriteString(" UNIQUE ")
		}

		bld.WriteString(" INDEX CONCURRENTLY IF NOT EXISTS ")
		bld.WriteString(ind.Name)
		bld.WriteString(" ON ?TableName (")
		bld.WriteString(ind.Field)
		bld.WriteString(") ")

		if ind.Descend {
			bld.WriteString(" DESC ")
		}

		if _, err := db.Model(ind.Table).Exec(bld.String()); err != nil {
			return err
		}
	}

	// Make External Session IP Constraint.
	if _, err := db.Exec(
		`CREATE UNIQUE INDEX IF NOT EXISTS unique_ip_address_open_sessions
		ON external_user_sessions USING btree
		(ip_address ASC NULLS LAST)
		TABLESPACE pg_default
		WHERE closed = false;`); err != nil {
		return err
	}

	return nil
}

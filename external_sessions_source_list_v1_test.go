package sqlstorage3

import (
	"context"
	"sort"
	"testing"

	"github.com/go-pg/pg/v10"
	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

type TestExternalSessionsSourceV1sSuite struct {
	db *pg.DB

	sqlStorage *SQLStorage

	suite.Suite
}

func (ts *TestExternalSessionsSourceV1sSuite) SetupTest() {
	db, err := connectDB()
	if err != nil {
		ts.FailNow("Can not connect to test DB", err)
	}

	ts.db = db
	ts.sqlStorage = &SQLStorage{db: db}

	// Create DB Tables.
	if err := recreateTables(ts.db); err != nil {
		ts.FailNow("Can not initialize DB", err)
	}

	// Create some init data.

	// ExternalUsersGroupsSource.
	var externalUsersGroupsSources = []ExternalUsersGroupsSource{
		{
			ETag:              1,
			GetMode:           storage.ExternalUsersGroupsGetModePassive,
			GroupsGetSettings: "get groups settings",
			GroupsToLoad:      []string{"group1", "group2"},
			UsersGetSettings:  "get users settings",
			Type:              storage.ExternalUsersGroupsSourceTypeLDAP,
			Name:              "source 1",
			PollInterval:      121,
		},
		{
			ETag:              1,
			GetMode:           storage.ExternalUsersGroupsGetModePassive,
			GroupsGetSettings: "get groups settings",
			GroupsToLoad:      []string{"group1", "group2"},
			UsersGetSettings:  "get users settings",
			Type:              storage.ExternalUsersGroupsSourceTypeLDAP,
			Name:              "source 2",
			PollInterval:      122,
		},
	}

	for i := range externalUsersGroupsSources {
		if _, err := db.Model(&externalUsersGroupsSources[i]).Insert(); err != nil {
			ts.FailNow("Can not create init data", err.Error())
		}
	}

	// Session Sources.
	var sessionSources = []ExternalSessionsSource{
		{
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[0].ID,
			ETag:                        1,
			GetMode:                     storage.ExternalSessionsGetModePassive,
			Name:                        "sessions source 1",
			Settings:                    []byte(`{"Name": "source 1", "Key1": "Value1", "Key2": "Value2"}`),
			Type:                        storage.ExternalSessionsSourceTypeMSADEvents,
		},
		{
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[1].ID,
			ETag:                        2,
			GetMode:                     storage.ExternalSessionsGetModePoll,
			Name:                        "sessions source 2",
			Settings:                    []byte(`{"Name": "source 2", "Key2": "Value2", "Key3": "Value3"}`),
			Type:                        storage.ExternalSessionsSourceTypeMSADEvents,
		},
		{
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[0].ID,
			ETag:                        3,
			GetMode:                     storage.ExternalSessionsGetModePoll,
			Name:                        "sessions source 3",
			Settings:                    []byte(`{"Name": "source 3", "Key2": "Value2", "Key3": "Value3"}`),
			Type:                        storage.ExternalSessionsSourceTypeMSADEvents,
		},
		{
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[1].ID,
			ETag:                        4,
			GetMode:                     storage.ExternalSessionsGetModePoll,
			Name:                        "sessions source 4",
			Settings:                    []byte(`{"Name": "source 4", "Key2": "Value2", "Key3": "Value3"}`),
			Type:                        storage.ExternalSessionsSourceTypeMSADEvents,
		},
		{
			ExternalUsersGroupsSourceID: externalUsersGroupsSources[1].ID,
			ETag:                        5,
			GetMode:                     storage.ExternalSessionsGetModePassive,
			Name:                        "sessions source 5",
			Settings:                    []byte(`{"Name": "source 5", "Key2": "Value2", "Key3": "Value3"}`),
			Type:                        storage.ExternalSessionsSourceTypeMSADEvents,
		},
	}

	for i := range sessionSources {
		if _, err := db.Model(&sessionSources[i]).Insert(); err != nil {
			ts.FailNow("Can not create init data", err.Error())
		}
	}
}

func (ts *TestExternalSessionsSourceV1sSuite) TearDownTest() {
	if ts.db != nil {
		if err := ts.db.Close(); err != nil {
			ts.FailNow("Can not Close DB", err)
		}
	}
}

func (ts *TestExternalSessionsSourceV1sSuite) TestGet() {
	var tests = []struct {
		filters []storage.ExternalSessionsSourceFilterV1

		expectedGroups []storage.ExternalSessionsSourceV1
	}{
		{
			filters: []storage.ExternalSessionsSourceFilterV1{
				// No such a source.
				{
					Name:    "sessions source 1",
					GetMode: storage.ExternalSessionsGetModePoll,
					Type:    storage.ExternalSessionsSourceTypeMSADEvents,
				},
			},
			expectedGroups: []storage.ExternalSessionsSourceV1{},
		},
		{
			filters: []storage.ExternalSessionsSourceFilterV1{
				// Two sources.
				{
					Name: "sessions source 2",
				},
				{
					Name:    "sessions source 3",
					GetMode: storage.ExternalSessionsGetModePoll,
				},
			},
			expectedGroups: []storage.ExternalSessionsSourceV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       2,
						Kind:       storage.ExternalSessionsSourceKind,
						Name:       "sessions source 2",
					},
					Data: storage.ExternalSessionsSourceDataV1{
						GetMode:                 storage.ExternalSessionsGetModePoll,
						ExternalUsersSourceName: "source 2",
						Settings: map[string]string{
							"Name": "source 2", "Key2": "Value2", "Key3": "Value3",
						},
						Type: storage.ExternalSessionsSourceTypeMSADEvents,
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       3,
						Kind:       storage.ExternalSessionsSourceKind,
						Name:       "sessions source 3",
					},
					Data: storage.ExternalSessionsSourceDataV1{
						GetMode:                 storage.ExternalSessionsGetModePoll,
						ExternalUsersSourceName: "source 1",
						Settings: map[string]string{
							"Name": "source 3", "Key2": "Value2", "Key3": "Value3",
						},
						Type: storage.ExternalSessionsSourceTypeMSADEvents,
					},
				},
			},
		},
		{
			// Full set.
			filters: []storage.ExternalSessionsSourceFilterV1{},
			expectedGroups: []storage.ExternalSessionsSourceV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       1,
						Kind:       storage.ExternalSessionsSourceKind,
						Name:       "sessions source 1",
					},
					Data: storage.ExternalSessionsSourceDataV1{
						GetMode:                 storage.ExternalSessionsGetModePassive,
						ExternalUsersSourceName: "source 1",
						Settings: map[string]string{
							"Name": "source 1", "Key1": "Value1", "Key2": "Value2",
						},
						Type: storage.ExternalSessionsSourceTypeMSADEvents,
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       2,
						Kind:       storage.ExternalSessionsSourceKind,
						Name:       "sessions source 2",
					},
					Data: storage.ExternalSessionsSourceDataV1{
						GetMode:                 storage.ExternalSessionsGetModePoll,
						ExternalUsersSourceName: "source 2",
						Settings: map[string]string{
							"Name": "source 2", "Key2": "Value2", "Key3": "Value3",
						},
						Type: storage.ExternalSessionsSourceTypeMSADEvents,
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       3,
						Kind:       storage.ExternalSessionsSourceKind,
						Name:       "sessions source 3",
					},
					Data: storage.ExternalSessionsSourceDataV1{
						GetMode:                 storage.ExternalSessionsGetModePoll,
						ExternalUsersSourceName: "source 1",
						Settings: map[string]string{
							"Name": "source 3", "Key2": "Value2", "Key3": "Value3",
						},
						Type: storage.ExternalSessionsSourceTypeMSADEvents,
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       4,
						Kind:       storage.ExternalSessionsSourceKind,
						Name:       "sessions source 4",
					},
					Data: storage.ExternalSessionsSourceDataV1{
						GetMode:                 storage.ExternalSessionsGetModePoll,
						ExternalUsersSourceName: "source 2",
						Settings: map[string]string{
							"Name": "source 4", "Key2": "Value2", "Key3": "Value3",
						},
						Type: storage.ExternalSessionsSourceTypeMSADEvents,
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       5,
						Kind:       storage.ExternalSessionsSourceKind,
						Name:       "sessions source 5",
					},
					Data: storage.ExternalSessionsSourceDataV1{
						GetMode:                 storage.ExternalSessionsGetModePassive,
						ExternalUsersSourceName: "source 2",
						Settings: map[string]string{
							"Name": "source 5", "Key2": "Value2", "Key3": "Value3",
						},
						Type: storage.ExternalSessionsSourceTypeMSADEvents,
					},
				},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.sqlStorage.GetExternalSessionsSourceV1sContext(context.TODO(), test.filters)

		ts.NoError(gotErr, "must not return error")

		if !ts.NotNil(got, "nil result") {
			ts.FailNow("nil result - can not continue")
		}

		sort.Slice(test.expectedGroups, func(i, j int) bool {
			return test.expectedGroups[i].Metadata.Name < test.expectedGroups[j].Metadata.Name //nolint:scopelint
		})

		sort.Slice(got, func(i, j int) bool {
			return got[i].Metadata.Name < got[j].Metadata.Name
		})

		ts.Equal(test.expectedGroups, got, "Unexpected result")
	}
}

func (ts *TestExternalSessionsSourceV1sSuite) TestCreateSuccess() {
	var tests = []struct {
		getResult bool

		sources []storage.ExternalSessionsSourceV1
	}{
		{
			sources: []storage.ExternalSessionsSourceV1{{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "external sessions source 10",
				},
				Data: storage.ExternalSessionsSourceDataV1{
					GetMode:                 storage.ExternalSessionsGetModePassive,
					ExternalUsersSourceName: "source 1",
					Settings: map[string]string{
						"key 10": "value 10",
					},
					Type: storage.ExternalSessionsSourceTypeMSADEvents,
				},
			}},
		},
		{
			getResult: true,
			sources: []storage.ExternalSessionsSourceV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       1,
						Kind:       storage.ExternalSessionsSourceKind,
						Name:       "external sessions source 11",
					},
					Data: storage.ExternalSessionsSourceDataV1{
						GetMode:                 storage.ExternalSessionsGetModePoll,
						ExternalUsersSourceName: "source 2",
						Settings: map[string]string{
							"key 11": "value 11",
						},
						Type: storage.ExternalSessionsSourceTypeMSADEvents,
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       1,
						Kind:       storage.ExternalSessionsSourceKind,
						Name:       "external sessions source 12",
					},
					Data: storage.ExternalSessionsSourceDataV1{
						GetMode:                 storage.ExternalSessionsGetModePoll,
						ExternalUsersSourceName: "source 2",
						Settings: map[string]string{
							"key 11": "value 11",
						},
						Type: storage.ExternalSessionsSourceTypeMSADEvents,
					},
				},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.sqlStorage.CreateExternalSessionsSourceV1sContext(context.TODO(), test.sources, test.getResult)

		if !ts.NoError(gotErr, "must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Sort result for further processing.
		if got != nil {
			sort.Slice(got, func(i, j int) bool {
				return got[i].Metadata.Name > got[j].Metadata.Name
			})

			sort.Slice(test.sources, func(i, j int) bool {
				return test.sources[i].Metadata.Name > test.sources[j].Metadata.Name //nolint:scopelint
			})
		}

		for i := range test.sources {
			expSrc := &test.sources[i]

			// Check that the Source was created.
			var dbSource = &ExternalSessionsSource{}

			if err := ts.db.Model(dbSource).Relation("ExternalUsersGroupsSource").
				Where("external_sessions_source.name = ?", expSrc.Metadata.Name).
				Select(); err != nil {
				ts.FailNow("Can not get created ExternalSessionsSource from DB", err)
			}

			// Compare.
			dbConverted, err := dbSource.toStorageExternalSessionsSource()
			if err != nil {
				ts.FailNow("Can not convert DB Model to ExternalSessionsSource", err)
			}

			ts.Equal(expSrc, dbConverted, "Unexpected DB ExternalSessionsSource")

			// Check result
			if !test.getResult {
				continue
			}

			if !ts.NotNil(got, "Must not return nil ExternalSessionsSourceV1") {
				ts.FailNow("nil as ExternalSessionsSourceV1 - can not continue")
			}

			ts.Equal(expSrc, &got[i], "Unexpected result")
		}
	}
}

func (ts *TestExternalSessionsSourceV1sSuite) TestCreateExternalUsersGroupsSourceNotFound() {
	var tests = []struct {
		expectedError error

		sources []storage.ExternalSessionsSourceV1
	}{
		{
			expectedError: storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       "non existing source 1",
				},
			),
			sources: []storage.ExternalSessionsSourceV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       1,
						Kind:       storage.ExternalSessionsSourceKind,
						Name:       "external sessions source 10",
					},
					Data: storage.ExternalSessionsSourceDataV1{
						GetMode:                 storage.ExternalSessionsGetModePassive,
						ExternalUsersSourceName: "non existing source 1",
						Settings: map[string]string{
							"key 10": "value 10",
						},
						Type: storage.ExternalSessionsSourceTypeMSADEvents,
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       1,
						Kind:       storage.ExternalSessionsSourceKind,
						Name:       "external sessions source 11",
					},
					Data: storage.ExternalSessionsSourceDataV1{
						GetMode:                 storage.ExternalSessionsGetModePassive,
						ExternalUsersSourceName: "existing source 1",
						Settings: map[string]string{
							"key 10": "value 10",
						},
						Type: storage.ExternalSessionsSourceTypeMSADEvents,
					},
				},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.sqlStorage.CreateExternalSessionsSourceV1sContext(context.TODO(), test.sources, true)

		// Check that the Source was not created.
		for si := range test.sources {
			src := &test.sources[si]

			var dbSource = &ExternalSessionsSource{}

			created, err := ts.db.Model(dbSource).Relation("ExternalUsersGroupsSource").
				Where("external_sessions_source.name = ?", src.Metadata.Name).
				Exists()
			if err != nil {
				ts.FailNow("Can not check is created ExternalSessionsSource is created", err)
			}

			ts.False(created, "Must return created==false")
		}

		// Check error.
		if !ts.Error(gotErr, "must return error") {
			ts.FailNow("nil error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")

		ts.Nil(got, "Must return nil result")
	}
}

func (ts *TestExternalSessionsSourceV1sSuite) TestCreateConflict() {
	var tests = []struct {
		expectedError error

		sources []storage.ExternalSessionsSourceV1
	}{
		{
			expectedError: storage.NewErrResourceAlreadyExists([]interface{}{
				storage.ExternalSessionsSourceV1{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       1,
						Kind:       storage.ExternalSessionsSourceKind,
						Name:       "sessions source 1",
					},
					Data: storage.ExternalSessionsSourceDataV1{
						GetMode:                 storage.ExternalSessionsGetModePassive,
						ExternalUsersSourceName: "source 1",
						Settings: map[string]string{
							"key 10": "value 10",
						},
						Type: storage.ExternalSessionsSourceTypeMSADEvents,
					},
				},
			}),
			sources: []storage.ExternalSessionsSourceV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       1,
						Kind:       storage.ExternalSessionsSourceKind,
						Name:       "sessions source 10",
					},
					Data: storage.ExternalSessionsSourceDataV1{
						GetMode:                 storage.ExternalSessionsGetModePassive,
						ExternalUsersSourceName: "source 1",
						Settings: map[string]string{
							"key 10": "value 10",
						},
						Type: storage.ExternalSessionsSourceTypeMSADEvents,
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       1,
						Kind:       storage.ExternalSessionsSourceKind,
						Name:       "sessions source 1",
					},
					Data: storage.ExternalSessionsSourceDataV1{
						GetMode:                 storage.ExternalSessionsGetModePassive,
						ExternalUsersSourceName: "source 1",
						Settings: map[string]string{
							"key 10": "value 10",
						},
						Type: storage.ExternalSessionsSourceTypeMSADEvents,
					},
				},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.sqlStorage.CreateExternalSessionsSourceV1sContext(context.TODO(), test.sources, true)

		// Check error.
		if !ts.Error(gotErr, "must return error") {
			ts.FailNow("nil error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")

		ts.Nil(got, "Must return nil result")
	}
}

func TestExternalSessionsSourceV1s(t *testing.T) {
	suite.Run(t, &TestExternalSessionsSourceV1sSuite{})
}

package sqlstorage3

import (
	"context"

	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func (s *SQLStorage) GetWebResourceCategoryV1sContext(ctx context.Context,
	filters []storage.WebResourceCategoryFilterV1) ([]storage.WebResourceCategoryV1, error) {
	var (
		models WebResourceCategorySlice
		q      = s.db.ModelContext(ctx, &models)
	)

	// Filters.
	for i := range filters {
		fltExpr := &filters[i]

		q = q.WhereOrGroup(func(iq *orm.Query) (*orm.Query, error) {
			if !isZero(fltExpr.Name) {
				iq = iq.Where("web_resource_category.name = ?", fltExpr.Name)
			}

			if !isZero(fltExpr.NameSubstringMatch) {
				iq = iq.Where("web_resource_category.name LIKE ?",
					"%"+fltExpr.NameSubstringMatch+"%")
			}

			if !isZero(fltExpr.DescriptionSubstringMatch) {
				iq = iq.Where("web_resource_category.description LIKE ?",
					"%"+fltExpr.DescriptionSubstringMatch+"%")
			}

			return iq, nil
		})
	}

	if err := q.Select(); err != nil {
		return nil, err
	}

	return models.toStorageWebResourceCategories(), nil
}

func (s *SQLStorage) CreateWebResourceCategoryV1sContext(ctx context.Context,
	categories []storage.WebResourceCategoryV1, getCreated bool) ([]storage.WebResourceCategoryV1, error) {
	var models WebResourceCategorySlice

	if getCreated {
		models = make(WebResourceCategorySlice, 0, len(categories))
	}

	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		for i := range categories {
			cp := &categories[i]

			m, err := createWebResourceCategoryV1Tx(tx, cp)
			if err != nil {
				return err
			}

			if getCreated {
				models = append(models, m)
			}
		}

		return nil
	}); err != nil {
		return nil, err
	}

	if getCreated {
		return models.toStorageWebResourceCategories(), nil
	}

	return nil, nil
}

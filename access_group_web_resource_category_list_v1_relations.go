package sqlstorage3

import (
	"context"
	"errors"
	"time"

	"github.com/go-pg/pg/v10"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func (s *SQLStorage) GetAccessGroupWebResourceCategoryV1sContext(ctx context.Context,
	accessGroup storage.ResourceNameT) ([]storage.ResourceNameT, error) {
	var group AccessGroup

	if err := s.db.ModelContext(ctx, &group).
		Relation("WebResourceCategorys").
		Where("access_group.name = ?", accessGroup).Select(); err != nil {
		if errors.Is(err, pg.ErrNoRows) {
			return nil, storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroupKind,
					Name:       accessGroup,
				},
			)
		}

		return nil, err
	}

	var result = make([]storage.ResourceNameT, len(group.WebResourceCategorys))

	for i, cat := range group.WebResourceCategorys {
		result[i] = cat.Name
	}

	return result, nil
}

// checkNotAllWebResCategoriesLoadedError returns ErrRelatedResourcesNotFound if
// there are at least one required WebResourceCategory which is not in loaded.
func checkNotAllWebResCategoriesLoadedError(required []storage.ResourceNameT,
	loaded WebResourceCategorySlice) error {
	var notFoundGroups = make([]storage.ResourceNameT, 0, len(required)-len(loaded))

	for _, rg := range required {
		var isFound bool

		for _, ld := range loaded {
			if ld.Name == rg {
				isFound = true

				continue
			}
		}

		if !isFound {
			notFoundGroups = append(notFoundGroups, rg)
		}
	}

	if len(notFoundGroups) > 0 {
		return storage.NewErrRelatedResourcesNotFound(storage.APIVersionV1Value,
			storage.WebResourceCategoryKind, notFoundGroups, nil)
	}

	return nil
}

func loadWebResourceCategoriesTx(tx *pg.Tx, webResourceCategories []storage.ResourceNameT) (
	WebResourceCategorySlice, error) {
	var cats WebResourceCategorySlice

	if err := tx.Model(&cats).
		Where("web_resource_category.name IN (?)", pg.In(webResourceCategories)).
		Select(); err != nil {
		return nil, err
	}

	// Check that All required Web Resource Categories are loaded.
	if err := checkNotAllWebResCategoriesLoadedError(webResourceCategories, cats); err != nil {
		return nil, err
	}

	return cats, nil
}

func bindAccessGroup2WebResourceCategoryV1sTx(tx *pg.Tx,
	accessGroup storage.ResourceNameT, webResourceCategories []storage.ResourceNameT) error {
	// Load Access Group.
	accessGroupModel, err := loadAccessGroupIDTx(tx, accessGroup)
	if err != nil {
		return err
	}

	// Lookup WebResource Categories.
	cats, err := loadWebResourceCategoriesTx(tx, webResourceCategories)
	if err != nil {
		return err
	}

	// Update AccessGroup ETag.
	accessGroupModel.ETag++

	if _, err := tx.Model(accessGroupModel).WherePK().Column("e_tag").Update(); err != nil {
		return err
	}

	// Try to Bind.
	var (
		binds = make([]*WebResourceCategoryToAccessGroup, len(cats))
		ts    = time.Now()
	)

	for i, cat := range cats {
		binds[i] = &WebResourceCategoryToAccessGroup{
			AccessGroupID:         accessGroupModel.ID,
			CreatedAt:             ts,
			WebResourceCategoryID: cat.ID,
		}

		// ETag.
		cat.ETag++
	}

	// Could be used with Soft Delete after fixing models.
	if _, err := tx.Model(&binds).
		OnConflict("( access_group_id, web_resource_category_id ) DO NOTHING").
		Insert(); err != nil {
		return err
	}

	// Update WebResourceCategories ETags.
	if _, err := tx.Model(&cats).WherePK().Column("e_tag").Update(); err != nil {
		return err
	}

	return nil
}

func (s *SQLStorage) BindAccessGroup2WebResourceCategoryV1sContext(ctx context.Context,
	accessGroup storage.ResourceNameT, webResourceCategories []storage.ResourceNameT) error {
	return s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		return bindAccessGroup2WebResourceCategoryV1sTx(tx, accessGroup, webResourceCategories)
	})
}

func unbindAccessGroup2WebResourceCategoryV1sTx(tx *pg.Tx,
	accessGroup storage.ResourceNameT, webResourceCategories []storage.ResourceNameT) error {
	// Load Access Group.
	accessGroupModel, err := loadAccessGroupIDTx(tx, accessGroup)
	if err != nil {
		return err
	}

	// Lookup WebResource Categories.
	cats, err := loadWebResourceCategoriesTx(tx, webResourceCategories)
	if err != nil {
		return err
	}

	// Update AccessGroup ETag.
	accessGroupModel.ETag++

	if _, err := tx.Model(accessGroupModel).WherePK().Column("e_tag").Update(); err != nil {
		return err
	}

	// Try to UnBind.
	var webResCatIDs = make([]int64, len(cats))

	for i, cat := range cats {
		webResCatIDs[i] = cat.ID

		// Web Resource Category ETag.
		cat.ETag++
	}

	// Update WebResourceCategory ETags.
	if _, err := tx.Model(&cats).WherePK().Column("e_tag").Update(); err != nil {
		return err
	}

	// Unbind.
	if _, err := tx.Model((*WebResourceCategoryToAccessGroup)(nil)).
		Where("access_group_id = ?", accessGroupModel.ID).
		WhereIn("web_resource_category_id IN (?)", webResCatIDs).Delete(); err != nil {
		return err
	}

	return nil
}

func (s *SQLStorage) UnBindAccessGroup2WebResourceCategoryV1sContext(ctx context.Context,
	accessGroup storage.ResourceNameT, webResourceCategories []storage.ResourceNameT) error {
	return s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		return unbindAccessGroup2WebResourceCategoryV1sTx(tx, accessGroup, webResourceCategories)
	})
}

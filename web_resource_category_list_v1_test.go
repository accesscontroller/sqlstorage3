package sqlstorage3

import (
	"context"
	"sort"
	"testing"
	"time"

	"github.com/go-pg/pg/v10"
	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

type TestWebResourceCategoryV1sSuite struct {
	db *pg.DB

	sqlStorage *SQLStorage

	suite.Suite
}

func (ts *TestWebResourceCategoryV1sSuite) SetupTest() {
	db, err := connectDB()
	if err != nil {
		ts.FailNow("Can not connect to test DB", err)
	}

	ts.db = db
	ts.sqlStorage = &SQLStorage{db: db}

	// Create DB Tables.
	if err := recreateTables(ts.db); err != nil {
		ts.FailNow("Can not initialize DB", err)
	}

	// Create some init data.
	var webResourceCategories = WebResourceCategorySlice{
		{
			ETag:        1,
			Name:        "category 1",
			Description: "cat 1 description",
		},
		{
			ETag:        2,
			Name:        "category 2",
			Description: "cat 2 description",
		},
		{
			ETag:        3,
			Name:        "category 3",
			Description: "cat 3 description",
		},
		{
			ETag:        4,
			Name:        "category 4",
			Description: "cat 4 description",
		},
	}

	for _, c := range webResourceCategories {
		if _, err := db.Model(c).Insert(); err != nil {
			ts.FailNow("Can not Insert webResourceCategory model", err)
		}
	}
}

func (ts *TestWebResourceCategoryV1sSuite) TearDownTest() {
	if ts.db != nil {
		if err := ts.db.Close(); err != nil {
			ts.FailNow("Can not Close DB", err)
		}
	}
}

func (ts *TestWebResourceCategoryV1sSuite) TestGet() {
	var tests = []struct {
		filters []storage.WebResourceCategoryFilterV1

		expected []storage.WebResourceCategoryV1
	}{
		{
			filters: []storage.WebResourceCategoryFilterV1{
				{
					Name:                      "category 4",
					DescriptionSubstringMatch: "cat 4",
				},
				{
					NameSubstringMatch: "3",
				},
				{
					NameSubstringMatch:        "1",
					DescriptionSubstringMatch: "cat 2",
				},
			},
			expected: []storage.WebResourceCategoryV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       4,
						Kind:       storage.WebResourceCategoryKind,
						Name:       "category 4",
					},
					Data: storage.WebResourceCategoryDataV1{
						Description: "cat 4 description",
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       3,
						Kind:       storage.WebResourceCategoryKind,
						Name:       "category 3",
					},
					Data: storage.WebResourceCategoryDataV1{
						Description: "cat 3 description",
					},
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.GetWebResourceCategoryV1sContext(context.TODO(),
			test.filters)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		sort.Slice(test.expected, func(i, j int) bool {
			return test.expected[i].Metadata.Name < test.expected[j].Metadata.Name
		})

		sort.Slice(got, func(i, j int) bool {
			return got[i].Metadata.Name < got[j].Metadata.Name
		})

		ts.Equal(test.expected, got, "Unexpected result value")
	}
}

func (ts *TestWebResourceCategoryV1sSuite) TestCreateSuccess() {
	var tests = []struct {
		getCreated bool

		categories []storage.WebResourceCategoryV1
	}{
		{
			getCreated: true,
			categories: []storage.WebResourceCategoryV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       101,
						Kind:       storage.WebResourceCategoryKind,
						Name:       "category 101",
					},
					Data: storage.WebResourceCategoryDataV1{
						Description: "category 101 description",
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       1101,
						Kind:       storage.WebResourceCategoryKind,
						Name:       "category 1101",
					},
					Data: storage.WebResourceCategoryDataV1{
						Description: "category 1101 description",
					},
				},
			},
		},
		{
			categories: []storage.WebResourceCategoryV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       102,
						Kind:       storage.WebResourceCategoryKind,
						Name:       "category 102",
					},
					Data: storage.WebResourceCategoryDataV1{
						Description: "category 102 description",
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       1102,
						Kind:       storage.WebResourceCategoryKind,
						Name:       "category 1102",
					},
					Data: storage.WebResourceCategoryDataV1{
						Description: "category 1102 description",
					},
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.CreateWebResourceCategoryV1sContext(context.TODO(),
			test.categories, test.getCreated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Check that the Category is created.
		for cID := range test.categories {
			expCat := &test.categories[cID]

			var dbCat WebResourceCategory

			if err := ts.db.Model(&dbCat).Where(
				"web_resource_category.name = ?", expCat.Metadata.Name).
				Select(); err != nil {
				ts.FailNow("Can not get a created Category from DB", err)
			}

			ts.Equal(expCat.Metadata.Name, dbCat.Name)
			ts.Equal(expCat.Metadata.ETag, dbCat.ETag)
			ts.Equal(expCat.Data.Description, dbCat.Description)
			ts.WithinDuration(time.Now(), dbCat.CreatedAt, time.Minute)
		}

		if !test.getCreated {
			continue
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		sort.Slice(test.categories, func(i, j int) bool {
			return test.categories[i].Metadata.Name < test.categories[j].Metadata.Name
		})

		sort.Slice(got, func(i, j int) bool {
			return got[i].Metadata.Name < got[j].Metadata.Name
		})

		ts.Equal(test.categories, got, "Unexpected result value")
	}
}

func (ts *TestWebResourceCategoryV1sSuite) TestCreateConflict() {
	var tests = []struct {
		categories []storage.WebResourceCategoryV1

		expectedError error
	}{
		{
			categories: []storage.WebResourceCategoryV1{
				{
					Metadata: storage.MetadataV1{
						Name:       "category 1",
						APIVersion: storage.APIVersionV1Value,
						ETag:       112,
						Kind:       storage.WebResourceCategoryKind,
					},
				},
				{
					Metadata: storage.MetadataV1{
						Name:       "category 100",
						APIVersion: storage.APIVersionV1Value,
						ETag:       112,
						Kind:       storage.WebResourceCategoryKind,
					},
					Data: storage.WebResourceCategoryDataV1{
						Description: "description cat 100",
					},
				},
			},
			expectedError: storage.NewErrResourceAlreadyExists([]interface{}{
				storage.MetadataV1{
					Name:       "category 1",
					APIVersion: storage.APIVersionV1Value,
					ETag:       112,
					Kind:       storage.WebResourceCategoryKind,
				},
			}),
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.sqlStorage.CreateWebResourceCategoryV1sContext(context.TODO(),
			test.categories, true)

		ts.Nil(got, "Must return nil result")

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func TestWebResourceCategoryV1s(t *testing.T) {
	suite.Run(t, &TestWebResourceCategoryV1sSuite{})
}

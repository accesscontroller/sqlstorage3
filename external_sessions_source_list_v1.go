package sqlstorage3

import (
	"context"

	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

// GetExternalSessionsSourceV1sContext loads and returns filtered
// ExternalSessionsSourceV1s.
func (s *SQLStorage) GetExternalSessionsSourceV1sContext(ctx context.Context,
	filters []storage.ExternalSessionsSourceFilterV1) ([]storage.ExternalSessionsSourceV1, error) {
	var models ExternalSessionsSourceSlice

	// Query.
	var q = s.db.ModelContext(ctx, &models).Relation("ExternalUsersGroupsSource")

	// Add filters.
	for i := range filters {
		p := &filters[i]

		q = q.WhereOrGroup(func(qp *orm.Query) (*orm.Query, error) {
			if !isZero(p.Name) {
				qp = qp.Where("external_sessions_source.name = ?", p.Name)
			}

			if !isZero(p.Type) {
				qp = qp.Where("external_sessions_source.type = ?", p.Type)
			}

			if !isZero(p.GetMode) {
				qp = qp.Where("external_sessions_source.get_mode = ?", p.GetMode)
			}

			return qp, nil
		})
	}

	if err := q.Select(); err != nil {
		return nil, err
	}

	return models.toStorageExternalSessionsSources()
}

// CreateExternalSessionsSourceV1sContext creates all given sources in DB.
func (s *SQLStorage) CreateExternalSessionsSourceV1sContext(ctx context.Context,
	sources []storage.ExternalSessionsSourceV1, getCreated bool) ([]storage.ExternalSessionsSourceV1, error) {
	var models ExternalSessionsSourceSlice

	if err := s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		if getCreated {
			models = make(ExternalSessionsSourceSlice, 0, len(sources))
		}

		// Create.
		for i := range sources {
			md, err := createExternalSessionsSourceV1Tx(tx, &sources[i])
			if err != nil {
				return err
			}

			if getCreated {
				models = append(models, md)
			}
		}

		return nil
	}); err != nil {
		return nil, err
	}

	if getCreated {
		return models.toStorageExternalSessionsSources()
	}

	return nil, nil
}

package sqlstorage3

import (
	"context"
	"errors"
	"time"

	"github.com/go-pg/pg/v10"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func (s *SQLStorage) GetExternalGroupExternalUserV1sContext(ctx context.Context,
	externalGroupSource, externalGroup storage.ResourceNameT) (*storage.ExternalGroup2ExternalUserListRelationsV1, error) {
	var group ExternalGroup

	if err := s.db.ModelContext(ctx, &group).Relation("ExternalUsers").
		Relation("ExternalUsersGroupsSource").
		Where("external_group.name = ? AND external_users_groups_source.name = ?",
			externalGroup, externalGroupSource).Select(); err != nil {
		if errors.Is(err, pg.ErrNoRows) {
			return nil, storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: externalGroupSource,
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: externalGroup,
				},
			)
		}

		return nil, err
	}

	var result = storage.NewExternalGroup2ExternalUserListRelationsV1(externalGroup)

	result.Data.ExternalGroup = externalGroup
	result.Data.ExternalUsersGroupsSource = externalGroupSource

	result.Data.ExternalUsers = make([]storage.ResourceNameT, len(group.ExternalUsers))

	for i, u := range group.ExternalUsers {
		result.Data.ExternalUsers[i] = u.Name
	}

	return result, nil
}

func loadExternalGroupTx(tx *pg.Tx, source, group storage.ResourceNameT) (*ExternalGroup, error) {
	var model ExternalGroup

	if err := tx.Model(&model).Relation("ExternalUsersGroupsSource").
		Where("external_users_groups_source.name = ? AND external_group.name = ?",
			source, group).Select(); err != nil {
		if errors.Is(err, pg.ErrNoRows) {
			return nil, storage.NewErrResourceNotFound(
				&storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: source,
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: group,
				},
			)
		}

		return nil, err
	}

	return &model, nil
}

func checkNotAllUsersLoadedMapError(required []storage.ResourceNameT, loaded ExternalUserSlice) error {
	var (
		ldM      = make(map[storage.ResourceNameT]bool, len(loaded))
		notFound = make([]storage.ResourceNameT, 0, len(required)-len(loaded))
	)

	// Prepare map.
	for _, u := range loaded {
		ldM[u.Name] = true
	}

	for i := range required {
		if _, ok := ldM[required[i]]; !ok {
			notFound = append(notFound, required[i])
		}
	}

	if len(notFound) > 0 {
		return storage.NewErrRelatedResourcesNotFound(storage.APIVersionV1Value,
			storage.ExternalUserKind, notFound, nil)
	}

	return nil
}

func checkNotAllUsersLoadedSliceError(required []storage.ResourceNameT, loaded ExternalUserSlice) error {
	var (
		notFound = make([]storage.ResourceNameT, 0, len(required)-len(loaded))
	)

	for i := range required {
		var isFound bool

		for _, u := range loaded {
			if required[i] == u.Name {
				isFound = true

				break
			}
		}

		if !isFound {
			notFound = append(notFound, required[i])
		}
	}

	if len(notFound) > 0 {
		return storage.NewErrRelatedResourcesNotFound(storage.APIVersionV1Value,
			storage.ExternalUserKind, notFound, nil)
	}

	return nil
}

func checkNotAllUsersLoadedError(required []storage.ResourceNameT, loaded ExternalUserSlice) error {
	/*
		According to my benchmarks there is a reason to use Maps in case len(required) > 40.

		BenchmarkCheckNotAllUsersLoadedSliceError10-8        	 3726356	       338 ns/op	       0 B/op	       0 allocs/op
		BenchmarkCheckNotAllUsersLoadedSliceError20-8        	  875066	      1383 ns/op	       0 B/op	       0 allocs/op
		BenchmarkCheckNotAllUsersLoadedSliceError30-8        	  408716	      2957 ns/op	       0 B/op	       0 allocs/op
		BenchmarkCheckNotAllUsersLoadedSliceError40-8        	  235681	      5100 ns/op	       0 B/op	       0 allocs/op
		BenchmarkCheckNotAllUsersLoadedSliceError50-8        	  151645	      7791 ns/op	       0 B/op	       0 allocs/op
		BenchmarkCheckNotAllUsersLoadedMapError10-8          	 1473751	       801 ns/op	     323 B/op	       1 allocs/op
		BenchmarkCheckNotAllUsersLoadedMapError20-8          	  637414	      1728 ns/op	     666 B/op	       1 allocs/op
		BenchmarkCheckNotAllUsersLoadedMapError30-8          	  460922	      2776 ns/op	    1291 B/op	       1 allocs/op
		BenchmarkCheckNotAllUsersLoadedMapError40-8          	  338055	      3748 ns/op	    1350 B/op	       1 allocs/op
		BenchmarkCheckNotAllUsersLoadedMapError50-8          	  262340	      4617 ns/op	    1492 B/op	       2 allocs/op
		BenchmarkCheckNotAllUsersLoadedSliceErrorError10-8   	 1000000	      1175 ns/op	     576 B/op	       6 allocs/op
		BenchmarkCheckNotAllUsersLoadedSliceErrorError20-8   	  362012	      3381 ns/op	    1088 B/op	       7 allocs/op
		BenchmarkCheckNotAllUsersLoadedSliceErrorError30-8   	  193772	      6252 ns/op	    1088 B/op	       7 allocs/op
		BenchmarkCheckNotAllUsersLoadedSliceErrorError40-8   	  112755	     10653 ns/op	    2112 B/op	       8 allocs/op
		BenchmarkCheckNotAllUsersLoadedSliceErrorError50-8   	   75187	     15889 ns/op	    2112 B/op	       8 allocs/op
		BenchmarkCheckNotAllUsersLoadedMapErrorError10-8     	  877467	      1392 ns/op	     899 B/op	       7 allocs/op
		BenchmarkCheckNotAllUsersLoadedMapErrorError20-8     	  454592	      2659 ns/op	    1754 B/op	       8 allocs/op
		BenchmarkCheckNotAllUsersLoadedMapErrorError30-8     	  325190	      3692 ns/op	    2379 B/op	       8 allocs/op
		BenchmarkCheckNotAllUsersLoadedMapErrorError40-8     	  243118	      4972 ns/op	    3462 B/op	       9 allocs/op
		BenchmarkCheckNotAllUsersLoadedMapErrorError50-8     	  199356	      6016 ns/op	    3604 B/op	      10 allocs/op
	*/
	if len(required) > 40 { // nolint:gomnd
		return checkNotAllUsersLoadedMapError(required, loaded)
	}

	return checkNotAllUsersLoadedSliceError(required, loaded)
}

// loadExternalUsersTx tries to load all requested "users" from DB
// and returns error if at least one of the users is not found.
// on empty "users" return empty slice and no error.
func loadExternalUsersTx(tx *pg.Tx,
	source storage.ResourceNameT, users []storage.ResourceNameT) (ExternalUserSlice, error) {
	// Check empty request.
	if len(users) == 0 {
		return ExternalUserSlice{}, nil
	}

	var userModels ExternalUserSlice

	if err := tx.Model(&userModels).
		Relation("ExternalUsersGroupsSource").
		Where("external_users_groups_source.name = ?", source).
		WhereIn("external_user.name IN (?)", users).Select(); err != nil {
		return nil, err
	}

	// Check that all the required users are loaded.
	if err := checkNotAllUsersLoadedError(users, userModels); err != nil {
		return nil, err
	}

	return userModels, nil
}

func (s *SQLStorage) BindExternalGroup2ExternalUserV1sContext(ctx context.Context,
	relations *storage.ExternalGroup2ExternalUserListRelationsV1) error {
	return s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		// Load Group.
		group, err := loadExternalGroupTx(tx,
			relations.Data.ExternalUsersGroupsSource, relations.Data.ExternalGroup)
		if err != nil {
			return err
		}

		// Load Users.
		users, err := loadExternalUsersTx(tx,
			group.ExternalUsersGroupsSource.Name, relations.Data.ExternalUsers)
		if err != nil {
			return err
		}

		// Update Group ETag and UpdatedAt to show that the update performed.
		var currentTS = time.Now()

		group.ETag++
		group.UpdatedAt = currentTS

		if _, err := tx.Model(group).WherePK().Column("e_tag", "updated_at").Update(); err != nil {
			return err
		}

		// Check that if the Bind Users slice is empty than we should not even try to bind 0 Users.
		if len(relations.Data.ExternalUsers) == 0 {
			return nil
		}

		// Bind.
		var bindings = make([]*ExternalUserToExternalGroup, len(users))

		for i, u := range users {
			bindings[i] = &ExternalUserToExternalGroup{
				ExternalGroupID: group.ID,
				CreatedAt:       currentTS,
				ExternalUserID:  u.ID,
			}

			// Update ETag and UpdatedAt.
			u.UpdatedAt = currentTS
			u.ETag++
		}

		if _, err := tx.Model(&bindings).
			OnConflict("( external_user_id, external_group_id ) DO NOTHING").Insert(); err != nil {
			return err
		}

		// Increment ETags and UpdatedAt for Users.
		if _, err := tx.Model(&users).Column("e_tag", "updated_at").Update(); err != nil {
			return err
		}

		return nil
	})
}

func (s *SQLStorage) UnBindExternalGroup2ExternalUserV1sContext(ctx context.Context,
	relations *storage.ExternalGroup2ExternalUserListRelationsV1) error {
	return s.db.RunInTransaction(ctx, func(tx *pg.Tx) error {
		// Load Group.
		group, err := loadExternalGroupTx(tx,
			relations.Data.ExternalUsersGroupsSource, relations.Data.ExternalGroup)
		if err != nil {
			return err
		}

		// Load Users.
		users, err := loadExternalUsersTx(tx,
			group.ExternalUsersGroupsSource.Name, relations.Data.ExternalUsers)
		if err != nil {
			return err
		}

		// Update Group ETag and UpdatedAt to show that the update performed.
		var currentTS = time.Now()

		group.ETag++
		group.UpdatedAt = currentTS

		if _, err := tx.Model(group).WherePK().Column("e_tag", "updated_at").Update(); err != nil {
			return err
		}

		// Check that if the Bind Users slice is empty than we should not even try to bind 0 Users
		// but we updated the Group ETag above to show that the update action was performed.
		if len(relations.Data.ExternalUsers) == 0 {
			return nil
		}

		// Make UserIDs to unbind.
		var userIDs = make([]int64, len(users))

		for i, u := range users {
			userIDs[i] = u.ID

			// Update ETag.
			u.ETag++
			u.UpdatedAt = currentTS
		}

		if _, err := tx.Model((*ExternalUserToExternalGroup)(nil)).
			Where("external_group_id = ?", group.ID).
			WhereIn("external_user_id IN (?)", userIDs).Delete(); err != nil {
			return err
		}

		// Update Users' ETags.
		if _, err := tx.Model(&users).Column("e_tag", "updated_at").Update(); err != nil {
			return err
		}

		return nil
	})
}

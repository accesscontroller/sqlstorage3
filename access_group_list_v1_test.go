package sqlstorage3

import (
	"context"
	"testing"

	"github.com/go-pg/pg/v10"
	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

type TestAccessGroupV1sSuite struct {
	db         *pg.DB
	sqlStorage *SQLStorage

	suite.Suite
}

func (ts *TestAccessGroupV1sSuite) SetupTest() {
	db, err := connectDB()
	if err != nil {
		ts.FailNow("Can not connect to test DB", err)
	}

	ts.db = db
	ts.sqlStorage = &SQLStorage{db: db}

	// Create DB Tables.
	if err := recreateTables(ts.db); err != nil {
		ts.FailNow("Can not initialize DB", err)
	}

	// Create some init data.
	var grps = []AccessGroup{
		{
			Name:          "access group 1",
			DefaultPolicy: storage.DefaultAccessPolicyAllow,
			WebResourceCategorys: []*WebResourceCategory{
				{
					Name: "web resource category 1",
				},
			},
			ETag: 1,
			ExternalGroups: []*ExternalGroup{
				{
					Name: "external group 1 at source 1",
					ExternalUsersGroupsSource: ExternalUsersGroupsSource{
						Name: "source 1",
					},
				},
			},
			OrderInAccessRulesList:  10,
			TotalGroupSpeedLimitBps: -1,
			UserGroupSpeedLimitBps:  -1,
		},
		{
			Name:          "access group 2",
			DefaultPolicy: storage.DefaultAccessPolicyAllow,
			WebResourceCategorys: []*WebResourceCategory{
				{
					Name: "web resource category 2",
				},
				{
					Name: "web resource category 1",
				},
			},
			ETag: 1,
			ExternalGroups: []*ExternalGroup{
				{
					Name: "external group 1",
					ExternalUsersGroupsSource: ExternalUsersGroupsSource{
						Name: "source 1",
					},
				},
				{
					Name: "external group 1",
					ExternalUsersGroupsSource: ExternalUsersGroupsSource{
						Name: "source 2",
					},
				},
			},
			OrderInAccessRulesList:  11,
			TotalGroupSpeedLimitBps: 1_000_000_000,
			UserGroupSpeedLimitBps:  1_000_000,
		},
	}

	for i := range grps {
		gr := &grps[i]

		if _, err := db.Model(gr).Insert(); err != nil {
			ts.FailNow("Can not create init data", err.Error())
		}
	}
}

func (ts *TestAccessGroupV1sSuite) TearDownTest() {
	if ts.db != nil {
		if err := ts.db.Close(); err != nil {
			ts.FailNow("Can not Close DB", err)
		}
	}
}

func (ts *TestAccessGroupV1sSuite) TestGet() {
	var filters = []struct {
		filter []storage.AccessGroupFilterV1

		expectedResult []storage.AccessGroupV1
	}{
		{
			filter: []storage.AccessGroupFilterV1{
				{
					Name: "access group 1",
					OrderInAccessRulesList: func(v int64) *int64 {
						return &v
					}(10),
				},
				{
					DefaultPolicy: storage.DefaultAccessPolicyDeny,
				},
			},
			expectedResult: []storage.AccessGroupV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       1,
						Kind:       storage.AccessGroupKind,
						Name:       "access group 1",
					},
					Data: storage.AccessGroupDataV1{
						DefaultPolicy:           storage.DefaultAccessPolicyAllow,
						ExtraAccessDenyMessage:  "",
						OrderInAccessRulesList:  10,
						TotalGroupSpeedLimitBps: -1,
						UserGroupSpeedLimitBps:  -1,
					},
				},
			},
		},
		{
			filter: []storage.AccessGroupFilterV1{
				{
					Name: "access group 2",
					OrderInAccessRulesList: func(v int64) *int64 {
						return &v
					}(10),
				},
			},
			expectedResult: []storage.AccessGroupV1{},
		},
		{
			filter: []storage.AccessGroupFilterV1{
				{
					Name: "access group 2",
				},
			},
			expectedResult: []storage.AccessGroupV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       1,
						Kind:       storage.AccessGroupKind,
						Name:       "access group 2",
					},
					Data: storage.AccessGroupDataV1{
						DefaultPolicy:           storage.DefaultAccessPolicyAllow,
						ExtraAccessDenyMessage:  "",
						OrderInAccessRulesList:  11,
						TotalGroupSpeedLimitBps: 1_000_000_000,
						UserGroupSpeedLimitBps:  1_000_000,
					},
				},
			},
		},
	}

	for _, flt := range filters {
		got, gotErr := ts.sqlStorage.GetAccessGroupV1sContext(context.TODO(), flt.filter)

		ts.Assert().NoError(gotErr, "Unexpected error")

		ts.Assert().NotNil(got, "Unexpected nil result")
		ts.Assert().Equal(flt.expectedResult, got, "Unexpected got")
	}
}

func (ts *TestAccessGroupV1sSuite) TestCreate() {
	var tests = []struct {
		srcGroups []storage.AccessGroupV1

		getCreated bool
	}{
		{
			srcGroups: []storage.AccessGroupV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       1,
						Name:       "access group 10",
						Kind:       storage.AccessGroupKind,
					},
					Data: storage.AccessGroupDataV1{
						DefaultPolicy:           storage.DefaultAccessPolicyAllow,
						ExtraAccessDenyMessage:  "deny msg",
						OrderInAccessRulesList:  100,
						TotalGroupSpeedLimitBps: 10000,
						UserGroupSpeedLimitBps:  100,
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       1,
						Name:       "access group 11",
						Kind:       storage.AccessGroupKind,
					},
					Data: storage.AccessGroupDataV1{
						DefaultPolicy:           storage.DefaultAccessPolicyAllow,
						ExtraAccessDenyMessage:  "deny msg",
						OrderInAccessRulesList:  101,
						TotalGroupSpeedLimitBps: 10000,
						UserGroupSpeedLimitBps:  100,
					},
				},
			},
			getCreated: true,
		},
		{
			srcGroups: []storage.AccessGroupV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       1,
						Name:       "access group 12",
						Kind:       storage.AccessGroupKind,
					},
					Data: storage.AccessGroupDataV1{
						DefaultPolicy:           storage.DefaultAccessPolicyAllow,
						ExtraAccessDenyMessage:  "deny msg",
						OrderInAccessRulesList:  102,
						TotalGroupSpeedLimitBps: 10000,
						UserGroupSpeedLimitBps:  100,
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       1,
						Name:       "access group 13",
						Kind:       storage.AccessGroupKind,
					},
					Data: storage.AccessGroupDataV1{
						DefaultPolicy:           storage.DefaultAccessPolicyAllow,
						ExtraAccessDenyMessage:  "deny msg",
						OrderInAccessRulesList:  103,
						TotalGroupSpeedLimitBps: 10000,
						UserGroupSpeedLimitBps:  100,
					},
				},
			},
			getCreated: false,
		},
	}

	for _, test := range tests {
		got, gotErr := ts.sqlStorage.CreateAccessGroupV1sContext(context.TODO(), test.srcGroups, test.getCreated)

		ts.Assert().NoError(gotErr, "Unexpected error")

		// Check existence.
		for _, g := range test.srcGroups {
			var ag AccessGroup
			exists, err := ts.db.Model(&ag).Where("name = ?", g.Metadata.Name).Exists()

			ts.Assert().NoError(err, "Unexpected DB error")
			ts.Assert().Truef(exists, "AccessGroup %q does not exist in DB", g.Metadata.Name)
		}

		if test.getCreated {
			ts.Assert().NotNil(got, "nil result")
			ts.Assert().Equal(test.srcGroups, got, "Unexpected result")
		} else {
			ts.Assert().Nil(got, "not nil result")
		}
	}
}

func (ts *TestAccessGroupV1sSuite) TestCreateError() {
	var tests = []struct {
		srcGroups []storage.AccessGroupV1

		expectedError error
	}{
		{ // Conflict - Order conflict.
			srcGroups: []storage.AccessGroupV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       10,
						Name:       "access group 12",
						Kind:       storage.AccessGroupKind,
					},
					Data: storage.AccessGroupDataV1{
						DefaultPolicy:           storage.DefaultAccessPolicyAllow,
						ExtraAccessDenyMessage:  "deny msg",
						OrderInAccessRulesList:  10, // Order conflict
						TotalGroupSpeedLimitBps: 10000,
						UserGroupSpeedLimitBps:  100,
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       30,
						Name:       "access group 13",
						Kind:       storage.AccessGroupKind,
					},
					Data: storage.AccessGroupDataV1{
						DefaultPolicy:           storage.DefaultAccessPolicyAllow,
						ExtraAccessDenyMessage:  "deny msg",
						OrderInAccessRulesList:  103,
						TotalGroupSpeedLimitBps: 10000,
						UserGroupSpeedLimitBps:  100,
					},
				},
			},
			expectedError: nil,
		},
		{ // Conflict - name is already exists.
			srcGroups: []storage.AccessGroupV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       10,
						Name:       "access group 1",
						Kind:       storage.AccessGroupKind,
					},
					Data: storage.AccessGroupDataV1{
						DefaultPolicy:           storage.DefaultAccessPolicyAllow,
						ExtraAccessDenyMessage:  "deny msg",
						OrderInAccessRulesList:  102,
						TotalGroupSpeedLimitBps: 10000,
						UserGroupSpeedLimitBps:  100,
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       20,
						Name:       "access group 13",
						Kind:       storage.AccessGroupKind,
					},
					Data: storage.AccessGroupDataV1{
						DefaultPolicy:           storage.DefaultAccessPolicyAllow,
						ExtraAccessDenyMessage:  "deny msg",
						OrderInAccessRulesList:  103,
						TotalGroupSpeedLimitBps: 10000,
						UserGroupSpeedLimitBps:  100,
					},
				},
			},
			expectedError: nil,
		},
	}

	for _, test := range tests {
		got, gotErr := ts.sqlStorage.CreateAccessGroupV1sContext(context.TODO(), test.srcGroups, false)

		ts.Assert().Error(gotErr, "must be error")
		ts.Assert().Nil(got, "not nil result")

		ts.Assert().IsType(&storage.ErrResourceAlreadyExists{}, gotErr, "Unexpected error type")
	}
}

func TestAccessGroupV1s(t *testing.T) {
	suite.Run(t, &TestAccessGroupV1sSuite{})
}
